classdef bearing_obZ
    
    properties
        img_time
        centroidx
        labels
        bearings
        sensortype
        associations
        innovations
    end
    
    
    methods
        
        function obj = bearing_obZ(cameraParams,img_time,frame,centroid,labels)
            
            obj.img_time=img_time;
            obj.centroidx=centroid;
            obj.labels=labels;

            PP=cameraParams.PrincipalPoint;
            centerX=PP(1);
            
            %             centroids(:,1)=(dimensions(:,1)+dimensions(:,3))/2;
            %             centroids(:,2)=(dimensions(:,2)+dimensions(:,4))/2;
            %             rawbearings=(centroids(:,1)-centerX)*rad_per_pix;
            
            %Dimension is centroid x here             
            
            % REALSENSES
            
            if (frame=='LC')
                
                h_fov=deg2rad(69.4);
                h_width=640;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX)*rad_per_pix;
                
                %bear=0.7394-rawbearings;%ICRA VALUES
                bear=0.8244-rawbearings; %IROS VALUES 
                gradang=wrapTo2Pi(bear);
                obj.sensortype='LC';
                
            elseif (frame=='RC')
                
                h_fov=deg2rad(69.4);
                h_width=640;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX)*rad_per_pix;
                
                %bear=0.838+rawbearings; %ICRA VALUES
                bear=-0.8117+rawbearings; %IROS VALUES
                gradang=wrapTo2Pi(-bear);
                obj.sensortype='RC';
                            
            %RICOH FORWARD
            elseif (frame=='LF')
                
                h_fov=deg2rad(60);
                h_width=480;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX)*rad_per_pix;
                
                bear=1.0472-rawbearings; %60 deg
                gradang=wrapTo2Pi(bear);
                obj.sensortype='LF';
                
             elseif (frame=='RF')
                
                h_fov=deg2rad(60); 
                h_width=480;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX)*rad_per_pix;
                
                bear=1.0472+rawbearings;   %60 deg
                gradang=wrapTo2Pi(-bear);
                obj.sensortype='RF';
                
            
            %RICOH BACK
            elseif (frame=='LB')
                
                h_fov=deg2rad(60);
                h_width=480;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX)*rad_per_pix;
                
                bear=2.0944-rawbearings;  %120 DEG
                gradang=wrapTo2Pi(bear);
                obj.sensortype='LB';
                
                
             elseif (frame=='RB')
                
                h_fov=deg2rad(60);
                h_width=480;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX)*rad_per_pix;
                
                bear=2.0944+rawbearings; %120 DEG
                gradang=wrapTo2Pi(-bear);
                obj.sensortype='RB';

                
            end
            
            obj.bearings=gradang;
        end
        
    end
    
    
end
