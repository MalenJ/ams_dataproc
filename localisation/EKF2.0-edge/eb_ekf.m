close all
clear all

%Load stuff

fileroot='2019-09-01-Wentworth';
filename='2019-09-01-Wentworth';
PATH='/home/maleen/cloud_academic/ams_data/datasets/';
load(strcat(PATH,fileroot,'/localready/',filename,'-localready5-fixed.mat'));

start_time=50;
end_time=175;

global xMing
global yMing
global resg
global sizeYg
global sizeXg
global lasermap
global featuremap
global realtimeplotter
global robothan
global laserhan
global featurehan
global drawInit
global sigma_v;
global sigma_w;
global sigma_motionPd;
global sigma_motionPtheta;
global sigma_bearing;
global IG_bearing;
global sigma_edge;
global sigma_edgeP;
global sigma_bearingP;

%Noise vals for tuning

sigma_v=0.05;
sigma_w=0.1;
sigma_motionPd=0.01;
sigma_motionPtheta=0.009;%;

sigma_bearing=17;
IG_bearing=0.48;
sigma_bearingP=0.001;

sigma_edge=0.15;
sigma_edgeP=1;

% sigma_v=0.01;
% sigma_w=0.1;
% sigma_motionPd=0.01;
% sigma_motionPtheta=0.009;%;

% sigma_bearing=17;
% IG_bearing=2;
% sigma_bearingP=0;
% 
% sigma_edge=1;
% sigma_edgeP=0.5;

realtimeplotter=1;
startiter=find(data_time<start_time, 1, 'last' );
enditer=find(data_time<end_time, 1, 'last' );
iter=0;
sz=25;

%Realtime plot of edgemap
% xMin=10;
% yMin=40;
% sizeY=2000;
% sizeX=2375;
% res=0.04;

xMing=xMin;
yMing=yMin;
resg=res;
sizeYg=sizeY;
sizeXg=sizeX;

% %LMmap(1,4)=1000;
% %LMmap(14,4)=1000;
% % LMmap(35,4)=1000;
% % LMmap(6,4)=1000;
% % LMmap(7,4)=1000;
% 
% LMmap(35,:)=[35,-8.5,-1/5,1];
% LMmap(2,:)=[2,26.436187704533065,31.782558796202157,4];
% LMmap(3,:)=[3,10.267645759581542,31.782558796202157,4];
% LMmap(5,:)=[5,-0.812312159505042,29.908434776850644,1];
% LMmap(8,:)=[8,-0.995803601793157,26,1];
% LMmap(9,:)=[9,-0.815649108714856,9.5,1];
% 
% LMmap(14,:)=[14,36.612278554503840,31.782558796202157,1];
% 
% LMmap(11,4)=1000;
% % LMmap(13,4)=1000;
% % LMmap(38,4)=1000;
% 
% 
% LMmap(51,:)=[51,-9,0,1];
% LMmap(52,:)=[52,-8.5,-21,1];
% 
% LMmap(53,:)=[53,4,43-3,1];
% LMmap(1,:)=[1,26.8307016465488,43.0827778179414-3,4];
% LMmap(13,:)=[13,34.732968977667845,43.133683745806540-3,1];
% 
% 
% % LMmap(54,:)=[54,36.9334,16.3449,1];
% % LMmap(55,:)=[55,37.0591,25.6042,1];
% % LMmap(56,:)=[56,40.5994,24.6435,1];


if realtimeplotter==1
    drawInit = 0;
    featuremap=LMmap;
    figure(1)
    lasermap = subplot(1,1,1);
    hold on
    imshow(imcomplement(binary_map))
    for i=1:size(LMmap,1)
        scatter(lasermap,(LMmap(i,2)+xMin)./res,sizeY-(LMmap(i,3)+yMin)./res,sz,'x','k')
        text(lasermap,(LMmap(i,2)+xMin)./res,sizeY-(LMmap(i,3)+yMin)./res, num2str(i));
    end
    
    
    figure(2)
    errorplotX= subplot(3,1,1);
    hold on
    eplotxhandle.error=plot(errorplotX,0,0);
    eplotxhandle.covup=plot(errorplotX,0,0);
    eplotxhandle.covdown=plot(errorplotX,0,0);
    xlabel(errorplotX,'time (s)')
    ylabel(errorplotX,'X error (m)')
    
    
    errorplotY= subplot(3,1,2);
    hold on
    eplotyhandle.error=plot(errorplotY,0,0);
    eplotyhandle.covup=plot(errorplotY,0,0);
    eplotyhandle.covdown=plot(errorplotY,0,0);
    xlabel(errorplotY,'time (s)')
    ylabel(errorplotY,'Y error (m)')
    
    errorplotTheta= subplot(3,1,3);
    hold on
    eplotthetahandle.error=plot(errorplotTheta,0,0);
    eplotthetahandle.covup=plot(errorplotTheta,0,0);
    eplotthetahandle.covdown=plot(errorplotTheta,0,0);
    xlabel(errorplotTheta,'time (s)')
    ylabel(errorplotTheta,'Theta error (m)')
end

error_data_cnt=0;

for i=startiter:enditer
    
    iter=iter+1;
    
    previous_time=data_time(i-1);
    current_time=data_time(i);
    current_data=data_type{i};
    dt=current_time-previous_time;
    
    U_k(1,1)=(ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib;
    U_k(2,1)=ppval(imu_interp,previous_time);
    
    odomobj=odom_obsZ(U_k);
    
    if (i-1<startiter)
        
        trans=[ppval(rtkinterp_x,previous_time),ppval(rtkinterp_y,previous_time),ppval(rtkinterp_z,previous_time)];
        rotz=ppval(rtkinterp_theta,previous_time);
        T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
        T_world_robot=T_world_RTK*inv(T_robot_RTK);
        
        X_k=[T_world_robot(1,4);T_world_robot(2,4);rotz];
        %X_k=[0;0;0];
        P_k=zeros(3);
        %P_k=[(0.05)^2 0 0;0 (0.05)^2 0;0 0 (deg2rad(0.1))^2];
        
    else
        iterval=1+(iter-2)*3;
        X_k=X_k1k1(:,iter-1);
        P_k=P_k1k1(1:3,iterval:iterval+2);
        
    end

    [X_k1k,P_k1k] = prediction(X_k,P_k,U_k,dt);
    X_k1k(3,1)=wrapTo2Pi(X_k1k(3,1));
    
    if (current_data=='O')
        
        %current_data
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        
        drawEKFresults(odomobj, X_k1k1(:,iter));
        
    elseif (current_data=='LC')
        
        %current_data
        rows=yolo_observations.lamp_times==current_time;
        
        centroids=yolo_observations(rows,{'centroid_x'});
        semantics=yolo_observations(rows,{'yolo_labels'});
        
        bear_obs=bearing_obZ(LC_params,current_time,current_data,centroids{:,1},semantics{:,1});
        
        iterval=1+(iter-1)*3;
        [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),bear_obs] = observation_bearing(X_k1k,P_k1k,bear_obs,LMmap);
        drawEKFresults(bear_obs, X_k1k1(:,iter));
        
%         %Isolate
%                 iterval=1+(iter-1)*3;
%                 X_k1k1(:,iter)=X_k1k;
%                 P_k1k1(1:3,iterval:iterval+2)=P_k1k;
%                 drawEKFresults(odomobj, X_k1k1(:,iter));
        
        
    elseif(current_data=='RC')
        
        %current_data
        rows=yolo_observations.lamp_times==current_time;
        
        centroids=yolo_observations(rows,{'centroid_x'});
        semantics=yolo_observations(rows,{'yolo_labels'});
        
        bear_obs=bearing_obZ(RC_params,current_time,current_data,centroids{:,1},semantics{:,1});
        
        iterval=1+(iter-1)*3;
        [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),bear_obs] = observation_bearing(X_k1k,P_k1k,bear_obs,LMmap);
        drawEKFresults(bear_obs, X_k1k1(:,iter));
        
%         %Isolate
%                 iterval=1+(iter-1)*3;
%                 X_k1k1(:,iter)=X_k1k;
%                 P_k1k1(1:3,iterval:iterval+2)=P_k1k;
%                 drawEKFresults(odomobj, X_k1k1(:,iter));
        
    elseif(current_data=='BC')
        
        current_data;
        
        iterval=1+(iter-1)*3;
        img_seq=find(depthedge_time==current_time);
        
        if ~isnan(img_seq)
            depthedge_img=depthedge{img_seq};
            
            img_seq;
            edge_obs=edge_obsZ(BC_params,current_time,depthedge_img,T_robot_backcam);
            
            if edge_obs.flagger>0 % && 2*edge_obs.flagger <2500
                [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2)] = observation_edge(X_k1k, P_k1k, edge_obs.ranges,edge_obs.bearings, dfobj);
                %[X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2), K] = update(X_k1k, P_k1k, V, jHx, S);
                drawEKFresults(edge_obs, X_k1k1(:,iter));
            else
                X_k1k1(:,iter)=X_k1k;
                P_k1k1(1:3,iterval:iterval+2)=P_k1k;
                drawEKFresults(odomobj, X_k1k1(:,iter));
            end
        else
            X_k1k1(:,iter)=X_k1k;
            P_k1k1(1:3,iterval:iterval+2)=P_k1k;
            drawEKFresults(odomobj, X_k1k1(:,iter));
        end
        
        %%Isolate
%                                 iterval=1+(iter-1)*3;
%                                 X_k1k1(:,iter)=X_k1k;
%                                 P_k1k1(1:3,iterval:iterval+2)=P_k1k;
%                                 drawEKFresults(odomobj, X_k1k1(:,iter));
    end
    
    iterval=1+(iter-1)*3;
    %     scatter(X_k1k1(1,iter),X_k1k1(2,iter),sz,'b');
    
    if (~isnan(P_k1k1(1,iterval:iterval+2)))
        
        
        if current_time<147.5 || current_time>150
            
            error_data_cnt=error_data_cnt+1;
            
            PK=P_k1k1(1:3,iterval:iterval+2);
            pose=X_k1k1(:,iter);
            
            datt(error_data_cnt,:)=current_time;
            
            p=0.95;
            s = -2 * log(1 - p);
            
            [V, D] = eig(PK(1:2,1:2) * s);
            t = linspace(0, 2 * pi);
            
            a = (V * sqrt(D)) * [cos(t(:))'; sin(t(:))'];
            
            trans=[ppval(rtkinterp_x,current_time),ppval(rtkinterp_y,current_time),ppval(rtkinterp_z,current_time)];
            rotz=ppval(rtkinterp_theta,current_time);
            T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
            T_world_robot=T_world_RTK*inv(T_robot_RTK);
            
            GTx(error_data_cnt)=T_world_robot(1,4);
            GTy(error_data_cnt)=T_world_robot(2,4);
            GTtheta(error_data_cnt)=rotz;
            
            %RTK_calib=[cos(pose(3,1)) -sin(pose(3,1)) -0.55; sin(pose(3,1)) cos(pose(3,1)) 0;0 0 1]*[GTx; GTy; 1];
            
            error(:,error_data_cnt)=[GTx(error_data_cnt);GTy(error_data_cnt); GTtheta(error_data_cnt)]-pose(:,1);
            sigmabounds(:,error_data_cnt)=2*sqrt(diag(PK));
            
            if realtimeplotter==1
                set(eplotxhandle.covup,'XData',datt,'YData',sigmabounds(1,:),'color', [0,0,1]);
                set(eplotxhandle.covdown,'XData',datt,'YData', -sigmabounds(1,:),'color', [0,0,1]);
                set(eplotxhandle.error,'XData',datt,'YData',error(1,:),'color', [1,0,0]);
                
                set(eplotyhandle.covup,'XData',datt,'YData', sigmabounds(2,:),'color', [0,0,1]);
                set(eplotyhandle.covdown,'XData',datt,'YData', -sigmabounds(2,:),'color', [0,0,1]);
                set(eplotyhandle.error,'XData',datt,'YData',error(2,:),'color', [1,0,0]);
                
                
                set(eplotthetahandle.covup,'XData',datt,'YData', sigmabounds(3,:),'color', [0,0,1]);
                set(eplotthetahandle.covdown,'XData',datt,'YData', -sigmabounds(3,:),'color', [0,0,1]);
                set(eplotthetahandle.error,'XData',datt,'YData',error(3,:),'color', [1,0,0]);
            end
        end
        
    end
    
end


%% PLOT

%close all

figure
axis equal
hold on
szGT=1.5;
szEKF=2;

for i=1:size(LMmap,1)
    scatter(LMmap(i,2),LMmap(i,3),'x','k')
   text(LMmap(i,2),LMmap(i,3), num2str(LMmap(i,1)));
end

%scatter(rtabx,rtaby,szGT,'r')
scatter(rtk_enux,rtk_enuy,szGT,'y')
scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b');
xlabel('X (m)')
ylabel('Y (m)')

% Error plots
figure
subplot(2,1,1)
set(gca,'XLim',[0 end_time-start_time]);
hold on
plot(datt-start_time,error(1,:),'r');
plot(datt-start_time, sigmabounds(1,:),'b');
plot(datt-start_time, -sigmabounds(1,:),'b');
plot(datt-start_time,error(1,:),'r');
xlabel('time (s)')
ylabel('X error (m)')
hold off
subplot(2,1,2)
set(gca,'XLim',[0 end_time-start_time]);
hold on
plot(datt-start_time,error(2,:),'r');
plot(datt-start_time, sigmabounds(2,:),'b');
plot(datt-start_time, -sigmabounds(2,:),'b');
plot(datt-start_time,error(2,:),'r');
hold off
xlabel('time (s)')
ylabel('Y error (m)')
% 
% subplot(3,1,3)
% hold on
% plot(datt(1:5000), rad2deg(sigmabounds(3,1:5000)),'r');
% plot(datt(1:5000), rad2deg(-sigmabounds(3,1:5000)),'r');
% plot(datt(1:5000),rad2deg(error(3,1:5000)),'b');
% hold off
% xlabel('time (s)')
% ylabel('Theta error (m)')


XMSE=(sum(error(1,:).^2)/numel(error(1,:)));
YMSE=(sum(error(2,:).^2)/numel(error(2,:)));

MAXX=max(abs(error(1,:)));
MAXY=max(abs(error(2,:)));
%MAXTHETA=rad2deg(sum(error(3,:).^2)/numel(error(3,:)))

MSE=[XMSE,YMSE]
MAX=[MAXX,MAXY]


%%



% xekf_interp=interp1(data_time(startiter:enditer),X_k1k1(1,:),'linear','pp');
% yekf_interp=interp1(data_time(startiter:enditer),X_k1k1(2,:),'linear','pp');
%
% startiter=find(data_time<start_time, 1, 'last' );
% enditer=find(data_time<end_time, 1, 'last' );
%
% for i=startiter:enditer
%
%     time=rtk_time(i);
%     x_e=rtk_enux(i)-ppval(xekf_interp,time);
%     y_e=rtk_enuy(i)-ppval(yekf_interp,time);
%
% end


