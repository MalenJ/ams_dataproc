load('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/el_maps/2019-09-04-Village/2019-09-04-Village-ELMAP.mat')
LMmap=final_map;

sz=100;

figure
axis equal
hold on


el_post_i=find(LMmap(:,4) ==1);
el_post=LMmap(el_post_i,:);

el_tree_i=find(LMmap(:,4) ==4);
el_tree=LMmap(el_tree_i,:);

el_pm_i=find(LMmap(:,4) ==2);
el_pm=LMmap(el_pm_i,:);

el_sgn_i=find(LMmap(:,4) ==3);
el_sgn=LMmap(el_sgn_i,:);


scatter(el_post(:,2),el_post(:,3),sz, 'x', 'r')
scatter(el_tree(:,2),el_tree(:,3),sz, 'x', 'g')
scatter(el_pm(:,2),el_pm(:,3),sz, 'x', 'b')
scatter(el_sgn(:,2),el_sgn(:,3),sz, 'x', 'y')




for i=1:length(LMmap)
    
    text(LMmap(i,2),LMmap(i,3)-1, num2str(i));
end