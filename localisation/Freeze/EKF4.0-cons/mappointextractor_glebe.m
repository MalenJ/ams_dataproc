clear all
close all
% 
CONI=imread('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/2019-09-04-Village/village_conmap.png');
% 
xMin=10;
yMin=40;
sizeX=2375;
sizeY=2000;
res=0.04;

lines=[0,-1.06;24.66,-0.79;28.21,-0.75;37.27,-0.65;38.67,1.93;39.05,32.72;40.82,33.47;76.91,34.35;77.59,33.54;66.73,2.17;...
    63.92,-0.38;65.16,-2.43;54.81,-32.11;52.63,-32.89;27.22,-32.32;25.82,-31.29;26.30,-2.73;24.09,-32.23;-0.31,-31.45;-1.07,-30.35;...
    -0.67,-3.01;-2.50,-1.94;-2.38,0.88;-2.38,32.26;-1.59,37.29;36,37.41]

CONBIN=imbinarize(rgb2gray(CONI));

[cols,rows]=find(CONBIN==0);
px=[rows,cols];
conmap_pts= img2cart(px,xMin, yMin, sizeX, sizeY, res);

sz=1
figure
scatter(conmap_pts(:,1),conmap_pts(:,2),sz, 'k')
axis equal
h = images.roi.Polyline(gca,'Position',lines, 'Color', 'green');
h = drawpolyline('Color','green');


