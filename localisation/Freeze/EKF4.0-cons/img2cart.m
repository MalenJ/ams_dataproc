function [cart] = img2cart(px, xMin, yMin, sizeX, sizeY, res)

cart(:,1)=(px(:,1).*res-xMin);

yval= sizeY-px(:,2);
cart(:,2)=(yval.*res-yMin);

end

