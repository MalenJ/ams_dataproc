clear all
close all
 
fileroot='2020-02-28-Wentworth';
filename='2020-02-28-Wentworth-P1';
configname='TF1.mat';

CONFIGPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/config/'); %CSV DATA
CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
IMGPATH=strcat('/media/maleen/malen_ssd/phd/ams_data/',fileroot,'/image_data/',filename,'/'); %IMAGE DATA

MAPPATH_feature='/home/maleen/cloud_academic/ams_data/map_data/'; %MAP DATA
MAPPATH_edge='/home/maleen/cloud_academic/ams_data/map_data/edge_maps/';

%Load config
load(strcat(CONFIGPATH, filename,'-prepconfig-', configname))

con_map='2019-08-22-Wentworth/2019-08-22-Wentworth-conmap.png';
I=imread(strcat(MAPPATH_edge,con_map)); %funk1

binary_map=imbinarize(rgb2gray(I));

[cols,rows]=find(binary_map==0);

px=[rows,cols];
mappoints= img2cart(px,xMin, yMin, sizeX, sizeY, res);



[CL_points,CL_size] = makeclothoidmap_went();

clpoints=CL_points(2,:);

sfpoints=[mappoints, zeros(length(mappoints),1)];

finpoints=gl2sf(clpoints,sfpoints');

   Iupv=find(finpoints(1,:)<0.1 & finpoints(1,:)>-0.1 & finpoints(2,:)>0);
   Ilwv=find(finpoints(1,:)<0.1 & finpoints(1,:)>-0.1 & finpoints(2,:)<0);
   
   upb=min(finpoints(2,Iupv))
   lwb=max(finpoints(2,Ilwv))
   
   
finpoints_gl=sf2gl(clpoints,finpoints);
scale=0.1

figure
hold on
axis equal
scatter(finpoints(1,:),finpoints(2,:),scale)
%scatter(finpoints_gl(1,:),finpoints_gl(2,:),'r')
scatter(0,upb,'k','x')
scatter(0,lwb,'r','x')
% scatter(mappoints(:,1),mappoints(:,2),scale,'r')
% scatter(clpoints(1,1),clpoints(1,2),'k','+')