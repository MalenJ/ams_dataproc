close all
clear all

fileroot='2020-02-19-Village';
filename='2020-02-19-Village-P1';
configname='TF1';

%Paths
CONFIGPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/config/'); %CSV DATA
CSVPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
IMGPATH=strcat('/media/maleen/malen_ssd/phd/ICRA2021_data/',fileroot,'/image_data/',filename,'/'); %IMAGE DATA

MAPPATH_feature='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/'; %MAP DATA
MAPPATH_edge='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/';

%Load config
load(strcat(CONFIGPATH, filename,'-prepconfig-', configname))

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');


roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;

lPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/');
load(strcat(lPATH,fileroot,'/localready/',filename,'-localready-',configname,'.mat'),'LMmap');
%%

figure
axis equal
hold on
szGT=15;
szEKF=4;

for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3),8,'x','k')
    %text(LMmap(i,2),LMmap(i,3), num2str(i));
end

xlabel('X (m)')
ylabel('Y (m)')

%% ODOM
load(strcat(filename,'-',configname,'-odom.mat'))

scatter(gtenux,gtenuy,szGT+5,'r','filled')

scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'m','filled');

RMSE_ODOM=RMSE_FIN

%% ODOM + CON
load(strcat(filename,'-',configname,'-odom-con.mat'))

scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'y','filled');

RMSE_ODOMCON=RMSE_FIN

%% OBS
load(strcat(filename,'-',configname,'-obs.mat'))

scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b','filled');

RMSE_OBS=sqrt(RMSE(1,1)^2+RMSE(1,2)^2)

%% FULL

load(strcat(filename,'-',configname,'-full.mat'))

scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'g','filled');

RMSE_FULL=RMSE_FIN


