clear all
close all

CONI=imread('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/2019-08-22-Wentworth/2019-08-22-Wentworth-conmap.png');

xMin=25;
yMin=25;
sizeX=750;
sizeY=1500;
res=0.04;

lines=[-0.419796178343937,-16.9651719745223;0.755694267515928,28.8789554140127;0.848241207875477,32.0679971818437;-0.858965511935860,29.5580641235780;-14.3457598192499,22.5022374113241;-15.9685467421037,21.5396135749378;-19.0800158083096,16.0762923324735;-18.9806970788337,14.9672331866597;-3.36223769001932,-17.4367111889032;-1.77982842568791,-18.6363709670951]

CONBIN=imbinarize(rgb2gray(CONI));

[cols,rows]=find(CONBIN==0);
px=[rows,cols];
conmap_pts= img2cart(px,xMin, yMin, sizeX, sizeY, res);

sz=1
figure
scatter(conmap_pts(:,1),conmap_pts(:,2),sz, 'k')
axis equal
h = images.roi.Polyline(gca,'Position',lines, 'Color', 'green');
h = drawpolyline('Color','green');


