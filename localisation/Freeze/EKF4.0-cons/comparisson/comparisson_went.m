close all
clear all

fileroot='2020-02-28-Wentworth';
filename='2020-02-28-Wentworth-P1';

CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
MAPPATH_feature='/home/maleen/cloud_academic/ams_data/map_data/'; %MAP DATA

rtab_data=readtable(strcat(CSVPATH, filename, '-rtabdata.txt'));
orb_data=readtable(strcat(CSVPATH, filename, '-orbdata.csv'));
%vins_data=readtable(strcat(CSVPATH, filename, '-vinsdata.csv'));
VINSVIO_data=readtable(strcat(CSVPATH, filename, '-VINS-vio_loop.csv'));

%VINSVIO_data=readtable(strcat('/home/maleen/rosbags/vio_loop.csv'));

rtablocal_data=readtable(strcat(CSVPATH, filename, '-rtablocaldata.csv'));
orblocal_data=readtable(strcat(CSVPATH, filename, '-orblocaldata.csv'));

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/IROScalib/'; %CLIB DATA

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');

% load(strcat(MAPPATH_feature,'/RTK_maps/2019-03-22-Wentworth-map/2019-03-22-Wentworth-satmap.mat'),'map_sat');
% [map_enu(:,1), map_enu(:,2), map_enu(:,3)]=geodetic2enu(map_sat(:,1),map_sat(:,2),map_sat(:,3),base_lat,base_long,base_alti, wgs84Ellipsoid);
% map_enu_ID=transpose(linspace(1,length(map_enu(:,1)),length(map_enu(:,1))));
% map_enu_semantic=[1;4;1;4;4;1;1;1;4;1;1;1;1;1;1;1;4];
% LMmap=[map_enu_ID,map_enu(:,1)+0.35,map_enu(:,2)-1.7,map_enu_semantic];
% 
% 
% %load(strcat(MAPPATH_feature,'/el_maps/2019-09-04-Village/2019-09-04-Village-ELMAP.mat'),'final_map');
% %LMmap=final_map;


roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;

%% Local data

%RTAB
rtablocal_time=rtablocal_data{:,3};
rtablocal_x=rtablocal_data{:,4};
rtablocal_y=rtablocal_data{:,5};
rtablocal_quat=rtablocal_data{:,6:9};
%rtablocal_theta=rtablocal_data{:,7};
rtablocaleul = quat2eul([rtablocal_quat(:,4) rtablocal_quat(:,1:3)]);

%scatter(rtablocal_x,rtablocal_y)

%ORB
orblocal_time=orblocal_data{:,3};
orblocal_x=orblocal_data{:,4};
orblocal_y=orblocal_data{:,5};
orblocal_z=orblocal_data{:,6};
orblocal_quat=orblocal_data{:,7:10};
orblocal_eul = quat2eul([orblocal_quat(:,4) orblocal_quat(:,1:3)]);

for i=1:length(orblocal_time)
    
%     trans=[orblocal_x(i),orblocal_y(i),orblocal_z(i)];
%     roter = eul2tform(orblocal_eul(i,:));
%     T_world_cam=makehgtform('translate',trans)*roter;
%     T_world_robot=T_world_cam*(-T_robot_backcam);
%     orblocal_xt(i,1)=T_world_robot(1,4);
%     orblocal_yt(i,1)=T_world_robot(2,4);

    trans=[orblocal_x(i);orblocal_y(i);orblocal_z(i);1];
    T_world_robot=(T_robot_backcam)*trans;
    orblocal_xt(i,1)=T_world_robot(1,1);
    orblocal_yt(i,1)=T_world_robot(2,1);
    orblocal_zt(i,1)=T_world_robot(3,1);
    
end

% figure
% hold on
% scatter3(orblocal_x,orblocal_y,orblocal_z, 'r')
% scatter3(orblocal_xt,orblocal_yt,orblocal_zt, 'b')



%% RTAB
%
rtab_time=rtab_data{:,1}-1582835388.77570145;
rtab_x=rtab_data{:,2};
rtab_y=rtab_data{:,3};
rtab_theta=rtab_data{:,7};
% rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);

%scatter(rtab_x,rtab_y)
%% ORB DATA

orb_time=orb_data{:,3};
orb_x=orb_data{:,4};
orb_y=orb_data{:,5};
orb_z=orb_data{:,6};
orb_quat=orb_data{:,7:10};
orb_eul = quat2eul([orb_quat(:,4) orb_quat(:,1:3)]);

for i=1:length(orb_time)
    
%     trans=[orb_x(i),orb_y(i),orb_z(i)];
%     roter = eul2tform(orb_eul(i,:));
%     T_world_cam=makehgtform('translate',trans)*roter;
%     T_world_robot=T_world_cam*(-T_robot_backcam);
%     orb_xt(i,1)=T_world_robot(1,4);
%     orb_yt(i,1)=T_world_robot(2,4);
    trans=[orb_x(i);orb_y(i);orb_z(i);1];
    T_world_robot=(T_robot_backcam)*trans;
    orb_xt(i,1)=T_world_robot(1,1);
    orb_yt(i,1)=T_world_robot(2,1);
    orb_zt(i,1)=T_world_robot(3,1);

 end


%scatter(orb_xt,orb_yt)
%% VINS DATA

% vins_time=vins_data{:,3};
% vinsx=vins_data{:,4};
% vinsy=vins_data{:,5};
% vinsz=vins_data{:,6};
% vinsquat=vins_data{:,7:10};
% vinsbeul = quat2eul([vinsquat(:,4) vinsquat(:,1:3)]);

vins_time=VINSVIO_data{:,1}/1000000000-1582835388.77570145;
vins_xt=VINSVIO_data{:,2};
vins_yt=VINSVIO_data{:,3};
vins_z=VINSVIO_data{:,4};
% vinsquat=VINSVIO_data{:,7:10};
% vinsbeul = quat2eul([vinsquat(:,4) vinsquat(:,1:3)]);
vins_eul=VINSVIO_data{:,6:8};

% for i=1:length(vins_time)
%     
%     
% %     roter = eul2tform(vins_eul(i,:));
% %     T_world_cam=makehgtform('translate',trans)*roter;
% %     T_world_robot=T_world_cam*(-T_robot_backcam);
% %     vins_xt(i,1)=T_world_robot(1,4);
% %     vins_yt(i,1)=T_world_robot(2,4);
%     
%     trans=[vins_x(i);vins_y(i);vins_z(i);1];
%    
%     T_world_robot=(T_robot_backcam)*trans;
%     vins_xt(i,1)=T_world_robot(1,1);
%     vins_yt(i,1)=T_world_robot(2,1);
%     vins_zt(i,1)=T_world_robot(3,1);
% 
% end

% 
% figure
% hold on
% scatter3(vins_x,vins_y,vins_z, 'r')
% scatter3(vins_xt,vins_yt,vins_zt, 'b')


%%

rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));
rtk_time=rtk_data{:,2};

load('2020-02-28-Wentworth-P1-VINS1-results.mat')



start_time=27;
% end_time=50+400;
% rtkoffsetiter=find(rtk_time<start_time, 1, 'last' );
% timeoffset=rtk_time(rtkoffsetiter);
rtkinterp_x=spline([0;rtk_time],[0;gtenux']);
rtkinterp_y=spline([0;rtk_time],[0;gtenuy']);
rtabx_interp1=interp1(rtab_time,rtab_x,'linear','pp');
rtaby_interp1=interp1(rtab_time,rtab_y,'linear','pp');
rtabtheta_interp1=interp1(rtab_time,rtab_y,'linear','pp');


RTK_POS=[ppval(rtkinterp_x ,30) ppval(rtkinterp_y ,30) 0];
RTAB_POS=[ppval(rtabx_interp1 ,30) ppval(rtaby_interp1 ,30) 0];
t=atan2(norm(cross(RTK_POS,RTAB_POS)), dot(RTK_POS,RTAB_POS))-deg2rad(4);
rad2deg(t)

Rzrtab = [cos(t) -sin(t) ppval(rtkinterp_x ,24)-0.05; sin(t) cos(t) ppval(rtkinterp_y ,24)-0.2; 0 0 1];

t1=t-deg2rad(1);
Rzorb = [cos(t1) -sin(t1) ppval(rtkinterp_x ,24)-0.15; sin(t1) cos(t1) ppval(rtkinterp_y ,24)-0.05;0 0 1];
Rzorbloc = [cos(t1) -sin(t1) ppval(rtkinterp_x ,24)-0.1; sin(t1) cos(t1) ppval(rtkinterp_y ,24)+0.15;0 0 1];
    
t2=t-deg2rad(0.15);
Rzvins = [cos(t2) -sin(t2) ppval(rtkinterp_x ,24); sin(t2) cos(t2) ppval(rtkinterp_y ,24)-0.15; 0 0 1];


rtab_pose=Rzrtab*[rtab_x'; rtab_y';ones(1,length(rtab_y))];
rtab_x=rtab_pose(1,:);
rtab_y=rtab_pose(2,:);

orb_pose=Rzorb*[orb_xt'; orb_yt';ones(1,length(orb_xt))];
orb_xt=orb_pose(1,:);
orb_yt=orb_pose(2,:);

vins_pose=Rzvins*[vins_xt'; vins_yt';ones(1,length(vins_xt))];
vins_xt=vins_pose(1,:);
vins_yt=vins_pose(2,:);


rtablocal_pose=Rzrtab*[rtablocal_x'; rtablocal_y';ones(1,length(rtablocal_x))];
rtablocal_x=rtablocal_pose(1,:);
rtablocal_y=rtablocal_pose(2,:);

orblocal_pose=Rzorbloc*[orblocal_xt'; orblocal_yt';ones(1,length(orblocal_xt))];
orblocal_xt=orblocal_pose(1,:);
orblocal_yt=orblocal_pose(2,:);


rtabx_interp=interp1(rtab_time,rtab_x,'linear','pp');
rtaby_interp=interp1(rtab_time,rtab_y,'linear','pp');
orbx_interp=interp1(orb_time,orb_xt,'linear','pp');
orby_interp=interp1(orb_time,orb_yt,'linear','pp');
vinsx_interp=interp1(vins_time,vins_xt,'linear','pp');
vinsy_interp=interp1(vins_time,vins_yt,'linear','pp');

rtablocalx_interp=interp1(rtablocal_time,rtablocal_x,'linear','pp');
rtablocaly_interp=interp1(rtablocal_time,rtablocal_y,'linear','pp');
orblocalx_interp=interp1(orblocal_time,orblocal_xt,'linear','pp');
orblocaly_interp=interp1(orblocal_time,orblocal_yt,'linear','pp');


si=find(rtk_time<start_time, 1, 'last' ); %101
ei=find(rtk_time<end_time, 1, 'last' );

for i=si:ei
    time=rtk_time(i);
    
    trans=[gtenux(i),gtenuy(i),0];
    rotz=ppval(theta_interp,time);
    T_world_RTK=makehgtform('translate',trans,'zrotate',0);
    T_world_robot=T_world_RTK*inv(T_robot_RTK);
    GTx(i)=T_world_robot(1,4);
    GTy(i)=T_world_robot(2,4);
    GTtheta(i)=rotz;
    
    rtabx_e(i)=GTx(i)-ppval(rtabx_interp,time);
    rtaby_e(i)=GTy(i)-ppval(rtaby_interp,time);
    orbx_e(i)=GTx(i)-ppval(orbx_interp,time);
    orby_e(i)=GTy(i)-ppval(orby_interp,time);
    vinsx_e(i)=GTx(i)-ppval(vinsx_interp,time);
    vinsy_e(i)=GTy(i)-ppval(vinsy_interp,time);
    
    rtablocalx_e(i)=GTx(i)-ppval(rtablocalx_interp,time);
    rtablocaly_e(i)=GTy(i)-ppval(rtablocaly_interp,time);
    orblocalx_e(i)=GTx(i)-ppval(orblocalx_interp,time);
    orblocaly_e(i)=GTy(i)-ppval(orblocaly_interp,time);
    
end

RTAB_XMSE=(sum(rtabx_e.^2)/numel(rtabx_e));
RTAB_YMSE=(sum(rtaby_e.^2)/numel(rtaby_e));
ORB_XMSE=(sum(orbx_e.^2)/numel(orbx_e));
ORB_YMSE=(sum(orby_e.^2)/numel(orby_e));
VINS_XMSE=(sum(vinsx_e.^2)/numel(vinsx_e));
VINS_YMSE=(sum(vinsy_e.^2)/numel(vinsy_e));


RTABLOCAL_XMSE=(sum(rtablocalx_e.^2)/numel(rtablocalx_e));
RTABLOCAL_YMSE=(sum(rtablocaly_e.^2)/numel(rtablocaly_e));
ORBLOCAL_XMSE=(sum(orblocalx_e.^2)/numel(orblocalx_e));
ORBLOCAL_YMSE=(sum(orblocaly_e.^2)/numel(orblocaly_e));

RTAB_RMSE=[sqrt(RTAB_XMSE),sqrt(RTAB_YMSE)];
ORB_RMSE=[sqrt(ORB_XMSE),sqrt(ORB_YMSE)];
VINS_RMSE=[sqrt(VINS_XMSE),sqrt(VINS_YMSE)];
RTABLOCAL_RMSE=[sqrt(RTABLOCAL_XMSE),sqrt(RTABLOCAL_YMSE)];
ORBLOCAL_RMSE=[sqrt(ORBLOCAL_XMSE),sqrt(ORBLOCAL_YMSE)];

%%
end_time=135;

orb_start=find(orb_time<start_time, 1, 'last' ); 
orb_end=find(orb_time<end_time, 1, 'last' );
rtab_start=find(rtab_time<start_time, 1, 'last' ); 
rtab_end=find(rtab_time<end_time, 1, 'last' );
vins_start=find(vins_time<start_time, 1, 'last' ); 
vins_end=find(vins_time<end_time, 1, 'last' );


orblocal_start=find(orblocal_time<start_time, 1, 'last' ); 
orblocal_end=find(orblocal_time<end_time, 1, 'last' );
rtablocal_start=find(rtablocal_time<start_time, 1, 'last' ); 
rtablocal_end=find(rtablocal_time<end_time, 1, 'last' );


figure
axis equal
hold on
szGT=15;
szEKF=5;




scatter((vins_xt(vins_start:vins_end)),(vins_yt(vins_start:vins_end)),szEKF,'y','filled')

scatter(orb_xt(orb_start:orb_end),orb_yt(orb_start:orb_end),szEKF,'g','filled')
scatter(orblocal_xt(orblocal_start:orblocal_end),orblocal_yt(orblocal_start:orblocal_end),szEKF,'c','filled')


scatter(rtab_x(rtab_start:rtab_end),rtab_y(rtab_start:rtab_end),szEKF+2,'m','filled')
scatter(rtablocal_x(rtablocal_start:rtablocal_end),rtablocal_y(rtablocal_start:rtablocal_end),szEKF+2,[0.8500 0.3250 0.0980],'filled')

scatter(gtenux,gtenuy,szGT,'r','filled')
scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b','filled');



for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3),8,'x','k')
    %text(LMmap(i,2),LMmap(i,3), num2str(i));
end

xlabel('X (m)')
ylabel('Y (m)')
%Wentworth -25 to +45 (Y), -55 TO 40
%%

% figure
% hold on
% scatter(x_e)
% scatter(rtabx_e)
% scatter(orbx_e)
% scatter(vinsx_e)

%% NEW rmse

RTAB_RMSE=sqrt(RTAB_RMSE(1,1)^2+RTAB_RMSE(1,2)^2)
ORB_RMSE=sqrt(ORB_RMSE(1,1)^2+ORB_RMSE(1,2)^2)
VINS_RMSE=sqrt(VINS_RMSE(1,1)^2+VINS_RMSE(1,2)^2)

EKF_RMSE=sqrt(RMSE(1,1)^2+RMSE(1,2)^2)

RTABLOCAL_RMSE=sqrt(RTABLOCAL_RMSE(1,1)^2+RTABLOCAL_RMSE(1,2)^2)
ORBLOCAL_RMSE=sqrt(ORBLOCAL_RMSE(1,1)^2+ORBLOCAL_RMSE(1,2)^2)
