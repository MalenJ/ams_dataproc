figure
plotmap = subplot(1,1,1);
han=quiver(plotmap,0,0,90,0);

set(han, 'XData', 5, 'YData', 5,'UData',cos(deg2rad(26)),'VData',sin(deg2rad(26)),'LineWidth', 5,'AutoScale',1);

