clear all 
close all

X_pos=[2,3];


A = [];
b = [];
Aeq = [];
beq = [];
lb = [-inf,-2];
ub = [inf,2];

x0=[0,0];


fun=@(X)(X-X_pos)*eye(2)*(X-X_pos)';

X_con = fmincon(fun,x0,A,b,Aeq,beq,lb,ub)

x_line=linspace(1,10,10);
y_up=2*ones(1,10);
y_down=-2*ones(1,10);


figure
hold on
axis equal
plot(x_line,y_up,'k')
plot(x_line,y_down,'k')
scatter(X_pos(1,1),X_pos(1,2),'b')
scatter(round(X_con(1,1),2),round(X_con(1,2),2),'r','d')


A = [0 1;0 -1];
b = [2;2];
Aeq = [];
beq = [];
lb = [];
ub = [];

x0=[0,0];


fun=@(X)(X-X_pos)*eye(2)*(X-X_pos)';

X_con2 = fmincon(fun,x0,A,b,Aeq,beq,lb,ub)

scatter(round(X_con2(1,1),2),round(X_con(1,2),2),'g','x')

