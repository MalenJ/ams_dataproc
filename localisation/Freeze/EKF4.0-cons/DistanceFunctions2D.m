 classdef DistanceFunctions2D
    properties
        DT
        DTx
        DTy
        dDTxdx
        dDTxdy
        dDTydx
        dDTydy
        xMin
        yMin
        sizeX
        sizeY
        res
        DTxBsp
        DTyBsp
        dDTxdxBsp
        dDTxdyBsp
        dDTydxBsp
        dDTydyBsp
        DTxpp
        DTypp
        dDTxdxpp
        dDTxdypp
        dDTydxpp
        dDTydypp
        DTxMeta
        DTyMeta
        dDTxdxMeta
        dDTxdyMeta
        dDTydxMeta
        dDTydyMeta
    end
    
    methods
        function obj = DistanceFunctions2D(mat, xMin, yMin, sizeX, sizeY, res)
            obj.sizeX = sizeX; obj.sizeY = sizeY;
            obj.xMin = xMin; obj.yMin = yMin; obj.res = res;
            
            [DT, DtIndex] = bwdist(mat, 'euclidean');
            obj.DT = double(DT);
            
            obj.DTx = zeros(sizeY, sizeX);
            obj.DTy = zeros(sizeY, sizeX);
            
            for i=1:sizeY
                for j=1:sizeX
                    if obj.DT(i,j) ~= 0
                        index = double(DtIndex(i,j));
                        col = double(floor(index/sizeY + 1));
                        row = double(floor(mod(index, sizeY)));
                        
                        if row == 0
                            row = sizeY;
                        end
                        
                        obj.DTx(i,j) = (col - j);
                        obj.DTy(i,j) = (i - row);
                    end
                end
            end
            
            [obj.dDTxdx, obj.dDTxdy] = gradient(obj.DTx);
            [obj.dDTydx, obj.dDTydy] = gradient(obj.DTy);
            
            % Change the direction of vertical gradient
            obj.dDTxdy = obj.dDTxdy.*(-1);
            obj.dDTydy = obj.dDTydy.*(-1);
            
            % Set resolution
            obj.DTx = obj.DTx.*res;
            obj.DTy = obj.DTy.*res;
            
            xx = xMin:res:xMin+((sizeX-1)*res);
            yy = yMin+((sizeY-1)*res):-res:yMin;          
            
            disp('DistanceFunction2D::spaps X')
            obj.DTxBsp = spaps({yy,xx}, obj.DTx, 0.0001);
            disp('DistanceFunction2D::spaps Y')
            obj.DTyBsp = spaps({yy,xx}, obj.DTy, 0.0001);
            disp('DistanceFunction2D::spaps dDTxdx')
            obj.dDTxdxBsp = spaps({yy,xx}, obj.dDTxdx, 0.0001);
            disp('DistanceFunction2D::spaps dDTxdy')
            obj.dDTxdyBsp = spaps({yy,xx}, obj.dDTxdy, 0.0001);
            disp('DistanceFunction2D::spaps dDTydx')
            obj.dDTydxBsp = spaps({yy,xx}, obj.dDTydx, 0.0001);
            disp('DistanceFunction2D::spaps dDTydy')
            obj.dDTydyBsp = spaps({yy,xx}, obj.dDTydy, 0.0001);
            
            disp('DistanceFunction2D::sp2pp X')
            obj.DTxpp = sp2pp(obj.DTxBsp);
            disp('DistanceFunction2D::sp2pp Y')
            obj.DTypp = sp2pp(obj.DTyBsp);
            disp('DistanceFunction2D::sp2pp dDTxdx')
            obj.dDTxdxpp = sp2pp(obj.dDTxdxBsp);
            disp('DistanceFunction2D::sp2pp dDTxdy')
            obj.dDTxdypp = sp2pp(obj.dDTxdyBsp);
            disp('DistanceFunction2D::sp2pp dDTydx')
            obj.dDTydxpp = sp2pp(obj.dDTydxBsp);
            disp('DistanceFunction2D::sp2pp dDTydy')
            obj.dDTydypp = sp2pp(obj.dDTydyBsp);
            
            disp('DistanceFunctions2D::getSplineMetaData X')
            obj.DTxMeta = obj.getSplineMetaData(obj.DTxBsp);
            disp('DistanceFunctions2D::getSplineMetaData Y')
            obj.DTyMeta = obj.getSplineMetaData(obj.DTyBsp);
            disp('DistanceFunctions2D::getSplineMetaData dDTxdx')
            obj.dDTxdxMeta = obj.getSplineMetaData(obj.dDTxdxBsp);
            disp('DistanceFunctions2D::getSplineMetaData dDTxdy')
            obj.dDTxdyMeta = obj.getSplineMetaData(obj.dDTxdyBsp);
            disp('DistanceFunctions2D::getSplineMetaData dDTydx')
            obj.dDTydxMeta = obj.getSplineMetaData(obj.dDTydxBsp);
            disp('DistanceFunctions2D::getSplineMetaData dDTydy')
            obj.dDTydyMeta = obj.getSplineMetaData(obj.dDTydyBsp);
            
            disp('Exit DistanceFunction2D() with success')
        end
        
        function spMetaData = getSplineMetaData(obj, sp)
           [t,a,n,~,d] = spbrk(sp);
           
           spMetaData.t = t;
           spMetaData.a = a;
           spMetaData.n = n;
           spMetaData.d = d;
           spMetaData.m = length(t);
           spMetaData.tmp = cell2mat(fnbrk(sp,'interv')).';
           spMetaData.mm = 2:2:(2*spMetaData.m);
        end
       
        function v = queryDTx(obj,x)
            nx = size(x,2);
            v = ppual(obj.DTxpp, x);
            v(min([x - repmat(obj.DTxMeta.tmp(obj.DTxMeta.mm-1),1,nx); repmat(obj.DTxMeta.tmp(obj.DTxMeta.mm),1,nx) - x])<0)=0;
        end
        
        function v = queryDTy(obj,x)
            nx = size(x,2);
            v = ppual(obj.DTypp, x);
            v(min([x - repmat(obj.DTyMeta.tmp(obj.DTyMeta.mm-1),1,nx); repmat(obj.DTyMeta.tmp(obj.DTyMeta.mm),1,nx) - x])<0)=0;
        end
        
        function v = querydDTxdx(obj,x)
            nx = size(x,2);
            v = ppual(obj.dDTxdxpp, x);
            v(min([x - repmat(obj.dDTxdxMeta.tmp(obj.dDTxdxMeta.mm-1),1,nx); repmat(obj.dDTxdxMeta.tmp(obj.dDTxdxMeta.mm),1,nx) - x])<0)=0;
        end
        
        function v = querydDTxdy(obj,x)
            nx = size(x,2);
            v = ppual(obj.dDTxdypp, x);
            v(min([x - repmat(obj.dDTxdyMeta.tmp(obj.dDTxdyMeta.mm-1),1,nx); repmat(obj.dDTxdyMeta.tmp(obj.dDTxdyMeta.mm),1,nx) - x])<0)=0;
        end
        
        function v = querydDTydx(obj,x)
            nx = size(x,2);
            v = ppual(obj.dDTydxpp, x);
            v(min([x - repmat(obj.dDTydxMeta.tmp(obj.dDTydxMeta.mm-1),1,nx); repmat(obj.dDTydxMeta.tmp(obj.dDTydxMeta.mm),1,nx) - x])<0)=0;
        end
        
        function v = querydDTydy(obj,x)
            nx = size(x,2);
            v = ppual(obj.dDTydypp, x);
            v(min([x - repmat(obj.dDTydyMeta.tmp(obj.dDTydyMeta.mm-1),1,nx); repmat(obj.dDTydyMeta.tmp(obj.dDTydyMeta.mm),1,nx) - x])<0)=0;
        end
    end    
end
