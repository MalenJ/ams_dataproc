close all
clear all

%Load localisation CSV's

% fileroot='2020-02-15-Wentworth';
% filename='2020-02-15-Wentworth-P2';
fileroot='2020-02-19-Village';
filename='2020-02-19-Village-P1';

CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
IMGPATH=strcat('/home/maleen/rosbags/',fileroot,'/image_data/'); %IMAGE DATA

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/IROScalib/'; %CLIB DATA

MAPPATH_feature='/home/maleen/cloud_academic/ams_data/map_data/'; %MAP DATA
MAPPATH_edge='/home/maleen/cloud_academic/ams_data/map_data/edge_maps/';

odom_sorted=readtable(strcat(CSVPATH, filename, '-sorted.csv'));
rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));

enc_data=readtable(strcat(CSVPATH, filename, '-encoder.csv'));
imu_data=readtable(strcat(CSVPATH, filename, '-imu.csv'));

%yolo_observations=readtable(strcat(CSVPATH,dataset, filename, '-lampdata.csv'));
lamp_data_lb=readtable(strcat(CSVPATH,filename, '-ylb-lampdata.csv'));
lamp_data_lf=readtable(strcat(CSVPATH,filename, '-ylf-lampdata.csv'));
lamp_data_rb=readtable(strcat(CSVPATH,filename, '-yrb-lampdata.csv'));
lamp_data_rf=readtable(strcat(CSVPATH,filename, '-yrf-lampdata.csv'));
lamp_sorted_lb=readtable(strcat(CSVPATH,filename, '-ylb-sorted_yolo.csv'));
lamp_sorted_lf=readtable(strcat(CSVPATH,filename, '-ylf-sorted_yolo.csv'));
lamp_sorted_rb=readtable(strcat(CSVPATH,filename, '-yrb-sorted_yolo.csv'));
lamp_sorted_rf=readtable(strcat(CSVPATH,filename, '-yrf-sorted_yolo.csv'));

backcam_depthdata=readtable(strcat(CSVPATH,filename, '-back_depth.csv'));
backcam_edgedata=readtable(strcat(CSVPATH,filename, '-back_rgb.csv'));

%Depth Image data
load(strcat(IMGPATH, filename, '-depthimgs.mat'),'depth_imgs');

%Camera Params

load(strcat(CALIBPATH,'backcam/back_calibapp.mat'),'cameraParams');
BC_params=cameraParams;
load(strcat(CALIBPATH,'left/left_calibapp.mat'),'cameraParams');
LC_params=cameraParams;
load(strcat(CALIBPATH,'right/right_calibapp.mat'),'cameraParams');
RC_params=cameraParams;
load(strcat(CALIBPATH,'left_front/leftfront_calibapp.mat'),'cameraParams');
LF_params=cameraParams;
load(strcat(CALIBPATH,'left_back/leftback_calibapp.mat'),'cameraParams');
LB_params=cameraParams;
load(strcat(CALIBPATH,'right_back/rightback_calibapp.mat'),'cameraParams');
RB_params=cameraParams;
load(strcat(CALIBPATH,'right_front/rightfront_calibapp.mat'),'cameraParams');
RF_params=cameraParams;

centerX=ones(1,6);
centerX(1)=213;%LF_params.PrincipalPoint(1);
centerX(2)=213;%RF_params.PrincipalPoint(1);
centerX(3)=213;%LB_params.PrincipalPoint(1);
centerX(4)=213;%RB_params.PrincipalPoint(1);
centrX(5)=LC_params.PrincipalPoint(1);
centrX(6)=LC_params.PrincipalPoint(2);

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');

%LOAD EL MAP

%load(strcat(MAPPATH_feature,'/RTK_maps/2019-03-22-Wentworth-map/2019-03-22-Wentworth-satmap.mat'),'map_sat');
load(strcat(MAPPATH_feature,'/el_maps/2019-09-04-Village/2019-09-04-Village-ELMAP.mat'),'final_map');

%LOAD EDGE MAP

%I=imread(strcat(MAPPATH_edge,'2019-08-22-Wentworth/','2019-08-22-Wentworth-map-clean3.png'));
I=imread(strcat(MAPPATH_edge,'2019-09-04-Village/','village_map4.png'));

%% DATA STREAM

% data_time=data_stream{:,2};
% data_type=data_stream{:,3};

BFYT=vertcat(lamp_data_lb,lamp_data_lf,lamp_data_rb,lamp_data_rf);
yolo_observations = sortrows(BFYT,'lamp_times');

yolo_time=yolo_observations{:,3};

uj=1;
labels={};

for i=1:size(yolo_time)
    
    if i==1
        overall_time(uj)=yolo_time(i);
        labels(uj)={'Y'};
        uj=uj+1;
    else
        if yolo_time(i)>yolo_time(i-1)
            overall_time(uj)=yolo_time(i);
            labels(uj)={'Y'};
            uj=uj+1;
        end
        
    end
    
end


yolo_sorted=table(transpose(overall_time),transpose(labels));
yolo_sorted.Properties.VariableNames = {'overall_time' 'labels'};

BFTS=vertcat(odom_sorted(:,{'overall_time', 'labels'}),yolo_sorted);

data_stream = sortrows(BFTS,'overall_time');

data_time=data_stream{:,1};
data_type=data_stream{:,2};

%% Analyse RTK

T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]);

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};

%Wentworth ICRA
% base_lat=-33.878911084893204;
% base_long=1.511945376668094e+02;
% base_alti=26.187976892944185;

% %VILLAGE ICRA
% base_lat=-33.880632824130295;
% base_long=1.511895158304587e+02;
% base_alti=43.434044433462200;

rtk_iter=find(rtk_time<40, 1, 'last' );

% base_lat=mean(rtk_lat(1:rtk_iter));
% base_long=mean(rtk_long(1:rtk_iter));
% base_alti=mean(rtk_alti(1:rtk_iter));

base_lat=rtk_lat(421);
base_long=rtk_long(421);
base_alti=rtk_alti(421);

[rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);

rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);

figure
hold on
scatter(rtk_enux,rtk_enuy)
scatter(0,0,'r','x')
axis('equal')

% figure
% scatter(rtk_time,rtk_enux)


%% Finding rotation between RTK map and odometry

RTK_POS=[ppval(rtkinterp_x ,54) ppval(rtkinterp_y ,54) 0];
RTAB_POS=[0 1 0];
%
t=atan2(norm(cross(RTK_POS,RTAB_POS)), dot(RTK_POS,RTAB_POS))-deg2rad(3);

rad2deg(t)

%% ROTATE AND TRANSLATE RTK DATA

%Translation was figured out based on running EKF in the begining and then translating it based on RTK location relative to the baselink.

for i=1:length(rtk_enuy)
    
    cunt_time=rtk_time(i);
    
    if (95<cunt_time) && (cunt_time<120) %2
        Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0.5; sin(-t-pi/2) cos(-t-pi/2) 0; 0 0 1];
        enu_rot(:,i)=Rz*[rtk_enux(i); rtk_enuy(i);1];
        
    elseif (200<cunt_time) && (cunt_time<220) %3
        Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0.55; sin(-t-pi/2) cos(-t-pi/2) 0; 0 0 1];
        enu_rot(:,i)=Rz*[rtk_enux(i); rtk_enuy(i);1];
        
    elseif (395<cunt_time) && (cunt_time<420) %4
        Rz = [cos(-t-pi/2) -sin(-t-pi/2) -0.8; sin(-t-pi/2) cos(-t-pi/2) -0.6; 0 0 1];
        enu_rot(:,i)=Rz*[rtk_enux(i); rtk_enuy(i);1];
        
    elseif (450<cunt_time) && (cunt_time<510) %5
        Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0; sin(-t-pi/2) cos(-t-pi/2) 0; 0 0 1];
        enu_rot(:,i)=Rz*[rtk_enux(i); rtk_enuy(i);1];
        
    elseif (610<cunt_time) && (cunt_time<550) %6
        Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0.3; sin(-t-pi/2) cos(-t-pi/2) -0.07; 0 0 1];
        enu_rot(:,i)=Rz*[rtk_enux(i); rtk_enuy(i);1];
        
    else  %ELSE 1
        Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0; sin(-t-pi/2) cos(-t-pi/2) 0; 0 0 1];
        enu_rot(:,i)=Rz*[rtk_enux(i); rtk_enuy(i);1];
        
    end
end

figure
scatter(enu_rot(1,:),enu_rot(2,:))

rtk_enux=enu_rot(1,:);
rtk_enuy=enu_rot(2,:);


%% RTK Heading

for i=1:length(rtk_time)-15
    
    rtkXdist=rtk_enux(i+15)-rtk_enux(i);
    rtkYdist=rtk_enuy(i+15)-rtk_enuy(i);
    
    sqrt(rtkXdist^2+rtkYdist^2);
    rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
    rtk_heading(i,:)=atan2(rtkYdist,rtkXdist);
    rtkdist_timer(i,:)=rtk_time(i,:);
    
end

rtk_heading=wrapTo2Pi(rtk_heading);
rtkinterp_z = spline([0;rtk_time],[0;rtk_enuz]);
rtkinterp_theta= spline([0;rtkdist_timer],[0;rtk_heading]);

%test=linspace(0,212,106);

figure
hold on
scatter(rtkdist_timer,rad2deg(rtk_heading),'b')

% figure
% hold on
% plot(rtkdist_timer,rad2deg(rtk_heading),'b')
% plot(rtkdist_timer,rad2deg(ppval(rtkinterp_theta,rtkdist_timer)),'r')
% % %
% figure
% hold on
% plot(rtk_enux,rtk_enuy,'b')
% plot(ppval(rtkinterp_x,test),ppval(rtkinterp_y,test),'-x')

%% ODOM DATA

enc_interp=spline(enc_data{:,3},enc_data{:,6});

encXcalib=6.608284615932709e-05;
encYcalib=6.603486193757895e-05;
calib=mean([encXcalib,encYcalib]);

%IMU DATA
imu_iter=find(imu_data{:,2}<40, 1, 'last' );

imu_bias=mean(imu_gtenux=rtk_enux+0.35;data{1:imu_iter,3});
imu_data{:,3}=imu_data{:,3}-imu_bias;

imu_smooth=movmean(imu_data{:,3},50);

figure
hold on
plot(imu_data{:,2},imu_data{:,3},'b')
plot(imu_data{:,2},imu_smooth,'r')


imu_interp=spline(imu_data{:,2},imu_data{:,3});
imu_interp1=spline(imu_data{:,2},imu_smooth);
imu_interp2=interp1(imu_data{:,2},imu_smooth,'linear','pp');

%Figuring out initial orientation

initial_theta=rtk_heading(450);

%% BACK CAMERA

roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;

j=0;
for i=1:length(backcam_depthdata.back_depth_time)
    
    depth_time=backcam_depthdata.back_depth_time(i);
    
    row=backcam_edgedata.back_rgb_time==depth_time;
    edge_indx=find(row>0);
    
    if ~isnan(edge_indx)
        j=j+1;
        
        
        edgeimg=imread(strcat(IMGPATH,'edge/', filename ,'-edge-',num2str(edge_indx), ".jpg"));
        
        %edgeimg=imread(strcat(IMGPATH,dataset,'edge/l5/', filename ,'-edge-',num2str(edge_indx), ".jpg"));
        
        BW = im2bw(edgeimg,0.5);
        %imshow(BW)
        
        BWU = undistortImage(BW, BC_params);
        depthimgU = undistortImage(depth_imgs{i},BC_params);
        
        depthedge_time(j)=depth_time;
        depthedge{j}=immultiply(BWU,depthimgU);
        
    else
        disp('skip')
    end
    
end

%% RTK MAP

% % %Wentworth
% [map_enu(:,1), map_enu(:,2), map_enu(:,3)]=geodetic2enu(map_sat(:,1),map_sat(:,2),map_sat(:,3),base_lat,base_long,base_alti, wgs84Ellipsoid);
% map_enu_ID=transpose(linspace(1,length(map_enu(:,1)),length(map_enu(:,1))));
% map_enu_semantic=[1;4;1;4;4;1;1;1;4;1;1;1;1;1;1;1;4];
% LMmap=[map_enu_ID,map_enu(:,1)+0.35,map_enu(:,2)-1.7,map_enu_semantic];

%Village
LMmap=final_map;

figure
axis equal
hold on
for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3))
    text(LMmap(i,2),LMmap(i,3), num2str(i));
end


%% EDGE MAP

% % %Wentworth
% xMin=25;
% yMin=25;
% sizeX=750;
% sizeY=1500;
% res=0.04;

%Village
xMin=10;
yMin=40;
sizeX=2375;
sizeY=2000;
res=0.04;

binary_map=imbinarize(rgb2gray(imcomplement(I)));
% SE = strel('square',2);
% dilatedmap = imdilate(binary_map,SE);

dfobj = DistanceFunctions2D(binary_map, -xMin, -yMin, sizeX, sizeY, res);

clear depth_imgs;

%% RTAB DATA
%
% rtab_data=readtable(strcat(CSVPATH,dataset,filename, '-rtabdata.csv'));
%
% rtab_time=rtab_data{:,3};
% rtabx=rtab_data{:,4};
% rtaby=rtab_data{:,5};
% rtabquat=rtab_data{:,6:9};
% rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);
%
% rtabinterp_x= interp1(rtab_time,rtabx,'linear','pp');%RTAB_world(1,:));
% rtabinterp_y = interp1(rtab_time,rtaby,'linear','pp');%RTAB_world(2,:));
% rtabinterp_z = interp1(rtab_time,zeros(length(rtabx),1),'linear','pp');%RTAB_world(3,:));
% rtabinterp_theta = interp1(rtab_time,rtabeul(:,1),'linear','pp');
%
% % rtabinterp_x= spline(rtab_time,rtabx);%RTAB_world(1,:));
% % rtabinterp_y = spline(rtab_time,rtaby);%RTAB_world(2,:));
% % rtabinterp_z = spline(rtab_time,zeros(length(rtabx),1));%RTAB_world(3,:));
% % rtabinterp_theta = spline(rtab_time,rtabeul(:,1));
%
% for i=1:length(rtab_time)-1
%
%     rtabkXdist=rtabx(i+1)-rtabx(i);
%     rtabkYdist=rtaby(i+1)-rtaby(i);
%
%     rtabdist(i,:)=sqrt(rtabkXdist^2+rtabkYdist^2);
%     rtab_heading(i,:)=atan2(rtabkYdist,rtabkXdist);
%     rtabdist_timer(i,:)=rtab_time(i,:);
%
% end
%
% rtabinterp_heading=interp1(rtabdist_timer,rtab_heading,'linear','pp');

