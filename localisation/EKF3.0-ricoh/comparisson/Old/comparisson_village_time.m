close all
clear all

fileroot='2020-02-19-Village';
filename='2020-02-19-Village-P1';

CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
MAPPATH_feature='/home/maleen/cloud_academic/ams_data/map_data/'; %MAP DATA

rtabshort_data=readtable(strcat(CSVPATH, filename, '-RTABlocal-short.csv')); 
orbshort_data=readtable(strcat(CSVPATH, filename, '-ORBlocal-short.csv')); % -orbshortdata -ORBlocal-short

rtablong_data=readtable(strcat(CSVPATH, filename, '-RTABlocal-long.csv'));
orblong_data=readtable(strcat(CSVPATH, filename, '-ORBlocal-long.csv'));

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/IROScalib/'; %CLIB DATA

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');

roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;

%% long data

%RTAB
rtablong_time=rtablong_data{:,3};
rtablong_x=rtablong_data{:,4};
rtablong_y=rtablong_data{:,5};
rtablong_quat=rtablong_data{:,6:9};
%rtablong_theta=rtablong_data{:,7};
rtablongeul = quat2eul([rtablong_quat(:,4) rtablong_quat(:,1:3)]);

%ORB
orblong_time=orblong_data{:,3};
orblong_x=orblong_data{:,4};
orblong_y=orblong_data{:,5};
orblong_z=orblong_data{:,6};
orblong_quat=orblong_data{:,7:10};
orblong_eul = quat2eul([orblong_quat(:,4) orblong_quat(:,1:3)]);

for i=1:length(orblong_time)
    
    trans=[orblong_x(i);orblong_y(i);orblong_z(i);1];
    T_world_robot=(T_robot_backcam)*trans;
    orblong_xt(i,1)=T_world_robot(1,1);
    orblong_yt(i,1)=T_world_robot(2,1);
    orblong_zt(i,1)=T_world_robot(3,1);
end

%scatter3(orblong_xt,orblong_yt,orblong_zt)

%% Short data

%RTAB
rtabshort_time=rtabshort_data{:,3};
rtabshort_x=rtabshort_data{:,4};
rtabshort_y=rtabshort_data{:,5};
rtabshort_quat=rtabshort_data{:,6:9};
rtabshort_eul = quat2eul([rtabshort_quat(:,4) rtabshort_quat(:,1:3)]);
%scatter(rtabshort_x,rtabshort_y)

%ORB
orbshort_time=orbshort_data{:,3};
orbshort_x=orbshort_data{:,4};
orbshort_y=orbshort_data{:,5};
orbshort_z=orbshort_data{:,6};
orbshort_quat=orbshort_data{:,7:10};
orbshort_eul = quat2eul([orbshort_quat(:,4) orbshort_quat(:,1:3)]);

for i=1:length(orbshort_time)
  
    trans=[orbshort_x(i);orbshort_y(i);orbshort_z(i);1];
    T_world_robot=(T_robot_backcam)*trans;
    orbshort_xt(i,1)=T_world_robot(1,1);
    orbshort_yt(i,1)=T_world_robot(2,1);
    orbshort_zt(i,1)=T_world_robot(3,1);
end

%scatter(orbshort_xt,orbshort_yt)

%% LOAD DATA

rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));
rtk_time=rtk_data{:,2};

load('2020-02-19-Village-P1-VINS4-results-rf.mat')

start_time=54;

T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]); %IROS CALIB

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};
stationary_time=40;
rtk_iter=find(rtk_time<stationary_time, 1, 'last' ); %Wentworth

% base_lat=mean(rtk_lat(1:rtk_iter));
% base_long=mean(rtk_long(1:rtk_iter));
% base_alti=mean(rtk_alti(1:rtk_iter));

base_lat=rtk_lat(421);
base_long=rtk_long(421);
base_alti=rtk_alti(421);

[rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);

rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);
RTK_POS=[ppval(rtkinterp_x ,54) ppval(rtkinterp_y ,54) 0];
RTAB_POS=[0 1 0];
%
t=atan2(norm(cross(RTK_POS,RTAB_POS)), dot(RTK_POS,RTAB_POS))-deg2rad(3);

rad2deg(t);

Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0; sin(-t-pi/2) cos(-t-pi/2) 0; 0 0 1];
rtk_pose=Rz*[rtk_enux'; rtk_enuy';ones(1,length(rtk_enux))];
rtk_x=rtk_pose(1,:);
rtk_y=rtk_pose(2,:);

% rtk_enux=rtk_enux+0.8034;
% rtk_enuy=rtk_enuy-1.5209;
rtkx_interp=interp1(rtk_time,rtk_x,'linear','pp');
rtky_interp=interp1(rtk_time,rtk_y,'linear','pp');

%CORRECTIONS
rtabshort_x=rtabshort_x+0.6;
rtabshort_y=rtabshort_y+0.2;
% 
% rtablong_x=rtablong_x+0.6;
% rtablong_y=rtablong_y+0.2;


torb=deg2rad(2);
Rzorb = [cos(torb) -sin(torb) 0.25; sin(torb) cos(torb) 0; 0 0 1];
orb_pose=Rzorb*[orbshort_xt'; orbshort_yt';ones(1,length(orbshort_yt))];
orbshort_xt=orb_pose(1,:);
orbshort_yt=orb_pose(2,:);

% torb=deg2rad(0);
% Rzorb = [cos(torb) -sin(torb) -1; sin(torb) cos(torb) 0.5; 0 0 1];
% orb_pose=Rzorb*[orbshort_xt'; orbshort_yt';ones(1,length(orbshort_yt))];
% orbshort_xt=orb_pose(1,:);
% orbshort_yt=orb_pose(2,:);


orblong_xt=orblong_xt+3;

rtabshortx_interp=interp1(rtabshort_time,rtabshort_x,'linear','pp');
rtabshorty_interp=interp1(rtabshort_time,rtabshort_y,'linear','pp');
orbshortx_interp=interp1(orbshort_time,orbshort_xt,'linear','pp');
orbshorty_interp=interp1(orbshort_time,orbshort_yt,'linear','pp');

rtablongx_interp=interp1(rtablong_time,rtablong_x,'linear','pp');
rtablongy_interp=interp1(rtablong_time,rtablong_y,'linear','pp');
orblongx_interp=interp1(orblong_time,orblong_xt,'linear','pp');
orblongy_interp=interp1(orblong_time,orblong_yt,'linear','pp');


si=find(rtk_time<start_time, 1, 'last' ); %101
ei=find(rtk_time<576, 1, 'last' );

for i=si:ei
    time=rtk_time(i);
    %trans=[ppval(rtkx_interp,time),ppval(rtky_interp,time),0];
    trans=[gtenux(i),gtenuy(i),0];
    rotz=ppval(theta_interp,time);
    T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
    T_world_robot=T_world_RTK*inv(T_robot_RTK);
    GTx(i)=T_world_robot(1,4);
    GTy(i)=T_world_robot(2,4);
    GTtheta(i)=rotz;
    
    rtabshortx_e(i)=GTx(i)-ppval(rtabshortx_interp,time);
    rtabshorty_e(i)=GTy(i)-ppval(rtabshorty_interp,time);
    orbshortx_e(i)=GTx(i)-ppval(orbshortx_interp,time);
    orbshorty_e(i)=GTy(i)-ppval(orbshorty_interp,time);
    
    rtablongx_e(i)=GTx(i)-ppval(rtablongx_interp,time);
    rtablongy_e(i)=GTy(i)-ppval(rtablongy_interp,time);
    orblongx_e(i)=GTx(i)-ppval(orblongx_interp,time);
    orblongy_e(i)=GTy(i)-ppval(orblongy_interp,time);
    
 
end



rtabshort_XMSE=(sum(rtabshortx_e.^2)/numel(rtabshortx_e));
rtabshort_YMSE=(sum(rtabshorty_e.^2)/numel(rtabshorty_e));
orbshort_XMSE=(sum(orbshortx_e.^2)/numel(orbshortx_e));
orbshort_YMSE=(sum(orbshorty_e.^2)/numel(orbshorty_e));

RTABlong_XMSE=(sum(rtablongx_e.^2)/numel(rtablongx_e));
RTABlong_YMSE=(sum(rtablongy_e.^2)/numel(rtablongy_e));
ORBlong_XMSE=(sum(orblongx_e.^2)/numel(orblongx_e));
ORBlong_YMSE=(sum(orblongy_e.^2)/numel(orblongy_e));

rtabshort_RMSE=[sqrt(rtabshort_XMSE),sqrt(rtabshort_YMSE)];
orbshort_RMSE=[sqrt(orbshort_XMSE),sqrt(orbshort_YMSE)];

RTABlong_RMSE=[sqrt(RTABlong_XMSE),sqrt(RTABlong_YMSE)];
ORBlong_RMSE=[sqrt(ORBlong_XMSE),sqrt(ORBlong_YMSE)];

%%

orbshort_start=find(orbshort_time<start_time, 1, 'last' ); 
orbshort_end=find(orbshort_time<end_time, 1, 'last' );
rtabshort_start=find(rtabshort_time<start_time, 1, 'last' ); 
rtabshort_end=find(rtabshort_time<end_time, 1, 'last' );

orblong_start=find(orblong_time<start_time, 1, 'last' ); 
orblong_end=find(orblong_time<end_time, 1, 'last' );
rtablong_start=find(rtablong_time<start_time, 1, 'last' ); 
rtablong_end=find(rtablong_time<end_time, 1, 'last' );


figure
axis equal
hold on
szGT=4;
szEKF=8;

scatter(rtablong_x(1:rtablong_end),rtablong_y(1:rtablong_end),szEKF+2,'b','filled')

scatter(orbshort_xt(orbshort_start:orbshort_end),orbshort_yt(orbshort_start:orbshort_end),szEKF+2,'g','filled')
scatter(rtabshort_x(rtabshort_start:rtabshort_end),rtabshort_y(rtabshort_start:rtabshort_end),szEKF+2,'m','filled')
scatter(orblong_xt(orblong_start:orblong_end),orblong_yt(orblong_start:orblong_end),szEKF+2,'c','filled')

%scatter(rtk_x,rtk_y,szGT,'r','filled')
scatter(gtenux,gtenuy,szGT,'r','filled')


% for i=1:length(LMmap)
%     scatter(LMmap(i,2),LMmap(i,3),8,'x','k')
%     %text(LMmap(i,2),LMmap(i,3), num2str(i));
% end

xlabel('X (m)')
ylabel('Y (m)')
%Wentworth -25 to +45 (Y), -55 TO 40
%%

% figure
% hold on
% scatter(x_e)
% scatter(rtabx_e)
% scatter(orbx_e)
% scatter(vinsx_e)

%% NEW rmse

rtabshort_RMSE=sqrt(rtabshort_RMSE(1,1)^2+rtabshort_RMSE(1,2)^2)
orbshort_RMSE=sqrt(orbshort_RMSE(1,1)^2+orbshort_RMSE(1,2)^2)


EKF_RMSE=sqrt(RMSE(1,1)^2+RMSE(1,2)^2)

RTABlong_RMSE=sqrt(RTABlong_RMSE(1,1)^2+RTABlong_RMSE(1,2)^2)
ORBlong_RMSE=sqrt(ORBlong_RMSE(1,1)^2+ORBlong_RMSE(1,2)^2)
