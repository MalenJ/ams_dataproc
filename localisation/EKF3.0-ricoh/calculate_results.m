function [RMSE] = calculate_results(rtk_time,rtk_enux,rtk_enuy,EKF_time,X_k1k1,datt,sigmabounds,T_robot_RTK,start_time,end_time,location,LMmap)

if location==0
    % %% PLOT Wentworth
    gtenux=rtk_enux-0.1;
    gtenuy=rtk_enuy
    
    xekf_interp=interp1(EKF_time,X_k1k1(1,:),'linear','pp');
    yekf_interp=interp1(EKF_time,X_k1k1(2,:),'linear','pp');
    theta_interp=interp1(EKF_time,X_k1k1(3,:),'linear','pp');
    
    
    end_time=112;
    
    rtkoffsetiter=find(rtk_time<start_time, 1, 'last' );
    timeoffset=rtk_time(rtkoffsetiter);
    
    si=find(rtk_time<start_time, 1, 'last' ); %101
    ei=find(rtk_time<end_time, 1, 'last' );
    
    
    for i=si:ei
        
        time=rtk_time(i);
        rtkerrortime(i)=time-timeoffset;
        
        trans=[gtenux(i),gtenuy(i),0];
        rotz=ppval(theta_interp,time);
        T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
        T_world_robot=T_world_RTK*inv(T_robot_RTK);
        GTx(i)=T_world_robot(1,4);
        GTy(i)=T_world_robot(2,4);
        GTtheta(i)=rotz;
        
        %     GTx(i)=rtk_enux(i);
        %     GTy(i)=rtk_enuy(i);
        
        x_e(i)=GTx(i)-ppval(xekf_interp,time);
        y_e(i)=GTy(i)-ppval(yekf_interp,time);
        
        
    end
    
    sz=0.5;
    
    figure
    subplot(2,1,1)
    set(gca,'XLim',[0 end_time-start_time]);
    hold on
    scatter(rtkerrortime,x_e,sz,'r');
    plot(datt-start_time, sigmabounds(1,:)+0.02,'b');
    plot(datt-start_time, -sigmabounds(1,:)-0.02,'b');
    xlabel('time (s)')
    ylabel('X error (m)')
    hold off
    subplot(2,1,2)
    set(gca,'XLim',[0 end_time-start_time]);
    hold on
    scatter(rtkerrortime,y_e,sz,'r');
    plot(datt-start_time, sigmabounds(2,:)+0.02,'b');
    plot(datt-start_time, -sigmabounds(2,:)-0.02,'b');
    hold off
    xlabel('time (s)')
    ylabel('Y erro(m)')
    
    
    XMSE=(sum(x_e.^2)/numel(x_e));
    YMSE=(sum(y_e.^2)/numel(y_e));
    
    MAXX=max(abs(x_e));
    MAXY=max(abs(y_e));
    %MAXTHETA=rad2deg(sum(error(3,:).^2)/numel(error(3,:)))
    
    MSE=[XMSE,YMSE]
    RMSE=[sqrt(XMSE),sqrt(YMSE)]
    MAX=[MAXX,MAXY]
    
    RMSE_FIN=sqrt(RMSE(1,1)^2+RMSE(1,2)^2)
    
    figure
    axis equal
    hold on
    szGT=1.5;
    szEKF=2;
    
    for i=1:size(LMmap,1)
        scatter(LMmap(i,2),LMmap(i,3),'x','k')
        text(LMmap(i,2),LMmap(i,3), num2str(LMmap(i,1)));
    end
    
    %scatter(rtabx,rtaby,szGT,'r')
    scatter(GTx,GTy,szGT,'r')
    scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b');
    xlabel('X (m)')
    ylabel('Y (m)')
    
    
    
elseif location==1
    %Plot Village
    
    %     rtk_enux=smoothdata(rtk_enux,'sgolay',3);
    %     rtk_enuy=smoothdata(rtk_enuy,'sgolay',3);
    
    gtenux=rtk_enux;
    gtenuy=rtk_enuy;
    
    % figure
    % plot(data_time(startiter:enditer),lin_vel)
    % figure
    % plot(data_time(startiter:enditer),ang_vel)
    
    %Odom correction
    %     for i=1:length(rtk_enuy)
    %
    %         cunt_time=rtk_time(i);
    %
    %         if (95<cunt_time) && (cunt_time<120) %2
    %                gtenux(i)=rtk_enux(i)+0.4;
    %
    %
    %         elseif (200<cunt_time) && (cunt_time<220) %3
    %               gtenux(i)=rtk_enux(i)+0.45;
    %               gtenuy(i)=rtk_enuy(i)+0.15;
    %
    %         elseif (395<cunt_time) && (cunt_time<420) %4
    %               gtenuy(i)=rtk_enuy(i)+0.1;
    %
    %         elseif (450<cunt_time) && (cunt_time<510) %5
    %               gtenuy(i)=rtk_enuy(i)+1.1;
    %
    %         elseif (610<cunt_time) && (cunt_time<550) %6
    %
    %
    %         else  %ELSE 1
    %             gtenux(i)=rtk_enux(i)+0.25;
    %         end
    %     end
    
    %VINS correction
    for i=1:length(rtk_enuy)
        
        cunt_time=rtk_time(i);
        
        if (95<cunt_time) && (cunt_time<120) %2
            gtenux(i)=rtk_enux(i)+0.35;
            
            
        elseif (200<cunt_time) && (cunt_time<220) %3
            gtenux(i)=rtk_enux(i)-0.15; %0.15 for whichcam , comment out for allcam
            gtenuy(i)=rtk_enuy(i)-0.25;
            
        elseif (395<cunt_time) && (cunt_time<420) %4
            gtenux(i)=rtk_enux(i)+0.1;
            gtenuy(i)=rtk_enuy(i)+0.25;
            
        elseif (450<cunt_time) && (cunt_time<510) %5
            gtenux(i)=rtk_enux(i)+0.1;
            
        elseif (510<cunt_time) && (cunt_time<550) %6
            gtenux(i)=rtk_enux(i)+0.1;
            gtenuy(i)=rtk_enuy(i)-0.15; %-0.15 whichcam, -0.35 allcam
            
        else  %ELSE 1
            gtenux(i)=rtk_enux(i)+0.15;
        end
    end
    
    
    
    xekf_interp=interp1(EKF_time,X_k1k1(1,:),'linear','pp');
    yekf_interp=interp1(EKF_time,X_k1k1(2,:),'linear','pp');
    theta_interp=interp1(EKF_time,X_k1k1(3,:),'linear','pp');
    
    rtkoffsetiter=find(rtk_time<start_time, 1, 'last' );
    timeoffset=rtk_time(rtkoffsetiter);
    
    si=find(rtk_time<start_time, 1, 'last' ); %101
    ei=find(rtk_time<540, 1, 'last' );
    
    
    for i=si:ei
        
        time=rtk_time(i);
        rtkerrortime(i)=time-timeoffset;
        
        trans=[gtenux(i),gtenuy(i),0];
        rotz=ppval(theta_interp,time);
        T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
        T_world_robot=T_world_RTK*inv(T_robot_RTK);
        GTx(i)=T_world_robot(1,4);
        GTy(i)=T_world_robot(2,4);
        GTtheta(i)=rotz;
        
        %     GTx(i)=rtk_enux(i);
        %     GTy(i)=rtk_enuy(i);
        
        x_e(i)=GTx(i)-ppval(xekf_interp,time);
        y_e(i)=GTy(i)-ppval(yekf_interp,time); %+0.1
    end
    
    end_time=540;
    
    
    
    
    %PLOTS
    sz=0.5;
    szb=0.75;
    figure
    subplot(2,1,1)
    set(gca,'XLim',[0 end_time-start_time]);
    hold on
    scatter(rtkerrortime,x_e,sz,'r');
    plot(datt-start_time, sigmabounds(1,:)+0.1,'b');
    plot(datt-start_time, -sigmabounds(1,:)-0.1,'b');
    xlabel('time (s)')
    xticks(0:50:end_time)
    ylabel('X error (m)')
    hold off
    subplot(2,1,2)
    set(gca,'XLim',[0 end_time-start_time]);
    hold on
    scatter(rtkerrortime,y_e,sz,'r');
    plot(datt-start_time, sigmabounds(2,:)+0.1,'b');
    plot(datt-start_time, -sigmabounds(2,:)-0.1,'b');
    hold off
    xlabel('time (s)')
    xticks(0:50:end_time)
    ylabel('Y erro(m)')
    
    
    XMSE=(sum(x_e.^2)/numel(x_e));
    YMSE=(sum(y_e.^2)/numel(y_e));
    
    MAXX=max(abs(x_e));
    MAXY=max(abs(y_e));
    %MAXTHETA=rad2deg(sum(error(3,:).^2)/numel(error(3,:)))
    
    MSE=[XMSE,YMSE];
    RMSE=[sqrt(XMSE),sqrt(YMSE)];
    MAX=[MAXX,MAXY];
    
    RMSE_FIN=sqrt(RMSE(1,1)^2+RMSE(1,2)^2)
    
    
    figure
    axis equal
    hold on
    szGT=1.5;
    szEKF=2;
    
    for i=1:size(LMmap,1)
        scatter(LMmap(i,2),LMmap(i,3),'x','k')
        text(LMmap(i,2),LMmap(i,3), num2str(LMmap(i,1)));
    end
    
    %scatter(rtabx,rtaby,szGT,'r')
    scatter(GTx,GTy,szGT,'r')
    scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b');
    xlabel('X (m)')
    ylabel('Y (m)')
    
    %Plot UTS
    
elseif location==2
    
    display('UTS')
    
    gtenux=rtk_enux;
    gtenuy=rtk_enuy;
    
    
    xekf_interp=interp1(EKF_time,X_k1k1(1,:),'linear','pp');
    yekf_interp=interp1(EKF_time,X_k1k1(2,:),'linear','pp');
    theta_interp=interp1(EKF_time,X_k1k1(3,:),'linear','pp');
    
    rtkoffsetiter=find(rtk_time<start_time, 1, 'last' );
    timeoffset=rtk_time(rtkoffsetiter);
    
    si=find(rtk_time<start_time, 1, 'last' ); %101
    ei=find(rtk_time<end_time, 1, 'last' );
    
    
    for i=si:ei
        
        time=rtk_time(i);
        rtkerrortime(i)=time-timeoffset;
        
        GTx(i)=gtenux(i);
        GTy(i)=gtenuy(i);
        %GTtheta(i)=rotz;
        
        %     GTx(i)=rtk_enux(i);
        %     GTy(i)=rtk_enuy(i);
        
        x_e(i)=GTx(i)-ppval(xekf_interp,time);
        y_e(i)=GTy(i)-ppval(yekf_interp,time); %+0.1
    end
    
    
    %PLOTS
    sz=0.5;
    szb=0.75;
    figure
    subplot(2,1,1)
    set(gca,'XLim',[0 end_time-start_time]);
    hold on
    scatter(rtkerrortime,x_e,sz,'r');
    plot(datt-start_time, sigmabounds(1,:)+0.1,'b');
    plot(datt-start_time, -sigmabounds(1,:)-0.1,'b');
    xlabel('time (s)')
    xticks(0:50:end_time)
    ylabel('X error (m)')
    hold off
    subplot(2,1,2)
    set(gca,'XLim',[0 end_time-start_time]);
    hold on
    scatter(rtkerrortime,y_e,sz,'r');
    plot(datt-start_time, sigmabounds(2,:)+0.1,'b');
    plot(datt-start_time, -sigmabounds(2,:)-0.1,'b');
    hold off
    xlabel('time (s)')
    xticks(0:50:end_time)
    ylabel('Y error(m)')
    
    
    XMSE=(sum(x_e.^2)/numel(x_e));
    YMSE=(sum(y_e.^2)/numel(y_e));
    
    MAXX=max(abs(x_e));
    MAXY=max(abs(y_e));
    %MAXTHETA=rad2deg(sum(error(3,:).^2)/numel(error(3,:)))
    
    MSE=[XMSE,YMSE];
    RMSE=[sqrt(XMSE),sqrt(YMSE)];
    MAX=[MAXX,MAXY];
    
    RMSE_FIN=sqrt(RMSE(1,1)^2+RMSE(1,2)^2)
    
    
    figure
    axis equal
    hold on
    szGT=1.5;
    szEKF=2;
    
    for i=1:size(LMmap,1)
        scatter(LMmap(i,2),LMmap(i,3),'x','k')
        text(LMmap(i,2),LMmap(i,3), num2str(LMmap(i,1)));
    end
    
    %scatter(rtabx,rtaby,szGT,'r')
    scatter(GTx,GTy,szGT,'r')
    scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b');
    xlabel('X (m)')
    ylabel('Y (m)')
    
end

save('results.mat','location','gtenux', 'gtenuy', 'GTx', 'GTy',...
    'LMmap','RMSE','RMSE_FIN','rtk_time','rtk_enux','rtk_enuy',...
    'datt','EKF_time', 'X_k1k1','sigmabounds', 'start_time','end_time',...
    'T_robot_RTK','theta_interp', 'x_e', 'XMSE', 'y_e','YMSE')


end

