close all

clear all
fileroot='2020-02-28-Wentworth';
filename='2020-02-28-Wentworth-P1';
localreadyname='RICOHVINS1.mat';

% clear all
% fileroot='2020-02-19-Village';
% filename='2020-02-19-Village-P1';
% localreadyname='RICOHVINS1.mat';

% %clear all
% fileroot='2021-01-11-UTS';
% filename='2021-01-11-UTS-P1';
% localreadyname='RICOH1.mat';


CONFIGPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/config/');
load(strcat(CONFIGPATH, filename,'-prepconfig-', localreadyname))
load(strcat(CONFIGPATH, filename,'-localconfig-', localreadyname))
PATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/');
load(strcat(PATH,fileroot,'/localready/',filename,'-localready-',localreadyname),loadvars{:},'LF_params','RF_params','LB_params','RB_params','LC_params','RC_params','vins_x','vins_y');
load(strcat(PATH,fileroot,'/localready/',filename,'-localready-',localreadyname),'LMmap');

% start_time=50;
% end_time=540;
usampvec=floor(linspace(400,350,(400-350)/1)); %FOR WENT

%load(strcat(PATH,fileroot,'/localready/',filename,'-localready-VINS5.mat'),'dfobj','binary_map');

% rtk_enux=rtabx;
% rtk_enuy=rtaby;
% rtk_time=rtab_time;



centerX=ones(1,6);
centerX(1)=LF_params.PrincipalPoint(1);
centerX(2)=RF_params.PrincipalPoint(1);
centerX(3)=LB_params.PrincipalPoint(1);
centerX(4)=RB_params.PrincipalPoint(1);
centerX(5)=LC_params.PrincipalPoint(1);
centerX(6)=RC_params.PrincipalPoint(1);
% centerX(1)=230;
% centerX(2)=230;
% centerX(3)=230;
% centerX(4)=230;
% centerX(5)=230;
% centerX(6)=230;

global xMing
global yMing
global resg
global sizeYg
global sizeXg
global lasermap
global featuremap
global realtimeplotter
global robothan
global laserhan
global featurehan
global drawInit
global sigma_v;
global sigma_w;
global sigma_motionPd;
global sigma_motionPtheta;
global sigma_bearing;
global IG_bearing;
global sigma_edge;
global sigma_edgeP;
global sigma_bearingP;
global edge_chi_sqr;
global stationaryflag;
stationaryflag=0;
xMing=xMin;
yMing=yMin;
resg=res;
sizeYg=sizeY;
sizeXg=sizeX;

global previouscam;
global num_detect;

start_pose=start_pose-[0;0.3;0];
sigma_v=0.02;
sigma_w=0.01;
sigma_motionPd=0.001;
sigma_motionPtheta=0.009;
sigma_bearing=25;
IG_bearing=0.15;
sigma_bearingP=0;
sigma_edge=0.15; %1
sigma_edgeP=1; %0.5
edge_chi_sqr=4;

realtimeplotter=1;
errorplotter=1;
covarianceplotter=1;

startiter=find(data_stream{:,1}<start_time, 1, 'last' );
enditer=find(data_stream{:,1}<end_time, 1, 'last' );
iter=0;
sz=25;
LMmap=correct_LM(LMmap,location);




if realtimeplotter==1
    drawInit = 0;
    featuremap=LMmap;
    figure(1)
    lasermap = subplot(1,1,1);
    hold on
    imshow(imcomplement(binary_map))
    
    scatter(lasermap,(rtk_enux+xMin)./resg,sizeY-(rtk_enuy+yMin)./resg,sz-24,'r')
    scatter(lasermap,(vins_x+xMin)./resg,sizeY-(vins_y+yMin)./resg,sz-24,'b')
    for i=1:size(LMmap,1)
        scatter(lasermap,(LMmap(i,2)+xMin)./resg,sizeYg-(LMmap(i,3)+yMing)./resg,sz,'x','k')
        text(lasermap,(LMmap(i,2)+xMin)./resg,sizeYg-(LMmap(i,3)+yMing)./resg, num2str(i));
    end
    
    laserhan = plot(lasermap,0,0,'g.');
    
    if covarianceplotter==1
        figure(2)
        errorplotX= subplot(3,1,1);
        hold on
        eplotxhandle.error=plot(errorplotX,0,0);
        eplotxhandle.covup=plot(errorplotX,0,0);
        eplotxhandle.covdown=plot(errorplotX,0,0);
        xlabel(errorplotX,'time (s)')
        ylabel(errorplotX,'X error (m)')
        errorplotY= subplot(3,1,2);
        hold on
        eplotyhandle.error=plot(errorplotY,0,0);
        eplotyhandle.covup=plot(errorplotY,0,0);
        eplotyhandle.covdown=plot(errorplotY,0,0);
        xlabel(errorplotY,'time (s)')
        ylabel(errorplotY,'Y error (m)')
        errorplotTheta= subplot(3,1,3);
        hold on
        eplotthetahandle.error=plot(errorplotTheta,0,0);
        eplotthetahandle.covup=plot(errorplotTheta,0,0);
        eplotthetahandle.covdown=plot(errorplotTheta,0,0);
        xlabel(errorplotTheta,'time (s)')
        ylabel(errorplotTheta,'Theta error (m)')
    end
end

error_data_cnt=0;


for i=startiter:enditer
    
    iter=iter+1;
    
    previous_time=data_stream{i-1,1};
    current_time=data_stream{i,1};
    EKF_time(iter)=current_time;
    current_data=data_stream{i,2}{:};
    dt=current_time-previous_time;
    
    U_k(1,1)=(ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib;
    U_k(2,1)=ppval(imu_interp,previous_time);
    
    odomobj=odom_obsZ(U_k);
    
    if (i-1<startiter)
        if calc_startpose==1
            trans=[ppval(rtkinterp_x,previous_time),ppval(rtkinterp_y,previous_time),ppval(rtkinterp_z,previous_time)];
            rotz=ppval(rtkinterp_theta,previous_time);
            T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
            T_world_robot=T_world_RTK*inv(T_robot_RTK);
            start_pose=[T_world_robot(1,4)-0.15;T_world_robot(2,4);rotz]; %went
            start_cov=[(0.1)^2 0 0;0 (0.1)^2 0;0 0 (deg2rad(5))^2];
        end
        X_k=start_pose;
        P_k=start_cov;%start_cov;
    else
        iterval=1+(iter-2)*3;
        X_k=X_k1k1(:,iter-1);
        P_k=P_k1k1(1:3,iterval:iterval+2);
    end
    
    
    if prep_VINS==0
        U_k(1,1)=(ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib;
        U_k(2,1)=ppval(imu_interp,previous_time);
        [X_k1k,P_k1k] = prediction(X_k,P_k,U_k,dt);
        odomobj=odom_obsZ(U_k);
        %         lin_vel(iter)=U_k(1,1)/dt;
        %         ang_vel(iter)= U_k(2,1);
    else
        vel=(ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib;
        U_k(1,1)=(ppval(vinsx_interp,current_time)-ppval(vinsx_interp,previous_time));
        U_k(2,1)=(ppval(vinsy_interp,current_time)-ppval(vinsy_interp,previous_time));
        U_k(3,1)=dt*(ppval(imu_interp,previous_time)); %-ppval(imu_interp,previous_time));
        %U_k(3,1)=(ppval(vinsyaw_interp,current_time)-ppval(vinsyaw_interp,previous_time));
        ang_vel=ppval(vinsyaw_interp,previous_time);
        [X_k1k,P_k1k] = prediction_VINS(X_k,P_k,U_k,dt,vel);
        odomobj=odom_obsZ(U_k);
    end
    
    %Prediction step
    
    X_k1k(3,1)=wrapTo2Pi(X_k1k(3,1));
    
    if (strcmp(current_data,'V') || strcmp(current_data,'O'))
        %current_data
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        drawEKFresults(odomobj, X_k1k1(:,iter));
        
        
    elseif (current_data=='Y') %whichcam implimentation
        
        rows1=yolo_observations.lamp_times==current_time;
        cameras_obs1=yolo_observations(rows1,{'yolo_index','camera','centroid_x','yolo_labels'});
        
        [wc,score] = whichcam(X_k1k,P_k1k,LMmap);
        %WHICH CAM
        previouscam=wc;
        
        if wc=='LF'
            rows2=strcmp(cameras_obs1.camera,'LF');
        elseif wc=='LB'
            rows2=0;
            rows2=strcmp(cameras_obs1.camera,'LB');
        elseif wc=='RF'
            rows2=strcmp(cameras_obs1.camera,'RF');
        elseif wc=='RB'
            rows2=0;
            rows2=strcmp(cameras_obs1.camera,'RB');
        elseif wc=='NA'
            rows2=0;
        end
        
        if max(rows2>0)
            cameras_obs=cameras_obs1(rows2,{'yolo_index','camera','centroid_x','yolo_labels'});
            bear_obs=bearing_obZ(centerX,current_time,cameras_obs);
            num_detect=max(rows2);
            iterval=1+(iter-1)*3;
            [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),bear_obs,correctionx,correctionp] = observation_bearing(X_k1k,P_k1k,bear_obs,LMmap);
            drawEKFresults(bear_obs, X_k1k1(:,iter));
        else
            num_detect=0;
            iterval=1+(iter-1)*3;
            X_k1k1(:,iter)=X_k1k;
            P_k1k1(1:3,iterval:iterval+2)=P_k1k;
            drawEKFresults(odomobj, X_k1k1(:,iter));
        end
%                     %Isolate
%                     iterval=1+(iter-1)*3;
%                     X_k1k1(:,iter)=X_k1k;
%                     P_k1k1(1:3,iterval:iterval+2)=P_k1k;
%                     drawEKFresults(odomobj, X_k1k1(:,iter));
        
%                     elseif (current_data=='Y') %All cam implimentation
%         
%                         lfcorrectionx=zeros(3,1);
%                         lfcorrectionp=zeros(3,3);
%                         lbcorrectionx=zeros(3,1);
%                         lbcorrectionp=zeros(3,3);
%                         rfcorrectionx=zeros(3,1);
%                         rfcorrectionp=zeros(3,3);
%                         rbcorrectionx=zeros(3,1);
%                         rbcorrectionp=zeros(3,3);
%         
%                         rows1=yolo_observations.lamp_times==current_time;
%         
%                         cameras_obs1=yolo_observations(rows1,{'yolo_index','camera','centroid_x','yolo_labels'});
%         
%                         rowslf=strcmp(cameras_obs1.camera,'LF');
%                         rowslb=strcmp(cameras_obs1.camera,'LB');
%                         rowsrf=strcmp(cameras_obs1.camera,'RF');
%                         rowsrb=strcmp(cameras_obs1.camera,'RB');
%         
%                         x1=X_k1k;
%                         p1=P_k1k;
%         
%                         if max(rowslf>0)
%         
%                             cameras_obs=cameras_obs1(rowslf,{'yolo_index','camera','centroid_x','yolo_labels'});
%                             bear_obslf=bearing_obZ(centerX,current_time,cameras_obs);
%                             [x1,p1,bear_obslf,lfcorrectionx,lfcorrectionp]  = observation_bearing(x1,p1,bear_obslf,LMmap);
%                             drawEKFresults(bear_obslf, x1);
%                         end
%         
%         
%                         if max(rowslb>0)
%         
%                             cameras_obs=cameras_obs1(rowslb,{'yolo_index','camera','centroid_x','yolo_labels'});
%                             bear_obslb=bearing_obZ(centerX,current_time,cameras_obs);
%                             [x1,p1,bear_obslb,lbcorrectionx,lbcorrectionp]  = observation_bearing(x1,p1,bear_obslb,LMmap);
%                             drawEKFresults(bear_obslb, x1);
%                         end
%         
%                         if max(rowsrf>0)
%         
%                             cameras_obs=cameras_obs1(rowsrf,{'yolo_index','camera','centroid_x','yolo_labels'});
%                             bear_obsrf=bearing_obZ(centerX,current_time,cameras_obs);
%                             [x1,p1,bear_obsrf,rfcorrectionx,rfcorrectionp]  = observation_bearing(x1,p1,bear_obsrf,LMmap);
%                             drawEKFresults(bear_obsrf, x1);
%                         end
%         
%                         if max(rowsrb>0)
%         
%                             cameras_obs=cameras_obs1(rowsrb,{'yolo_index','camera','centroid_x','yolo_labels'});
%                             bear_obsrb=bearing_obZ(centerX,current_time,cameras_obs);
%                             [x1,p1,bear_obsrb,rbcorrectionx,rbcorrectionp]  = observation_bearing(x1,p1,bear_obsrb,LMmap);
%                             drawEKFresults(bear_obsrb, x1);
%                         end
%                         %             %Isolate
%                         %             iterval=1+(iter-1)*3;
%                         %             X_k1k1(:,iter)=X_k1k;
%                         %             P_k1k1(1:3,iterval:iterval+2)=P_k1k;
%                         %             drawEKFresults(odomobj, X_k1k1(:,iter));
%                         iterval=1+(iter-1)*3;
%                         X_k1k1(:,iter)=x1;
%                         P_k1k1(1:3,iterval:iterval+2)=p1;
%         %                 X_k1k1(:,iter)=X_k1k+lfcorrectionx+lbcorrectionx+rfcorrectionx+rbcorrectionx;
%         %                 P_k1k1(1:3,iterval:iterval+2)=P_k1k+lfcorrectionp+lbcorrectionp+rfcorrectionp+rbcorrectionp;
%         %
    elseif(current_data=='BC')
        
        %         current_data;
        iterval=1+(iter-1)*3;
        
%                 img_seq=find(depthedge_time==current_time);
%         
%                 if ~isnan(img_seq)
%                     depthedge_img=depthedge{img_seq};
%         
%                     img_seq;
%                     edge_obs=edge_obsZ(BC_params,current_time,depthedge_img,T_robot_backcam,usampvec,vsampvec);
%                     %edge_obs=edge_obsZ(BC_params,current_time,depthedge_img,T_robot_backcam);
%         
%         
%                     if edge_obs.flagger>0 % && 2*edge_obs.flagger <2500
%                         [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2)] = observation_edge(X_k1k, P_k1k, edge_obs.ranges,edge_obs.bearings, dfobj);
%                         %[X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2), K] = update(X_k1k, P_k1k, V, jHx, S);
%                         drawEKFresults(edge_obs, X_k1k1(:,iter));
%                     else
%                         X_k1k1(:,iter)=X_k1k;
%                         P_k1k1(1:3,iterval:iterval+2)=P_k1k;
%                         drawEKFresults(odomobj, X_k1k1(:,iter));
%                     end
%                 else
%                     X_k1k1(:,iter)=X_k1k;
%                     P_k1k1(1:3,iterval:iterval+2)=P_k1k;
%                     drawEKFresults(odomobj, X_k1k1(:,iter));
%                 end
        
        %         Isolate
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        drawEKFresults(odomobj, X_k1k1(:,iter));
        
    end
    
    iterval=1+(iter-1)*3;
    %     scatter(X_k1k1(1,iter),X_k1k1(2,iter),sz,'b');
    
    if (~isnan(P_k1k1(1,iterval:iterval+2)))
        
        error_data_cnt=error_data_cnt+1;
        datt(error_data_cnt,:)=current_time;
        
        PK=P_k1k1(1:3,iterval:iterval+2);
        pose=X_k1k1(:,iter);
        
        p=0.95;
        s = -2 * log(1 - p);
        [V, D] = eig(PK(1:2,1:2) * s);
        t = linspace(0, 2 * pi);
        a = real((V * sqrt(D)) * [cos(t(:))'; sin(t(:))']);
        
        sigmabounds(:,error_data_cnt)=real(2*sqrt(diag(PK)));
        
        if errorplotter==1
            
            trans=[ppval(rtkinterp_x,current_time),ppval(rtkinterp_y,current_time),ppval(rtkinterp_z,current_time)];
            rotz=ppval(rtkinterp_theta,current_time);
            T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
            T_world_robot=T_world_RTK*inv(T_robot_RTK);
            GTx(error_data_cnt)=T_world_robot(1,4)-0.15;
            GTy(error_data_cnt)=T_world_robot(2,4);
            GTtheta(error_data_cnt)=rotz;
            error(:,error_data_cnt)=[GTx(error_data_cnt);GTy(error_data_cnt); GTtheta(error_data_cnt)]-pose(:,1);
        
%             GTx(error_data_cnt)=ppval(rtabinterp_x,current_time);
%             GTy(error_data_cnt)=ppval(rtabinterp_y,current_time);
%             GTtheta(error_data_cnt)=ppval(rtabinterp_theta,current_time);
%             error(:,error_data_cnt)=[GTx(error_data_cnt);GTy(error_data_cnt); GTtheta(error_data_cnt)]-pose(:,1);
%             
            
            set(eplotxhandle.error,'XData',datt,'YData',error(1,:),'color', [1,0,0]);
            set(eplotyhandle.error,'XData',datt,'YData',error(2,:),'color', [1,0,0]);
            set(eplotthetahandle.error,'XData',datt,'YData',error(3,:),'color', [1,0,0]);
            
        end
        
        if realtimeplotter==1 && covarianceplotter==1
            set(eplotxhandle.covup,'XData',datt,'YData',sigmabounds(1,:),'color', [0,0,1]);
            set(eplotxhandle.covdown,'XData',datt,'YData', -sigmabounds(1,:),'color', [0,0,1]);
            
            set(eplotyhandle.covup,'XData',datt,'YData', sigmabounds(2,:),'color', [0,0,1]);
            set(eplotyhandle.covdown,'XData',datt,'YData', -sigmabounds(2,:),'color', [0,0,1]);
            
            set(eplotthetahandle.covup,'XData',datt,'YData', sigmabounds(3,:),'color', [0,0,1]);
            set(eplotthetahandle.covdown,'XData',datt,'YData', -sigmabounds(3,:),'color', [0,0,1]);
            
        end
        
        
    end
    
end

%%
close all

[RMSE]=calculate_results(rtk_time,rtk_enux,rtk_enuy,EKF_time,X_k1k1,datt,sigmabounds,T_robot_RTK,start_time,end_time,location,LMmap)



