% Observation model for multiple laser endpoints
% Returns innovation, jacobian and innovation covariance

function[Xkp1, Pkp1] = observation_edge(Xkp1_k, Pkp1_k, laser,angles, dtObj)
global laserhan
global resg
global sizeYg
global xMing
global yMing
global sigma_edge;
global sigma_edgeP;
global edge_chi_sqr;
global realtimeplotter

R = (sigma_edge)^2;
chi_sqr_val=edge_chi_sqr; %100 for wentowrth
l=0;

ang = angles';
rng = laser';

xi = Xkp1_k(1) + laser.*cos(Xkp1_k(3)+angles);
yi = Xkp1_k(2) + laser.*sin(Xkp1_k(3)+angles);

yixi=[yi',xi'];

vx = -dtObj.queryDTx(yixi');
vy = -dtObj.queryDTy(yixi');

eucdist=sqrt(vx.^2+vy.^2);
euc_indx=find(eucdist>0.25);

vx(:,euc_indx)=[];
vy(:,euc_indx)=[];
yixi(euc_indx,:)=[];
ang(euc_indx,:)=[];
rng(euc_indx,:)=[];

cnt = size(yixi, 1);

%PLOT


if (cnt>0)
    
    V = zeros(2*cnt,1);
    jHx = zeros(2*cnt, 3);
    jHr = zeros(2*cnt, 1);
    
    dDTxdx = dtObj.querydDTxdx(yixi');
    dDTxdy = dtObj.querydDTxdy(yixi');
    dDTydx = dtObj.querydDTydx(yixi');
    dDTydy = dtObj.querydDTydy(yixi');
    
    for i=1:cnt
        V((2*i)-1) = vx(i);
        V(2*i) = vy(i);
        
        jHx((2*i)-1,:) = [dDTxdx(i), dDTxdy(i), -dDTxdx(i)*(l*sin(Xkp1_k(3)) + rng(i)*sin(Xkp1_k(3)+ang(i))) + dDTxdy(i)*(l*cos(Xkp1_k(3)) + rng(i)*cos(Xkp1_k(3)+ang(i)))];
        jHx(2*i,:) = [dDTydx(i), dDTydy(i), -dDTydx(i)*(l*sin(Xkp1_k(3)) + rng(i)*sin(Xkp1_k(3)+ang(i))) + dDTydy(i)*(l*cos(Xkp1_k(3)) + rng(i)*cos(Xkp1_k(3)+ang(i)))];
        
        jHr((2*i)-1) = [dDTxdx(i)*cos(Xkp1_k(3)+ang(i)) + dDTxdy(i)*sin(Xkp1_k(3)+ang(i))];
        jHr(2*i) = [dDTydx(i)*cos(Xkp1_k(3)+ang(i)) + dDTydy(i)*sin(Xkp1_k(3)+ang(i))];
    end
    S = (jHr * R * jHr') + (jHx * Pkp1_k * jHx');
    S = S + sigma_edgeP^2 * eye(size(S));
    
    iGate = V'*inv(S)*V;
    
    if abs(iGate) < chi_sqr_val 
        %disp("Intigrating edge observation")
        K = Pkp1_k * jHx'*inv(S);
        
      
        Xkp1 = Xkp1_k + K*V;
        Pkp1 = Pkp1_k - K*S*K';
        if realtimeplotter==1
            set(laserhan, 'color', [1,0,0], 'MarkerSize', 6);
            Xplot = (yixi(:,2)+xMing)/resg;
            Yplot =  sizeYg-(yixi(:,1)+yMing)/resg;
            set(laserhan, 'XData', Xplot, 'YData', Yplot);
            drawnow limitrate;
            %pause(0.001)
        end
    else
       %disp("Discarding edge observation")
        %K = -1*zeros(size(Pkp1_k * jHx' / S));
        Xkp1 = Xkp1_k;
        Pkp1 = Pkp1_k;
    end
    
else
    %disp("Discarding edge observation")
     Xkp1 = Xkp1_k;
     Pkp1 = Pkp1_k;
end


    
end





%
% % Observation model for multiple laser endpoints
% % Returns innovation, jacobian and innovation covariance
%
% function[V, jHx, S] = observation_edge(Xkp1_k, Pkp1_k, laser,angles, dtObj)
%
%     %global dtObj
%     R = (0.25)^2;
%     laser_min=0.5;
%     laser_max=20;
%     l=0;
%
%     yixi = [];
%     ang = [];
%     rng = [];
%
%     for i=1:length(laser)
%         r = laser(i);
%         theta = angles(i);
%
%         if r>= laser_min && r <= laser_max
%             xi = Xkp1_k(1) + r*cos(Xkp1_k(3)+theta);
%             yi = Xkp1_k(2) + r*sin(Xkp1_k(3)+theta);
%
%             if ~isnan(xi) && ~isnan(yi)
%                 yixi(end+1,:) = [yi, xi];
%                 ang(end+1,:) = theta;
%                 rng(end+1,:) = r;
%             end
%         end
%     end
%
%     %plot(yixi(:,2), yixi(:,1), '.k')
%
%     cnt = size(yixi, 1);
%
%     V = zeros(2*cnt,1);
%     jHx = zeros(2*cnt, 3);
%     jHr = zeros(2*cnt, 1);
%
%     vx = -dtObj.queryDTx(yixi');
%     vy = -dtObj.queryDTy(yixi');
%
%     dDTxdx = dtObj.querydDTxdx(yixi');
%     dDTxdy = dtObj.querydDTxdy(yixi');
%     dDTydx = dtObj.querydDTydx(yixi');
%     dDTydy = dtObj.querydDTydy(yixi');
%
%     for i=1:cnt
%         V((2*i)-1) = vx(i);
%         V(2*i) = vy(i);
%
%         jHx((2*i)-1,:) = [dDTxdx(i), dDTxdy(i), -dDTxdx(i)*(l*sin(Xkp1_k(3)) + rng(i)*sin(Xkp1_k(3)+ang(i))) + dDTxdy(i)*(l*cos(Xkp1_k(3)) + rng(i)*cos(Xkp1_k(3)+ang(i)))];
%         jHx(2*i,:) = [dDTydx(i), dDTydy(i), -dDTydx(i)*(l*sin(Xkp1_k(3)) + rng(i)*sin(Xkp1_k(3)+ang(i))) + dDTydy(i)*(l*cos(Xkp1_k(3)) + rng(i)*cos(Xkp1_k(3)+ang(i)))];
%
%         jHr((2*i)-1) = [dDTxdx(i)*cos(Xkp1_k(3)+ang(i)) + dDTxdy(i)*sin(Xkp1_k(3)+ang(i))];
%         jHr(2*i) = [dDTydx(i)*cos(Xkp1_k(3)+ang(i)) + dDTydy(i)*sin(Xkp1_k(3)+ang(i))];
%     end
%     S = (jHr * R * jHr') + (jHx * Pkp1_k * jHx');
%     S = S + 0.025^2 * eye(size(S));
% end

