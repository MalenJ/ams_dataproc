% Update by fusing the observation to the prediction
% Returns the state, covariance and Kalman gain (for varification)

function[Xkp1, Pkp1, K] = update(Xkp1_k, Pkp1_k, V, jH, S)
    
    chi_sqr_val=100;
       
    iGate = V'*inv(S)*V;
    
    if abs(iGate) < chi_sqr_val
        disp("Intigrating observation")
        K = Pkp1_k * jH' / S;
        Xkp1 = Xkp1_k + K*V;
        Pkp1 = Pkp1_k - K*S*K';
    else
        disp("Discarding observation")
        K = -1*zeros(size(Pkp1_k * jH' / S));
        Xkp1 = Xkp1_k;
        Pkp1 = Pkp1_k;
    end
end
