function [cov_rot] = rotate_cov(cov,ang)

R =[cos(ang) -sin(ang) 0; sin(ang) cos(ang) 0;0 0 1];

cov_rot=R*cov*R';


end

