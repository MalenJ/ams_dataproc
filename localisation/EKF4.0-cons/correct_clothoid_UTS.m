function [CL_points,CL_size] = correct_clothoid_UTS(mappoints)
% clear all
% close all
CONI=imread('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/2021-01-11-UTS/UTS-EDGE-8.5-conmap.png');
% 
xMin=30;
yMin=195;
sizeX=5000;
sizeY=5000;
res=0.04;

CONBIN=imbinarize(rgb2gray(CONI));
[cols,rows]=find(CONBIN==0);
px=[rows,cols];
mappoints= img2cart(px,xMin, yMin, sizeX, sizeY, res);

lines=[-5.53,-0.02;113.48,-2.81;154.30,-4.29;156.06,-10;152.66,-92.38;148.32,-186;146,-188.78;70,-188.57;57.56,-185.89;41.19,-185.01;37.95,-183.20;-12.29,-180.61;-13.30,-179.95;-6.20,-1.12;148.32,-188;-6.20,-0.25];


% sz=1
% figure
% hold on
% scatter(mappoints(:,1),mappoints(:,2),sz)
% axis equal
% h = images.roi.Polyline(gca,'Position',lines);
% h = drawpolyline('Color','green');

CL1 = LineSegment(lines(1,:), lines(2,:));
CL2 = LineSegment(lines(2,:), lines(3,:));
CL3 = LineSegment(lines(4,:), lines(5,:));
CL4= LineSegment(lines(5,:), lines(6,:));
CL45=LineSegment(lines(6,:), lines(15,:));
CL5= LineSegment(lines(7,:), lines(8,:));
CL6= LineSegment(lines(8,:), lines(9,:));
CL7= LineSegment(lines(9,:), lines(10,:));
CL8= LineSegment(lines(10,:), lines(11,:)); 
CL9= LineSegment(lines(11,:), lines(12,:));
CL10= LineSegment(lines(13,:), lines(14,:));
CL11= LineSegment(lines(14,:), lines(16,:));

CL1_points = point_creator(CL1,0,1,mappoints);
CL2_points = point_creator(CL2,0,1,mappoints);

CL3_points = point_creator(CL3,0,1,mappoints);
CL4_points = point_creator(CL4,0,1,mappoints);
CL45_points = point_creator(CL45,0,0,mappoints);

CL5_points = point_creator(CL5,0,1,mappoints);
CL6_points = point_creator(CL6,0,0,mappoints);
CL7_points = point_creator(CL7,0,1,mappoints);
CL8_points = point_creator(CL8,0,0,mappoints);
CL9_points = point_creator(CL9,0,1,mappoints);
CL10_points = point_creator(CL10,0,1,mappoints);
CL11_points = point_creator(CL11,0,0,mappoints);

CL_points=[CL1_points;CL2_points;CL3_points;CL4_points;CL45_points;CL5_points;CL6_points;CL8_points;CL7_points;CL9_points;CL10_points;CL11_points];


CL_size=size(CL_points,1);

% figure
% axis equal
% hold on
% szEKF=2;
% scatter(mappoints(:,1),mappoints(:,2),'g')
% scatter(CL_points(:,1),CL_points(:,2),szEKF,'k');
% scatter(CL_points(:,9),CL_points(:,10),szEKF,'b');
% scatter(CL_points(:,11),CL_points(:,12),szEKF,'r');
end