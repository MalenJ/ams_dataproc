function [CL_points,CL_size] = makeclothoidmap_glebe(mappoints)
clear all
close all
CONI=imread('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/2019-09-04-Village/village_conmap.png');
xMin=10;
yMin=40;
sizeX=2375;
sizeY=2000;
res=0.04;
CONBIN=imbinarize(rgb2gray(CONI));
[cols,rows]=find(CONBIN==0);
px=[rows,cols];
mappoints= img2cart(px,xMin, yMin, sizeX, sizeY, res);

lines=[0,-1.06;24.66,-0.79;28.21,-0.75;37.27,-0.65;38.67,1.93;39.05,32.72;40.82,33.47;76.5,34.35;77.59,33.54;66.73,2.17;...
    63.92,-0.38;65.16,-2.43;54.81,-32.11;52.63,-32.89;27.22,-32.32;25.82,-31.29;26.30,-2.73;24.09,-32.23;-0.31,-31.45;-1.07,-30.35;...
    -0.67,-3.01;-2.50,-1.94;-2.38,0.88;-2.38,32.26;-1.59,37.29;36,37.41];

% sz=1
% figure
% hold on
% scatter(mappoints(:,1),mappoints(:,2),sz)
% axis equal
% h = images.roi.Polyline(gca,'Position',lines);
% h = drawpolyline('Color','green');

CL1 = LineSegment(lines(1,:), lines(2,:));
CL2 = LineSegment(lines(2,:), lines(3,:));
CL3 = LineSegment(lines(3,:), lines(4,:));
CL5= LineSegment(lines(5,:), lines(6,:));
CL7= LineSegment(lines(7,:), lines(8,:));
CL9= LineSegment(lines(9,:), lines(10,:));
CL10= LineSegment(lines(10,:), lines(12,:)); %Point 11 is into the path
CL11= LineSegment(lines(12,:), lines(13,:));
CL13= LineSegment(lines(14,:), lines(15,:));
CL15= LineSegment(lines(16,:), lines(17,:));
CL18= LineSegment(lines(18,:), lines(19,:));
CL20= LineSegment(lines(20,:), lines(21,:));
CL22= LineSegment(lines(22,:), lines(23,:));
CL25= LineSegment(lines(23,:), lines(24,:));
CL26= LineSegment(lines(24,:), lines(25,:));
CL27= LineSegment(lines(25,:), lines(26,:));

% CL4 = ClothoidCurve();
% CL4.build_G1(lines(4,1), lines(4,2),wrapTo2Pi(atan2(lines(4,2)-lines(3,2),lines(4,1)-lines(3,1))), lines(5,1), lines(5,2),wrapTo2Pi(atan2(lines(6,2)-lines(5,2),lines(6,1)-lines(5,1))));
% CL6 = ClothoidCurve();
% CL6.build_G1(lines(6,1), lines(6,2),wrapTo2Pi(atan2(lines(6,2)-lines(5,2),lines(6,1)-lines(5,1))), lines(7,1), lines(7,2),wrapTo2Pi(atan2(lines(8,2)-lines(7,2),lines(8,1)-lines(7,1))));
% CL8 = ClothoidCurve();
% CL8.build_G1(lines(8,1), lines(8,2),wrapTo2Pi(atan2(lines(8,2)-lines(7,2),lines(8,1)-lines(7,1))), lines(9,1), lines(9,2),wrapTo2Pi(atan2(lines(10,2)-lines(9,2),lines(10,1)-lines(9,1))));
% CL12 = ClothoidCurve();
% CL12.build_G1(lines(13,1), lines(13,2),wrapTo2Pi(atan2(lines(13,2)-lines(12,2),lines(13,1)-lines(12,1))), lines(14,1), lines(14,2),wrapTo2Pi(atan2(lines(15,2)-lines(14,2),lines(15,1)-lines(14,1))));
% CL14 = ClothoidCurve();
% CL14.build_G1(lines(15,1), lines(15,2),wrapTo2Pi(atan2(lines(15,2)-lines(14,2),lines(15,1)-lines(14,1))), lines(16,1), lines(16,2),wrapTo2Pi(atan2(lines(17,2)-lines(16,2),lines(17,1)-lines(16,1))));
% CL16 = ClothoidCurve(); % Double turn in trouble space
% CL16.build_G1(lines(17,1), lines(17,2),wrapTo2Pi(atan2(lines(17,2)-lines(16,2),lines(17,1)-lines(16,1))), lines(2,1), lines(2,2),wrapTo2Pi(atan2(lines(1,2)-lines(2,2),lines(1,1)-lines(2,1))));
% CL17 = ClothoidCurve();
% CL17.build_G1(lines(16,1), lines(16,2),wrapTo2Pi(atan2(lines(16,2)-lines(17,2),lines(16,1)-lines(17,1))), lines(18,1), lines(18,2),wrapTo2Pi(atan2(lines(19,2)-lines(18,2),lines(19,1)-lines(18,1))));
% CL19 = ClothoidCurve();
% CL19.build_G1(lines(19,1), lines(19,2),wrapTo2Pi(atan2(lines(19,2)-lines(18,2),lines(19,1)-lines(18,1))), lines(20,1), lines(20,2),wrapTo2Pi(atan2(lines(21,2)-lines(20,2),lines(21,1)-lines(20,1))));
% CL21 = ClothoidCurve();
% CL21.build_G1(lines(21,1), lines(21,2),wrapTo2Pi(atan2(lines(21,2)-lines(20,2),lines(21,1)-lines(20,1))), lines(22,1), lines(22,2),wrapTo2Pi(atan2(lines(23,2)-lines(22,2),lines(23,1)-lines(22,1))));
% CL23 = ClothoidCurve();
% CL23.build_G1(lines(21,1), lines(21,2),wrapTo2Pi(atan2(lines(21,2)-lines(20,2),lines(21,1)-lines(20,1))), lines(1,1), lines(1,2),wrapTo2Pi(atan2(lines(1,2)-lines(22,2),lines(1,1)-lines(22,1))));
% CL24 = ClothoidCurve();
% CL24.build_G1(lines(1,1), lines(1,2),wrapTo2Pi(atan2(lines(1,2)-lines(2,2),lines(1,1)-lines(2,1))), lines(23,1), lines(23,2),wrapTo2Pi(atan2(lines(24,2)-lines(23,2),lines(24,1)-lines(23,1))));
% CL28 = ClothoidCurve();
% CL28.build_G1(lines(26,1), lines(26,2),wrapTo2Pi(atan2(lines(26,2)-lines(25,2),lines(26,1)-lines(25,1))), lines(6,1), lines(6,2),wrapTo2Pi(atan2(lines(5,2)-lines(6,2),lines(5,1)-lines(6,1))));

CL1_points = point_creator(CL1,0,1,mappoints);
CL2_points = point_creator(CL2,0,1,mappoints);
CL3_points = point_creator(CL3,0,1,mappoints);
% CL4_points = point_creator(CL4,1,0,mappoints);
CL5_points = point_creator(CL5,0,1,mappoints);
% CL6_points = point_creator(CL6,1,0,mappoints);
CL7_points = point_creator(CL7,0,1,mappoints);
% CL8_points = point_creator(CL8,1,0,mappoints);
CL9_points = point_creator(CL9,0,1,mappoints);
CL10_points = point_creator(CL10,0,0,mappoints);
CL11_points = point_creator(CL11,0,1,mappoints);
% CL12_points = point_creator(CL12,1,0,mappoints);
CL13_points = point_creator(CL13,0,1,mappoints);
% CL14_points = point_creator(CL14,1,0,mappoints);
CL15_points = point_creator(CL15,0,1,mappoints);
% CL16_points = point_creator(CL16,1,0,mappoints);
% CL17_points = point_creator(CL17,1,0,mappoints);
CL18_points = point_creator(CL18,0,1,mappoints);
% CL19_points = point_creator(CL19,1,0,mappoints);
CL20_points = point_creator(CL20,0,1,mappoints);
% CL21_points = point_creator(CL21,1,0,mappoints);
CL22_points = point_creator(CL22,0,0,mappoints);
% CL23_points = point_creator(CL23,1,0,mappoints);
% CL24_points = point_creator(CL24,1,0,mappoints);
CL25_points = point_creator(CL25,0,1,mappoints);
CL26_points = point_creator(CL26,0,0,mappoints);
CL27_points = point_creator(CL27,0,0,mappoints);
% CL28_points = point_creator(CL28,1,0,mappoints);

CL_points=[CL1_points;CL2_points;CL3_points;CL5_points;CL7_points;CL9_points;CL10_points;CL11_points; ...
    CL13_points;CL15_points;CL18_points;CL20_points;CL22_points;...
    CL25_points;CL26_points;CL27_points];


CL_size=size(CL_points,1)

figure
axis equal
hold on
szEKF=2;
scatter(mappoints(:,1),mappoints(:,2),'g');
scatter(CL_points(:,1),CL_points(:,2),szEKF,'k');
scatter(CL_points(:,9),CL_points(:,10),szEKF,'b');
scatter(CL_points(:,11),CL_points(:,12),szEKF,'r');
end