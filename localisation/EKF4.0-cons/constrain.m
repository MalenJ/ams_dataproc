function [X_k1k1_con,P_k1k1_con,sf_frame] = constrain(X_k1k1,P_k1k1,CL_points,CL_size,sf_framein,conmap_pts)

global collect_innov
global featurehan
global resg
global sizeYg
global xMing
global yMing

%CL_points(11,1:2)=[77.59,33.54]

[X_test,ang]=gl2sf(sf_framein,X_k1k1);
[cl_test,ang]=gl2sf(sf_framein,[CL_points(:,1:2)';zeros(1,CL_size)]);
CL_test=round(cl_test,4);
% I=find(CL_test(2,:)>-0.1 & CL_test(2,:)<0.1);
% CL_dists=abs(CL_test(1,I(I1))'-ones(length(CL_test(1,I(I1))),1).*X_test(1,1))
% % CL_dists=distmat(:,1).^2+distmat(:,2).^2;
% [M,I2]=min(CL_dists);
% I(I1)
% sf_frame=CL_points(I(I1),:)
distmat=CL_test(1:2,:)'-ones(CL_size,2).*transpose(X_test(1:2,:));
%CL_dists=distmat(:,1).^2+distmat(:,2).^2;
CL_dists=abs(distmat(:,1))+abs(distmat(:,2)); %
[M,I]=min(CL_dists);
%angs=wrapTo2Pi(atan2(CL_points(I,2),CL_points(I,1)))-wrapTo2Pi(ones(4,1).*X_k1k1(3,1)) %wrapTo2Pi(ones(4,1).*X_k1k1(3,1))
%[M2,I2]=min(angs);
%sf_frame_k=CL_points(I-1,:);

sf_frame=CL_points(I,:);

Xplot = (sf_frame(:,1)+xMing)/resg;
Yplot =  sizeYg-(sf_frame(:,2)+yMing)/resg;
set(featurehan, 'XData', Xplot, 'YData', Yplot);
drawnow limitrate;

[X_SF,gl2sfang]=gl2sf(sf_frame,X_k1k1);
P_k1k1_SF=rotate_cov(P_k1k1,-gl2sfang);

if sf_frame(1,13)>0 %sf_frame(1,7)>0 && sf_frame(1,8) <0
    %finding constraints
    sfpoints=[conmap_pts, zeros(length(conmap_pts),1)];
    [finpoints,ang]=gl2sf(sf_frame,sfpoints');
    Iupv=find(finpoints(1,:)<X_SF(1,1)+0.1 & finpoints(1,:)>X_SF(1,1)-0.1 & finpoints(2,:)>0);
    Ilwv=find(finpoints(1,:)<X_SF(1,1)+0.1 & finpoints(1,:)>X_SF(1,1)-0.1 & finpoints(2,:)<0);
    
    upb=min(finpoints(2,Iupv));
    lwb=max(finpoints(2,Ilwv));
    
    
    %Constrain
    A = [];
    b = [];
    Aeq = [];
    beq = [];
    %     lb = [-inf,sf_frame(1,8),-inf];
    %     ub = [inf,sf_frame(1,7),inf];
    ub = [inf,upb-0.25,inf];
    lb = [-inf,lwb+0.25,-inf];

    x0=[0,0,0];
    
    fun=@(X)(X-X_SF')*eye(3)*(X-X_SF')'; %eye(3)
    nonlcon = [];
    options = optimoptions('fmincon','Display','off');
    
    X_con = fmincon(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options);
    
    [Z_con,sf2glang]=sf2gl(sf_frame,[X_con';X_SF(3,1)]);
    
    P_k1k1_GL=rotate_cov(P_k1k1_SF,sf2glang);

    
    innov_test=round(Z_con,3)-round(X_k1k1,3);
    innov=round(Z_con,4)-round(X_k1k1,4);
    
    chi_test=innov'*inv(P_k1k1)*innov;
    
    collect_innov=[collect_innov;chi_test];
    
    if innov_test==0
        
        X_k1k1_con=X_k1k1;
        P_k1k1_con=P_k1k1;
        
    else
        
        %disp('phantom')
        
        if chi_test>100000 %6 AND 0.06
            X_k1k1_con=X_k1k1;
            P_k1k1_con=P_k1k1;
            disp('ew')
            
        else
            X_k1k1_con=Z_con;
            P_k1k1_con=P_k1k1;
            disp('phantom')
        end
        %Z_con=sf2gl(sf_frame,[X_SF(1,1);0;X_SF(3,1)]); %Y=0
    end
    
else
        X_k1k1_con=X_k1k1;
        P_k1k1_con=P_k1k1;
end



end

