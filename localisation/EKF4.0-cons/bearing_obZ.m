classdef bearing_obZ
    
    properties
        img_time
        centroidx
        labels
        bearings
        sensortype
        associations
        predictedR
        predictedB
        innovations
        yoloindx
        a
        b
        pose
    end
    
    
    methods
        
        function obj = bearing_obZ(centerX,img_time,cameraobs,left_camT,right_camT,ANGZ)
            
            obj.img_time=img_time;
            obj.yoloindx=cameraobs{:,1};
            frame=cameraobs.camera{1,1};
            obj.centroidx=cameraobs{:,3};
            obj.labels=cameraobs{:,4};
            
            obj.associations=zeros(length(cameraobs{:,1}),1);
            obj.predictedR=zeros(length(cameraobs{:,1}),1);
            obj.predictedB=zeros(length(cameraobs{:,1}),1);
            obj.innovations=1000*ones(length(cameraobs{:,1}),1);
            obj.pose=zeros(3,1);
            
            %bearings_vec=ones(length(cameraobs{:,1}),1);
            %cameraobs{:,2};
            % REALSENSES       
            %for i=1:length(cameraobs{:,1})
            
            centroid= obj.centroidx;
            
            if (frame=='LC')
                
                h_fov=deg2rad(69.4);
                h_width=640;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-320)*rad_per_pix; %centerx(5)
                
                %bear=0.7394-rawbearings;%ICRA VALUES
                %bear=0.8244-rawbearings; %IROS VALUES 
                bear=ANGZ(1)-rawbearings; %IROS VALUES 
                gradang=wrapTo2Pi(bear);
                
                obj.a=left_camT(1);
                obj.b=left_camT(2);

                obj.sensortype='LC';
                
            elseif (frame=='RC')
                
                h_fov=deg2rad(69.4);
                h_width=640;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-320)*rad_per_pix; %centerx(6)
                
                %bear=0.838+rawbearings; %ICRA VALUES
                %bear=0.8117+rawbearings; %IROS VALUES
                bear=ANGZ(2)+rawbearings;
                gradang=wrapTo2Pi(-bear);
                
                obj.a=right_camT(1);
                obj.b=right_camT(2);

                obj.sensortype='RC';
                
            %RICOH FORWARD
            elseif (frame=='LF')
                
                h_fov=deg2rad(60);
                h_width=480;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX(1))*rad_per_pix;
                
                bear=1.0472-rawbearings; %60 deg
                gradang=wrapTo2Pi(bear);
                
                obj.a=-0.0149;
                obj.b=0.0144;
                obj.sensortype='LF';
                
            %RICOH BACK
            elseif (frame=='LB')
                
                h_fov=deg2rad(60);
                h_width=480;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX(3))*rad_per_pix;
                
                bear=2.0944-rawbearings;  %120 DEG
                gradang=wrapTo2Pi(bear);
                obj.a=0.0679;
                obj.b=0.0522;
                obj.sensortype='LB';
                
            elseif (frame=='RF')
                
                h_fov=deg2rad(60);
                h_width=480;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX(2))*rad_per_pix;
                
                bear=1.0472+rawbearings;   %60 deg
                gradang=wrapTo2Pi(-bear);
                
                obj.a=0.0061;
                obj.b=0.0030;
                obj.sensortype='RF';
                
            elseif (frame=='RB')
                
                h_fov=deg2rad(60);
                h_width=480;
                rad_per_pix=h_fov/h_width;
                rawbearings=(centroid-centerX(4))*rad_per_pix;
                
                bear=2.0944+rawbearings; %120 DEG
                gradang=wrapTo2Pi(-bear);
                obj.a=0.0473;
                obj.b=-0.0701;
                obj.sensortype='RB';
                
            end
            
            %bearings_vec(i)=gradang;
            %end
            obj.bearings=gradang;
            
        end
        
    end
      
end




% classdef stat_bearing_obZ
%
%     properties
%         img_time
%         centroidx
%         labels
%         bearings
%         sensortype
%         associations
%         innovations
%     end
%
%
%     methods
%
%         function obj = stat_bearing_obZ(centerx,img_time,frame,centroid,labels)
%
%             obj.img_time=img_time;
%             obj.centroidx=centroid;
%             obj.labels=labels;
%
% %             PP=cameraParams.PrincipalPoint;
% %             centerX=PP(1);
%
%             %             centroids(:,1)=(dimensions(:,1)+dimensions(:,3))/2;
%             %             centroids(:,2)=(dimensions(:,2)+dimensions(:,4))/2;
%             %             rawbearings=(centroids(:,1)-centerX)*rad_per_pix;
%
%             %Dimension is centroid x here
%
%             % REALSENSES
%
%                 if (frame=='LC')
%
%                     h_fov=deg2rad(69.4);
%                     h_width=640;
%                     rad_per_pix=h_fov/h_width;
%                     rawbearings=(centroid-centerx)*rad_per_pix;
%
%                     bear=0.7394-rawbearings;
%                     gradang=wrapTo2Pi(bear);
%                     obj.sensortype='LC';
%
%
%                 elseif (frame=='RC')
%
%                     h_fov=deg2rad(69.4);
%                     h_width=640;
%                     rad_per_pix=h_fov/h_width;
%                     rawbearings=(centroid-centerx)*rad_per_pix;
%
%                     bear=0.838+rawbearings;
%                     gradang=wrapTo2Pi(-bear);
%                     obj.sensortype='RC';
%
%
%                     %RICOH FORWARD
%                 elseif (frame=='LF')
%
%                     h_fov=deg2rad(60);
%                     h_width=480;
%                     rad_per_pix=h_fov/h_width;
%                     rawbearings=(centroid-centerx)*rad_per_pix;
%
%                     bear=1.0472-rawbearings; %60 deg
%                     gradang=wrapTo2Pi(bear);
%                     obj.sensortype='LF';
%
%
%                 elseif (frame=='RF')
%
%                     h_fov=deg2rad(60);
%                     h_width=480;
%                     rad_per_pix=h_fov/h_width;
%                     rawbearings=(centroid-centerx)*rad_per_pix;
%
%                     bear=1.0472+rawbearings;   %60 deg
%                     gradang=wrapTo2Pi(-bear);
%                     obj.sensortype='RF';
%
%                     %RICOH BACK
%                 elseif (frame=='LB')
%
%                     h_fov=deg2rad(60);
%                     h_width=480;
%                     rad_per_pix=h_fov/h_width;
%                     rawbearings=(centroid-centerx)*rad_per_pix;
%
%                     bear=2.0944-rawbearings;  %120 DEG
%                     gradang=wrapTo2Pi(bear);
%                     obj.sensortype='LB';
%
%                 elseif (frame=='RB')
%
%                     h_fov=deg2rad(60);
%                     h_width=480;
%                     rad_per_pix=h_fov/h_width;
%                     rawbearings=(centroid-centerx)*rad_per_pix;
%
%                     bear=2.0944+rawbearings; %120 DEG
%                     gradang=wrapTo2Pi(-bear);
%                     obj.sensortype='RB';
%
%                 end
%
%             obj.bearings=gradang;
%
%         end
%
%     end
%
%
% end
