close all
clear all

fileroot='2021-01-11-UTS';
filename='2021-01-11-UTS-P1';
data_zero=1610354651.436696707;

CSVPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
IMGPATH=strcat('/media/maleen/malen_ssd/phd/critical_ams_data/',fileroot,'/image_data/',filename,'/'); %IMAGE DATA

MAPPATH_feature='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/'; %MAP DATA
MAPPATH_edge='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/';


rtab_data=readtable(strcat(CSVPATH,filename, '-rtabdata.csv'));

%vins_data=readtable(strcat(CSVPATH, filename, '-vinsdata.csv'));
VINSVIO_data=readtable(strcat(CSVPATH, filename, '-VINS-vio.csv'));

orb_data=readtable(strcat(CSVPATH, filename, '-ORB.csv')); % -orbshortdata -ORBlocal-short
orblocal_data=readtable(strcat(CSVPATH, filename, '-ORB-LOCAL.csv'));

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/ICRA2021calib/'; %CLIB DATA

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');

roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;

I=imread(strcat(MAPPATH_edge,'2021-01-11-UTS/','UTS-EDGE-7.png'));

% CONI=I;
% % 
% xMin=30;
% yMin=195;
% sizeX=5000;
% sizeY=5000;
% res=0.04;
% 
% CONBIN=imbinarize(rgb2gray(CONI));
% 
% [cols,rows]=find(CONBIN==0);
% px=[rows,cols];
% conmap_pts= img2cart(px,xMin, yMin, sizeX, sizeY, res);

%% RTAB


rtab_time=rtab_data{:,3};
rtabx=rtab_data{:,4};
rtaby=rtab_data{:,5};
rtabquat=rtab_data{:,6:9};
rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);
rtabinterp_x= interp1(rtab_time,rtabx,'linear','pp');%RTAB_world(1,:));
rtabinterp_y = interp1(rtab_time,rtaby,'linear','pp');%RTAB_world(2,:));
rtabinterp_z = interp1(rtab_time,zeros(length(rtabx),1),'linear','pp');%RTAB_world(3,:));
rtabinterp_theta = interp1(rtab_time,rtabeul(:,1),'linear','pp');


%% ORB DATA

orb_time=orb_data{:,3};
orb_x=orb_data{:,4};
orb_y=orb_data{:,5};
orb_z=orb_data{:,6};
orb_quat=orb_data{:,7:10};
orb_eul = quat2eul([orb_quat(:,4) orb_quat(:,1:3)]);



for i=1:length(orb_time)
    
    trans=[orb_x(i);orb_y(i);orb_z(i);1];
    T_world_robot=(T_robot_backcam)*trans;
    orb_xt(i,1)=T_world_robot(1,1);
    orb_yt(i,1)=T_world_robot(2,1);
    orb_zt(i,1)=T_world_robot(3,1);
end
%scatter(orb_xt,orb_yt)

%Orb Local
orblocal_time=orblocal_data{:,3};
orblocal_x=orblocal_data{:,4};
orblocal_y=orblocal_data{:,5};
orblocal_z=orblocal_data{:,6};
orblocal_quat=orblocal_data{:,7:10};
orblocal_eul = quat2eul([orblocal_quat(:,4) orblocal_quat(:,1:3)]);

for i=1:length(orblocal_time)
    
    trans=[orblocal_x(i);orblocal_y(i);orblocal_z(i);1];
    T_world_robot=(T_robot_backcam)*trans;
    orblocal_xt(i,1)=T_world_robot(1,1);
    orblocal_yt(i,1)=T_world_robot(2,1);
    orblocal_zt(i,1)=T_world_robot(3,1);
end

orblocalx_interp=spline(orblocal_time,orblocal_xt);
orblocaly_interp=spline(orblocal_time,orblocal_yt);


%% VINS DATA


vins_time=VINSVIO_data{:,1}/1000000000-data_zero;
vins_x=VINSVIO_data{:,2};
vins_y=VINSVIO_data{:,3};
vins_z=VINSVIO_data{:,4};

vinsx_interp=spline(vins_time,vins_x);
vinsy_interp=spline(vins_time,vins_y);

% figure
% scatter(vinsxt,vinsyt)


%% LOAD DATA

load('2021-01-11-UTS-P1-ODOM.mat')


RTAB_POS=[ppval(rtabinterp_x ,100) ppval(rtabinterp_y ,100) 0];
VINS_POS=[ppval(vinsx_interp ,100) ppval(vinsy_interp ,100) 0];
t=atan2(norm(cross(RTAB_POS,VINS_POS)), dot(RTAB_POS,VINS_POS))-deg2rad(7);
rad2deg(t)
Rzvins=[cos(t) -sin(t) ppval(rtabinterp_x ,15)-4; sin(t) cos(t) (ppval(rtabinterp_y ,15)+1); 0 0 1];
vins_pose=Rzvins*[vins_x'; vins_y';ones(1,length(vins_x))];
vins_xt=vins_pose(1,:);
vins_yt=vins_pose(2,:);


RTAB_POS=[ppval(rtabinterp_x ,100) ppval(rtabinterp_y ,100) 0];
ORB_POS=[ppval(orblocalx_interp ,100) ppval(orblocaly_interp ,100) 0];
t=atan2(norm(cross(RTAB_POS,VINS_POS)), dot(RTAB_POS,VINS_POS))-deg2rad(6);
rad2deg(t)
Rzorb=[cos(t) -sin(t) ppval(rtabinterp_x ,15)-2; sin(t) cos(t) (ppval(rtabinterp_y ,15)); 0 0 1];

orb_pose=Rzorb*[orb_xt'; orb_yt';ones(1,length(orb_xt))];
orb_xt=orb_pose(1,:);
orb_yt=orb_pose(2,:);

orb_pose_local=Rzorb*[orblocal_xt'; orblocal_yt';ones(1,length(orblocal_xt))];
orblocal_xt=orb_pose_local(1,:);
orblocal_yt=orb_pose_local(2,:);


% %corrections
%vins_yt=vins_yt;
% orb_yt=orb_yt;
% orblong_xt=orblong_xt;

orbx_interp=interp1(orb_time,orb_xt,'linear','pp');
orby_interp=interp1(orb_time,orb_yt,'linear','pp');
vinsx_interp=interp1(vins_time,vins_xt,'linear','pp');
vinsy_interp=interp1(vins_time,vins_yt,'linear','pp');

orblocalx_interp=interp1(orblocal_time,orblocal_xt,'linear','pp');
orblocaly_interp=interp1(orblocal_time,orblocal_yt,'linear','pp');


si=find(rtab_time<start_time, 1, 'last' ); %101
ei=find(rtab_time<end_time, 1, 'last' ); %576

for i=si:ei
    time=rtab_time(i);
    
    error_time(i)=time;
    
    GTxr(i)=rtabx(i);
    GTyr(i)=rtaby(i);
    
    orbx_e(i)=GTxr(i)-ppval(orbx_interp,time);
    orby_e(i)=GTyr(i)-ppval(orby_interp,time);
    
    vinsx_e(i)=GTxr(i)-ppval(vinsx_interp,time);
    vinsy_e(i)=GTyr(i)-ppval(vinsy_interp,time);
    
    orblocalx_e(i)=GTx(i)-ppval(orblocalx_interp,time);
    orblocaly_e(i)=GTy(i)-ppval(orblocaly_interp,time);
    
end


ORB_XMSE=(sum(orbx_e.^2)/numel(orbx_e));
ORB_YMSE=(sum(orby_e.^2)/numel(orby_e));

VINS_XMSE=(sum(vinsx_e.^2)/numel(vinsx_e));
VINS_YMSE=(sum(vinsy_e.^2)/numel(vinsy_e));

ORBLOCAL_XMSE=(sum(orblocalx_e.^2)/numel(orblocalx_e));
ORBLOCAL_YMSE=(sum(orblocaly_e.^2)/numel(orblocaly_e));


ORB_RMSE=[sqrt(ORB_XMSE),sqrt(ORB_YMSE)];
VINS_RMSE=[sqrt(VINS_XMSE),sqrt(VINS_YMSE)];
ORBLOCAL_RMSE=[sqrt(ORBLOCAL_XMSE),sqrt(ORBLOCAL_YMSE)];



%%

orb_start=find(orb_time<start_time, 1, 'last' );
orb_end=find(orb_time<end_time, 1, 'last' );

% vins_start=find(vins_time<start_time, 1, 'last' );
% vins_end=find(vins_time<end_time, 1, 'last' );

orblocal_start=find(orblocal_time<start_time, 1, 'last' );
orblocal_end=find(orblocal_time<end_time, 1, 'last' );



figure
axis equal
hold on
szGT=4;
szEKF=8;

scatter(orb_xt(orb_start:orb_end),orb_yt(orb_start:orb_end),szEKF,'g', 'filled')

scatter(vins_xt,vins_yt,szEKF,'y','filled')

scatter(orblocal_xt(orblocal_start:orblocal_end),orblocal_yt(orblocal_start:orblocal_end),szEKF,'c','filled')
% sz=1;
% scatter(conmap_pts(:,1),conmap_pts(:,2),sz, 'k')
% load('2021-01-11-UTS-P1-ODOM.mat')
% 
% scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'m','filled');
% 
% load('2021-01-11-UTS-P1-ODOM+CON.mat')
% scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'y','filled');
% 
% load('2021-01-11-UTS-P1-LM+CON.mat')
% scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'c','filled');

% load('2021-01-11-UTS-P1-LM+CON.mat')
% scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'c','filled');

% load('2021-01-11-UTS-P1-VINS6-2.mat')
% scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b','filled');
% 
% load('2021-01-11-UTS-P1-RICOH1-WHICHCAM.mat')
% scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'c','filled');

load('2021-01-11-UTS-P1-FULL.mat')
scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b','filled');

%scatter(rtk_x,rtk_y,szGT,'r','filled')
scatter(GTxr,GTyr,szGT,'r','filled')

for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3),8,'x','k')
    %text(LMmap(i,2),LMmap(i,3), num2str(i));
end

xlabel('X (m)')
ylabel('Y (m)')
%Wentworth -25 to +45 (Y), -55 TO 40

hold off
%%

% figure
% hold on
% scatter(x_e)
% scatter(rtabx_e)
% scatter(orbx_e)
% scatter(vinsx_e)

%% NEW rmse
% 
% ORB_RMSE=sqrt(ORB_RMSE(1,1)^2+ORB_RMSE(1,2)^2)

% VINS_RMSE=sqrt(VINS_RMSE(1,1)^2+VINS_RMSE(1,2)^2)
% 
EKF_RMSE=sqrt(RMSE(1,1)^2+RMSE(1,2)^2)
% 
% ORBLOCAL_RMSE=sqrt(ORBLOCAL_RMSE(1,1)^2+ORBLOCAL_RMSE(1,2)^2)

%%
% xMin=10;
% yMin=40;
% sizeX=2375;
% sizeY=2000;
% res=0.04;
% figure
%
%
% imshow(I)
% hold on
% scatter((rtk_x+xMin)./res,sizeY-(rtk_y+yMin)./res,szEKF,'r')


sz=0.5;
szb=0.75;
figure
subplot(2,1,1)
set(gca,'XLim',[0 end_time-start_time]);
hold on
scatter(error_time,x_e,sz,'r');
plot(datt-start_time, sigmabounds(1,:)+0.1,'b');
plot(datt-start_time, -sigmabounds(1,:)-0.1,'b');
xlabel('time (s)')
xticks(0:50:end_time)
ylabel('X error (m)')
hold off
subplot(2,1,2)
set(gca,'XLim',[0 end_time-start_time]);
hold on
scatter(error_time,y_e,sz,'r');
plot(datt-start_time, sigmabounds(2,:)+0.1,'b');
plot(datt-start_time, -sigmabounds(2,:)-0.1,'b');
hold off
xlabel('time (s)')
xticks(0:50:end_time)
ylabel('Y error(m)')

