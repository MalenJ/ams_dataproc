% clear all
close all
% 
CONI=imread('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/2021-01-11-UTS/UTS-EDGE-8.5-conmap.png');
% 
xMin=30;
yMin=195;
sizeX=5000;
sizeY=5000;
res=0.04;

rtab_data=readtable('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/2021-01-11-UTS/csv_data/2021-01-11-UTS-P1-rtabdata.csv');

rtab_time=rtab_data{:,3};
rtabx=rtab_data{:,4};
rtaby=rtab_data{:,5};

lines=[-5.53,-0.02;113.48,-2.81;154.30,-4.29;156.06,-10;152.66,-92.38;148.32,-186;146,-188.78;70,-188.57;57.56,-185.89;41.19,-185.01;37.95,-183.20;-12.29,-180.61;-13.30,-179.95;-6.20,-1.12;148.32,-188;-6.20,-0.25];

CONBIN=imbinarize(rgb2gray(CONI));

[cols,rows]=find(CONBIN==0);
px=[rows,cols];
conmap_pts= img2cart(px,xMin, yMin, sizeX, sizeY, res);

sz=1;
figure
hold on
scatter(conmap_pts(:,1),conmap_pts(:,2),sz, 'k')
scatter(rtabx,rtaby,sz,'r')
scatter(rtabx,rtaby+0.25,sz,'b')
scatter(rtabx,rtaby-0.25,sz,'b')
axis equal
h = images.roi.Polyline(gca,'Position',lines, 'Color', 'green');
h = drawpolyline('Color','green');


