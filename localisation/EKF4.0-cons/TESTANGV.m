clear all
close all

% figure
% test=movmean(ang_vel,3);
% plot(EKF_time,rad2deg(test))

W=eye(3);

A=[-0.9997 0.0254 0;0.9997 -0.0254 0;0 0 1];

gamma=inv(W)*A'*inv(A*inv(W)*A)