function [X_k1k1_fin,P_k1k1_fin] = obs_phantang(X_k1k1,P_k1k1,theta_r)


X_k1k1_fin=X_k1k1;

P_k1k1_fin=P_k1k1;

predicted_obs=round(tan(X_k1k1(3,1)-theta_r),4);

obs=0;

innov=0-predicted_obs;

rad2deg(innov)

R=deg2rad(5);

S=R+((sec(X_k1k1(3,1)-theta_r))^2)*P_k1k1(3,3)*(sec(X_k1k1(3,1)-theta_r))^2;


K=P_k1k1(3,3)*(sec(X_k1k1(3,1)-theta_r))^2*inv(S);

X_k1k1_fin(3,1)=X_k1k1(3,1)+K*innov;

P_k1k1_fin(3,3)=P_k1k1(3,3)-K*S*K;


IG=innov*inv(S)*innov;

if IG >0.48
    
    
    X_k1k1_fin=X_k1k1;
    
    P_k1k1_fin=P_k1k1;
    
    disp('orientation constraints discarded')
    
else
    
     disp('accepted')
    
end


end

