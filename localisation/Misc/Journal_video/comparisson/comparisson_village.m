close all
clear all

fileroot='2020-02-19-Village';
filename='2020-02-19-Village-P1';

CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
MAPPATH_feature='/home/maleen/cloud_academic/ams_data/map_data/'; %MAP DATA
MAPPATH_edge='/home/maleen/cloud_academic/ams_data/map_data/edge_maps/';


rtab_data=readtable(strcat(CSVPATH, filename, '-rtabdata.txt'));
orb_data=readtable(strcat(CSVPATH, filename, '-orbdata.csv'));
%vins_data=readtable(strcat(CSVPATH, filename, '-vinsdata.csv'));
VINSVIO_data=readtable(strcat(CSVPATH, filename, '-VINS-vio_loop.csv'));

% rtablocal_data=readtable(strcat(CSVPATH, filename, '-rtablocaldata.csv'));
% orblocal_data=readtable(strcat(CSVPATH, filename, '-orblocaldata.csv'));

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/IROScalib/'; %CLIB DATA

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');

roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;

I=imread(strcat(MAPPATH_edge,'2019-09-04-Village/','village_map_funk1.png')); %funk1

%% RTAB

rtab_time=rtab_data{:,1}-1582057675.511864159;
rtab_x=rtab_data{:,2};
rtab_y=rtab_data{:,3};
rtab_theta=rtab_data{:,7};
% rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);


%% ORB DATA

orb_time=orb_data{:,3};
orb_x=orb_data{:,4};
orb_y=orb_data{:,5};
orb_z=orb_data{:,6};
orb_quat=orb_data{:,7:10};
orb_eul = quat2eul([orb_quat(:,4) orb_quat(:,1:3)]);

for i=1:length(orb_time)
    
    trans=[orb_x(i);orb_y(i);orb_z(i);1];
    T_world_robot=(T_robot_backcam)*trans;
    orb_xt(i,1)=T_world_robot(1,1);
    orb_yt(i,1)=T_world_robot(2,1);
    orb_zt(i,1)=T_world_robot(3,1);
end
%scatter(orb_xt,orb_yt)

%% VINS DATA

% vins_time=vins_data{:,3};
% vins_x=vins_data{:,4};
% vins_y=vins_data{:,5};
% vins_z=vins_data{:,6};
% vinsquat=vins_data{:,7:10};
% vins_eul = quat2eul([vinsquat(:,4) vinsquat(:,1:3)]);

vins_time=VINSVIO_data{:,1}/1000000000-1582057675.511864159;
vins_x=VINSVIO_data{:,2};
vins_y=VINSVIO_data{:,3};
vins_z=VINSVIO_data{:,4};
% vinsquat=VINSVIO_data{:,7:10};
% vinsbeul = quat2eul([vinsquat(:,4) vinsquat(:,1:3)]);
vins_eul=VINSVIO_data{:,6:8};

for i=1:length(vins_time)
    
    trans=[vins_x(i);vins_y(i);vins_z(i);1];
    T_world_robot=(T_robot_backcam)*trans;
    vins_xt(i,1)=T_world_robot(1,1);
    vins_yt(i,1)=T_world_robot(2,1);
    vins_zt(i,1)=T_world_robot(3,1);
end

% figure
% scatter(vinsxt,vinsyt)


%% LOAD DATA

rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));
rtk_time=rtk_data{:,2};

load('2020-02-19-Village-P1-VINS4-results-rf.mat')

% load('IROS_results.mat','X_k1k1')
% end_time=450;

start_time=54;

T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]); %IROS CALIB

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};
stationary_time=40;
rtk_iter=find(rtk_time<stationary_time, 1, 'last' ); %Wentworth

% base_lat=mean(rtk_lat(1:rtk_iter));
% base_long=mean(rtk_long(1:rtk_iter));
% base_alti=mean(rtk_alti(1:rtk_iter));

base_lat=rtk_lat(421);
base_long=rtk_long(421);
base_alti=rtk_alti(421);

[rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);

rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);
RTK_POS=[ppval(rtkinterp_x ,54) ppval(rtkinterp_y ,54) 0];
RTAB_POS=[0 1 0];
%
t=atan2(norm(cross(RTK_POS,RTAB_POS)), dot(RTK_POS,RTAB_POS))-deg2rad(3);

rad2deg(t);

Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0; sin(-t-pi/2) cos(-t-pi/2) 0; 0 0 1];
rtk_pose=Rz*[rtk_enux'; rtk_enuy';ones(1,length(rtk_enux))];
rtk_x=rtk_pose(1,:);
rtk_y=rtk_pose(2,:);

% rtk_enux=rtk_enux+0.8034;
% rtk_enuy=rtk_enuy-1.5209;
rtkx_interp=interp1(rtk_time,rtk_x,'linear','pp');
rtky_interp=interp1(rtk_time,rtk_y,'linear','pp');


% %corrections
vins_yt=vins_yt+0.4;
orb_yt=orb_yt+0.4;

rtabx_interp=interp1(rtab_time,rtab_x,'linear','pp');
rtaby_interp=interp1(rtab_time,rtab_y,'linear','pp');
orbx_interp=interp1(orb_time,orb_xt,'linear','pp');
orby_interp=interp1(orb_time,orb_yt,'linear','pp');
vinsx_interp=interp1(vins_time,vins_xt,'linear','pp');
vinsy_interp=interp1(vins_time,vins_yt,'linear','pp');

% rtablocalx_interp=interp1(rtablocal_time,rtablocal_x,'linear','pp');
% rtablocaly_interp=interp1(rtablocal_time,rtablocal_y,'linear','pp');
% orblocalx_interp=interp1(orblocal_time,orblocal_xt,'linear','pp');
% orblocaly_interp=interp1(orblocal_time,orblocal_yt,'linear','pp');


si=find(rtk_time<start_time, 1, 'last' ); %101
ei=find(rtk_time<end_time, 1, 'last' ); %576
for i=si:ei
    time=rtk_time(i);
    %trans=[ppval(rtkx_interp,time),ppval(rtky_interp,time),0];
    trans=[gtenux(i),gtenuy(i),0];
    rotz=ppval(theta_interp,time);
    T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
    T_world_robot=T_world_RTK*inv(T_robot_RTK);
    GTxr(i)=T_world_robot(1,4);
    GTyr(i)=T_world_robot(2,4);
    GTthetar(i)=rotz;
    
    
    rtabx_e(i)=GTxr(i)-ppval(rtabx_interp,time);
    rtaby_e(i)=GTyr(i)-ppval(rtaby_interp,time);
    orbx_e(i)=GTxr(i)-ppval(orbx_interp,time);
    orby_e(i)=GTyr(i)-ppval(orby_interp,time);
    vinsx_e(i)=GTxr(i)-ppval(vinsx_interp,time);
    vinsy_e(i)=GTyr(i)-ppval(vinsy_interp,time);
    
%     rtablocalx_e(i)=GTx(i)-ppval(rtablocalx_interp,time);
%     rtablocaly_e(i)=GTy(i)-ppval(rtablocaly_interp,time);
%     orblocalx_e(i)=GTx(i)-ppval(orblocalx_interp,time);
%     orblocaly_e(i)=GTy(i)-ppval(orblocaly_interp,time);

end

RTAB_XMSE=(sum(rtabx_e.^2)/numel(rtabx_e));
RTAB_YMSE=(sum(rtaby_e.^2)/numel(rtaby_e));
ORB_XMSE=(sum(orbx_e.^2)/numel(orbx_e));
ORB_YMSE=(sum(orby_e.^2)/numel(orby_e));
VINS_XMSE=(sum(vinsx_e.^2)/numel(vinsx_e));
VINS_YMSE=(sum(vinsy_e.^2)/numel(vinsy_e));


% RTABLOCAL_XMSE=(sum(rtablocalx_e.^2)/numel(rtablocalx_e));
% RTABLOCAL_YMSE=(sum(rtablocaly_e.^2)/numel(rtablocaly_e));
% ORBLOCAL_XMSE=(sum(orblocalx_e.^2)/numel(orblocalx_e));
% ORBLOCAL_YMSE=(sum(orblocaly_e.^2)/numel(orblocaly_e));

RTAB_RMSE=[sqrt(RTAB_XMSE),sqrt(RTAB_YMSE)];
ORB_RMSE=[sqrt(ORB_XMSE),sqrt(ORB_YMSE)];
VINS_RMSE=[sqrt(VINS_XMSE),sqrt(VINS_YMSE)];
% RTABLOCAL_RMSE=[sqrt(RTABLOCAL_XMSE),sqrt(RTABLOCAL_YMSE)];
% ORBLOCAL_RMSE=[sqrt(ORBLOCAL_XMSE),sqrt(ORBLOCAL_YMSE)];

%%

orb_start=find(orb_time<start_time, 1, 'last' ); 
orb_end=find(orb_time<end_time, 1, 'last' );
rtab_start=find(rtab_time<start_time, 1, 'last' ); 
rtab_end=find(rtab_time<end_time, 1, 'last' );
vins_start=find(vins_time<start_time, 1, 'last' ); 
vins_end=find(vins_time<end_time, 1, 'last' );


% orblocal_start=find(orblocal_time<start_time, 1, 'last' ); 
% orblocal_end=find(orblocal_time<end_time, 1, 'last' );
% rtablocal_start=find(rtablocal_time<start_time, 1, 'last' ); 
% rtablocal_end=find(rtablocal_time<end_time, 1, 'last' );


figure
axis equal
hold on
szGT=4;
szEKF=8;

% scatter(rtab_x(rtab_start:rtab_end),rtab_y(rtab_start:rtab_end),szEKF+2,'m', 'filled')
% 
% scatter(orb_xt(orb_start:orb_end),orb_yt(orb_start:orb_end),szEKF,'g', 'filled')
% 
% scatter(vins_xt(vins_start:vins_end),vins_yt(vins_start:vins_end),szEKF,'y','filled')

% scatter(rtablocal_x(rtablocal_start:rtablocal_end),rtablocal_y(rtablocal_start:rtablocal_end),szEKF,'m','filled')
% scatter(orblocal_xt(orblocal_start:orblocal_end),orblocal_yt(orblocal_start:orblocal_end),szEKF,'g','filled')

scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b','filled');

%scatter(rtk_x,rtk_y,szGT,'r','filled')
scatter(GTxr,GTyr,szGT,'r','filled')

for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3),8,'x','k')
    %text(LMmap(i,2),LMmap(i,3), num2str(i));
end

xlabel('X (m)')
ylabel('Y (m)')
%Wentworth -25 to +45 (Y), -55 TO 40

hold off
%%

% figure
% hold on
% scatter(x_e)
% scatter(rtabx_e)
% scatter(orbx_e)
% scatter(vinsx_e)

%% NEW rmse

RTAB_RMSE=sqrt(RTAB_RMSE(1,1)^2+RTAB_RMSE(1,2)^2)
ORB_RMSE=sqrt(ORB_RMSE(1,1)^2+ORB_RMSE(1,2)^2)
VINS_RMSE=sqrt(VINS_RMSE(1,1)^2+VINS_RMSE(1,2)^2)

EKF_RMSE=sqrt(RMSE(1,1)^2+RMSE(1,2)^2)

% RTABLOCAL_RMSE=sqrt(RTABLOCAL_RMSE(1,1)^2+RTABLOCAL_RMSE(1,2)^2)
% ORBLOCAL_RMSE=sqrt(ORBLOCAL_RMSE(1,1)^2+ORBLOCAL_RMSE(1,2)^2)

%%
% xMin=10;
% yMin=40;
% sizeX=2375;
% sizeY=2000;
% res=0.04;
% figure
% 
% 
% imshow(I)
% hold on
% scatter((rtk_x+xMin)./res,sizeY-(rtk_y+yMin)./res,szEKF,'r')

