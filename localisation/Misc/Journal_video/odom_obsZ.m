classdef odom_obsZ
    %ODOM_OBSZ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        sensortype
        linear_vel
        angular_vel
    end
    
    methods
        function obj = odom_obsZ(U_k)
            obj.sensortype='OD';
            obj.linear_vel=U_k(1,1);
            obj.angular_vel=U_k(2,1);

        end
        

    end
end

