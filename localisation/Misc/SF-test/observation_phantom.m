function [X_k1k1,P_k1k1] = observation_phantom(X_k1k,P_k1k,sf_frame_k,sf_frame_k1, thetar_k1)

X_k1k_SF=gl2sf(sf_frame_k1,thetar_k1,X_k1k);
ds=sf_frame_k1(1,3)-sf_frame_k(1,3);

% obs=X_k1k_SF;
% obs(2,1)=0;
% 
% predicted_obs=X_k1k_SF;

innov=[0;-X_k1k_SF(2,1);0];

var_phant=(0.2)^2;
%Innovation cov


S=2*P_k1k(2,2);

%UPDATE

% if rcond(S) > 1e-12
%     
%     disp('yei')
%     K=P_k1k(2,2)*inv(S);
% else
%     K=zeros(3,size(S,1));
% end

K=P_k1k(2,2)/S;


K*innov;

X_k1k1_SF=X_k1k_SF+K*innov;
X_k1k1=sf2gl(sf_frame_k1,thetar_k1,X_k1k1_SF);
P_k1k1=P_k1k-[0 0 0;0 K*S*transpose(K) 0;0 0 0];


end

