
function [X_k1k,P_k1k] = predict(X_k,P_k,U_k,dt)

global sigma_v;
global sigma_w;
global sigma_motionPd;
global sigma_motionPtheta;

%MOTION MODEL
 

X_k1k= X_k + [U_k(1,1)*cos(X_k(3,1)); U_k(1,1)*sin(X_k(3,1)) ;dt*U_k(2,1)];

%X_k1k=X_pt+[0.15*cos(X_pt(3,1));0.15*sin(X_pt(3,1));0];

%CONTROL NOISE COVARIANCE Q
% 
% var_v=(0.05)^2;
% var_w=(0.0667)^2;

%SLOWVALS
% var_v=(0.05)^2;
% var_w=(deg2rad(0.5))^2;


%experiment vals
var_v=sigma_v^2;
var_w=(deg2rad(sigma_w))^2;


Q=[var_v 0;0 var_w];

%PREDICTION COVARIACNCE 

%JACOBIANS
dFX=[1 0 -U_k(1,1)*sin(X_k(3,1)); 0 1 U_k(1,1)*cos(X_k(3,1)); 0 0 1];

dFU=[cos(X_k(3,1)) 0; sin(X_k(3,1)) 0; 0 1].*dt;

P_k1k=dFX*P_k*transpose(dFX)+dFU*Q*transpose(dFU)+[sigma_motionPd^2 0 0;0 sigma_motionPd^2 0;0 0 (deg2rad(sigma_motionPtheta))^2];


end 