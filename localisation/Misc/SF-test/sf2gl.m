function [gl_pose] = sf2gl(sf_frame,theta_r,sf_pose)

trans_r=sf_frame(1,1:2);

gl_point_rot=[cos(theta_r) -sin(theta_r); sin(theta_r) cos(theta_r)]*sf_pose(1:2);

gl_point=gl_point_rot+transpose(trans_r);

gl_theta=wrapTo2Pi(sf_pose(3,1)+theta_r);
gl_pose=[gl_point;gl_theta];
end

