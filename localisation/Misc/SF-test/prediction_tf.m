
function [X_k1k,P_k1k,thetar_k1] = prediction_tf(X_k,P_k,U_k,dt,sf_frame_k,sf_frame_k1,thetar_k)

global sigma_v;
global sigma_w;
global sigma_motionPd;
global sigma_motionPtheta;

%MOTION MODEL
sf_frame_k;
sf_frame_k1;

dtang=sf_frame_k1(1,1:2)-sf_frame_k(1,1:2);

thetar_k1=wrapTo2Pi(sf_frame_k1(1,6));

% ds_vec=transpose([cos(thetar_k1) -sin(thetar_k1); sin(thetar_k1) cos(thetar_k1)])*transpose(dtang);
% ds=ds_vec(1,1);

ds=sf_frame_k1(1,3)-sf_frame_k(1,3);

% if ds>0
%     thetar_k1=wrapTo2Pi(atan2((dtang(1,2)),(dtang(1,1))));
% elseif ds<0
%     thetar_k1=wrapTo2Pi(atan2((dtang(1,2)),(dtang(1,1))));
%     thetar_k1=wrapTo2Pi(thetar_k1-pi);
% end
% 
% if dtang==0
%     thetar_k1=wrapTo2Pi(thetar_k);
% end
% if ds>0
%     dthetar=wrapToPi(thetar_k1-thetar_k);
% elseif ds<0
%     dthetar=-wrapToPi(thetar_k1-thetar_k);
% else
%     dthetar=wrapToPi(thetar_k1-thetar_k);
% end




X_k_SF=gl2sf(sf_frame_k1,thetar_k1,X_k);

DT=U_k(1,1)*cos(X_k_SF(3,1)); %Both of these are already multiplied by dt
DN=U_k(1,1)*sin(X_k_SF(3,1)); %Both of these are already multiplied by dt
DTHETA=U_k(2,1)*dt;

X_k1k_SF= X_k_SF + [DT;DN;DTHETA];

X_k1k_SF(3,1)=wrapTo2Pi(X_k1k_SF(3,1));

X_k1k=sf2gl(sf_frame_k1,thetar_k1,X_k1k_SF);

%X_k1k= X_k + [U_k(1,1)*cos(X_k(3,1)); U_k(1,1)*sin(X_k(3,1)) ;dt*U_k(2,1)];


%CONTROL NOISE COVARIANCE Q
%
% var_v=(0.05)^2;
% var_w=(0.0667)^2;

%SLOWVALS
% var_v=(0.05)^2;
% var_w=(deg2rad(0.5))^2;


%experiment vals
var_v=sigma_v^2;
var_w=(deg2rad(sigma_w))^2;


Q=[var_v 0;0 var_w];

%PREDICTION COVARIACNCE

%JACOBIANS
%dFX=[1 0 -U_k(1,1)*sin(X_k(3,1)); 0 1 U_k(1,1)*cos(X_k(3,1)); 0 0 1];

% dFX=[1 dthetar -U_k(1,1)*sin(X_k_SF(3,1)); -dthetar 1 U_k(1,1)*cos(X_k_SF(3,1)); 0 0 1];
% dFU=[cos(X_k_SF(3,1)) 0; sin(X_k_SF(3,1)) 0; 0 1].*dt;
dFX=[1 0 -U_k(1,1)*sin(X_k_SF(3,1)); 0 1 U_k(1,1)*cos(X_k_SF(3,1)); 0 0 1];

dFU=[cos(X_k_SF(3,1)) 0; sin(X_k_SF(3,1)) 0; 0 1].*dt;

P_k1k=dFX*P_k*transpose(dFX)+dFU*Q*transpose(dFU)+[sigma_motionPd^2 0 0;0 sigma_motionPd^2 0;0 0 (deg2rad(sigma_motionPtheta))^2];

end