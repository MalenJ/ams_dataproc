close all
clear all

global xMin
global yMin
global res
global sizeY
global sizeX
global lasermap
global featuremap
global realtimeplotter
global robothan
global laserhan
global featurehan
global drawInit
global sigma_v;
global sigma_w;
global sigma_motionPd;
global sigma_motionPtheta;
global sigma_bearing;
global IG_bearing;
global sigma_edge;
global sigma_edgeP;
global sigma_bearingP;
global sf_framepath;

%Load stuff

fileroot='2019-09-01-Wentworth';
filename='2019-09-01-Wentworth-yolo';
PATH='/home/maleen/cloud_academic/ams_data/datasets/';
load(strcat(PATH,fileroot,'/localready/',fileroot,'-localready.mat'));

%Localisation data

start_time=50;
end_time=175;

sigma_v=0.01;
sigma_w=0.1;
sigma_motionPd=0;%0.01;
sigma_motionPtheta=0;%0.01;%;

sigma_bearing=5;
IG_bearing=0.1;
sigma_bearingP=0;

sigma_edge=0.15;
sigma_edgeP=1;

realtimeplotter=1;
startiter=find(data_time<start_time, 1, 'last' );
enditer=find(data_time<end_time, 1, 'last' );
iter=0;
sz=25;

%Realtime plot of edgemap
xMin=25;
yMin=25;
sizeX=750;
sizeY=1500;
res=0.04;

%Clothoid stuff
[CL_points,CL_size]=makeclothoidmap();
npts = 1000;

%Phantom stuff
sp=1;
phantom_time=transpose(linspace(start_time+sp,end_time-sp,(end_time+sp-start_time-sp)/sp));
phantom_obs=cell(length(phantom_time),1);
phantom_obs(:)={'P'};
phantom_tab= table(phantom_time,phantom_obs);
phantom_tab.Properties.VariableNames = {'overall_time' 'labels'};
data_stream.Var1=[];

BFTS=vertcat(data_stream,phantom_tab);
BFTS = sortrows(BFTS,'overall_time');
data_time=BFTS{:,1};
data_type=BFTS{:,2};



if realtimeplotter==1
    drawInit = 0;
    featuremap=LMmap;
    figure(1)
    lasermap = subplot(1,1,1);
    hold on
    imshow(imcomplement(binary_map))
    for i=1:size(LMmap,1)
        scatter(lasermap,(LMmap(i,2)+xMin)./res,sizeY-(LMmap(i,3)+yMin)./res,sz,'x','k')
        scatter(lasermap,(CL_points(:,1)+xMin)./res,sizeY-(CL_points(:,2)+yMin)./res,10,'x','k')
        
        scatter(lasermap,(1+xMin)./res,sizeY-(0+yMin)./res,10,'x','r')
        
        text(lasermap,(LMmap(i,2)+xMin)./res,sizeY-(LMmap(i,3)+yMin)./res, num2str(i));
    end
    
    
    figure(2)
    errorplotX= subplot(3,1,1);
    hold on
    eplotxhandle.error=plot(errorplotX,0,0);
    eplotxhandle.covup=plot(errorplotX,0,0);
    eplotxhandle.covdown=plot(errorplotX,0,0);
    xlabel(errorplotX,'time (s)')
    ylabel(errorplotX,'X error (m)')
    
    
    errorplotY= subplot(3,1,2);
    hold on
    eplotyhandle.error=plot(errorplotY,0,0);
    eplotyhandle.covup=plot(errorplotY,0,0);
    eplotyhandle.covdown=plot(errorplotY,0,0);
    xlabel(errorplotY,'time (s)')
    ylabel(errorplotY,'Y error (m)')
    
    errorplotTheta= subplot(3,1,3);
    hold on
    eplotthetahandle.error=plot(errorplotTheta,0,0);
    eplotthetahandle.covup=plot(errorplotTheta,0,0);
    eplotthetahandle.covdown=plot(errorplotTheta,0,0);
    xlabel(errorplotTheta,'time (s)')
    ylabel(errorplotTheta,'Theta error (m)')
    
    
    figure(3)
    covellipsehandle=plot(0,0);
    
    
end

error_data_cnt=0;

for i=startiter:enditer
    
    iter=iter+1
    
    previous_time=data_time(i-1);
    current_time=data_time(i);
    current_data=data_type{i};
    dt=current_time-previous_time;
    
    U_k(1,1)=(ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib;
    U_k(2,1)=ppval(imu_interp,previous_time);
    
    odomobj=odom_obsZ(U_k);
    
    if (i-1<startiter)
        
        trans=[ppval(rtkinterp_x,previous_time),ppval(rtkinterp_y,previous_time),ppval(rtkinterp_z,previous_time)];
        rotz=ppval(rtkinterp_theta,previous_time);
        T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
        T_world_robot=T_world_RTK*inv(T_robot_RTK);
        
        X_k=[T_world_robot(1,4);T_world_robot(2,4);rotz];
        
        distmat=CL_points(:,1:2)-ones(CL_size,2).*transpose(X_k(1:2,:));
        CL_dists=distmat(:,1).^2+distmat(:,2).^2;
        [M,I]=min(CL_dists);
        %sf_frame_k=CL_points(I-1,:);
        sf_frame_k=CL_points(I,:);
        sf_frame_k1=CL_points(I,:);
        
        %thetar_k=atan2(abs(sf_frame_k1(1,2)),abs(sf_frame_k1(1,1)));
        thetar_k=wrapTo2Pi(CL_points(I,6));
        
        %X_k=[0;0;0];
        %P_k=zeros(3);
        P_k=[(0.05)^2 0 0;0 (0.05)^2 0;0 0 (deg2rad(0.1))^2];
        
    else
        iterval=1+(iter-2)*3;
        X_k=X_k1k1(:,iter-1);
        P_k=P_k1k1(1:3,iterval:iterval+2);
        
%         sf_frame_k=sf_frame_k1;
%         thetar_k=thetar_k1;
        
%         distmat=CL_points(:,1:2)-ones(CL_size,2).*transpose(X_k(1:2,:));
%         CL_dists=sqrt(distmat(:,1).^2+distmat(:,2).^2);
%         
%         dsmat=CL_points(:,3)-sf_frame_k(1,3);
%           
%         if U_k(1,1)*cos(wrapTo2Pi(X_k(3,1)-thetar_k))>0
%             indxsz=find(dsmat>0); 
%             [M,I]=min(CL_dists(indxsz));
%             sf_frame_k1=CL_points(indxsz(I),:);
%         elseif U_k(1,1)*cos(wrapTo2Pi(X_k(3,1)-thetar_k))<0
%             indxsz=find(dsmat<0);
%             [M,I]=min(CL_dists(indxsz));
%             sf_frame_k1=CL_points(indxsz(I),:);
%         else
%             sf_frame_k1=sf_frame_k;
%         end
        
        distmat=CL_points(:,1:2)-ones(CL_size,2).*transpose(X_k(1:2,:));
        CL_dists=distmat(:,1).^2+distmat(:,2).^2;
        [M,I]=min(CL_dists);

        sf_frame_k1=CL_points(I,:);

        
        
%         distvecs=CL_points(I,1:2)-ones(10,2).*sf_frame_k(:,1:2);
%         vvec=[U_k(1,1)*cos(X_k(3,1)), U_k(1,1)*sin(X_k(3,1))];
%         
%         for i=1:length(distvecs)
%             angs(i) = atan2d(distvecs(i,1)*vvec(1,2)-distvecs(i,2)*vvec(1,1),distvecs(i,1)*vvec(1,1)+distvecs(i,2)*vvec(1,2))%rad2deg(wrapTo2Pi(dot(distvecs(i,:),vvec)/(norm(distvecs(i,:))*norm(vvec))))
%         end
%         
%         [M2,I2]=min(abs(angs))
        
        
        
    end
    
    [X_k1k,P_k1k] = prediction(X_k,P_k,U_k,dt);
    %[X_k1k,P_k1k,thetar_k1] = prediction_tf(X_k,P_k,U_k,dt,sf_frame_k,sf_frame_k1,thetar_k);
    X_k1k(3,1)=wrapTo2Pi(X_k1k(3,1));
    
    
    if (current_data=='P')
        %current_data;
%         iterval=1+(iter-1)*3;
%         
%         [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2)] = observation_phantom(X_k1k,P_k1k,sf_frame_k,sf_frame_k1,thetar_k1);
%         drawEKFresults(odomobj, X_k1k1(:,iter));
        
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        drawEKFresults(odomobj, X_k1k1(:,iter));
        
    elseif (current_data=='O')
        
        %current_data
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        
        drawEKFresults(odomobj, X_k1k1(:,iter));
        
        
        
    elseif (current_data=='LC')
        
        %current_data
%                 rows=yolo_observations.lamp_times==current_time;
%         
%                 centroids=yolo_observations(rows,{'centroid_x'});
%                 semantics=yolo_observations(rows,{'yolo_labels'});
%         
%                 bear_obs=bearing_obZ(LC_params,current_time,current_data,centroids{:,1},semantics{:,1});
%         
%                 iterval=1+(iter-1)*3;
%                 [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),bear_obs] = observation_bearing_tf(X_k1k,P_k1k,bear_obs,sf_frame_k,sf_frame_k1,thetar_k1,LMmap);
%                 drawEKFresults(bear_obs, X_k1k1(:,iter));
        
        %Isolate
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        drawEKFresults(odomobj, X_k1k1(:,iter));
        %
        
    elseif(current_data=='RC')
        
        %current_data
%                 rows=yolo_observations.lamp_times==current_time;
%         
%                 centroids=yolo_observations(rows,{'centroid_x'});
%                 semantics=yolo_observations(rows,{'yolo_labels'});
%         
%                 bear_obs=bearing_obZ(RC_params,current_time,current_data,centroids{:,1},semantics{:,1});
%         
%                 iterval=1+(iter-1)*3;
%                 [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),bear_obs] = observation_bearing_tf(X_k1k,P_k1k,bear_obs,sf_frame_k,sf_frame_k1,thetar_k1,LMmap);
%                 drawEKFresults(bear_obs, X_k1k1(:,iter));
%         
        %Isolate
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        drawEKFresults(odomobj, X_k1k1(:,iter));
        
    elseif(current_data=='BC')
        
        %         %         current_data;
        %         %
        %         iterval=1+(iter-1)*3;
        %         img_seq=find(depthedge_time==current_time);
        %
        %         if ~isnan(img_seq)
        %             depthedge_img=depthedge{img_seq};
        %
        %             img_seq;
        %             edge_obs=edge_obsZ(BC_params,current_time,depthedge_img,T_robot_backcam);
        %
        %             if edge_obs.flagger>0 % && 2*edge_obs.flagger <2500
        %                 [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2)] = observation_edge(X_k1k, P_k1k, edge_obs.ranges,edge_obs.bearings, dfobj);
        %                 %[X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2), K] = update(X_k1k, P_k1k, V, jHx, S);
        %                 drawEKFresults(edge_obs, X_k1k1(:,iter));
        %             else
        %                 X_k1k1(:,iter)=X_k1k;
        %                 P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        %                 drawEKFresults(odomobj, X_k1k1(:,iter));
        %             end
        %         else
        %             X_k1k1(:,iter)=X_k1k;
        %             P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        %             drawEKFresults(odomobj, X_k1k1(:,iter));
        %         end
        
        %Isolate
        
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        drawEKFresults(odomobj, X_k1k1(:,iter));
    end
    
    iterval=1+(iter-1)*3;
    %     scatter(X_k1k1(1,iter),X_k1k1(2,iter),sz,'b');
    
    if (~isnan(P_k1k1(1,iterval:iterval+2)))
        
        
        if current_time<147.5 || current_time>150
            
            error_data_cnt=error_data_cnt+1;
            
            PK=P_k1k1(1:3,iterval:iterval+2);
            pose=X_k1k1(:,iter);
            %pose=gl2sf(sf_frame_k1,thetar_k1,X_k1k1(:,iter));
            
            datt(error_data_cnt,:)=current_time;
            
            p=0.95;
            s = -2 * log(1 - p);
            
            [V, D] = eig(PK(1:2,1:2) * s);
            t = linspace(0, 2 * pi);
            
            a = real((V * sqrt(D)) * [cos(t(:))'; sin(t(:))']);
            
            trans=[ppval(rtkinterp_x,current_time),ppval(rtkinterp_y,current_time),ppval(rtkinterp_z,current_time)];
            rotz=ppval(rtkinterp_theta,current_time);
            T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
            T_world_robot=T_world_RTK*inv(T_robot_RTK);
            
            GTx(error_data_cnt)=T_world_robot(1,4);
            GTy(error_data_cnt)=T_world_robot(2,4);
            GTtheta(error_data_cnt)=rotz;
            
            %GT_SF(:,error_data_cnt)=gl2sf(sf_frame_k1,thetar_k1,[GTx(error_data_cnt); GTy(error_data_cnt);GTtheta(error_data_cnt)]);
            
            %RTK_calib=[cos(pose(3,1)) -sin(pose(3,1)) -0.55; sin(pose(3,1)) cos(pose(3,1)) 0;0 0 1]*[GTx; GTy; 1];
            
            error(:,error_data_cnt)=[GTx(error_data_cnt);GTy(error_data_cnt); GTtheta(error_data_cnt)]-pose(:,1); 
            %error(:,error_data_cnt)=[GT_SF(1,error_data_cnt);GT_SF(2,error_data_cnt); GT_SF(3,error_data_cnt)]-pose(:,1);
            sigmabounds(:,error_data_cnt)=real(2*sqrt(diag(PK)));
            
            
            if realtimeplotter==1
                set(eplotxhandle.covup,'XData',datt,'YData',sigmabounds(1,:),'color', [0,0,1]);
                set(eplotxhandle.covdown,'XData',datt,'YData', -sigmabounds(1,:),'color', [0,0,1]);
                set(eplotxhandle.error,'XData',datt,'YData',error(1,:),'color', [1,0,0]);
                
                set(eplotyhandle.covup,'XData',datt,'YData', sigmabounds(2,:),'color', [0,0,1]);
                set(eplotyhandle.covdown,'XData',datt,'YData', -sigmabounds(2,:),'color', [0,0,1]);
                set(eplotyhandle.error,'XData',datt,'YData',error(2,:),'color', [1,0,0]);
                
                
                set(eplotthetahandle.covup,'XData',datt,'YData', sigmabounds(3,:),'color', [0,0,1]);
                set(eplotthetahandle.covdown,'XData',datt,'YData', -sigmabounds(3,:),'color', [0,0,1]);
                set(eplotthetahandle.error,'XData',datt,'YData',error(3,:),'color', [1,0,0]);
                
                set(covellipsehandle,'XData',a(1, :),'YData',a(2, :),'color', [0,0,1])
            end
        end
        
    end
    
end


%% PLOT

%close all

figure
axis equal
hold on
szGT=1.5;
szEKF=2;

for i=1:size(LMmap,1)
    scatter(LMmap(i,2),LMmap(i,3),'x','k')
    text(LMmap(i,2),LMmap(i,3), num2str(LMmap(i,1)));
end

%scatter(rtabx,rtaby,szGT,'r')
scatter(rtk_enux,rtk_enuy,szGT,'r')
scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b');
scatter(CL_points(:,1),CL_points(:,2),szEKF,'k');

xlabel('X (m)')
ylabel('Y (m)')


