function [sf_pose] = gl2sf(sf_frame,theta_r,gl_pose)

trans_r=sf_frame(1,1:2);

gl_point_trans=gl_pose(1:2)-transpose(trans_r);
sf_point=[cos(-theta_r) -sin(-theta_r); sin(-theta_r) cos(-theta_r)]*gl_point_trans;


sf_theta=wrapTo2Pi(gl_pose(3,1)-theta_r);
rad2deg(sf_theta);

sf_pose=[sf_point;sf_theta];

end

