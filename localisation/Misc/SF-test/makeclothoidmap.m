function [CL_points,CL_size] = makeclothoidmap()
% clear all
% close all
% fileroot='2019-09-01-Wentworth';
% filename='2019-09-01-Wentworth-yolo';
% PATH='/home/maleen/cloud_academic/ams_data/datasets/';
% load(strcat(PATH,fileroot,'/localready/',fileroot,'-localready.mat'),'rtk_enux','rtk_enuy');

x0=0;
y0=0;
theta0=0;
x1=1;
y1=28.5;
x2=-0.8;
y2=29.4;
x3=-14.5;
y3=22.5;
x4=-16;
y4=21.5;
x5=-19;
y5=17;
x6=-19;
y6=15;
x7=-3.5;
y7=-17;
x8=-0.4;
y8=-17;

% x0=1;
% y0=0;
% theta0=0;
% x1=1.5;
% y1=30;
% x3=-1;
% y3=30;
% x4=-15;
% y4=23;
% theta1=0;


% x0=-1;
% y0=0;
% theta0=0;
% x1=-0.1;
% y1=28.5;
% x3=-0.5;
% y3=29;
% x4=-15;
% y4=21;
% theta1=0;

CL1 = LineSegment( [x0, y0], [x1, y1] );

CL2 = ClothoidCurve();
CL2.build_G1(x1, y1,wrapTo2Pi(atan2(y1-y0,x1-x0)), x2, y2,wrapTo2Pi(atan2(y3-y2,x3-x2)));

CL3 = LineSegment( [x2, y2], [x3, y3] );

CL4 = ClothoidCurve();
CL4.build_G1(x3, y3,wrapTo2Pi(atan2(y3-y2,x3-x2)), x4, y4,wrapTo2Pi(atan2(y5-y4,x5-x4)));

CL5 = LineSegment( [x4, y4], [x5, y5] );

CL6 = ClothoidCurve();
CL6.build_G1(x5, y5,wrapTo2Pi(atan2(y5-y4,x5-x4)), x6, y6,wrapTo2Pi(atan2(y7-y6,x7-x6)));

CL7 = LineSegment( [x6, y6], [x7, y7] );

CL8 = ClothoidCurve();
CL8.build_G1(x7, y7,wrapTo2Pi(atan2(y7-y6,x7-x6)), x8, y8,wrapTo2Pi(atan2(y0-y8,x0-x8)));

CL9 = LineSegment( [x8, y8], [x0, y0] );




% CL4_size=CL4.length()/point_res;
% CL5_size=CL5.length()/point_res;
% CL6_size=CL6.length()/point_res;
% CL7_size=CL7.length()/point_res;
% CL8_size=CL8.length()/point_res;
% CL9_size=CL9.length()/point_res;


point_res=5;
CL1_size=CL1.length()/point_res;
for i=1:CL1_size

    CL1_points(i,1:2)=CL1.eval(point_res*i);
    CL1_points(i,3)=point_res*i;
    tang=CL1.eval_D(point_res*i);
    CL1_points(i,4:5)=tang;
    CL1_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));

end

point_res=0.01;
CL2_size=CL2.length()/point_res;
for i=1:CL2_size

    CL2_points(i,1:2)=CL2.eval(point_res*i);
    CL2_points(i,3)=CL1_points(end,3)+point_res*i;
    tang=CL2.eval_D(point_res*i);
    CL2_points(i,4:5)=tang;
    CL2_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));

    
end

point_res=5;
CL3_size=CL3.length()/point_res;
for i=1:CL3_size

    CL3_points(i,1:2)=CL3.eval(point_res*i);
    CL3_points(i,3)=CL2_points(end,3)+point_res*i;
    tang=CL3.eval_D(point_res*i);
    CL3_points(i,4:5)=tang;
    CL3_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));

end

point_res=0.01;
CL4_size=CL4.length()/point_res;

for i=1:CL4_size

    CL4_points(i,1:2)=CL4.eval(point_res*i);
    CL4_points(i,3)=CL3_points(end,3)+point_res*i;
    tang=CL4.eval_D(point_res*i);
    CL4_points(i,4:5)=tang;
    CL4_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));

end

point_res=5;
CL5_size=CL5.length()/point_res;

for i=1:CL5_size

    CL5_points(i,1:2)=CL5.eval(point_res*i);
    CL5_points(i,3)=CL4_points(end,3)+point_res*i;
    tang=CL5.eval_D(point_res*i);
    CL5_points(i,4:5)=tang;
    CL5_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));
end

point_res=0.01;
CL6_size=CL6.length()/point_res;

for i=1:CL6_size

    CL6_points(i,1:2)=CL6.eval(point_res*i);
    CL6_points(i,3)=CL5_points(end,3)+point_res*i;
    tang=CL6.eval_D(point_res*i);
    CL6_points(i,4:5)=tang;
    CL6_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));

end

point_res=5;
CL7_size=CL7.length()/point_res;

for i=1:CL7_size

    CL7_points(i,1:2)=CL7.eval(point_res*i);
    CL7_points(i,3)=CL6_points(end,3)+point_res*i;
    tang=CL7.eval_D(point_res*i);
    CL7_points(i,4:5)=tang;
    CL7_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));

end

point_res=0.01;
CL8_size=CL8.length()/point_res;

for i=1:CL8_size

    CL8_points(i,1:2)=CL8.eval(point_res*i);
    CL8_points(i,3)=CL7_points(end,3)+point_res*i;
    tang=CL8.eval_D(point_res*i);
    CL8_points(i,4:5)=tang;
    CL8_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));

end


point_res=5;
CL9_size=CL9.length()/point_res;

for i=1:CL9_size

    CL9_points(i,1:2)=CL9.eval(point_res*i);
    CL9_points(i,3)=CL8_points(end,3)+point_res*i;
    tang=CL9.eval_D(point_res*i);
    CL9_points(i,4:5)=tang;
    CL9_points(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));

end



CL_points=[CL1_points;CL2_points;CL3_points;CL4_points;CL5_points;CL6_points;CL7_points;CL8_points;CL9_points];

CL_size=length(CL_points);

% figure
% axis equal
% hold on
% szEKF=2;
% scatter(rtk_enux,rtk_enuy,szEKF,'r')
% scatter(CL_points(:,1),CL_points(:,2),szEKF,'k');
end


%% latest version before changing to drawing

% function [CL_points,CL_size] = makeclothoidmap_went(mappoints)
% 
% fileroot='2019-09-01-Wentworth';
% filename='2019-09-01-Wentworth-yolo';
% PATH='/home/maleen/cloud_academic/ams_data/datasets/';
% load(strcat(PATH,fileroot,'/localready/',fileroot,'-localready.mat'),'rtk_enux','rtk_enuy');
% 
% % fileroot='2020-02-28-Wentworth';
% % filename='2020-02-28-Wentworth-P1';
% % configname='TF1.mat';
% % CONFIGPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/config/'); %CSV DATA
% % CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
% % IMGPATH=strcat('/media/maleen/malen_ssd/phd/ams_data/',fileroot,'/image_data/',filename,'/'); %IMAGE DATA
% % MAPPATH_feature='/home/maleen/cloud_academic/ams_data/map_data/'; %MAP DATA
% % MAPPATH_edge='/home/maleen/cloud_academic/ams_data/map_data/edge_maps/';
% % %Load config
% % load(strcat(CONFIGPATH, filename,'-prepconfig-', configname))
% % con_map='2019-08-22-Wentworth/2019-08-22-Wentworth-conmap.png';
% % I=imread(strcat(MAPPATH_edge,con_map)); %funk1
% 
% 
% 
% x0=0;
% y0=0;
% x1=1;
% y1=28.5;
% x2=-0.8;
% y2=29.4;
% x3=-14.5;
% y3=22.5;
% x4=-16;
% y4=21.5;
% x5=-19;
% y5=17;
% x6=-19;
% y6=15;
% x7=-3.5;
% y7=-17;
% x8=-0.4;
% y8=-17;
% 
% CL1 = LineSegment( [x0, y0], [x1, y1] );
% 
% CL2 = ClothoidCurve();
% CL2.build_G1(x1, y1,wrapTo2Pi(atan2(y1-y0,x1-x0)), x2, y2,wrapTo2Pi(atan2(y3-y2,x3-x2)));
% 
% CL3 = LineSegment( [x2, y2], [x3, y3] );
% 
% CL4 = ClothoidCurve();
% CL4.build_G1(x3, y3,wrapTo2Pi(atan2(y3-y2,x3-x2)), x4, y4,wrapTo2Pi(atan2(y5-y4,x5-x4)));
% 
% CL5 = LineSegment( [x4, y4], [x5, y5] );
% 
% CL6 = ClothoidCurve();
% CL6.build_G1(x5, y5,wrapTo2Pi(atan2(y5-y4,x5-x4)), x6, y6,wrapTo2Pi(atan2(y7-y6,x7-x6)));
% 
% CL7 = LineSegment( [x6, y6], [x7, y7] );
% 
% CL8 = ClothoidCurve();
% CL8.build_G1(x7, y7,wrapTo2Pi(atan2(y7-y6,x7-x6)), x8, y8,wrapTo2Pi(atan2(y0-y8,x0-x8)));
% 
% CL9 = LineSegment( [x8, y8], [x0, y0] );
% 
% CL1_points = point_creator(CL1,0,mappoints);
% CL2_points = point_creator(CL2,1,mappoints);
% CL3_points = point_creator(CL3,0,mappoints);
% CL4_points = point_creator(CL4,1,mappoints);
% CL5_points = point_creator(CL5,0,mappoints);
% CL6_points = point_creator(CL6,1,mappoints);
% CL7_points = point_creator(CL7,0,mappoints);
% CL8_points = point_creator(CL8,1,mappoints);
% CL9_points = point_creator(CL9,0,mappoints);
% 
% CL_points=[CL1_points;CL2_points;CL3_points;CL4_points;CL5_points;CL6_points;CL7_points;CL8_points;CL9_points];
% 
% 
% CL_size=length(CL_points);
% 
% figure
% axis equal
% hold on
% szEKF=2;
% scatter(rtk_enux,rtk_enuy,szEKF,'r')
% scatter(CL_points(:,1),CL_points(:,2),szEKF,'k');
% scatter(CL_points(:,9),CL_points(:,10),szEKF,'b');
% scatter(CL_points(:,11),CL_points(:,12),szEKF,'r');
% end