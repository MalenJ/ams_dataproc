close all
clear all

%Load localisation CSV's
<<<<<<< HEAD
dataset='2019-09-01-Wentworth/';
filename='2019-09-01-Wentworth-yolo';
=======
dataset='2019-09-04-Village/';
filename='2019-09-04-Village-P5-yolo';
>>>>>>> bf1ba97f969d373abd9c78dbc3250b1049bb2668

CSVPATH='/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/csv_data/localisation_data/'; %CSV DATA
IMGPATH='//home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/img_data/'; %IMAGE DATA

MAPPATH_feature='/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/map_data/el_maps/'; %MAP DATA
MAPPATH_edge='/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/map_data/edge_maps/';

CALIBPATH='/home/ravindra/HDD-4TB/Maleen/ams_dataproc/calibration/data_arena/'; %CALIB DATA

data_stream=readtable(strcat(CSVPATH,dataset, filename, '-sorted.csv'));
rtk_data=readtable(strcat(CSVPATH,dataset, filename, '-RTK.csv'));

rtab_data=readtable(strcat(CSVPATH,dataset,filename, '-rtabdata.csv'));

enc_data=readtable(strcat(CSVPATH,dataset, filename, '-encoder.csv'));
imu_data=readtable(strcat(CSVPATH,dataset, filename, '-imu.csv'));

yolo_observations=readtable(strcat(CSVPATH,dataset, filename, '-lampdata.csv'));

backcam_depthdata=readtable(strcat(CSVPATH,dataset,filename, '-back_depth.csv'));
backcam_edgedata=readtable(strcat(CSVPATH,dataset,filename, '-back_rgb.csv'));

%Depth Image data
load(strcat(IMGPATH,dataset, filename, '-depthimgs.mat'),'depth_imgs');

%Camera Params

load(strcat(CALIBPATH,'back_calibapp.mat'),'cameraParams');
BC_params=cameraParams;

load(strcat(CALIBPATH,'left_calibapp.mat'),'cameraParams');
LC_params=cameraParams;

load(strcat(CALIBPATH,'right_calibapp.mat'),'cameraParams');
RC_params=cameraParams;

load(strcat(CALIBPATH,'back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');

%LOAD EL MAP

%load(strcat(MAPPATH_feature,'2019-03-22-Wentworth-map/2019-03-22-Wentworth-satmap.mat'),'map_sat');
load(strcat(MAPPATH_feature,'2019-09-04-Village/2019-09-04-Village-ELMAP.mat'),'final_map');


%LOAD EDGE MAP

<<<<<<< HEAD
I=imread(strcat(MAPPATH_edge,'2019-08-22-Wentworth/','2019-08-22-Wentworth-map-clean.png'));
=======
I=imread(strcat(MAPPATH_edge,'2019-09-04-Village/','village_map2.png'));
>>>>>>> bf1ba97f969d373abd9c78dbc3250b1049bb2668


%DATA STREAM

data_time=data_stream{:,2};
data_type=data_stream{:,3};

%CONVERT RTK

T_robot_RTK=makehgtform('translate',[-0.1273 0.0114 1.4476]);

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};



% base_lat=mean(rtk_lat(1:rtk_iter));
% base_long=mean(rtk_long(1:rtk_iter));
% base_alti=mean(rtk_alti(1:rtk_iter));
base_lat=-33.880632824130295;
base_long=1.511895158304587e+02;
base_alti=43.434044433462200;

[rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);

sampleval=15;
for i=1:length(rtk_time)-sampleval
    
    rtkXdist=rtk_enux(i+sampleval)-rtk_enux(i);
    rtkYdist=rtk_enuy(i+sampleval)-rtk_enuy(i);
    
    sqrt(rtkXdist^2+rtkYdist^2);
    rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
    rtk_heading(i,:)=wrapTo2Pi(atan2(rtkYdist,rtkXdist));
    
    rtkdist_timer(i,:)=rtk_time(i,1);
    
end

for i=1:length(rtkdist_timer)-sampleval
    
    d_theta=rtk_heading(i+sampleval)-rtk_heading(i);
    dt=rtkdist_timer(i+sampleval)-rtkdist_timer(i);
    rtk_angvel(i,:)=d_theta/dt;
    rtkvel_timer(i,:)=rtkdist_timer(i);
end


%y = movmean(rtk_heading,3);

rtk_heading=wrapTo2Pi(rtk_heading);
%y=wrapTo2Pi(y);

% RTK_meanX=movmean(rtk_enux,20);
% RTK_meanY=movmean(rtk_enuy,20);
% 
% figure
% scatter(RTK_meanX,RTK_meanY)

rtk_iter1=find(rtk_time<147, 1, 'last' );
rtk_iter2=find(rtk_time<148, 1, 'last' );

% RTK_meanX=movmean(rtk_enux(rtk_iter1:rtk_iter2),25);
% RTK_meanY=movmean(rtk_enuy(rtk_iter1:rtk_iter2),25);

% 
% rtk_time(rtk_iter1:rtk_iter2)=[];
% rtk_enux(rtk_iter1:rtk_iter2)=[];
% rtk_enuy(rtk_iter1:rtk_iter2)=[];
% rtk_enuz(rtk_iter1:rtk_iter2)=[];

% rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
% rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);
% rtkinterp_z = spline([0;rtk_time],[0;rtk_enuz]);
%rtkinterp_theta= spline([0;rtkdist_timer],[0;rtk_heading]);
<<<<<<< HEAD
rtkinterp_theta = interp1([0;rtkdist_timer],[0;rtk_heading],'linear','pp');
=======
rtkinterp_theta=interp1([0;rtkdist_timer],[0;rtk_heading],'linear','pp');
>>>>>>> bf1ba97f969d373abd9c78dbc3250b1049bb2668
rtkinterp_angv= spline([0;rtkvel_timer],[0;rtk_angvel]);



rtkinterp_x = interp1([0;rtk_time],[0;rtk_enux],'linear','pp');
rtkinterp_y = interp1([0;rtk_time],[0;rtk_enuy],'linear','pp');
rtkinterp_z = interp1([0;rtk_time],[0;rtk_enuz],'linear','pp');

test=linspace(0,170,1840);
%
figure
hold on
plot(rtkdist_timer,rad2deg(rtk_heading),'b')
%plot(rtkdist_timer,rad2deg(y),'r')
%plot(rtkdist_timer,rad2deg(ppval(rtkinterp_theta,rtkdist_timer)),'r')

figure
axis equal
hold on
scatter(rtk_enux,rtk_enuy,'b')
scatter(ppval(rtkinterp_x,test),ppval(rtkinterp_y,test),'r')
%plot(ppval(rtkinterp_x,test),ppval(rtkinterp_y,test),'-x')

figure
hold on
plot(rtkvel_timer,rtk_angvel)


%% ODOM DATA

enc_interp=spline(enc_data{:,3},enc_data{:,6});

encXcalib=6.608284615932709e-05;
encYcalib=6.603486193757895e-05;
calib=mean([encXcalib,encYcalib]);

%IMU DATA
imu_iter=find(imu_data{:,2}<35, 1, 'last' );

imu_bias=mean(imu_data{1:imu_iter,3});
imu_data{:,3}=imu_data{:,3}-imu_bias;

imu_smooth=movmean(imu_data{:,3},50);

figure
hold on
plot(imu_data{:,2},imu_data{:,3},'b')
plot(imu_data{:,2},imu_smooth,'r')


imu_interp=spline(imu_data{:,2},imu_data{:,3});
imu_interp1=spline(imu_data{:,2},imu_smooth);
imu_interp2=interp1(imu_data{:,2},imu_smooth,'linear','pp');

%Figuring out initial orientation

initial_theta=rtk_heading(450);

%% BACK CAMERA

roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;

%filename='2019-08-22-Wentworth';

j=0;
for i=1:length(backcam_depthdata.back_depth_time)
    
    depth_time=backcam_depthdata.back_depth_time(i);
    
    row=backcam_edgedata.back_rgb_time==depth_time;
    edge_indx=find(row>0);
    
    if ~isnan(edge_indx)
        j=j+1;
        
<<<<<<< HEAD
        edgeimg=imread(strcat(IMGPATH,dataset,'edge/', filename ,'-edge-',num2str(edge_indx), ".jpg"));
=======
        edgeimg=imread(strcat(IMGPATH,dataset,'edge/l5/', filename ,'-edge-',num2str(edge_indx), ".jpg"));
>>>>>>> bf1ba97f969d373abd9c78dbc3250b1049bb2668
        BW = im2bw(edgeimg,0.5);
        imshow(BW)
        
        BWU = undistortImage(BW, BC_params);
        depthimgU = undistortImage(depth_imgs{i},BC_params);
        
        depthedge_time(j)=depth_time;
        depthedge{j}=immultiply(BWU,depthimgU);
        
    else
        disp('skip')
    end
    
end

%RTK MAP

% [map_enu(:,1), map_enu(:,2), map_enu(:,3)]=geodetic2enu(map_sat(:,1),map_sat(:,2),map_sat(:,3),base_lat,base_long,base_alti, wgs84Ellipsoid);
% map_enu_ID=transpose(linspace(1,length(map_enu(:,1)),length(map_enu(:,1))));
% map_enu_semantic=[1;4;1;4;4;1;1;1;4;1;1;1;1;1;1;1;4];
% LMmap=[map_enu_ID,map_enu(:,1)+0.35,map_enu(:,2)-1.7,map_enu_semantic];

LMmap=final_map;


figure
axis equal
hold on
for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3))
    text(LMmap(i,2),LMmap(i,3), num2str(i));
end

%RTAB DATA
rtab_time=rtab_data{:,3};
rtabx=rtab_data{:,4};
rtaby=rtab_data{:,5};
rtabquat=rtab_data{:,6:9};
rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);

rtabinterp_x= interp1(rtab_time,rtabx,'linear','pp');%RTAB_world(1,:));
rtabinterp_y = interp1(rtab_time,rtaby,'linear','pp');%RTAB_world(2,:));
rtabinterp_z = interp1(rtab_time,zeros(length(rtabx),1),'linear','pp');%RTAB_world(3,:));
rtabinterp_theta = interp1(rtab_time,rtabeul(:,1),'linear','pp');

% rtabinterp_x= spline(rtab_time,rtabx);%RTAB_world(1,:));
% rtabinterp_y = spline(rtab_time,rtaby);%RTAB_world(2,:));
% rtabinterp_z = spline(rtab_time,zeros(length(rtabx),1));%RTAB_world(3,:));
% rtabinterp_theta = spline(rtab_time,rtabeul(:,1));

for i=1:length(rtab_time)-1
    
    rtabkXdist=rtabx(i+1)-rtabx(i);
    rtabkYdist=rtaby(i+1)-rtaby(i);
    
    rtabdist(i,:)=sqrt(rtabkXdist^2+rtabkYdist^2);
    rtab_heading(i,:)=atan2(rtabkYdist,rtabkXdist);
    rtabdist_timer(i,:)=rtab_time(i,:);
    
end

rtabinterp_heading=interp1(rtabdist_timer,rtab_heading,'linear','pp');


%EDGE MAP

binary_map=imbinarize(rgb2gray(imcomplement(I)));
% SE = strel('square',2);
% dilatedmap = imdilate(binary_map,SE);

dfobj = DistanceFunctions2D(binary_map, -10, -40, 2375, 2000, 0.04);


clear depth_imgs;

