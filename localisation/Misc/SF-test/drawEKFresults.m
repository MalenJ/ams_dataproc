function[] = drawEKFresults(obsobj, Xk)
global res
global sizeY
global lasermap
global robothan
global laserhan
global drawInit
global featuremap
global featurehan
global xMin
global yMin
global realtimeplotter

if realtimeplotter==1
    if ~drawInit
        robothan = plot(lasermap,0,0,'g.');
        set(robothan, 'color', [0,0,1], 'MarkerSize', 15);
        laserhan = plot(lasermap,0,0,'g.');
        set(laserhan, 'color', [0,1,0], 'MarkerSize', 6);
        featurehan = plot(lasermap,0,0,'g.');
        set(featurehan, 'color', [1,0,0], 'MarkerSize', 0.1,'Marker','o');
        drawInit = 1;
        
    end
    
    Xk(1) = (Xk(1)+xMin)/res;
    Xk(2) = (Xk(2)+yMin)/res;
    
    
    if(obsobj.sensortype=='OD')
        
        Xk(2) = sizeY - Xk(2);
        set(featurehan, 'XData', Xk(1), 'YData', Xk(2),'MarkerSize', 0.1);
        set(laserhan, 'XData',Xk(1), 'YData', Xk(2));
        set(robothan, 'XData', Xk(1), 'YData', Xk(2));
        
    elseif(obsobj.sensortype=='RC')
        
        I=find(obsobj.associations>0);
        if ~isnan(I)
            IDs=obsobj.associations(I);
            xmap=(featuremap(IDs,2)+xMin)./res;
            ymap=(featuremap(IDs,3)+yMin)./res;
            ymap= sizeY - ymap;
            set(laserhan, 'XData',Xk(1), 'YData', Xk(2));
            set(featurehan, 'XData', xmap, 'YData', ymap,'color', [1,0,0],'MarkerSize', 7.5);
        end
        Xk(2) = sizeY - Xk(2);
        set(robothan, 'XData', Xk(1), 'YData', Xk(2));
        
    elseif(obsobj.sensortype=='LC')
        
        I=find(obsobj.associations>0);
        if ~isnan(I)
            IDs=obsobj.associations(I);
            xmap=(featuremap(IDs,2)+xMin)./res;
            ymap=(featuremap(IDs,3)+yMin)./res;
            ymap= sizeY - ymap;
            set(laserhan, 'XData',Xk(1), 'YData', Xk(2));
            set(featurehan, 'XData', xmap, 'YData', ymap,'color', [1,0,0],'MarkerSize',7.5);
        end
        Xk(2) = sizeY - Xk(2);
        set(robothan, 'XData', Xk(1), 'YData', Xk(2));
        
    elseif(obsobj.sensortype=='BC')
        % Convert ranges to pixels units
        laser = obsobj.ranges;
        angles= obsobj.bearings;
        
        laser = laser/res;
        
        Xoi = zeros(2,length(laser));
        Xoi(1,:) = Xk(1) + laser.*cos(Xk(3) + angles);
        Xoi(2,:) = Xk(2) + laser.*sin(Xk(3) + angles);
        
        % Change coordinates, x same y reverse
        Xoi(2,:) = sizeY - Xoi(2,:);
        Xk(2) = sizeY - Xk(2);
        set(laserhan, 'XData', Xoi(1,:), 'YData', Xoi(2,:));
        set(featurehan, 'XData', Xk(1), 'YData', Xk(2),'MarkerSize', 0.1);
        set(robothan, 'XData', Xk(1), 'YData', Xk(2));
    end
    
    pause(0.01)
    %drawnow limitrate;
end
end