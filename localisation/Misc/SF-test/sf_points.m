classdef sf_points

    
    properties
        POI
        SF_curve
        S
        Tang
        Nom
        Theta_r
        time
    end
    
    methods
        function obj = sf_points(CL,poi,time)
            obj.POI=poi(1:2);
            obj.time=time;
            obj.SF_curve=CL;
      
            obj.Tang= CL.eval_D(obj.S);
            obj.Theta_r=atan(obj.Tang(1))
            
        end
        
        function sf_pt = global2sf(globalpt)
           sf_pt=0;
            
        end
        
        
         function globalpt = sf2global(sfpt)
          globalpt=0;
           
        end
        
        
    end
end

