
%WAYPOINTS
close all;
clear all;

path = [0 0; 1.25 1.75; 5.25 8.25; 7.25  8.75; 11.75  10.75; 13 14;14 10;12 6;10 4;8 1;2.00 1.00];

map=[1.25,3.75,1; 3.25, 3.25, 2; 5.25,6.25,3; 3.75,8.25,4;7.75,9.75,5;11.25,9.75,6;11.25,13.25,7;14.75,12.25,8;13.75,7.75,9;10.25,5.75,10;10.25,2.25,11;4.75,1.75,12;6.75,0.25,13];

robotCurrentLocation = path(1,:);
robotGoal = path(end,:);
initialOrientation = pi/4;

robotCurrentPose = [robotCurrentLocation initialOrientation];


%SIMULATOR
robotRadius = 0.4;
robot = ExampleHelperRobotSimulator('emptyMap',2);
robot.enableLaser(false);
robot.setRobotSize(robotRadius);
robot.showTrajectory(true);
robot.setRobotPose(robotCurrentPose);


plot(path(:,1), path(:,2),'k--d')
scatter(map(:,1),map(:,2))
xlim([0 16])
ylim([0 16])

%CONTROLLER
controller = robotics.PurePursuit;
controller.Waypoints = path;

controller.DesiredLinearVelocity = 5;
controller.MaxAngularVelocity = 2;
controller.LookaheadDistance = 0.75;

%DRIVE THE ROBOT

goalRadius = 0.1;
distanceToGoal = 100;
controlRate = robotics.Rate(15);

%X_k=transpose(robotCurrentPose);
% P_k=zeros(3);
%
% t1=datevec(now);
%
% viz = Visualizer2D;
% %  g=scatter(0,0,'r');


i=1;

%while( distanceToGoal > -goalRadius )
condition=1;

while( condition ==1 )
    
    timestamp(i,:)=rem(now,1)* 24 * 60 * 60;
    GT_pose(i,:)=robot.getRobotPose;

    % Compute the controller outputs, i.e., the inputs to the robot
    [v, omega] = controller(robot.getRobotPose);
   
    % Simulate the robot using the controller outputs.
    
    U_kmat(i,:)=[v, omega];
    
    drive(robot, v, omega);

    
    % Extract current location information ([X,Y]) from the current pose of the
    % robot
    
    robotCurrentPose = robot.getRobotPose;

    
    % Re-compute the distance to the goal
    distanceToGoal = norm(robotCurrentPose(1:2) - robotGoal);
    i=i+1;
    
    waitfor(controlRate);
    
    
end




