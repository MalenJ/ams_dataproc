
function [X_k1k,P_k1k] = scooter_predict(X_k,P_k,U_k,dt)

%MOTION MODEL

X_k1k= X_k + [U_k(1,1)*cos(X_k(3,1)); U_k(1,1)*sin(X_k(3,1)) ;U_k(2,1)].*dt;

%CONTROL NOISE COVARIANCE Q

var_v=(0.3)^2;
var_w=(0.2)^2;

Q=[var_v 0;0 var_w];

%PREDICTION COVARIACNCE 

%JACOBIANS
dFX=[1 0 -dt* U_k(1,1)*sin(X_k(3,1)); 0 1 dt*U_k(1,1)*cos(X_k(3,1)); 0 0 dt];

dFU=[cos(X_k(3,1)) 0; sin(X_k(3,1)) 0; 0 1].*dt;

P_k1k=dFX*P_k*transpose(dFX)+dFU*Q*transpose(dFU);



end 