function []=slam_lop(flag,job)
%filename: slam_lop.m

clc;

global env car ecar mrk emrk mea emea;
global CAR ECAR MRK EMRK EP EV EL LST LOC P R u v x;

CAR=[]; ECAR=[]; MRK=[]; EMRK=[]; EP=[]; EV=[]; EL=[]; LST=[]; LOC=[];

switch flag,
case 0,%repeat
  rand('state',env.rst);
  randn('state',env.nst);
  flag=env.flg;
otherwise,
  env.rst=rand('state');
  env.nst=randn('state');
  env.flg=flag;
end;

slam_drw(0);
mrk=slam_mrk(0,mrk);
car=slam_car(0,car);
slam_drw(1);
ecar=slam_car(0,car);
emrk=slam_mrk(1,mrk);
slam_drw(3);
mea=slam_mea(2,car,mrk,mea);
emea=slam_mea(0,ecar,emrk,mea);
slam_drw(2);

P=zeros(3,3);
u=diag([car.n.s car.n.s]);
x=[ecar.x; ecar.y; ecar.q];
switch flag,
case 1,
  R=diag([mea.n.r mea.n.b]);
case 2,
  R=mea.n.b;
end;
switch (job),
case 1,
  set(env.fig,'name','SLAM - Tracking');
case 2,
  set(env.fig,'name','SLAM - Mapping');
case 3,
  set(env.fig,'name','SLAM');
end;

slam_drw(5);
slam_drw(11);

env.sim=1;
for lop=1:env.run,
  if env.sim>0,
  mea=slam_mea(2,car,mrk,mea);
  emea=slam_mea(0,ecar,emrk,mea);
  slam_kmf(flag,job);
  slam_drw(2);
  slam_drw(4);
  slam_drw(6,0,job);
  if mod(lop,env.sav)==0,
    slam_sav(3);%save data
  end;
  car=slam_car(1,car);%move real car
  ecar=slam_car(2,ecar);%move est car

  title(sprintf('Car moving %d iteration',lop));
  pause(0.05);
  if env.pus>0,
    pause;
  end;
  end;
end;
