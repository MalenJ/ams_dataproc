function [car]=slam_car(flag,car)
%filename: slam_car.m

global env mrk mea ecar;

switch flag,
case 0,%init real car
  car.x=0; car.y=0; car.q=0;
  car=car_pat(car);
  car.v=car.u;
case 1,%move real car
  car.v=car.u;
  car=car_see(car,mea);%see any mark
  car=car_avd(env,car);%avoid walls
  car=car_obs(env,car,mea);%avoid obstacles
  car=car_trp(car);%avoid trapes
  car=car_mov(1,env,car);
  car=car_pat(car);
  ecar.v=car.v;
case 2,%move est car
  car=car_mov(2,env,car);
  car=car_pat(car);
case 3,%move car with dead recog
  car=slam_car(2,car);
  car.x=car.x+randn*sqrt(car.n.x);
  car.y=car.y+randn*sqrt(car.n.y);
  car.q=car.q+randn*sqrt(car.n.q);
end;

function [car]=car_new(env,car)
  car.x=rand*(env.spc/2-2*env.clr)+env.clr;
  car.y=rand*(env.spc/2-2*env.clr)+env.clr;
  car.q=rand*pi/4;

function [car]=car_mov(flag,env,car)
  switch flag,
  case 1,%with slip
    car.z.l=car.v.l+randn*sqrt(car.n.s);
    car.z.r=car.v.r+randn*sqrt(car.n.s);
  case 2,%no slip
    car.z.l=car.v.l;
    car.z.r=car.v.r;
  end;
  ds=env.tim*(car.z.r+car.z.l)/2;
  dq=env.tim*(car.z.r-car.z.l)/car.w;
  car.q=car.q+dq;
  car.x=car.x+ds*cos(car.q);
  car.y=car.y+ds*sin(car.q);

function [car]=car_see(car,mea);
  if sum(mea.s)<car.see,%not seen one or more
    car.v.l=sign(randn)*car.u.l*2;
    car.v.r=sign(randn)*car.u.r*2;
  end;

function [car]=car_avd(env,car)
  bd1=-env.spc+env.clr; bd2=env.spc-env.clr; bd3=slam_wrp(car.q);
  if car.x>bd2,%east
    if bd3>0, car.v.l=0; else car.v.r=0; end;
  end;
  if car.x<bd1,%west
    if bd3<0, car.v.l=0; else car.v.r=0; end;
  end;
  if car.y>bd2,%north
    if bd3>pi/2, car.v.l=0; else car.v.r=0; end;
  end;
  if car.y<bd1,%south
    if bd3>-pi/2, car.v.l=0; else car.v.r=0; end;
  end;

function [car]=car_obs(env,car,mea)
  [rmin,nmin]=min(mea.r);
  bmin=slam_wrp(mea.b(nmin));
  if rmin<env.clr,
    if bmin<pi/2 & bmin>0, car.v.r=0; end;
    if bmin>-pi/2 & bmin<0, car.v.l=0; end;
  end;

function [car]=car_trp(car)
  if car.v.l==0 & car.v.r==0,
    car.v.l=sign(randn)*car.u.l*2;
    car.v.r=sign(randn)*car.u.r*2;
  end;

function [car]=car_pat(car)
  cq=cos(car.q);
  sq=sin(car.q);
  car.a.x=car.x+car.w*cq*1.2;
  car.a.y=car.y+car.w*sq*1.2;
  car.b.x=car.x-car.w*sq/2;
  car.b.y=car.y+car.w*cq/2;
  car.c.x=car.x+car.w*sq/2;
  car.c.y=car.y-car.w*cq/2;
