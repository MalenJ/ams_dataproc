function []=slam_sav(flag)
%filename: slam_sav.m

global env car ecar mrk emrk mea emea;
global CAR ECAR MRK EMRK EP EV EL LST LOC P R u v x;

switch flag,
case 1,
  CAR=[CAR; car.x car.y car.q];
  ECAR=[ECAR; ecar.x ecar.y ecar.q];
  MRK=[MRK; SaveMrk(mrk)];
  EMRK=[EMRK; SaveMrk(emrk)];
  EL=[EL; sum(mea.s)];
  EP=[EP; SaveP(LST,P,mrk)];
case 2,%innovation
  EV=[EV; v'];
case 3,%save all
  eval(['save ' env.file ';']);
end;

function [m]=SaveMrk(mrk)
  for n=1:mrk.no,
    m(2*n-1:2*n)=[mrk.x(n) mrk.y(n)];
  end;

function [p]=SaveP(LST,P,mrk)
  p=zeros(1,2*mrk.no+3);
  p(1:3)=2*diag(sqrtm(P(1:3,1:3)));
  for m=1:size(LST,1),
    n=LST(m);
    p(2*n+2:2*n+3)=2*diag(sqrtm(P(2*m+2:2*m+3,2*m+2:2*m+3)));
  end;
