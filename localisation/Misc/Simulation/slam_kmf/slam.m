%filename: slam.m

clc; clear all;

global env car ecar mrk emrk mea emea;
global CAR ECAR MRK EMRK EP EV EL LST LOC P R u v x;

rand('state',0);
randn('state',0);
env.rst=rand('state');
env.nst=randn('state');

env.flg=1;%default range+bearing
env.sim=1;%simulate
env.pus=0;%pause
env.sav=50;%save data
env.file='myslam';

env.fig=figure('name','SLAM','menubar','none','numbertitle','off');

mustr=str2mat('Simulation','>SLAM','>>Tracking','>>Mapping','>>SLAM',...
              '>Repeat','>Save','>Pause','>Stop');
cbstr=str2mat('','','slam_lop(1,1);','slam_lop(1,2);','slam_lop(1,3);',...
                    'slam_lop(0);','slam_sav(3);',...
              'if env.pus==0, env.pus=1; else env.pus=0; end;','env.sim=0;');
makemenu(env.fig,mustr,cbstr);

env.run=300;%sim iteration
env.tim=0.5;%sim time step
env.spc=5;%env space
env.clr=1;%clearance
env.cvs=5;%cov scale
mustr=str2mat('Environment','>Iteration','>Time step','>Space',...
              '>Clearance','>Landmarks');
cbstr=str2mat('','slam_prm(11);','slam_prm(12);','slam_prm(13);',...
              'slam_prm(14);','slam_prm(15);');
makemenu(env.fig,mustr,cbstr);

car.u.l=0.2;%vel command
car.u.r=0.25;%vel command
car.n.s=2e-5;%slip 5mm
car.w=0.5;%car width
car.see=1;%min seen marks
car.n.x=1e-4;%model errors
car.n.y=1e-4;
car.n.q=1e-4;
mustr=str2mat('Vehicle','>Vel-left','>Vel-right','>Slip');
cbstr=str2mat('','slam_prm(21);','slam_prm(22);','slam_prm(23);');
makemenu(env.fig,mustr,cbstr);

mrk.no=10;%landmarks
mea.n.r=1e-3;%range noise 3cm
mea.n.b=1e-5;%bearing noise 1deg
mea.v=60*pi/180;%vidwing angle
mea.k=3*pi/180;%blocking angle
env.brn=0.2*pi/180;%bearing threshold
env.cov=2;%cov threshold
mustr=str2mat('Measurement','>Range noise','>Bearing noise',...
              '>Viewing angle','>Blocking angle',...
              '>Bearing Threshold','>Covarance Threshold');
cbstr=str2mat('','slam_prm(31);','slam_prm(32);',...
              'slam_prm(33);','slam_prm(34);',...
              'slam_prm(35);','slam_prm(36);');
makemenu(env.fig,mustr,cbstr);

mustr=str2mat('Graph','>Vehicle','>>Position','>>Error',...
              '>Landmark','>>Position','>>Error',...
              '>Innovation','>Senario','>Axis');
cbstr=str2mat('','','slam_plt(11);','slam_plt(12);',...
              '','slam_plt(21);','slam_plt(22);',...
              'slam_plt(31);','slam_plt(41);','axlimdlg;');
makemenu(env.fig,mustr,cbstr);

makemenu(env.fig,'Exit','close all;');
