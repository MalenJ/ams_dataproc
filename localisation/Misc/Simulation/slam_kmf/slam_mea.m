function [mea]=slam_mea(flag,car,mrk,mea)
%filename: slam_mea.m

switch flag,
case -1,%measure between marks
  mea.r=dis(car,mrk);
case 0,%measure with no noise
  mea.r=mea_rng(car,mrk);
  mea.b=mea_brn(car,mrk);
case 1,%measure with noise
  mea=slam_mea(0,car,mrk,mea);
  mea.r=mea.r+randn(1,mrk.no)*sqrt(mea.n.r);
  mea.b=mea.b+randn(1,mrk.no)*sqrt(mea.n.b);
case 2,%measure with noise & bolcking
  mea=slam_mea(1,car,mrk,mea);
  mea=mea_blk(mrk,mea);
end;

function [mea]=mea_blk(mrk,mea)
  mea.s=ones(1,mrk.no);
  for m=1:mrk.no,
    if mea.s==1,%seen
      blk=abs(slam_wrp(mea.b-mea.b(m)))<mea.k;
      if sum(blk)>1,
        mea.s(blk)=0;
        b=find(blk>0);
        [tmp,n]=min(mea.r(blk));
        mea.s(b(n))=1;%not blocked
      end;
    end;
    mea.s(m)=mea.s(m) & abs(slam_wrp(mea.b(m)))<mea.v;
  end;

function [dis]=dis(car,mrk)
  for m=1:mrk.no,
    for n=1:mrk.no,
      dis(m,n)=((mrk.x(m)-car.x(n))^2+(mrk.y(m)-car.y(n))^2)^0.5;
    end;
  end;

function [rng]=mea_rng(car,mrk)
  rng=((mrk.x-car.x).^2+(mrk.y-car.y).^2).^0.5;

function [brn]=mea_brn(car,mrk)
  brn=atan2(mrk.y-car.y,mrk.x-car.x)-car.q;
  