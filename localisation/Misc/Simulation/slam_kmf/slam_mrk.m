function [mrk]=slam_mrk(flag,mrk)
%function:slam_mrk.m

global env car mea;

switch flag,
case 0,%init real mrk
  OK=0; cnt=1; stop=100;
  while OK==0 & cnt<stop,
    mrk=mrk_new(env,mrk);
    mea=slam_mea(-1,mrk,mrk,mea);
    OK=all(all((mea.r+diag(ones(1,mrk.no))*env.clr*2)>env.clr));
    title(sprintf('Generating landmarks %d iteration',cnt));
    pause(0.00);
    cnt=cnt+1;
  end;
case 1,%init est mrk
  mea=slam_mea(1,car,mrk,mea);
  mrk.x=mea.r.*cos(mea.b);
  mrk.y=mea.r.*sin(mea.b);
end;

function [mrk]=mrk_new(env,mrk)
  for m=1:mrk.no,
    mrk.x(m)=sign(randn)*rand*env.spc;
    mrk.y(m)=sign(randn)*rand*env.spc;
  end;