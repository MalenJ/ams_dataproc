function []=slam_kmf(flag,job)
%filename: slam_kmf.m

global env car ecar mrk emrk mea emea;
global CAR ECAR MRK EMRK EP EV EL LST LOC P R u v x;

switch flag,
case 1,%range & bearing
  for m=1:mrk.no,
    if mea.s(m)==1,%mark seen
      slam_drw(12,m);
      if isempty(LST)==1,%init list
        LST=[LST;m];
        P=InitP(LST,ecar,emea,mea,P);
      elseif isempty(find(LST==m))==1,%not yet in list
        LST=[LST;m];
        P=InitP(LST,ecar,emea,mea,P);
      end;
      v=[mea.r(m)-emea.r(m); slam_wrp(mea.b(m)-emea.b(m))];
      H=JacobH(size(LST,1),ecar.x,ecar.y,emrk.x(m),emrk.y(m),find(LST==m));
    else%mark not seen
      v=[0;0];
      H=JacobH(size(LST,1),ecar.x,ecar.y,emrk.x(m),emrk.y(m),0);
    end;    
    n=size(LST,1);
    x=StateX(LST,n,ecar,emrk);
    F=JacobF(n,ecar.v.r,ecar.v.l,ecar.w,ecar.q,env.tim);
    U=JacobU(n,ecar.v.r,ecar.v.l,ecar.w,ecar.q,env.tim);
    [x,P]=Recur(x,v,u,F,U,H,P,R);
    [ecar,emrk]=Update(LST,n,x,ecar,emrk);
    switch (job),
    case 1,%tracking
      emrk=mrk;
    case 2,%mapping
      ecar=car;
    end;
    emea=slam_mea(0,ecar,emrk,mea);
    slam_sav(2);
  end;
  slam_sav(1);

case 2,%bearing only
  for m=1:mrk.no,
    if mea.s(m)==1,%mark seen
      slam_drw(12,m);
      if isempty(LOC)==1,%init temp list
        LOC=[LOC; m ecar.x ecar.y ecar.q mea.b(m)];
      elseif isempty(find(LOC==m))==1,%not yet in temp list
        LOC=[LOC; m ecar.x ecar.y ecar.q mea.b(m)];
        elseif find(LOC(:,1)==m)>0,%already in temp list
          if isempty(LST)==1,%init list
            [LOC,LST,P]=B_List(LOC,LST,m,ecar,emrk,mea,env,P);
          elseif isempty(find(LST==m))==1,%not yet in list
            [LOC,LST,P]=B_List(LOC,LST,m,ecar,emrk,mea,env,P);
          end;
      end;
      n=size(LST,1);
      if n>0,%in list
        v=slam_wrp(mea.b(m)-emea.b(m));
        H=B_JacobH(size(LST,1),ecar.x,ecar.y,emrk.x(m),emrk.y(m),find(LST==m));
      else
        v=0; H=zeros(1,2*size(LST,1)+3);
      end;
    else%mark not seen
      v=0; H=zeros(1,2*size(LST,1)+3);
    end;
    n=size(LST,1);
    x=StateX(LST,n,ecar,emrk);
    F=JacobF(n,ecar.v.r,ecar.v.l,ecar.w,ecar.q,env.tim);
    U=JacobU(n,ecar.v.r,ecar.v.l,ecar.w,ecar.q,env.tim);
    [x,P]=Recur(x,v,u,F,U,H,P,R);
    [ecar,emrk]=Update(LST,n,x,ecar,emrk);
    emea=slam_mea(0,ecar,emrk,mea);
    slam_sav(2);

  end;
  slam_sav(1);

end;

function [x0,y0,q0,b0,x1,y1,q1,b1,a0,a1,ta0,ta1,dtan,b]=B_Loc(LOC,LST,m)
  u=find(LOC(:,1)==m);
  n=find(LST(:,1)==m);
  x0=LOC(u,2); y0=LOC(u,3); q0=LOC(u,4); b0=LOC(u,5);
  x1=LST(n,2); y1=LST(n,3); q1=LST(n,4); b1=LST(n,5);
  a0=q0+b0; a1=q1+b1; ta0=tan(a0); ta1=tan(a1); dtan=ta0-ta1;
  b=(x1-x0)*ta0-y1+y0;

function [LOC,LST,P]=B_List(LOC,LST,m,ecar,emrk,mea,env,P)
  n=find(LOC(:,1)==m);
  a1=LOC(n,4)+LOC(n,5);
  a2=ecar.q+mea.b(m);
  dbrn=abs(slam_wrp(a2-a1));
  LST_=LST; P_=P;
  LST_=[LST_; m ecar.x ecar.y ecar.q mea.b(m)];
  P_=B_InitP(LOC,LST_,m,ecar,mea,P_);
u=size(LST_,1);
P1=P_(1:3,1:3);
P2=P_(2*u+2:2*u+3,2*u+2:2*u+3);
Pt=sqrt(trace(P_));
Pt1=sqrt(trace(P1));
Pt2=sqrt(trace(P2));
  if dbrn>env.brn & (Pt1+Pt2)<(env.cov+emrk.no-u),
    LST=LST_; P=P_;
  else
    LOC(n,:)=[m ecar.x ecar.y ecar.q mea.b(m)];
  end;

function [P]=B_InitP(LOC,LST,m,ecar,mea,P)
  [x0,y0,q0,b0,x1,y1,q1,b1,a0,a1,ta0,ta1,dtan,b]=B_Loc(LOC,LST,m);
  n=size(LST,1);
  G=B_JacobG(n,ta0,ta1,dtan,b);
  P(2*n+2:2*n+3,2*n+2:2*n+3)=diag([mea.n.b mea.n.b]);
  P=G*P*G';

function [G]=B_JacobG(n,ta0,ta1,dtan,b)
  G=eye(2*n+3);
  G(2*n+2:2*n+3,1:3)=[-ta1/dtan     1/dtan   -(1+ta1^2)*b/dtan^2;
                      -ta1*ta0/dtan ta0/dtan -(1+ta1^2)*ta0*b/dtan^2];
  G(2*n+2:2*n+3,2*n+2:2*n+3)=[(1+ta0^2)*b/dtan^2     -(1+ta1^2)*b/dtan^2;
                              (1+ta0^2)*ta1*b/dtan^2 -(1+ta1^2)*ta0*b/dtan^2];

function [H]=B_JacobH(n,x1,y1,x2,y2,m)
  H1=x2-x1;
  H2=y2-y1;
  H3=H1^2+H2^2;
  H=zeros(1,2*n+3);
  if m>0,
    H(1:3)=[H2/H3 -H1/H3 -1];
    H(2*m+2:2*m+3)=[-H2/H3 H1/H3];
  end;

function [x]=StateX(LST,n,ecar,emrk)
  x=zeros(2*n+3,1);
  x(1:3)=[ecar.x ecar.y ecar.q];
  for m=1:n,
    x(2*m+2)=emrk.x(LST(m));
    x(2*m+3)=emrk.y(LST(m));
  end;

function [P]=InitP(LST,ecar,emea,mea,P)
  n=size(LST,1);
  G=JacobG(n,ecar.q,emea.r(LST(n)),emea.b(LST(n)));
  P(2*n+2:2*n+3,2*n+2:2*n+3)=diag([mea.n.r mea.n.b]);
  P=G*P*G';

function [G]=JacobG(n,Qv,Ri,Qi)
  q=Qi+Qv;
  G=diag(ones(1,2*n+3));
  G(2*n+2:2*n+3,1:3)=[1 0 -Ri*sin(q);
                      0 1  Ri*cos(q)];
  G(2*n+2:2*n+3,2*n+2:2*n+3)=[cos(q) -Ri*sin(q);
                              sin(q)  Ri*cos(q)];

function [H]=JacobH(n,x1,y1,x2,y2,m)
  H1=x2-x1;
  H2=y2-y1;
  H3=H1^2+H2^2;
  H=zeros(2,2*n+3);
  if m>0,
    H(1:2,1:3)=[-H1/H3^0.5 -H2/H3^0.5  0;
                 H2/H3     -H1/H3     -1];
    H(1:2,2*m+2:2*m+3)=[ H1/H3^0.5 H2/H3^0.5;
                        -H2/H3     H1/H3];
  end;

function [F]=JacobF(n,Wr,Wl,W,Qv,t)
  q=Qv+t*(Wr-Wl)/W;
  F=diag(ones(1,2*n+3));
  F(1:2,3)=[-t/2*sin(q)*(Wr+Wl);
             t/2*cos(q)*(Wr+Wl)];

function [U]=JacobU(n,Wr,Wl,W,Qv,t)
  q=Qv+t*(Wr-Wl)/W;
  u1=t*sin(q)*(Wr+Wl);
  u2=t*cos(q)*(Wr+Wl);
  cW=cos(q)*W;
  sW=sin(q)*W;
  W2=2*W;
  U=zeros(2*n+3,2);
  U(1:3,1:2)=[t/W2*(cW-u1) t/W2*(cW+u1);
              t/W2*(sW+u2) t/W2*(sW-u2);
              t/W         -t/W         ];

function [x,P]=Recur(x,v,u,F,U,H,P,R)
  P=F*P*F'+U*u*U';
  S=H*P*H'+R;
  W=P*H'*inv(S);
  x=x+W*v;
  P=P-W*S*W';

function [ecar,emrk]=Update(LST,n,x,ecar,emrk)
  ecar.x=x(1); ecar.y=x(2); ecar.q=x(3);
  for m=1:n,
    emrk.x(LST(m))=x(m*2+2);
    emrk.y(LST(m))=x(m*2+3);
  end;
