function []=slam_drw(flag,m,job)
%filename: slam_drw.m

global env car ecar mrk emrk mea emea;
global CAR ECAR MRK EMRK EP EV EL LST LOC P R u v x;

hdl=get(gcf,'userdata');
bkcolor=get(gca,'color');
switch flag,
case 0,%init draw
  cla; subplot(1,1,1); hold on;
  axis manual equal; axis([-1.2 1.2 -1.2 1.2]*env.spc);
  line([-1.1  1.1 1.1 -1.1 -1.1]*env.spc,...
       [-1.1 -1.1 1.1  1.1 -1.1]*env.spc,'color','m');
case 1,%init real car & mark
  hdl.car.r=line([car.a.x car.b.x car.c.x car.a.x],...
                 [car.a.y car.b.y car.c.y car.a.y],...
                 'color','b','erasemode','xor');
  for m=1:mrk.no,
    %text(mrk.x(m),mrk.y(m),num2str(m),'fontsize',12,'fontweight','bold');
    hdl.mrk.r(m)=plot(mrk.x(m),mrk.y(m),'bo',...
                     'markersize',8,'erasemode','xor');
  end;
case 2,%draw real car & mark
  set(hdl.car.r,'xdata',[car.a.x car.b.x car.c.x car.a.x],...
                'ydata',[car.a.y car.b.y car.c.y car.a.y]);
  for m=1:mrk.no,
    if mea.s(m)==0,%not seen
      set(hdl.mrk.r(m),'color',bkcolor);
    else
      set(hdl.mrk.r(m),'color','c');
    end;
  end;
case 3,%init est car & eat mark
  hdl.car.e=line([ecar.a.x ecar.b.x ecar.c.x ecar.a.x],...
                 [ecar.a.y ecar.b.y ecar.c.y ecar.a.y],...
                 'color','r','erasemode','xor');
  for m=1:mrk.no,
    hdl.mrk.e(m)=plot(emrk.x(m),emrk.y(m),'r.','erasemode','xor');
  end;
case 4,%draw est car & est mark
  set(hdl.car.e,'xdata',[ecar.a.x ecar.b.x ecar.c.x ecar.a.x],...
                'ydata',[ecar.a.y ecar.b.y ecar.c.y ecar.a.y]);
  for m=1:mrk.no,
    set(hdl.mrk.e(m),'xdata',emrk.x(m),'ydata',emrk.y(m));
  end;
case 5,%init covar
  hdl.car.cov=plot(car.x,car.y,'color','b','erasemode','xor');
  for m=1:mrk.no,
    hdl.cov(m)=plot(mrk.x(m),mrk.y(m),...
                    'color','b','erasemode','xor'); 
  end;
case 6,%draw covar
  if job==1 | job==3,
    cv=drw_cov(P(1:2,1:2),car.x,car.y,env);
    set(hdl.car.cov,'xdata',cv(1,:),'ydata',cv(2,:));
  end;
  if job==2 | job==3,
  for m=1:size(LST,1),
    cv=drw_cov(P(2*m+2:2*m+3,2*m+2:2*m+3),mrk.x(LST(m)),mrk.y(LST(m)),env);
    set(hdl.cov(m),'xdata',cv(1,:),'ydata',cv(2,:));
  end;
  end;
case 11,%init seen mark
  for m=1:mrk.no,
    hdl.mrk.s(m)=line([car.x mrk.x(m)],[car.y mrk.y(m)],...
                      'color',bkcolor,'erasemode','xor');
  end;
case 12,%draw seen mark
  if mea.s(m)==1,
    set(hdl.mrk.s(m),'xdata',[car.x mrk.x(m)],...
                     'ydata',[car.y mrk.y(m)],'color','g'); pause(0.005);
    set(hdl.mrk.s(m),'xdata',[car.x mrk.x(m)],...
                     'ydata',[car.y mrk.y(m)],'color',bkcolor);
  end;
%case 13,%ray trace
%  line([car.x mrk.x(m)],[car.y mrk.y(m)],'color','g','erasemode','xor'); 
%  line([car.x mrk.x(m)],[car.y mrk.y(m)],'erasemode','xor');
end;
set(gcf,'userdata',hdl);

function [cv]=drw_cov(p,x,y,env)
  k=10;
  if trace(p)>0,
    r=sqrtm(p);
  else
    r=zeros(size(p));
  end;
  for j=1:k+1
    q = 2*pi*(j-1)/k;
    cv(:,j)=env.cvs*2*r*[cos(q);sin(q)]+[x;y];
  end