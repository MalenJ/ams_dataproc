% syms xm ym x y a b phi real
%
%
%
%
% f=atan2(ym-y-a*sin(phi)-b*cos(phi),xm-x-a*cos(phi)+b*sin(phi))-phi;
%
% %diff(f,x)
% %diff(f,y)
% %diff(f,phi)
%
% j=jacobian(f,[x y phi]);
%

a=0.5;
b=0.6;

xm=5;
ym=6;
x=2;
y=3;
phi=4;

%JH=[-(y-ym)./((xm-x).^2 + (y-ym).^2),(x-xm)./((x-xm).^2 + (ym-y).^2), -ones(size(LM_ID,1),1)];

JH=[-(y-ym+b*cos(phi)+a*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2 +(y-ym+b*cos(phi)+a*sin(phi)).^2)...
    (x-xm+a*cos(phi)-b*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)...
    (((a*cos(phi)-b*sin(phi))./(x-xm+a*cos(phi)-b*sin(phi))+((b*cos(phi)+a*sin(phi)).*(y-ym+b*cos(phi)+a*sin(phi)))./(x-xm+a*cos(phi)-b*sin(phi)).^2).*(x-xm+a*cos(phi)-b*sin(phi)).^2)./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)-1];

JH