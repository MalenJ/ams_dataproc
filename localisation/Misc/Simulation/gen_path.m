close all
clear all


%% Define Vehicle
R = 0.1;                % Wheel radius [m]
L = 0.5;                % Wheelbase [m]
dd = DifferentialDrive(R,L);


%% Simulation parameters
sampleTime = 0.1;             % Sample time [s]
tVec = 0:sampleTime:30;         % Time array
initPose = [0;0;pi/4];             % Initial pose (x y theta)
pose = zeros(3,numel(tVec));    % Pose matrix
pose(:,1) = initPose;
% Define waypoints
waypoints = [0 0; 1.25 1.75; 5.25 8.25; 7.25  8.75; 11.75  10.75; 13 14;14 10;12 6;10 4;8 1;2.00 1.00];
map=[1.25,3.75,1; 3.25, 3.25, 2; 5.25,6.25,3; 3.75,8.25,4;7.75,9.75,5;11.25,9.75,6;11.25,13.25,7;14.75,12.25,8;13.75,7.75,9;10.25,5.75,10;10.25,2.25,11;4.75,1.75,12;6.75,0.25,13];
% Create visualizer
viz = Visualizer2D;
viz.hasWaypoints = true;


%% Pure Pursuit Controller
controller = robotics.PurePursuit;
controller.Waypoints = waypoints;
controller.LookaheadDistance = 1;
controller.DesiredLinearVelocity = 1.5;
controller.MaxAngularVelocity = 1.5;


%% Simulation loop
close all
r = robotics.Rate(1/sampleTime);

for idx = 2:numel(tVec) 
    % Run the Pure Pursuit controller and convert output to wheel speeds
    [vRef,wRef] = controller(pose(:,idx-1));
    [wL,wR] = inverseKinematics(dd,vRef,wRef);
    
    % Compute the velocities
    [v,w] = forwardKinematics(dd,wL,wR);
    velB = [v;0;w]; % Body velocities [vx;vy;w]
    vel = bodyToWorld(velB,pose(:,idx-1));  % Convert from body to world
    U_kmat(:,idx-1)=[v,w];
    
    % Perform forward discrete integration step
    pose(:,idx) = pose(:,idx-1) + vel*sampleTime; 
    
    % Update visualization
    viz(pose(:,idx),waypoints)
    waitfor(r);
end