function [I,JH,S] = scooter_observe(X_k1k,P_k1k,Z,map)

map_num=size(map,1);
Z_num=size(Z,1);

%PREDICTED OBSERVATIONS

dx=map(:,1)-ones(map_num,1)*X_k1k(1,1);
dy=map(:,2)-ones(map_num,1)*X_k1k(2,1);
  
theta_p=atan2(dy,dx)-ones(map_num,1)*X_k1k(3,1);
range_p=sqrt((dx.^2)+(dy.^2));

%SELECT LANDMARKS

LM_ID=Z(:,3);
    

%INNOVATION

% pose=rad2deg(X_k1k(3,1))
% predicted=rad2deg(theta_p(LM_ID))
% observed=[Z(:,1) rad2deg(Z(:,2)) Z(:,3)]

I=Z(:,2)-theta_p(LM_ID);

%INNOVATION COVARIANCE

xm=map(LM_ID,1);
ym=map(LM_ID,2);
x=ones(size(LM_ID,1),1)*X_k1k(1,1);
y=ones(size(LM_ID,1),1)*X_k1k(2,1);
phi=ones(size(LM_ID,1),1)*X_k1k(3,1);

JH=[(ym-y)./((x-xm).^2 + (ym-y).^2),(x-xm)./((x-xm).^2 + (ym-y).^2), -ones(size(LM_ID,1),1)];

var_theta=(0.02)^2;

R=eye(size(LM_ID,1))*var_theta;

S=R + JH*P_k1k*transpose(JH);

end

