function [X_k1k1,P_k1k1] = scooter_update(X_k1k,P_k1k,I,JH,S)

if cond(S) <10
    K=P_k1k*transpose(JH)*inv(S);
else
    K=zeros(3,size(S,1));
end

X_k1k1=X_k1k+ K*I;

P_k1k1=P_k1k-K*S*transpose(K);

end

