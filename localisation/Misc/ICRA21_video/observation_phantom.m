function [X_k1k1_con,P_k1k1_con] = observation_phantom(X_k1k,P_k1k,sf_frame, Z_con,gamma)

% theta_r=wrapTo2Pi(sf_frame(1,6));
% % X_k1k
% % Z_con
% % P_k1k
% 
% innov=round(Z_con,4)-round(X_k1k,4);
% 
% if innov==0
%     
%     X_k1k1_con=X_k1k;
%     P_k1k1_con=P_k1k;
%     
% else
%     
%     %Innovation cov
%     noise=[0.05,0,0;0,0.05,0;0,0,0.05];
%     
%     %noise_rot = rotate_cov(noise,theta_r);
%     
%     S=P_k1k+noise;
%     
%     %UPDATE
%     
%     K=P_k1k*eye(3)*inv(S);
%     
%     pose_correction=K*innov
%     
%     X_k1k1_con=X_k1k+K*innov;
%     
%     %twobytwo=K*S(1:2,1:2)*transpose(K);
%     
%     % P_k1k1_con=P_k1k-[twobytwo(1,:),0;twobytwo(1,:),0;0 0 0]
%     cov_correction=K*S*transpose(K)
%     P_k1k1_con=P_k1k-K*S*transpose(K);
% end

X_k1k1_con=Z_con;
P_k1k1_con=P_k1k;

end

