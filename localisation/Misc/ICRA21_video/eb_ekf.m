close all

clear all
fileroot='2020-10-03-Wentworth';
filename='2020-10-03-Wentworth-P4';
localreadyname='TF2.mat';
CONFIGPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/config/');
load(strcat(CONFIGPATH, filename,'-prepconfig-', localreadyname))
load(strcat(CONFIGPATH, filename,'-localconfig-', localreadyname))
PATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/');
load(strcat(PATH,fileroot,'/localready/',filename,'-localready-',localreadyname),loadvars{:});
load(strcat(PATH,fileroot,'/localready/',filename,'-localready-',localreadyname),'conmap_pts');
% start_time=24;
% end_time=135;


% clear all
% fileroot='2020-02-19-Village';
% filename='2020-02-19-Village-P1';
% localreadyname='TF1.mat';
% CONFIGPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/config/');
% load(strcat(CONFIGPATH, filename,'-prepconfig-', localreadyname))
% load(strcat(CONFIGPATH, filename,'-localconfig-', localreadyname))
% PATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/');
% load(strcat(PATH,fileroot,'/localready/',filename,'-localready-',localreadyname),loadvars{:}); %localreadyRS-3.mat'
% load(strcat(PATH,fileroot,'/localready/',filename,'-localready-',localreadyname),'LMmap');
% start_time=50;
% end_time=540;
% %prep_VINS=0;
%
load('Results/Went-VINS/2020-10-03-Wentworth-P4-TF2-full.mat','gtenux','gtenuy')

rtk_enux=gtenux;
rtk_enuy=gtenuy;

% si=find(rtk_time<start_time, 1, 'last' ); %101
% ei=find(rtk_time<end_time, 1, 'last' );

% fileroot='2020-02-19-Village';
% filename='2020-02-19-Village-P1';
% localreadyname='TF1.mat';
% PATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/');
% load(strcat(PATH,fileroot,'/localready/',filename,'-localready-',localreadyname),'CL_points','CL_size','rtk_enux','rtk_enuy','conmap_pts');
% %prep_VINS=0;
% % centerX(5)=centrX(5); ONLY IF NEEDED
% % centerX(6)=centrX(6);
% CONI=imread('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/2019-09-04-Village/village_map_funk2.5 (copy).png');
% binary_map=imbinarize(rgb2gray(imcomplement(CONI)));

global collect_innov
collect_innov=0;

% CL_points(11,1:2)=[77.59,33.54];
% CL_points(13:14,13)=[1;1];
% CL_points(25:26,13)=[1;1];

global xMing
global yMing
global resg
global sizeYg
global sizeXg
global lasermap
global featuremap
global realtimeplotter
global robothan
global laserhan
global featurehan
global drawInit
global sigma_v;
global sigma_w;
global sigma_motionPd;
global sigma_motionPtheta;
global sigma_bearing;
global IG_bearing;
global sigma_edge;
global sigma_edgeP;
global sigma_bearingP;
global edge_chi_sqr;
global stationaryflag;
global nodehan
stationaryflag=0;
xMing=xMin;
yMing=yMin;
resg=res;
sizeYg=sizeY;
sizeXg=sizeX;

%Noise vals
%
% %WENTWORTH
% sigma_v=0.05;
% sigma_w=0.1;
% sigma_motionPd=0.025;
% sigma_motionPtheta=0.009;%;
% sigma_bearing=5;
% IG_bearing=0.48;
% sigma_bearingP=0;
% sigma_edge=0.15;
% sigma_edgeP=1;
% edge_chi_sqr=5;

% %VILLAGE
% sigma_v=0.01;
% sigma_w=0.1;
% sigma_motionPd=0.01;
% sigma_motionPtheta=0.009;
% sigma_bearing=5;
% IG_bearing=0.48;
% sigma_bearingP=0;
% sigma_edge=0.15; %1
% sigma_edgeP=1; %0.5
% edge_chi_sqr=5;

%VINSVALS
sigma_v=0.0222;
sigma_w=0.1;
sigma_motionPd=0.001;
sigma_motionPtheta=0.1;
sigma_bearing=5;
IG_bearing=0.48;
sigma_bearingP=0;
sigma_edge=0.15; %1
sigma_edgeP=1; %0.5
edge_chi_sqr=5;

realtimeplotter=1;
errorplotter=0;
covarianceplotter=0;

startiter=find(data_stream{:,1}<start_time, 1, 'last' );
enditer=find(data_stream{:,1}<end_time, 1, 'last' );
iter=0;
sz=25;

LMmap=correct_LM(LMmap,location);



if realtimeplotter==1
    
    drawInit = 0;
    featuremap=LMmap;
    figure(1)
    set(gcf,'color','w');
    
    lasermap = subplot(1,1,1);
    hold on
    imshow(imcomplement(binary_map))
    scatter(lasermap,(rtk_enux+xMin)./resg,sizeY-(rtk_enuy+yMin)./resg,sz-24,'r')
    
    for i=1:size(LMmap,1)
        if LMmap(i,4)==1000
            scatter(lasermap,(LMmap(i,2)+xMin)./resg,sizeY-(LMmap(i,3)+yMin)./resg,sz,'x','r')
            %text(lasermap,(LMmap(i,2)+xMin)./resg,sizeY-(LMmap(i,3)+yMin)./resg, num2str(i));
        else
            scatter(lasermap,(LMmap(i,2)+xMin)./resg,sizeY-(LMmap(i,3)+yMin)./resg,sz,'x','r')
            %text(lasermap,(LMmap(i,2)+xMin)./resg,sizeY-(LMmap(i,3)+yMin)./resg, num2str(i));
        end
    end
    
    laserhan = plot(lasermap,0,0,'g.');
    
    if covarianceplotter==1
        figure(2)
        set(gcf,'color','w');
        
        errorplotX= subplot(3,1,1);
        hold on
        eplotxhandle.error=plot(errorplotX,0,0);
        eplotxhandle.covup=plot(errorplotX,0,0);
        eplotxhandle.covdown=plot(errorplotX,0,0);
        xlabel(errorplotX,'time (s)')
        ylabel(errorplotX,'X error (m)')
        errorplotY= subplot(3,1,2);
        hold on
        eplotyhandle.error=plot(errorplotY,0,0);
        eplotyhandle.covup=plot(errorplotY,0,0);
        eplotyhandle.covdown=plot(errorplotY,0,0);
        xlabel(errorplotY,'time (s)')
        ylabel(errorplotY,'Y error (m)')
        errorplotTheta= subplot(3,1,3);
        hold on
        eplotthetahandle.error=plot(errorplotTheta,0,0);
        eplotthetahandle.covup=plot(errorplotTheta,0,0);
        eplotthetahandle.covdown=plot(errorplotTheta,0,0);
        xlabel(errorplotTheta,'time (s)')
        ylabel(errorplotTheta,'Theta error (m)')
    end
end

error_data_cnt=0;

figure(1)
path=scatter( 0,0,2,'b');
% [X1,map1]=imread('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/nodetects2.jpg');
% [X2,map2]=imread('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/ground-rgb/2020-02-19-Village-P1-1.jpg');
% [X3,map3]=imread(strcat("/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/edge/2020-02-19-Village-P1-edge-1.jpg"));
[X4,map4]=imread('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/BLANK.jpg');
yolocount=0;
conflag=2;
for i=startiter:enditer
    
    iter=iter+1;
    previous_time=data_stream{i-1,1};
    current_time=data_stream{i,1};
    EKF_time(iter)=current_time;
    current_data=data_stream{i,2}{:};
    dt=current_time-previous_time;
    
    if (i-1<startiter)
        if calc_startpose==1
            trans=[ppval(rtkinterp_x,previous_time),ppval(rtkinterp_y,previous_time),ppval(rtkinterp_z,previous_time)];
            rotz=ppval(rtkinterp_theta,previous_time);
            T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
            T_world_robot=T_world_RTK*inv(T_robot_RTK);
            start_pose=[T_world_robot(1,4)-0.15;T_world_robot(2,4);rotz]; %went
            start_cov=[(0.1)^2 0 0;0 (0.1)^2 0;0 0 (deg2rad(5))^2];
        end
        X_k=start_pose;
        P_k=start_cov;%start_cov;
    else
        iterval=1+(iter-2)*3;
        X_k=X_k1k1(:,iter-1);
        P_k=P_k1k1(1:3,iterval:iterval+2);
    end
    
    
    if prep_VINS==0
        U_k(1,1)=(ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib;
        U_k(2,1)=ppval(imu_interp,previous_time);
        [X_k1k,P_k1k] = prediction(X_k,P_k,U_k,dt);
        odomobj=odom_obsZ(U_k);
        %         lin_vel(iter)=U_k(1,1)/dt;
        %         ang_vel(iter)= U_k(2,1);
    else
        vel=(ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib;
        U_k(1,1)=(ppval(vinsx_interp,current_time)-ppval(vinsx_interp,previous_time));
        U_k(2,1)=(ppval(vinsy_interp,current_time)-ppval(vinsy_interp,previous_time));
        %U_k(3,1)=(ppval(vinsyaw_interp,current_time)-ppval(vinsyaw_interp,previous_time));
        U_k(3,1)=dt*(ppval(imu_interp,previous_time)); %-ppval(imu_interp,previous_time));
        ang_vel(iter)=U_k(3,1);
        [X_k1k,P_k1k] = prediction_VINS(X_k,P_k,U_k,dt,vel);
        odomobj=odom_obsZ(U_k);
        
    end
    
    %Prediction step
    
    X_k1k(3,1)=wrapTo2Pi(X_k1k(3,1));
    
    if (strcmp(current_data,'V') || strcmp(current_data,'O'))
        %current_data
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        drawEKFresults(odomobj, X_k1k1(:,iter));
        
    elseif (current_data=='Y')
        %current_data
        iterval=1+(iter-1)*3;
        yolocount=0;
        rows=yolo_observations.lamp_times==current_time;
        cameras_obs=yolo_observations(rows,{'yolo_index','camera','centroid_x','yolo_labels'});
        yoloD=cameras_obs{1,1};
        %[X1,map1]=imread(strcat('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/yolo/','2020-02-19-Village-P1-yrs','-',num2str(yoloD),'.0.jpg'));
        bear_obs=bearing_obZ(centerX,current_time,cameras_obs,left_camT,right_camT);
        [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),bear_obs,correctionx,correctionp] = observation_bearing(X_k1k,P_k1k,bear_obs,LMmap);
        drawEKFresults(bear_obs, X_k1k1(:,iter));
        
        % %                 Isolate
        %                 X_k1k1(:,iter)=X_k1k;
        %                 P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        %                 drawEKFresults(odomobj, X_k1k1(:,iter));
        %
    elseif(current_data=='BC')
        %current_data
        iterval=1+(iter-1)*3;
        
        yolocount=yolocount+1;
        
        img_seq=find(depthedge_time==current_time);
        if ~isnan(img_seq) %&& abs(U_k(2,1))<0.1
            depthedge_img=depthedge{img_seq};
            img_seq;
            edge_obs=edge_obsZ(BC_params,current_time,depthedge_img,T_robot_backcam,usampvec,vsampvec);
            
            if edge_obs.flagger>0 % && 2*edge_obs.flagger <2500
                [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2)] = observation_edge(X_k1k, P_k1k, edge_obs.ranges,edge_obs.bearings, dfobj);
                %%[X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2), K] = update(X_k1k, P_k1k, V, jHx, S);
                %%drawEKFresults(edge_obs, X_k1k1(:,iter));
            else
                X_k1k1(:,iter)=X_k1k;
                P_k1k1(1:3,iterval:iterval+2)=P_k1k;
                drawEKFresults(odomobj, X_k1k1(:,iter));
            end
            %             if yolocount>5
            %                 [X1,map1]=imread('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/blankwhite.jpg');
            %                 [X1,map1]=imread('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/nodetects2.jpg');
            %             end
            %             [X2,map2]=imread(strcat("/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/ground-rgb/2020-02-19-Village-P1-",num2str(img_seq), ".jpg"));
            %             [X3,map3]=imread(strcat("/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/edge/2020-02-19-Village-P1-edge-",num2str(img_seq), ".jpg"));
            %
        else
            X_k1k1(:,iter)=X_k1k;
            P_k1k1(1:3,iterval:iterval+2)=P_k1k;
            drawEKFresults(odomobj, X_k1k1(:,iter));
        end
        
        %                 %Isolate
        %                 X_k1k1(:,iter)=X_k1k;
        %                 P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        %                 drawEKFresults(odomobj, X_k1k1(:,iter));
    end
    iterval=1+(iter-1)*3;
    
    if iter==1
        %[Z_con,sf_frame] = constrain(X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),CL_points,CL_size,CL_points(1,:),conmap_pts);
        [X_k1k1_con,P_k1k1_con,sf_frame,conflag] = constrain4(X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),CL_points,CL_size,CL_points(1,:),conmap_pts);
    else
        sf_framein=sf_frame;
        %[Z_con,sf_frame] = constrain(X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),CL_points,CL_size,sf_framein,conmap_pts);
        [X_k1k1_con,P_k1k1_con,sf_frame,conflag] = constrain4(X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),CL_points,CL_size,sf_framein,conmap_pts);
    end
    %[X_k1k1_con,P_k1k1_con] = observation_phantom(X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),sf_frame, Z_con,gamma);
    
    X_k1k1_uncon(:,iter)=X_k1k1(:,iter);
    P_k1k1_uncon(1:3,iterval:iterval+2)=P_k1k1(1:3,iterval:iterval+2);
    X_k1k1(:,iter)=X_k1k1_con;
    P_k1k1(1:3,iterval:iterval+2)=P_k1k1_con;
    
    %iterval=1+(iter-1)*3;
    %outloop=rad2deg(X_k1k1(3,iter))
    
    %     figure(3)
    %     set(gcf,'color','w');
    %     set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.02, 0.5, 0.5]);
    %     subplot(1,3,1), imshow(X1,map1,'Border','tight')
    %     subplot(1,3,2), imshow(X2,map2,'Border','tight')
    %     subplot(1,3,3), imshow(X3,map3,'Border','tight')
    
        figure(4)
        set(gcf,'color','w');
    
        if conflag==2
            [X4,map4]=imread('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/BLANK.jpg');
        elseif conflag==1
            [X4,map4]=imread('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/ACCEPTED.jpg');
        else
            [X4,map4]=imread('/media/maleen/malen_ssd/phd/critical_ams_data/2020-02-19-Village/image_data/2020-02-19-Village-P1/REJECTED.jpg');
        end
    
        imshow(X4,map4,'Border','tight')
    
    
    if (~isnan(P_k1k1(1,iterval:iterval+2)))
        
        error_data_cnt=error_data_cnt+1;
        datt(error_data_cnt,:)=current_time;
        
        PK=P_k1k1(1:3,iterval:iterval+2);
        pose=X_k1k1(:,iter);
        
        p=0.95;
        s = -2 * log(1 - p);
        [V, D] = eig(PK(1:2,1:2) * s);
        t = linspace(0, 2 * pi);
        a = real((V * sqrt(D)) * [cos(t(:))'; sin(t(:))']);
        
        sigmabounds(:,error_data_cnt)=real(2*sqrt(diag(PK)));
        
        if errorplotter==1
            
            trans=[ppval(rtkinterp_x,current_time),ppval(rtkinterp_y,current_time),ppval(rtkinterp_z,current_time)];
            rotz=ppval(rtkinterp_theta,current_time);
            T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
            T_world_robot=T_world_RTK*inv(T_robot_RTK);
            GTx(error_data_cnt)=T_world_robot(1,4); %-0.15
            GTy(error_data_cnt)=T_world_robot(2,4);
            GTtheta(error_data_cnt)=rotz;
            error(:,error_data_cnt)=[GTx(error_data_cnt);GTy(error_data_cnt); GTtheta(error_data_cnt)]-pose(:,1);
            set(eplotxhandle.error,'XData',datt,'YData',error(1,:),'color', [1,0,0]);
            set(eplotyhandle.error,'XData',datt,'YData',error(2,:),'color', [1,0,0]);
            set(eplotthetahandle.error,'XData',datt,'YData',error(3,:),'color', [1,0,0]);
            
        end
        
        if realtimeplotter==1 && covarianceplotter==1
            set(eplotxhandle.covup,'XData',datt,'YData',sigmabounds(1,:),'color', [0,0,1]);
            set(eplotxhandle.covdown,'XData',datt,'YData', -sigmabounds(1,:),'color', [0,0,1]);
            
            set(eplotyhandle.covup,'XData',datt,'YData', sigmabounds(2,:),'color', [0,0,1]);
            set(eplotyhandle.covdown,'XData',datt,'YData', -sigmabounds(2,:),'color', [0,0,1]);
            
            set(eplotthetahandle.covup,'XData',datt,'YData', sigmabounds(3,:),'color', [0,0,1]);
            set(eplotthetahandle.covdown,'XData',datt,'YData', -sigmabounds(3,:),'color', [0,0,1]);
            
        end
        
        
    end
    
    Xplot = ( X_k1k1(1,:)+xMing)/resg;
    Yplot = ( X_k1k1(2,:)+yMing)/resg;
    figure(1)
    Yplot = sizeYg - Yplot;
    delete(path)
    path=scatter( Xplot,Yplot,2,'b');
    
end

%%

[RMSE]=calculate_results(rtk_time,rtk_enux,rtk_enuy,EKF_time,X_k1k1,datt,sigmabounds,T_robot_RTK,start_time,end_time,location,LMmap,conmap_pts)



