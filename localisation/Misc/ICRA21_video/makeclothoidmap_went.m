function [CL_points,CL_size] = makeclothoidmap_went(mappoints)

% close all
% clear all
% CONI=imread('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/2019-08-22-Wentworth/2019-08-22-Wentworth-conmap.png');
% xMin=25;
% yMin=25;
% sizeX=750;
% sizeY=1500;
% res=0.04;
% CONBIN=imbinarize(rgb2gray(CONI));
% [cols,rows]=find(CONBIN==0);
% px=[rows,cols];
% mappoints= img2cart(px,xMin, yMin, sizeX, sizeY, res);
% sz=1
% figure
% scatter(conmap_pts(:,1),conmap_pts(:,2),sz, 'k')
% axis equal
% h = images.roi.Polyline(gca,'Position',lines, 'Color', 'green');
% h = drawpolyline('Color','green');


lines=[-0.42,-17.25;0.75,28.88;0.85,32.07;-0.86,29.56;-14.35,22.50;-15.97,21.54;-19.08,16.08;-18.98,14.97;-3.25,-17.6;-1.78,-18.64]

CL1 = LineSegment(lines(1,:), lines(2,:));
CL2 = LineSegment(lines(2,:), lines(3,:));
CL3 = LineSegment(lines(4,:), lines(5,:));
CL4= LineSegment(lines(6,:), lines(7,:));
CL5= LineSegment(lines(8,:), lines(9,:));
CL6= LineSegment(lines(9,:), lines(10,:));
CL7= LineSegment(lines(10,:), lines(1,:));

CL1_points = point_creator(CL1,0,1,mappoints);
CL2_points = point_creator(CL2,0,1,mappoints);
CL3_points = point_creator(CL3,0,1,mappoints);
CL4_points = point_creator(CL4,0,1,mappoints);
CL5_points = point_creator(CL5,0,1,mappoints);
% CL6_points = point_creator(CL6,0,1,mappoints);
% CL7_points = point_creator(CL7,0,1,mappoints);


CL_points=[CL1_points;CL2_points;CL3_points;CL4_points;CL5_points];

CL_size=size(CL_points,1);

figure
axis equal
hold on
szEKF=2;
%scatter(rtk_enux,rtk_enuy,szEKF,'r')
scatter(mappoints(:,1),mappoints(:,2),'g')
scatter(CL_points(:,1),CL_points(:,2),szEKF,'k');
scatter(CL_points(:,9),CL_points(:,10),szEKF,'b');
scatter(CL_points(:,11),CL_points(:,12),szEKF,'r');
end