function [sf_pose,theta_r,TFmat] = gl2sf(sf_frame,gl_pose)

trans_r=sf_frame(1,1:2);
theta_r=wrapTo2Pi(sf_frame(1,6));
% gl_point_trans=gl_pose(1:2,:)-trans_r';
% 
% sf_point=[cos(-theta_r) -sin(-theta_r); sin(-theta_r) cos(-theta_r)]*gl_point_trans
% 
sf_theta=wrapTo2Pi(gl_pose(3,:)-theta_r);
% %rad2deg(sf_theta);
% 
% sf_pose=[sf_point(1:2,:);sf_theta];

TFmat=[cos(-theta_r) -sin(-theta_r) 0; sin(-theta_r) cos(-theta_r) 0;0 0 1]*[1 0 -trans_r(1,1);0 1 -trans_r(1,2);0 0 1];

posi=TFmat*[gl_pose(1:2,:);ones(1,size(gl_pose,2))];

sf_pose=[posi(1:2,:);sf_theta];

end

