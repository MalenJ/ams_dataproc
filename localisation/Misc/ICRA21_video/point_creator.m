function [CLpoints] = point_creator(CL,type,con,mappoints)

if type==0
%     point_res=0.25;
%     CL_size=CL.length()/point_res;
    iter=0;
    for i=1:2
        iter_res=iter*CL.length();
        CLpoints(i,1:2)=CL.eval(iter_res); %iter_res
        CLpoints(i,3)=iter_res; %iter_res;
        tang=CL.eval_D(iter_res); %iter_res;
        CLpoints(i,4:5)=tang;
        CLpoints(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));
        
        if con==1
            sfpoints=[mappoints, zeros(length(mappoints),1)];
            finpoints=gl2sf(CLpoints(i,:),sfpoints');
            Iupv=find(finpoints(1,:)<0.1 & finpoints(1,:)>-0.1 & finpoints(2,:)>0);
            Ilwv=find(finpoints(1,:)<0.1 & finpoints(1,:)>-0.1 & finpoints(2,:)<0);
            
            upb=min(finpoints(2,Iupv));
            lwb=max(finpoints(2,Ilwv));
            
            CLpoints(i,7:8)=[upb,lwb];
            bndpts=[zeros(1,2); CLpoints(i,7:8); zeros(1,2)];
            glbndpts=sf2gl(CLpoints(i,:),bndpts);
            CLpoints(i,9:12)=[glbndpts(1:2,1)',glbndpts(1:2,2)'];
            CLpoints(i,13)=1;
        else
            CLpoints(i,7:8)=[0,0];
            
            bndpts=[zeros(1,2); CLpoints(i,7:8); zeros(1,2)];
            glbndpts=sf2gl(CLpoints(i,:),bndpts);
            CLpoints(i,9:12)=[glbndpts(1:2,1)',glbndpts(1:2,2)'];
            CLpoints(i,13)=0;
        end
        
        
        iter=iter+1;
    end
    
else
    point_res=0.5;
    CL_size=CL.length()/point_res;
    for i=1:CL_size
        CLpoints(i,1:2)=CL.eval(point_res*i);
        CLpoints(i,3)=point_res*i;
        tang=CL.eval_D(point_res*i);
        CLpoints(i,4:5)=tang;
        CLpoints(i,6)=wrapTo2Pi(atan2(tang(2),tang(1)));
        
        CLpoints(i,7:8)=[0,0];
        
        bndpts=[zeros(1,2); CLpoints(i,7:8); zeros(1,2)];
        glbndpts=sf2gl(CLpoints(i,:),bndpts);
        CLpoints(i,9:12)=[glbndpts(1:2,1)',glbndpts(1:2,2)'];
        CLpoints(i,13)=0;
    end
    
end


end

