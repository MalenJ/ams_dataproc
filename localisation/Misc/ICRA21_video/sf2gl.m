function [gl_pose,theta_r] = sf2gl(sf_frame,sf_pose)

trans_r=sf_frame(1,1:2);
theta_r=wrapTo2Pi(sf_frame(1,6));

gl_point_rot=[cos(theta_r) -sin(theta_r); sin(theta_r) cos(theta_r)]*sf_pose(1:2,:);

gl_point=gl_point_rot+trans_r';

gl_theta=wrapTo2Pi(sf_pose(3,:)+theta_r);

gl_pose=[gl_point(1:2,:);gl_theta];
end

