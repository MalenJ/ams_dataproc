close all
%
clear all
fileroot='2020-02-19-Village';
filename='2020-02-19-Village-P1';
PATH='/home/maleen/cloud_academic/ams_data/datasets/';
load(strcat(PATH,fileroot,'/localready/',filename,'-localready7.mat'));
start_time=50;
end_time=50+400; %576;%50+455;

rosshutdown
rosinit

ricoh_data=readtable(strcat(CSVPATH, filename, '-ricoh_data.csv'));

chatpub = rospublisher('/chatter','nav_msgs/Odometry');
msg = rosmessage(chatpub);

LMmap=final_map;


global xMing
global yMing
global resg
global sizeYg
global sizeXg
global lasermap
global featuremap
global realtimeplotter
global robothan
global laserhan
global featurehan
global drawInit
global sigma_v;
global sigma_w;
global sigma_motionPd;
global sigma_motionPtheta;
global sigma_bearing;
global IG_bearing;
global sigma_edge;
global sigma_edgeP;
global sigma_bearingP;
global previouscam;
global num_detect;
global ricoh_seq;

%Noise vals for tuning

% sigma_v=0.01;
% sigma_w=0.01;
% sigma_motionPd=0.01;
% sigma_motionPtheta=0.009;%;
% sigma_bearing=2;
% IG_bearing=0.48;
% sigma_bearingP=0;
% sigma_edge=1;
% sigma_edgeP=0.5;

sigma_v=0.01;
sigma_w=0.01;
sigma_motionPd=0.01;
sigma_motionPtheta=0.009;
sigma_bearing=17;
IG_bearing=0.48;
sigma_bearingP=0;
sigma_edge=0.15; %1
sigma_edgeP=1; %0.5

realtimeplotter=1;
startiter=find(data_time<start_time, 1, 'last' );
enditer=find(data_time<end_time, 1, 'last' );
iter=0;
sz=25;

%LMmap(1,4)=1000;
%LMmap(14,4)=1000;
% LMmap(35,4)=1000;
% LMmap(6,4)=1000;

% LMmap(7,4)=1000;
LMmap(13,4)=1000;
% LMmap(38,4)=1000;
% LMmap(54,:)=[54,36.9334,16.3449,1];
% LMmap(55,:)=[55,37.0591,25.6042,1];
% LMmap(56,:)=[56,40.5994,24.6435,1];
LMmap(1,:)=[1,26.8307016465488,44.5841-0.244,4];%3.0827778179414-3
LMmap(2,:)=[2,26.436187704533065,31.782558796202157,4];
LMmap(3,:)=[3,10.267645759581542,31.782558796202157,4];
LMmap(3,4)=1000;
LMmap(5,:)=[5,0.9846,32.0317-0.24,1];
LMmap(6,:)=[6,-8.629,28.0234-0.24,1];
LMmap(7,4)=1000;
LMmap(8,:)=[8,-0.9411,24.1247,1];
LMmap(9,:)=[9,-0.97828,7.9357-0.24,1];
LMmap(9,4)=1000;
LMmap(13,:)=[13,34.732968977667845,44.5841-0.244,1];%43.133683745806540-3
LMmap(35,:)=[35,-8.5,-15,1];
LMmap(51,:)=[51,-9,0,1];
LMmap(52,:)=[52,-8.5,-21,1];
LMmap(53,:)=[53,1.1155,44.7-0.24,1];
LMmap(54,:)=[54,5.7374,44.5841-0.24,1];

%newlms
LMmap(14,:)=[14,36.612278554503840,34,1];
% LMmap(13,4)=1000;
% LMmap(25,4)=1000;
% LMmap(26,4)=1000;

%LMmap(40,4)=1000;
% LMmap(41,4)=1000;
% LMmap(42,4)=1000;
% LMmap(43,4)=1000;


LMmap(11,2)=LMmap(11,2)-0.25; %0.5
LMmap(11,3)=-3.663334286889554;

LMmap(11,4)=1000;
LMmap(2,4)=1000;
LMmap(27,4)=1000;
%LMmap(37,4)=1000;

set(gcf,'color','w');
xMing=xMin;
yMing=yMin;
resg=res;
sizeYg=sizeY;
sizeXg=sizeX;

if realtimeplotter==1
    drawInit = 0;
    featuremap=LMmap;
    figure(1)
    lasermap = subplot(1,1,1);
    hold on
    imshow(imcomplement(binary_map))
    for i=1:size(LMmap,1)
        scatter(lasermap,(LMmap(i,2)+xMin)./res,sizeY-(LMmap(i,3)+yMin)./res,40,'x','r')
        %text(lasermap,(LMmap(i,2)+xMin)./res,sizeY-(LMmap(i,3)+yMin)./res, num2str(i));
    end
    
    %atpub,msg);
    %     figure(2)
    %     errorplotX= subplot(3,1,1);
    %     hold on
    %     eplotxhandle.error=plot(errorplotX,0,0);
    %     eplotxhandle.covup=plot(errorplotX,0,0);
    %     eplotxhandle.covdown=plot(errorplotX,0,0);
    %     xlabel(errorplotX,'time (s)')
    %     ylabel(errorplotX,'X error (m)')
    %
    %     errorplotY= subplot(3,1,2);
    %     hold on
    %     eplotyhandle.error=plot(errorplotY,0,0);
    %     eplotyhandle.covup=plot(errorplotY,0,0);
    %     eplotyhandle.covdown=plot(errorplotY,0,0);
    %     xlabel(errorplotY,'time (s)')
    %     ylabel(errorplotY,'Y error (m)')
    %
    %     errorplotTheta= subplot(3,1,3);
    %     hold on
    %     eplotthetahandle.error=plot(errorplotTheta,0,0);
    %     eplotthetahandle.covup=plot(errorplotTheta,0,0);
    %     eplotthetahandle.covdown=plot(errorplotTheta,0,0);
    %     xlabel(errorplotTheta,'time (s)')
    %     ylabel(errorplotTheta,'Theta error (m)')
end

error_data_cnt=0;

% figure(2)
% dualfisheye = subplot(1,1,1);
[X4,map4]=imread(strcat("/home/maleen/image_data/ricoh/2020-02-19-Village-P1-ricohraw-", num2str(1), ".jpg"));
% imshow(handles.I);
figure(1)
path=scatter( 0,0,2,'b');

set(gcf,'color','w');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.02, 0.5, 0.5]);
[X1,map1]=imread('/home/maleen/rosbags/2020-02-19-Village/image_data/blankwhite.jpg');
[X2,map2]=imread('/home/maleen/rosbags/2020-02-19-Village/image_data/blankwhite.jpg');
[X3,map3]=imread(strcat("/home/maleen/rosbags/2020-02-19-Village/image_data/edge/2020-02-19-Village-P1-edge-1.jpg"));
yolocount=0;

%subplot(1,3,4), imshow(X4,map4)



for i=startiter:enditer
    
    iter=iter+1;
    
    previous_time=data_time(i-1);
    current_time=data_time(i);
    current_data=data_type{i};
    dt=current_time-previous_time;
    
    U_k(1,1)=(ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib;
    U_k(2,1)=ppval(imu_interp,previous_time);
    
    odomobj=odom_obsZ(U_k);
    
    fin_seq=find_img(current_time,ricoh_data);
    [X4,map4]=imread(strcat("/home/maleen/image_data/ricoh/2020-02-19-Village-P1-ricohraw-", num2str(fin_seq), ".jpg"));
    msg.Pose.Pose.Position.X = fin_seq;
    
    if (i-1<startiter)
        
        X_k=[0.14;0;0];
        P_k=[(0.1)^2 0 0;0 (0.5)^2 0;0 0 (deg2rad(2))^2]; %0.2 0.2 for Village
        
    else
        iterval=1+(iter-2)*3;
        X_k=X_k1k1(:,iter-1);
        P_k=P_k1k1(1:3,iterval:iterval+2);
        
    end
    
    
    [X_k1k,P_k1k] = prediction(X_k,P_k,U_k,dt);
    X_k1k(3,1)=wrapTo2Pi(X_k1k(3,1));
    
    if (current_data=='O')
        
        %current_data
        iterval=1+(iter-1)*3;
        X_k1k1(:,iter)=X_k1k;
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        
        drawEKFresults(odomobj, X_k1k1(:,iter));
        %[X1,map1]=imread('/home/maleen/rosbags/2020-02-19-Village/image_data/nodetects.jpg');
        %[X4,map4]=imread(strcat("/home/maleen/image_data/ricoh/2020-02-19-Village-P1-ricohraw-", num2str(fin_seq), ".jpg"));
        
    elseif (current_data=='Y') %whichcam implimentation
        
        
        rows1=yolo_observations.lamp_times==current_time;
        cameras_obs1=yolo_observations(rows1,{'yolo_index','camera','centroid_x','yolo_labels'});
        
        [wc,score] = whichcam(X_k1k,P_k1k,LMmap);
        %WHICH CAM
        previouscam=wc;
        
        if wc=='LF'
            rows2=strcmp(cameras_obs1.camera,'LF');
            msg.Pose.Pose.Position.Y = 1;
            filstring='ylf';
        elseif wc=='LB'
            rows2=strcmp(cameras_obs1.camera,'LB');
            msg.Pose.Pose.Position.Y = 2;
            filstring='ylb';
        elseif wc=='RF'
            rows2=strcmp(cameras_obs1.camera,'RF');
            msg.Pose.Pose.Position.Y = 3;
            filstring='yrf';
        elseif wc=='RB'
            rows2=strcmp(cameras_obs1.camera,'RB');
            msg.Pose.Pose.Position.Y = 4;
            filstring='yrb';
        elseif wc=='NA'
            rows2=0;
            msg.Pose.Pose.Position.Y = 5;
            filstring='blankwhite';
        end
        
        [X1,map1]=imread(strcat('/home/maleen/rosbags/2020-02-19-Village/image_data/',filstring,'.jpg'));
        
        if max(rows2>0)
            
            yolocount=0;
            cameras_obs=cameras_obs1(rows2,{'yolo_index','camera','centroid_x','yolo_labels'});
            yoloD=cameras_obs{1,1};
            bear_obs=bearing_obZ(centerX,current_time,cameras_obs);
            num_detect=max(rows2);
            iterval=1+(iter-1)*3;
            [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),bear_obs,correctionx,correctionp] = observation_bearing(X_k1k,P_k1k,bear_obs,LMmap);
            drawEKFresults(bear_obs, X_k1k1(:,iter));
            
            [X2,map2]=imread(strcat('/home/maleen/rosbags/2020-02-19-Village/image_data/',filstring,'/2020-02-19-Village-P1-',filstring,'-',num2str(yoloD),'.0.jpg'));
            
        else
            [X2,map2]=imread('/home/maleen/rosbags/2020-02-19-Village/image_data/nodetects.jpg');
            num_detect=0;
            iterval=1+(iter-1)*3;
            X_k1k1(:,iter)=X_k1k;
            P_k1k1(1:3,iterval:iterval+2)=P_k1k;
            drawEKFresults(odomobj, X_k1k1(:,iter));
        end
        %             %Isolate
        %             iterval=1+(iter-1)*3;
        %             X_k1k1(:,iter)=X_k1k;
        %             P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        %             drawEKFresults(odomobj, X_k1k1(:,iter));
        
    elseif(current_data=='BC')
        
        yolocount=yolocount+1;
        %if current_time<410 || current_time>450
        iterval=1+(iter-1)*3;
        img_seq=find(depthedge_time==current_time);
        
        if ~isnan(img_seq)
            depthedge_img=depthedge{img_seq};
            
            img_seq;
            edge_obs=edge_obsZ(BC_params,current_time,depthedge_img,T_robot_backcam);
            
            if edge_obs.flagger>0 % && 2*edge_obs.flagger <2500
                [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2)] = observation_edge(X_k1k, P_k1k, edge_obs.ranges,edge_obs.bearings, dfobj);
                %[X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2), K] = update(X_k1k, P_k1k, V, jHx, S);
                drawEKFresults(edge_obs, X_k1k1(:,iter));
            else
                X_k1k1(:,iter)=X_k1k;
                P_k1k1(1:3,iterval:iterval+2)=P_k1k;
                drawEKFresults(odomobj, X_k1k1(:,iter));
            end
            
            if yolocount>5
                [X1,map1]=imread('/home/maleen/rosbags/2020-02-19-Village/image_data/blankwhite.jpg');
                [X2,map2]=imread('/home/maleen/rosbags/2020-02-19-Village/image_data/nodetects.jpg');
            end
            % clear all
            % fileroot='2020-02-19-Village';
            % filename='2020-02-19-Village-P1';
            % PATH='/home/maleen/cloud_academic/ams_data/datasets/';
            % load(strcat(PATH,fileroot,'/localready/',filename,'-localready7.mat'));
            
            [X3,map3]=imread(strcat("/home/maleen/rosbags/2020-02-19-Village/image_data/edge/2020-02-19-Village-P1-edge-",num2str(img_seq), ".jpg"));
        else
            X_k1k1(:,iter)=X_k1k;
            P_k1k1(1:3,iterval:iterval+2)=P_k1k;
            drawEKFresults(odomobj, X_k1k1(:,iter));
        end
        
        %[X1,map1]=imread('/home/maleen/rosbags/2020-02-19-Village/image_data/nodetects.jpg');
        
        %Isolate    elseif(current_data=='BC')
        %else
        %                                         iterval=1+(iter-1)*3;
        %                                         X_k1k1(:,iter)=X_k1k;
        %                                         P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        %                                         drawEKFresults(odomobj, X_k1k1(:,iter));
        %end
    end
    
    iterval=1+(iter-1)*3;
    %     scatter(X_k1k1(1,iter),X_k1k1(2,iter),sz,'b');
    send(chatpub,msg);
    
    %dualfisheye,imshow(X4,map4);
    figure(3)
    set(gcf,'color','w');
    subplot(1,1,1), imshow(X4,map4,'Border','tight')
    figure(2)
    set(gcf,'color','w');
    subplot(1,3,1), imshow(X1,map1,'Border','tight')
    subplot(1,3,2), imshow(X2,map2,'Border','tight')
    subplot(1,3,3), imshow(X3,map3,'Border','tight')
    
    
    if (~isnan(P_k1k1(1,iterval:iterval+2)))
        
        
        %if current_time<147.5 || current_time>150
        
        error_data_cnt=error_data_cnt+1;
        
        PK=P_k1k1(1:3,iterval:iterval+2);
        pose=X_k1k1(:,iter);
        
        datt(error_data_cnt,:)=current_time;
        
        p=0.95;
        s = -2 * log(1 - p);
        
        [V, D] = eig(PK(1:2,1:2) * s);
        t = linspace(0, 2 * pi);
        
        a = (V * sqrt(D)) * [cos(t(:))'; sin(t(:))'];
        
        %         trans=[ppval(rtkinterp_x,current_time),ppval(rtkinterp_y,current_time),ppval(rtkinterp_z,current_time)];
        %         rotz=ppval(rtkinterp_theta,current_time);
        %         T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
        %         T_world_robot=T_world_RTK*inv(T_robot_RTK);
        %
        %         GTx(error_data_cnt)=T_world_robot(1,4);
        %         GTy(error_data_cnt)=T_world_robot(2,4);
        %         GTtheta(error_data_cnt)=rotz;
        %
        %         error(:,error_data_cnt)=[GTx(error_data_cnt);GTy(error_data_cnt); GTtheta(error_data_cnt)]-pose(:,1);
        sigmabounds(:,error_data_cnt)=2*sqrt(diag(PK));
        
        %         if realtimeplotter==1
        %             set(eplotxhandlestart_time.covup,'XData',datt,'YData',sigmabounds(1,:),'color', [0,0,1]);
        %             set(eplotxhandle.covdown,'XData',datt,'YData', -sigmabounds(1,:),'color', [0,0,1]);
        %             set(eplotxhandle.error,'XData',datt,'YData',error(1,:),'color', [1,0,0]);
        %
        %             set(eplotyhandle.covup,'XData',datt,'YData', sigmabounds(2,:),'color', [0,0,1]);
        %             set(eplotyhandle.covdown,'XData',datt,'YData', -sigmabounds(2,:),'color', [0,0,1]);
        %             set(eplotyhandle.error,'XData',datt,'YData',error(2,:),'color', [1,0,0]);
        %
        %             set(eplotthetahandle.covup,'XData',datt,'YData', sigmabounds(3,:),'color', [0,0,1]);
        %             set(eplotthetahandle.covdown,'XData',datt,'YData', -sigmabounds(3,:),'color', [0,0,1]);
        %             set(eplotthetahandle.error,'XData',datt,'YData',error(3,:),'color', [1,0,0]);
        %         end
        %end
        
    end
    
    %     else
    %             iterval=1+(iter-1)*3;
    %             X_k1k1(:,iter)=X_k;
    %             P_k1k1(1:3,iterval:iterval+2)=P_k;
    %     end
    
    Xplot = ( X_k1k1(1,:)+xMing)/resg;
    Yplot = ( X_k1k1(2,:)+yMing)/resg;
    figure(1)
    Yplot = sizeYg - Yplot;
    delete(path)
    path=scatter( Xplot,Yplot,2,'b');
   
end

%     XMSE=(sum(error(1,:).^2)/numel(error(1,:)));
%     YMSE=(sum(error(2,:).^2)/numel(error(2,:)));
%     MAXX=max(abs(error(1,:)));
%     MAXY=max(abs(error(2,:)));
%     %MAXTHETA=rad2deg(sum(error(3,:).^2)/numel(error(3,:)))
%     MSE=[XMSE,YMSE]
%     RMSE=[sqrt(XMSE),sqrt(YMSE)]
%     MAX=[MAXX,MAXY]


%% PLOT
gtenux=rtk_enux;
gtenuy=rtk_enuy;


for i=1:length(rtk_enuy)
    
    cunt_time=rtk_time(i);
    
    if (95<cunt_time) && (cunt_time<120) %2
        %         gtenux(i)=rtk_enux(i)%-0.25;
        
    elseif (200<cunt_time) && (cunt_time<220) %3
        %         gtenux(i)=rtk_enux(i)%-0.20;
        
    elseif (395<cunt_time) && (cunt_time<420) %4
        gtenux(i)=rtk_enux(i)+0.95;
        gtenuy(i)=rtk_enuy(i)+0.7;
        
    elseif (450<cunt_time) && (cunt_time<510) %5
        
    elseif (610<cunt_time) && (cunt_time<550) %6
        
        
    else  %ELSE 1
        gtenux(i)=rtk_enux(i)+0.25;
    end
end

xekf_interp=interp1(data_time(startiter:enditer),X_k1k1(1,:),'linear','pp');
yekf_interp=interp1(data_time(startiter:enditer),X_k1k1(2,:),'linear','pp');
theta_interp=interp1(data_time(startiter:enditer),X_k1k1(3,:),'linear','pp');

%%

rtkoffsetiter=find(rtk_time<start_time, 1, 'last' );
timeoffset=rtk_time(rtkoffsetiter);


si=find(rtk_time<start_time, 1, 'last' ); %101
ei=find(rtk_time<end_time, 1, 'last' );


for i=si:ei
    
    time=rtk_time(i);
    rtkerrortime(i)=time-timeoffset;
    
    trans=[gtenux(i),gtenuy(i),0];
    rotz=ppval(theta_interp,time);
    T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
    T_world_robot=T_world_RTK*inv(T_robot_RTK);
    GTx(i)=T_world_robot(1,4);
    GTy(i)=T_world_robot(2,4);
    GTtheta(i)=rotz;
    
    %     GTx(i)=rtk_enux(i);
    %     GTy(i)=rtk_enuy(i);
    
    x_e(i)=GTx(i)-ppval(xekf_interp,time);
    y_e(i)=GTy(i)+0.1-ppval(yekf_interp,time); %+0.1
    
    
end

sz=1;

figure
subplot(2,1,1)
set(gca,'XLim',[0 end_time-start_time]);
hold on
scatter(rtkerrortime,x_e,sz,'r');
plot(datt-start_time, sigmabounds(1,:),'b');
plot(datt-start_time, -sigmabounds(1,:),'b');
xlabel('time (s)')
ylabel('X error (m)')
hold off
subplot(2,1,2)
set(gca,'XLim',[0 end_time-start_time]);
hold on
scatter(rtkerrortime,y_e,sz,'r');
plot(datt-start_time, sigmabounds(2,:),'b');
plot(datt-start_time, -sigmabounds(2,:),'b');
hold off
xlabel('time (s)')
ylabel('Y erro(m)')


XMSE=(sum(x_e.^2)/numel(x_e));
YMSE=(sum(y_e.^2)/numel(y_e));

MAXX=max(abs(x_e));
MAXY=max(abs(y_e));
%MAXTHETA=rad2deg(sum(error(3,:).^2)/numel(error(3,:)))

MSE=[XMSE,YMSE]
RMSE=[sqrt(XMSE),sqrt(YMSE)]
MAX=[MAXX,MAXY]


figure
axis equal
hold on
szGT=1.5;
szEKF=2;

for i=1:size(LMmap,1)
    scatter(LMmap(i,2),LMmap(i,3),'x','k')
    text(LMmap(i,2),LMmap(i,3), num2str(LMmap(i,1)));
end

%scatter(rtabx,rtaby,szGT,'r')
scatter(GTx,GTy,szGT,'r')
scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b');
xlabel('X (m)')
ylabel('Y (m)')