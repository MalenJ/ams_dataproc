function [P_k1k1] = observation_pbearing(cam,X_k1k,P_k1k,LMmap)

global sigma_bearing;
global IG_bearing;
global sigma_bearingP;

var_theta=(deg2rad(sigma_bearing))^2;
IG_gate=IG_bearing;

map_num=size(LMmap,1);

if (cam=='LC')
    a=-0.037;
    b=0.1073;
elseif (cam=='RC')
    a=-0.0292;
    b=-0.0895;
elseif (cam=='LF')
    a=-0.0149;
    b=0.0144;
    bear=1.0472;
elseif (cam=='LB')
    a=0.0679;
    b=0.0522;
    bear=2.0944;
elseif (cam=='RF')
    a=0.0061;
    b=0.0030;
    bear=-1.0472;
elseif (cam=='RB')
    a=0.0473;
    b=-0.0701;
    bear=-2.0944;
end

xr = X_k1k(1,1) + a*cos(X_k1k(3,1)) - b*sin(X_k1k(3,1));
yr = X_k1k(2,1) + a*sin(X_k1k(3,1)) + b*cos(X_k1k(3,1));

dx=LMmap(:,2)-ones(map_num,1)*xr;
dy=LMmap(:,3)-ones(map_num,1)*yr;

theta_p=wrapTo2Pi(atan2(dy,dx)-ones(map_num,1)*X_k1k(3,1));
range_p=sqrt((dx.^2)+(dy.^2));

select_indx_range=range_p <10;
select_indx_theta=wrapTo2Pi(bear-deg2rad(30)) < theta_p & theta_p < wrapTo2Pi(bear+deg2rad(30));


logicselect=select_indx_range.*select_indx_theta;
select_indx=find(logicselect>0);

if ~isempty(select_indx)
    
    p_obs=theta_p(select_indx);
    pobs_size=length(p_obs);
    
    xm=LMmap(select_indx,1);
    ym=LMmap(select_indx,2);
    
    x=ones(pobs_size,1)*X_k1k(1,1);
    y=ones(pobs_size,1)*X_k1k(2,1);
    phi=ones(pobs_size,1)*X_k1k(3,1);
    
    JH=[-(y-ym+b*cos(phi)+a*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2 +(y-ym+b*cos(phi)+a*sin(phi)).^2)...
        (x-xm+a*cos(phi)-b*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)...
        (((a*cos(phi)-b*sin(phi))./(x-xm+a*cos(phi)-b*sin(phi))+((b*cos(phi)+a*sin(phi)).*(y-ym+b*cos(phi)+a*sin(phi)))./(x-xm+a*cos(phi)-b*sin(phi)).^2).*(x-xm+a*cos(phi)-b*sin(phi)).^2)./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)-1];
    
    R=eye(pobs_size)*var_theta;
    
    S=R + JH*P_k1k*transpose(JH);
    
    S=S +sigma_bearingP * eye(size(S));
    
    %UPDATE
    
    if cond(S) <10
        K=P_k1k*transpose(JH)*inv(S);
    else
        K=zeros(3,size(S,1));
    end
    
    %disp("Intigrating bearing observation")
    
    P_k1k1=P_k1k-K*S*transpose(K);

else
    P_k1k1=P_k1k;
    
end

