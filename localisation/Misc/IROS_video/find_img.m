function [img_sequence] = find_img(time,ricohdat)

global ricoh_seq;

ricoh_time=ricohdat{:,3};
seqs=ricohdat{:,2};

startiter=find(ricoh_time<=time, 1, 'last' );


if ~isnan(startiter)
  seq_num=seqs(startiter); 
  ricoh_seq=[ricoh_seq; seq_num];
end


img_sequence=ricoh_seq(end)

end

