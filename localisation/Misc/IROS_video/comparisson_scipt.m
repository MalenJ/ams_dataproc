close all
clear all

fileroot='2020-02-19-Village';
filename='2020-02-19-Village-P1';

CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
MAPPATH_feature='/home/maleen/cloud_academic/ams_data/map_data/'; %MAP DATA

rtab_data=readtable(strcat(CSVPATH, filename, '-rtabdata.txt'));
orb_data=readtable(strcat(CSVPATH, filename, '-orbdata.csv'));
vins_data=readtable(strcat(CSVPATH, filename, '-vinsdata.csv'));

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/IROScalib/'; %CLIB DATA

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');

load(strcat(MAPPATH_feature,'/el_maps/2019-09-04-Village/2019-09-04-Village-ELMAP.mat'),'final_map');
LMmap=final_map;


roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;


%% RTAB
%
rtab_time=rtab_data{:,1}-1582057675.511864159;
rtab_x=rtab_data{:,2};
rtab_y=rtab_data{:,3};
rtab_theta=rtab_data{:,7};
% rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);
%

%% ORB DATA


orb_time=orb_data{:,3};
orbx=orb_data{:,4};
orby=orb_data{:,5};
orbz=orb_data{:,6};
orbquat=orb_data{:,7:10};
orbbeul = quat2eul([orbquat(:,4) orbquat(:,1:3)]);

for i=1:length(orb_time)
    
    trans=[orbx(i),orby(i),orbz(i)];
    roter = eul2tform(orbbeul(i,:));
    T_world_cam=makehgtform('translate',trans)*roter;
    T_world_robot=T_world_cam*(-T_robot_backcam);
    orbxt(i,1)=T_world_robot(1,4);
    orbyt(i,1)=T_world_robot(2,4);
end



%% VINS DATA

vins_time=vins_data{:,3};
vinsx=vins_data{:,4};
vinsy=vins_data{:,5};
vinsz=vins_data{:,6};
vinsquat=vins_data{:,7:10};
vinsbeul = quat2eul([vinsquat(:,4) vinsquat(:,1:3)]);

for i=1:length(vins_time)
    
    trans=[vinsx(i),vinsy(i),vinsz(i)];
    roter = eul2tform(orbbeul(i,:));
    T_world_cam=makehgtform('translate',trans)*roter;
    T_world_robot=T_world_cam*(-T_robot_backcam);
    vinsxt(i,1)=T_world_robot(1,4);
    vinsyt(i,1)=T_world_robot(2,4);
end


%figure


%%
rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));
rtk_time=rtk_data{:,2};

load('ekf_results1.mat')
% start_time=50;
% end_time=50+400;
% rtkoffsetiter=find(rtk_time<start_time, 1, 'last' );
% timeoffset=rtk_time(rtkoffsetiter);


orbx_interp=interp1(orb_time,orbxt,'linear','pp');
orby_interp=interp1(orb_time,orbyt,'linear','pp');
vinsx_interp=interp1(vins_time,vinsxt,'linear','pp');
vinsy_interp=interp1(vins_time,vinsyt,'linear','pp');
rtabx_interp=interp1(rtab_time,rtab_x,'linear','pp');
rtaby_interp=interp1(rtab_time,rtab_y,'linear','pp');

si=find(rtk_time<start_time, 1, 'last' ); %101
ei=find(rtk_time<end_time, 1, 'last' );
for i=si:ei
    time=rtk_time(i);
    rtkerrortime(i)=time-timeoffset;
    trans=[gtenux(i),gtenuy(i),0];
    rotz=ppval(theta_interp,time);
    T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
    T_world_robot=T_world_RTK*inv(T_robot_RTK);
    GTx(i)=T_world_robot(1,4);
    GTy(i)=T_world_robot(2,4);
    GTtheta(i)=rotz;
    
    
    rtabx_e(i)=GTx(i)-ppval(rtabx_interp,time);
    rtaby_e(i)=GTy(i)-ppval(rtaby_interp,time);
    orbx_e(i)=GTx(i)-ppval(orbx_interp,time);
    orby_e(i)=GTy(i)-ppval(orby_interp,time);
    vinsx_e(i)=GTx(i)-ppval(vinsx_interp,time);
    vinsy_e(i)=GTy(i)-ppval(vinsy_interp,time);

end

RTABXMSE=(sum(rtabx_e.^2)/numel(rtabx_e));
RTABYMSE=(sum(rtaby_e.^2)/numel(rtaby_e));
ORBXMSE=(sum(orbx_e.^2)/numel(orbx_e));
ORBYMSE=(sum(orby_e.^2)/numel(orby_e));
VINSXMSE=(sum(vinsx_e.^2)/numel(vinsx_e));
VINSYMSE=(sum(vinsy_e.^2)/numel(vinsy_e));

RTABRMSE=[sqrt(RTABXMSE),sqrt(RTABYMSE)];
ORBRMSE=[sqrt(ORBXMSE),sqrt(ORBYMSE)];
VINSRMSE=[sqrt(VINSXMSE),sqrt(VINSYMSE)];

%%


orb_start=find(orb_time<start_time, 1, 'last' ); 
orb_end=find(orb_time<end_time, 1, 'last' );
rtab_start=find(rtab_time<start_time, 1, 'last' ); 
rtab_end=find(rtab_time<end_time, 1, 'last' );
vins_start=find(vins_time<start_time, 1, 'last' ); 
vins_end=find(vins_time<end_time, 1, 'last' );


figure
axis equal
hold on
szGT=8;
szEKF=4;

scatter(X_k1k1(1,:),X_k1k1(2,:),szEKF,'b','filled');
scatter(vinsxt(vins_start:vins_end),vinsyt(vins_start:vins_end),szEKF,'c','filled')
scatter(orbxt(orb_start:orb_end),orbyt(orb_start:orb_end),szEKF,'g','filled')
scatter(rtab_x(rtab_start:rtab_end),rtab_y(rtab_start:rtab_end),szGT,'m','filled')
scatter(GTx,GTy,szGT,'r','filled')


for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3),8,'x','k')
    %text(LMmap(i,2),LMmap(i,3), num2str(i));
end

xlabel('X (m)')
ylabel('Y (m)')

%%

% figure
% hold on
% scatter(x_e)
% scatter(rtabx_e)
% scatter(orbx_e)
% scatter(vinsx_e)

%% NEW rmse

RTAB_RMSE=sqrt(RTABRMSE(1,1)^2+RTABRMSE(1,2)^2)
ORB_RMSE=sqrt(ORBRMSE(1,1)^2+ORBRMSE(1,2)^2)
VINS_RMSE=sqrt(VINSRMSE(1,1)^2+VINSRMSE(1,2)^2)
active=sqrt(0.0588^2+0.0959^2)
uncons=sqrt(0.0663^2+0.0891^2)
