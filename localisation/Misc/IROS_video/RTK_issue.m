%ALways get the location of two of your landmarks
clear all
close all

load('/home/maleen/cloud_academic/ams_data/datasets/2019-09-07-Wentworth/localready/2019-09-07-Wentworth-localready3.mat','rtk_enux','rtk_enuy')

map_enux=rtk_enux;
map_enuy=rtk_enuy;

fileroot='2020-02-15-Wentworth';
filename='2020-02-15-Wentworth-P3';CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};

% base_lat=mean(rtk_lat(1:rtk_iter));
% base_long=mean(rtk_long(1:rtk_iter));
% base_alti=mean(rtk_alti(1:rtk_iter));

%Wentworthmap
base_lat=-33.878911084893204;
base_long=1.511945376668094e+02;
base_alti=26.187976892944185;

%Villagemap
% base_lat=-33.880632824130295;
% base_long=1.511895158304587e+02;
% base_alti=43.434044433462200;

[rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);

current_enux=rtk_enux;
current_enuy=rtk_enuy;

figure
axis equal
hold on
axis
plot(map_enux,map_enuy,'b')
plot(current_enux+0.83,current_enuy-1.6,'r')