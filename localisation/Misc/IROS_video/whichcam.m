function [wc,score] = whichcam(X_k1k,P_k1k,LMmap)


global previouscam;
global num_detect;

[P_k1k1_lf] = observation_pbearing('LF',X_k1k,P_k1k,LMmap);
[P_k1k1_lb] = observation_pbearing('LB',X_k1k,P_k1k,LMmap);
[P_k1k1_rf] = observation_pbearing('RF',X_k1k,P_k1k,LMmap);
[P_k1k1_rb] = observation_pbearing('RB',X_k1k,P_k1k,LMmap);


lf_score=trace(P_k1k)-trace(P_k1k1_lf);
lb_score=trace(P_k1k)-trace(P_k1k1_lb);
rf_score=trace(P_k1k)-trace(P_k1k1_rf);
rb_score=trace(P_k1k)-trace(P_k1k1_rb);


score=table([lf_score;lb_score;rf_score;rb_score],['LF';'LB';'RF';'RB']);
score.Properties.VariableNames = {'score' 'camera'};



if(lf_score==0 && lb_score==0 && rf_score==0 && rb_score==0)
    wc='NA';
    
else
    score = sortrows(score,'score');
    wchighest=score{4,2};
    QF=((score{4,1}-score{3,1})/score{4,1})*100;
    
    if strcmp(previouscam,wchighest)
        
        if QF<25 || num_detect<1
            
            wc=score{3,2};
            %disp('2nd highest!')
            
        else
            wc=score{4,2};
        end
        
    else
        wc=score{4,2};
    end
    
    
end

%wc


end



