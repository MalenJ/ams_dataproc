% %% WENTWORTH =0
% clear all
% close all
% 
% fileroot='2020-02-28-Wentworth';
% filename='2020-02-28-Wentworth-P1';
% disp(strcat('Prep','',filename))
% 
% CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/IROScalib/'; %CALIB DATA
% disp(strcat('Calib path :',CALIBPATH))
% 
% location=0;
% data_zero=1582835388.77570145; %NEED TO SET IF COMPARISSON OR FIX
% %COMPARISSON
% stationary_time=20;
% 
% %Maps
% el_map='/RTK_maps/2019-03-22-Wentworth-map/2019-03-22-Wentworth-satmap.mat';
% maptype='map_sat';
% ed_map='2019-08-22-Wentworth/2019-08-22-Wentworth-map-clean4.png';
% clothoid_map=1;
% con_map='2019-08-22-Wentworth/2019-08-22-Wentworth-conmap.png';
% 
% %RTK
% T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]); %IROS CALIB
% %T_robot_RTK=makehgtform('translate',[-0.1347 0.0136 1.4697]); %ICRA2021 CALIB
% calc_RTK_base=0; %0:use set base vals, 1:use set iter vals, 2: use mean
% 
% if calc_RTK_base==0
%     %Wentworth ICRA 2020 OG map base
%     base_lat=-33.878911084893204;
%     base_long=1.511945376668094e+02;
%     base_alti=26.187976892944185;
% elseif (calc_RTK_base==1)  
%      
% end
% 
% calc_RZ=0; 
% RTK_tx=0.8034; %IROS 2020
% RTK_ty=-1.5209;
% % RTK_tx=1.8412; ICRA 2021
% % RTK_ty=-2.8224;
% 
% %Edge map parameters
% 
% xMin=25;
% yMin=25;
% sizeX=750;
% sizeY=1500;
% res=0.04;
% 
% prep_RTAB=0;
% prep_VINS=1;


%% VILLAGE =1
clear all
close all

fileroot='2020-02-19-Village';
filename='2020-02-19-Village-P1';
disp(strcat('Prep: ',filename))

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/IROScalib/'; %CALIB DATA
disp(strcat('Calib path :',CALIBPATH))

location=1;
data_zero=1582057675.511864159;

stationary_time=40;

%Maps
el_map='/el_maps/2019-09-04-Village/2019-09-04-Village-ELMAP.mat';
maptype='final_map';
ed_map='2019-09-04-Village/village_map_funk2.5.png';
clothoid_map=1;
con_map='2019-09-04-Village/village_conmap2.png';

%RTK
T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]); %IROS CALIB
calc_RTK_base=1; %0:use set base vals, 1:use set iter vals, 2: use mean

if calc_RTK_base==0
    % %VILLAGE ICRA
    % base_lat=-33.880632824130295;
    % base_long=1.511895158304587e+02;
    % base_alti=43.434044433462200;
elseif (calc_RTK_base==1)  
     RTK_base_iter=421;
end

calc_RZ=1; 
RTK_tx=0;
RTK_ty=0;

%Edge map parameters
xMin=10;
yMin=40;
sizeX=2375;
sizeY=2000;
res=0.04;

prep_RTAB=0;
prep_VINS=1;


%% UTS =2
% clear all
% close all
% 
% fileroot='2021-01-11-UTS';
% filename='2021-01-11-UTS-P1';
% disp(strcat('Prep: ',filename))
% 
% CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/ICRA2021calib/'; %CALIB DATA
% disp(strcat('Calib path :',CALIBPATH))
% 
% location=2;
% data_zero=1610354651.436696707;
% 
% stationary_time=15;
% 
% %Maps
% el_map='/el_maps/2021-01-11-UTS/2021-01-11-UTS-ELMAP.mat';
% maptype='final_map';
% ed_map='2021-01-11-UTS/UTS-EDGE-10.png';
% clothoid_map=1;
% con_map='2021-01-11-UTS/UTS-EDGE-8.5-conmap.png';
% 
% %RTK
% T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]); %IROS CALIB
% calc_RTK_base=0; %0:use set base vals, 1:use set iter vals, 2: use mean
% 
% if calc_RTK_base==0
%     %VILLAGE ICRA
%     base_lat=-33.880632824130295;
%     base_long=1.511895158304587e+02;
%     base_alti=43.434044433462200;
% elseif (calc_RTK_base==1)  
%      RTK_base_iter=421;
% end
% 
% calc_RZ=0; 
% RTK_tx=0;
% RTK_ty=0;
% 
% %Edge map parameters
% xMin=30;
% yMin=195;
% sizeX=5000;
% sizeY=5000;
% res=0.04;
% 
% prep_RTAB=1;
% prep_VINS=1;

