% %% WENTWORTH
% close all
% clear all
% 
% fileroot='2020-02-28-Wentworth';
% filename='2020-02-28-Wentworth-P1';
% PATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/');
% 
% loadvars={'data_stream','enc_interp','imu_interp','rtk_time','rtkinterp_x','rtkinterp_y','rtkinterp_theta','calib'...
%     'rtkinterp_z','T_robot_RTK','rtk_enux','rtk_enuy'...
%     'yolo_observations','depthedge_time','depthedge','dfobj'...
%     'T_robot_RTK','T_robot_backcam','BC_params','centerX'...
%     'LMmap','binary_map','ANGZ'...
%     'vinsx_interp','vinsy_interp','vinsyaw_interp','CL_points','CL_size','conmap_pts','rtab_time','rtabx','rtaby','rtabinterp_x','rtabinterp_y','rtabinterp_theta'};
% 
% start_time=24;
% end_time=135;
% 
% calc_startpose=0;
% 
% if calc_startpose==0
%     %     start_pose=[1.315;26.77;-1.57079632679490];
%     %     start_cov=[(0.1)^2 0 0;0 (0.1)^2 0;0 0 (deg2rad(2))^2]; %ICRA 2021
%     
%     start_pose=[-0.38;-14.28;deg2rad(90)];
%     start_cov=[(0.1)^2 0 0;0 (0.1)^2 0;0 0 (deg2rad(2))^2]; %IROS 2020
%     
%     %     start_pose=[0.14;0;0];
%     %     start_cov=[(0.1)^2 0 0;0 (0.5)^2 0;0 0 (deg2rad(2))^2]; %0.2 0.2 for Village
%     % start_pose=[26.0996;-29.7645; 1.5708];%1.5708 1.4830
%     % start_cov=[(0.1)^2 0 0;0 (0.1)^2 0;0 0 (deg2rad(2))^2]; %0.2 0.2 for Village
% end
% 
% %Yolo params
% 
% % %ICRA 2021 VALUES
% % left_camT=[-0.1628, 0.1061]; 
% % right_camT=[-0.1593, -0.0636]; 
% % ANGZ=[0.8109,(2*pi-5.4676)];
% 
% %IROS 2020 VALUES
% left_camT=[-0.1540, 0.1056]; 
% right_camT=[-0.1574, -0.0644]; 
% ANGZ=[0.8285,(2*pi-5.4789)];
% 
% %Edge params
% 
% %usampvec=floor(linspace(450,150,(450-150)/4));%150-450 VILLAGE best
% usampvec=floor(linspace(450,300,(450-300)/2)); %Wentworth
% vsampvec=floor(linspace(1,640,(640-1)/4)); %639


%% Village
close all
clear all

fileroot='2020-02-19-Village';
filename='2020-02-19-Village-P1';
PATH='/home/maleen/cloud_academic/ams_data/datasets/';

loadvars={'data_stream','enc_interp','imu_interp','rtk_time','rtkinterp_x','rtkinterp_y','rtkinterp_theta','calib'...
           'rtkinterp_z','T_robot_RTK','rtk_enux','rtk_enuy'...
           'yolo_observations','depthedge_time','depthedge','dfobj'...
           'T_robot_RTK','T_robot_backcam','BC_params','centerX'...
           'LMmap','binary_map'...
           'vinsx_interp','vinsy_interp','vinsyaw_interp','CL_points','CL_size','conmap_pts','rtab_time','rtabx','rtaby','rtabinterp_x','rtabinterp_y','rtabinterp_theta'};

start_time=50;
end_time=540;

calc_startpose=0;

if calc_startpose==0
    start_pose=[0.14;0;0];
    start_cov=[(0.1)^2 0 0;0 (0.5)^2 0;0 0 (deg2rad(2))^2]; %0.2 0.2 for Village
%     start_pose=[0.14;0;0];
%     start_cov=[(0.1)^2 0 0;0 (0.5)^2 0;0 0 (deg2rad(2))^2]; %0.2 0.2 for Village
    % start_pose=[26.0996;-29.7645; 1.5708];%1.5708 1.4830
    % start_cov=[(0.1)^2 0 0;0 (0.1)^2 0;0 0 (deg2rad(2))^2]; %0.2 0.2 for Village
end

% %ICRA 2021 VALUES
% left_camT=[-0.1628, 0.1061]; 
% right_camT=[-0.1593, -0.0636]; 
% ANGZ=[0.8109,(2*pi-5.4676)];

%IROS 2020 VALUES
left_camT=[-0.1540, 0.1056]; 
right_camT=[-0.1574, -0.0644]; 
ANGZ=[0.8285,(2*pi-5.4789)];

%Edge params

usampvec=floor(linspace(450,150,(450-150)/4));%150-450 VILLAGE best
%usampvec=floor(linspace(450,300,(450-300)/2)); %Wentworth
vsampvec=floor(linspace(1,640,(640-1)/4)); %639

%% UTS
% close all
% clear all
% 
% disp('UTS')
% 
% fileroot='2021-01-11-UTS';
% filename='2021-01-11-UTS-P1';
% PATH='/home/maleen/cloud_academic/ams_data/datasets/';
% 
% loadvars={'data_stream','enc_interp','imu_interp','rtk_time','rtkinterp_x','rtkinterp_y','rtkinterp_theta','calib'...
%            'rtkinterp_z','T_robot_RTK','rtk_enux','rtk_enuy'...
%            'yolo_observations','depthedge_time','depthedge','dfobj'...
%            'T_robot_RTK','T_robot_backcam','BC_params','centerX'...
%            'LMmap','binary_map'...
%            'vins_time','vins_x','vins_y','vinsx_interp','vinsy_interp','vinsyaw_interp','CL_points','CL_size','conmap_pts','rtab_time','rtabx','rtaby','rtabinterp_x','rtabinterp_y','rtabinterp_theta'};
% %'vinsx_interp','vinsy_interp','vinsyaw_interp'
% 
% start_time=15;
% end_time=800;
% 
% calc_startpose=0;
% 
% if calc_startpose==0
%     start_pose=[0.0244;-0.0231;0];
%     start_cov=[(0.05)^2 0 0;0 (0.05)^2 0;0 0 (deg2rad(1))^2]; %0.2 0.2 for Village
% %     start_pose=[0.14;0;0];
% %     start_cov=[(0.1)^2 0 0;0 (0.5)^2 0;0 0 (deg2rad(2))^2]; %0.2 0.2 for Village
%     % start_pose=[26.0996;-29.7645; 1.5708];%1.5708 1.4830
%     % start_cov=[(0.1)^2 0 0;0 (0.1)^2 0;0 0 (deg2rad(2))^2]; %0.2 0.2 for Village
% end
% 
% %Yolo params
% 
% %%ICRA 2021 VALUES
% left_camT=[-0.1628, 0.1061]; 
% right_camT=[-0.1593, -0.0636]; 
% ANGZ=[0.8109,(2*pi-5.4676)];
% 
% 
% %Edge params
% 
% usampvec=floor(linspace(450,200,(450-200)/4));%150-450 VILLAGE best
% %usampvec=floor(linspace(450,300,(450-300)/2)); %Wentworth
% vsampvec=floor(linspace(1,640,(640-1)/4)); %639