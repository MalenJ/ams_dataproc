clear all
close all

load(strcat('/home/maleen/git/ams_primary/mapping/2019-03-22-Wentworth-P4-prelabel.mat'))


enctimes=[0; deadreck_data{:,5}];
encdist=deadreck_data{:,3};
h=1;

for i=1:length(rtk_time)-1

    rtkXdist=rtk_enux(i)-rtk_enux(i);
    rtkYdist=rtk_enuy(i)-rtk_enuy(i);
    
    sqrt(rtkXdist^2+rtkYdist^2)
    rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
    rtk_heading(i,:)=wrapTo2Pi(atan2(rtkYdist,rtkXdist));
    rtkdist_timer(i,:)=rtk_time(i+1,:);

end


vel_enc=encdist(:,1)./diff(enctimes(:,1));

vel_rtk=rtkdist(:,1)./rtkdist_time(:,1);

new_rtk_vel=diff(sqrt(rtk_enux.^2+rtk_enuy.^2))./diff(rtk_time(:,1));



vel_enc_interp=spline([0; deadreck_data{:,5}],[0; vel_enc]);
vel_rtk_interp=spline([0; rtkdist_time],[0; vel_rtk]);

vel_error=ppval(vel_rtk_interp,deadreck_data{:,5})-vel_enc;


% figure 
% hold on
% 
% plot(rtkdist_time,new_rtk_vel,'b')
% %plot(rtkdist_time,rtkdist,'r')
% 
% 
% figure 
% hold on
% 
% %plot(deadreck_data{:,5},encdist,'b')
% plot(rtkdist_time,rtkdist,'r')
% 
% plot(rtkdist_time,rtk_heading)


figure 
hold on

plot(deadreck_data{(949:1337),5},vel_enc(949:1337),'b')
% plot(rtkdist_time,vel_rtk,'r')
% 
% plot(deadreck_data{:,5},vel_error ,'g')
mean=mean(vel_enc(949:1337))
var=sqrt(var(vel_enc(949:1337)))

% figure
% 
% hist(vel_error)
% mean(vel_error)
% var(vel_error)
