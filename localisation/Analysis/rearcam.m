clear all
close all
bag = rosbag('/home/maleen/rosbags/village-test.bag');

depthImages = select(bag,'Topic', '/front_cam/aligned_depth_to_color/image_raw');
firstDepthImage = readMessages(depthImages, 450);

RGBImages = select(bag,'Topic', '/front_cam/color/image_raw');
firstRGBImage = readMessages(RGBImages, 450);

d_img=double(readImage(firstDepthImage{1}))/1000;
rgb_img=readImage(firstRGBImage{1});

% figure
% imshow(rgb_img)
% figure
% imshow(uint8(double(d_img)/6000*256))

middlesec=d_img(240:400,:);

imshow(rgb_img(240:400,:))