clear all
close all

load(strcat('/home/maleen/git/ams_primary/mapping/2019-02-11-Wentworth-gtmap.mat'))
load(strcat('/home/maleen/git/ams_primary/mapping/2019-02-11-Wentworth-P7-prelabel.mat'))
load(strcat('/home/maleen/git/ams_primary/mapping/2019-02-11-Wentworth-P7-yololabeled.mat'))

LM=yolo_ex{:,13};
yolo_index=yolo_ex{:,2};
lamp_THETA=yolo_ex{:,10};
bearings=yolo_ex{:,5};
lamp_RTKX=yolo_ex{:,8};
lamp_RTKY=yolo_ex{:,9};
camera=yolo_ex{:,4};

mapsize=15;

%% CALCULATING OBSERVATION ERROR

a=0.055;

for i=1:length(bearings)
    
    cam=camera{i};
    
if (cam=='C1')
    
    b=0.055;
    observation(i,1)=wrapTo2Pi(pi/4-bearings(i));
    
elseif (cam=='C2')
    
    b=-0.055;
    observation(i,1)=wrapTo2Pi(-(pi/4+bearings(i)));
    
end

xr = lamp_RTKX(i,1) + a*cos(lamp_THETA(i,1)) - b*sin(lamp_THETA(i,1));
yr = lamp_RTKY(i,1) + a*sin(lamp_THETA(i,1)) + b*cos(lamp_THETA(i,1));

robot_pose(i,:)=[lamp_RTKX(i,1),lamp_RTKY(i,1),lamp_THETA(i,1)];
camera_location(i,:)=[xr,yr];

landmark=LM(i);

dx=gtmap_enu(1,:)-ones(1,mapsize)*xr;
dy=gtmap_enu(2,:)-ones(1,mapsize)*yr;

predicted_bearing(i,:)=wrapTo2Pi(atan2(dy,dx)-(ones(1,mapsize))*lamp_THETA(i,1));

predicted_range(i,:)=sqrt((dx.^2)+(dy.^2));

if landmark>0
obs_error(i,1)=predicted_bearing(i,landmark)-observation(i,1);

else
  obs_error(i,1)=1000;
  end
    
end



observation_data = table(lamp_time,yolo_index,robot_pose,camera_location,camera,LM,observation,predicted_bearing,predicted_range,obs_error);

writetable(observation_data,'observation_data.csv')

%% PLOTTING DATA


j=1;

figure
subplot(1,2,1)
hold on
plot(enu_rot(1,:), enu_rot(2,:))
scatter(gtmap_enu(1,:),gtmap_enu(2,:))
axis('equal')



while(mapsize>0)
    
    w=waitforbuttonpress;
    
    if (w==1)
        j
        LMS=find(LM==j);
        
        subplot(1,2,2)
        stem(rad2deg(obs_error(LMS)))
        %scatter(rad2deg(predicted_bearing(LMS)),rad2deg(obs_error(LMS)))
        %histogram(rad2deg(obs_error(LMS)),'BinWidth',1);
        
        for k=1:size(LMS)
            
            scooter_theta=lamp_THETA(LMS(k));
            bear=observation(LMS(k));
            
            gradang=scooter_theta+bear;
            gradang=wrapTo2Pi(gradang);
            lampgradient=tan(gradang);  
            anglecheck=rad2deg(gradang);
            
            if abs(lampgradient) > 1
                raylength=abs(60/lampgradient);
            else
                raylength=60;
            end
            
            if (anglecheck<=90) || (anglecheck>=270)
                
                xplotz=linspace(0,raylength);
                
            elseif (anglecheck>90 && anglecheck<270)
                
                xplotz=linspace(-raylength,0);
                
            end
            
            yplotz=xplotz.*lampgradient;
            
            subplot(1,2,1)
            P(k)=plot((xplotz+camera_location(LMS(k),1)),(yplotz+camera_location(LMS(k),2)));
            
           h(k)= text(0,50,num2str(j));
          
            %P2=scatter(landmarks_enu(:,1),landmarks_enu(:,2),'d','r');
            
        end
        
        w=0;
        
        while (w==0)
            w=waitforbuttonpress;
        end
        delete(P);
        %delete(E);
        delete(h)
        
        if j==15
            
            j=0;
            
        end
        
        j=j+1;
        
    end
    
end




