close all
clear all

%Load stuff

filename='2019-08-22-Wentworth';
PATH='/home/maleen/cloud_academic/ams_data/localisation_data/2019-08-22-Wentworth/';

%Localisation data
load(strcat(PATH,filename,'-localready.mat'));

yolo_time=yolo_observations.lamp_times;

%57-63 right cam LM 5

%96-100 rightcam LM 15
%136-141 rightcam LM 4

%102-112 left cam post LM 3
%102-118 left cam tree LM 2



landmark=3;
startiter=find(yolo_time<102, 1, 'last' );
enditer=find(yolo_time<112, 1, 'last' );

i=0;

for j=startiter:enditer
    
    current_time=yolo_time(j);
    cam=yolo_observations.camera{j};
    
    rows=yolo_observations.lamp_times==current_time;
    centroids=yolo_observations(rows,{'centroid_x'});
    semantics=yolo_observations(rows,{'yolo_labels'});
    
    
    
    if cam=='LC'
        
        bear_obs=bearing_obZ(LC_params,current_time,cam,centroids{:,1},semantics{:,1});
        
        if semantics{1,1}{1}=='post'
            
            if length(bear_obs.bearings)==1
                
                
                i=i+1;
                
                if (cam=='LC')
                    
                    a=-0.037;
                    b=0.1073;
                elseif (cam=='RC')
                    
                    a=-0.0292;
                    b=-0.0895;
                end
                
                Z=rad2deg(bear_obs.bearings)
                
                trans=[ppval(rtkinterp_x,current_time),ppval(rtkinterp_y,current_time),ppval(rtkinterp_z,current_time)];
                rotz=ppval(rtkinterp_theta,current_time);
                T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
                T_world_robot=T_world_RTK*inv(T_robot_RTK);
                
                X_k=[T_world_robot(1,4);T_world_robot(2,4);rotz];
                
                xr = X_k(1,1) + a*cos(X_k(3,1)) - b*sin(X_k(3,1));
                yr = X_k(2,1) + a*sin(X_k(3,1)) + b*cos(X_k(3,1));
                
                dx=LMmap(landmark,2)-xr;
                dy=LMmap(landmark,3)-yr;
                
                theta_p=rad2deg(wrapTo2Pi(atan2(dy,dx)-X_k(3,1)))
                range_p=sqrt((dx.^2)+(dy.^2));
                
                obs_error_sq(i,1)=(theta_p-Z)^2;
            end
            
      end
    end
    
end

figure
plot(sqrt(obs_error_sq))

figure
histogram(sqrt(obs_error_sq))
SD=sqrt(mean(obs_error_sq))