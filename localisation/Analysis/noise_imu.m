close all
clear all

%Load stuff

filename='2019-09-01-Wentworth';
PATH='/home/maleen/cloud_academic/ams_data/localisation_data/2019-09-01-Wentworth/';

%Localisation data
load(strcat(PATH,filename,'-localready.mat'));

imu_time=imu_data{:,2};
imuZ=imu_data{:,3};

%51 to 68 straight1
%100 to 115 straight2

startiter=find(imu_time<100, 1, 'last' );
enditer=find(imu_time<115, 1, 'last' );

figure
plot(imu_time,imuZ)


%% Moving reading compared to RTK
close all
figure
plot(rtk_angvel)

for i=startiter:enditer
    
    time=imu_time(i);
    IMU_angv=imuZ(i);
    
    RTK_angv=ppval(rtkinterp_angv,time);
    
    imu_error_sq(i)=(rad2deg(RTK_angv-IMU_angv))^2;
    
end

figure
histogram(sqrt(imu_error_sq))
SD=sqrt(mean(imu_error_sq))
figure
plot(sqrt(imu_error_sq))

%% Moving reading compared to itself
close all

histogram(rad2deg(imuZ))
var(rad2deg(imuZ(startiter:enditer)))