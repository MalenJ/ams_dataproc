close all
clear all

%Load stuff

filename='2019-08-22-Wentworth';
PATH='/home/maleen/cloud_academic/ams_data/localisation_data/2019-08-22-Wentworth/';

%Localisation data
load(strcat(PATH,filename,'-localready.mat'));

depth_time=backcam_depthdata.back_depth_time;

startiter=find(depth_time<5, 1, 'last' );
enditer=find(depth_time<45, 1, 'last' );

sigmamat=zeros(480,640);

meanrangemat=zeros(480,640);

PP=BC_params.PrincipalPoint;
FL=BC_params.FocalLength;
j=1;

for i=startiter:enditer
    current_time=depth_time(i);
    depthimgU = undistortImage(depth_imgs{i},BC_params);
    
    for u=1:480
        
        for v=1:640
            
            depth=double(depthimgU(u,v))/1000;
            
            z_cam=depth;
            x_cam=((u-PP(2))*depth)/(FL(2));
            y_cam=((v-PP(1))*depth)/(FL(1));
            
            edge_cam=T_robot_backcam*[z_cam;-y_cam;-x_cam;1];
            ranges=sqrt((edge_cam(1))^2+(edge_cam(2))^2);
            
            d(i,u,v)=ranges;
            
            depthmat(i,u,v)=depth;
            
        end
        
    end
    
end


for u=1:480
    
    for v=1:640
        
        sigmamat(u,v)=var(d(:,u,v));
        meanrangemat(u,v)=mean(d(:,u,v));
        depthmat_sigma(u,v)=var(depthmat(:,u,v));
    end
    
end

mesh(sigmamat)

%% Mean of variances


i=1;
for u=240:400
    
    for v=320:640
        
      sigmavec(i)=sigmamat(u,v);  
      i=i+1;
        
    end
    
end

finalSD=sqrt(mean(sigmavec))