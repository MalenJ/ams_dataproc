close all
clear all


errox=[-0.176 -0.0772 -0.3173 -0.3276 -0.4578 -0.5445 -0.3148 -0.1378 -0.212 0.1776];

erroy=[-0.004 0.0843 0.9401 0.6407 0.2534 0.2638 0.2275 0.1104 -0.2033 0.4776];

sigmax=[0.7581 0.4857 0.7246 0.5646 0.6049 0.5637 0.2084 0.3767 1.1909 0.1954];

sigmay=[0.0118 0.3199 0.7425 0.5022 0.4161 0.4657 0.2776 0.4231 0.8221 0.5018];


figure
subplot(2,1,1)
hold on
stem(errox)
stem(sigmax,'x','r')
stem(-sigmax,'x','r')
xlabel('RTK surveyed location')
ylabel('X error (m)')
hold off
subplot(2,1,2)
hold on
stem(erroy)
stem(sigmay,'x','r')
stem(-sigmay,'x','r')
hold off
xlabel('RTK surveyed location')
ylabel('Y error (m)')


