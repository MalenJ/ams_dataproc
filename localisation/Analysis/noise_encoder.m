close all
clear all

%Load stuff

filename='2019-09-01-Wentworth';
PATH='/home/maleen/cloud_academic/ams_data/localisation_data/2019-09-01-Wentworth/';

%Localisation data
load(strcat(PATH,filename,'-localready2.mat'));

enc_time=enc_data{:,3};
enc_count=enc_data{:,6};

startiter=find(enc_time<50, 1, 'last' );
enditer=find(enc_time<71, 1, 'last' );

%% Moving reading compared to RTK
close all

for i=startiter:enditer
    
    time1=enc_time(i);
    time2=enc_time(i+5);
    dt=time2-time1;
    22
    enc_dist=(enc_count(i+5)-enc_count(i))*calib;
    
    enc_vel(i)=enc_dist/dt;
    
    RTKx1=ppval(rtkinterp_x,time1);
    RTKY1=ppval(rtkinterp_y,time1);
    
    RTKx2=ppval(rtkinterp_x,time1);
    RTKY2=ppval(rtkinterp_y,time2);
    
    
    rtkXdist=RTKx2-RTKx1;
    rtkYdist=RTKY2-RTKY1;
    
    rtkdist=sqrt(rtkXdist^2+rtkYdist^2);
    rtk_vel=rtkdist/dt;
    
    vel_error_sq(i)=(rtk_vel-enc_vel(i))^2;
    
end

figure
plot(sqrt(vel_error_sq))
histogram(sqrt(vel_error_sq))
SD=sqrt(mean((sqrt(vel_error_sq))))

%% Moving reading compared to itself
close all

histogram(enc_vel)
var(enc_vel)