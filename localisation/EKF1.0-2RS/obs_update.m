function [X_k1k1,P_k1k1] = obs_update(X_k1k,P_k1k,Z,map,cam)


f=find(Z(:,2)==0);

Z(f,:)=[];

map_num=size(map,1);
Z_num=size(Z,1);

%PREDICTED OBSERVATIONS

a=-0.1;
if (cam=='C1')
    
    b=0.055;
    
elseif (cam=='C2')
    
    b=-0.055;
end

xr = X_k1k(1,1) + a*cos(X_k1k(3,1)) - b*sin(X_k1k(3,1));
yr = X_k1k(2,1) + a*sin(X_k1k(3,1)) + b*cos(X_k1k(3,1));


dx=map(:,1)-ones(map_num,1)*xr;
dy=map(:,2)-ones(map_num,1)*yr;

theta_p=wrapTo2Pi(atan2(dy,dx)-(ones(map_num,1)*X_k1k(3,1)));
range_p=sqrt((dx.^2)+(dy.^2));

%SELECT LANDMARKS

LM_ID=Z(:,2);


%INNOVATION

% pose=rad2deg(X_k1k(3,1))
% predicted=rad2deg(theta_p(LM_ID))
% observed=[rad2deg(Z(:,1)) Z(:,2)]

I=Z(:,1)-theta_p(LM_ID);

%INNOVATION COVARIANCE

xm=map(LM_ID,1);
ym=map(LM_ID,2);
x=ones(size(LM_ID,1),1)*X_k1k(1,1);
y=ones(size(LM_ID,1),1)*X_k1k(2,1);
phi=ones(size(LM_ID,1),1)*X_k1k(3,1);

%JH=[-(y-ym)./((xm-x).^2 + (y-ym).^2),(x-xm)./((x-xm).^2 + (ym-y).^2), -ones(size(LM_ID,1),1)];

JH=[-(y-ym+b*cos(phi)+a*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2 +(y-ym+b*cos(phi)+a*sin(phi)).^2)...
    (x-xm+a*cos(phi)-b*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)...
    (((a*cos(phi)-b*sin(phi))./(x-xm+a*cos(phi)-b*sin(phi))+((b*cos(phi)+a*sin(phi)).*(y-ym+b*cos(phi)+a*sin(phi)))./(x-xm+a*cos(phi)-b*sin(phi)).^2).*(x-xm+a*cos(phi)-b*sin(phi)).^2)./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)-1];


var_theta=(0.2)^2;

R=eye(size(LM_ID,1))*var_theta;

S=R + JH*P_k1k*transpose(JH);

%UPDATE

if cond(S) <10
    K=P_k1k*transpose(JH)*inv(S);
else
    K=zeros(3,size(S,1));
end

X_k1k1=X_k1k+ K*I;

P_k1k1=P_k1k-K*S*transpose(K);


end

