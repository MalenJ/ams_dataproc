
function [X_k1k,P_k1k] = predict(X_k,P_k,U_k,dt)

%MOTION MODEL
 
X_k1k= X_k + [U_k(1,1)*cos(X_k(3,1)); U_k(1,1)*sin(X_k(3,1)) ;dt*U_k(2,1)];

%X_k1k=X_pt+[0.15*cos(X_pt(3,1));0.15*sin(X_pt(3,1));0];

%CONTROL NOISE COVARIANCE Q
% 
% var_v=(0.05)^2;
% var_w=(0.0667)^2;

%SLOWVALS
var_v=(0.35)^2;
var_w=(deg2rad(1.6))^2;


%experiment vals
% var_v=(0.05)^2;
% var_w=(0.01)^2;

Q=[var_v 0;0 var_w];

%PREDICTION COVARIACNCE 

%JACOBIANS
dFX=[1 0 -U_k(1,1)*sin(X_k(3,1)); 0 1 U_k(1,1)*cos(X_k(3,1)); 0 0 1];

dFU=[cos(X_k(3,1)) 0; sin(X_k(3,1)) 0; 0 1].*dt;

P_k1k=dFX*P_k*transpose(dFX)+dFU*Q*transpose(dFU);


end 