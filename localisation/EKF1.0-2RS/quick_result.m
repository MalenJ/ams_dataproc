close all
clear all

%Load stuff

%filename='2019-03-22-Wentworth-P4-yolo'; 
%2019-04-11-MSY-P3-prelabeled-tree.mat 2019-04-11-MSY-P3-enumap-tree.mat

load('/home/maleen/git/ams_primary/mapping/2019-04-11-MSY/2019-04-11-MSY-P3-prelabeled-tree.mat','data_stream','enu_rot','rtk_data','imu_data','enc_data','lamp_data');

load('/home/maleen/git/ams_primary/mapping/2019-04-11-MSY/2019-04-11-MSY-P3-enumap-tree.mat','gtmap_enu');

%DEFINE MAP

env=[transpose(gtmap_enu(1,:)) transpose(gtmap_enu(2,:)) transpose(gtmap_enu(4,:))];

%DATA STREAM

data_time=data_stream{:,2};
data_type=data_stream{:,3};

%INTERP OBJECTS


enc_interp=spline(enc_data{:,3},enc_data{:,6});
imu_interp=spline(imu_data{:,2},imu_data{:,3});

encXcalib=6.608284615932709e-05;
encYcalib=6.603486193757895e-05;

calib=mean([encXcalib,encYcalib]);

%YOLO DATA

observations=lamp_data;

%GROUND TRUTH
rtk_time=rtk_data{:,2};

GTX=transpose(enu_rot(1,:));
GTY=transpose(enu_rot(2,:));

GTinterp_x = spline([0; rtk_time],[0; transpose(enu_rot(1,:))]);
GTinterp_y = spline([0; rtk_time],[0; transpose(enu_rot(2,:))]);

no_detects=0;
in_count=1;

%Iterate

for i=2:length(data_time)
  
    
    iter=i;
    
    previous_time=data_time(iter-1);
    current_time=data_time(iter);
    current_data=data_type{iter};
    dt=current_time-previous_time;
    
    
    U_k(1,1)=((ppval(enc_interp,current_time)-ppval(enc_interp,previous_time))*calib);
    U_k(2,1)=ppval(imu_interp,previous_time);
    
    if (iter-1==1)
        X_k=[0;0;0];
        P_k=zeros(3);
        
    else
        iterval=1+(iter-2)*3;
        X_k=X_k1k1(:,iter-1);
        P_k=P_k1k1(1:3,iterval:iterval+2);
        
    end
    
    
    [X_k1k,P_k1k] = predict(X_k,P_k,U_k,dt);
    
    X_k1k(3,1)=wrapTo2Pi(X_k1k(3,1));
    
    
    if (current_data=='O')
        
        iterval=1+(iter-1)*3;
        
        X_k1k1(:,iter) =X_k1k;
        
        P_k1k1(1:3,iterval:iterval+2)=P_k1k;
        
        
    elseif (current_data=='C1')
        
        
        rows=observations.lamp_times==current_time;
        %obs=app.observations(rows,{'bearings','LM'});
        obs=observations(rows,{'bearings'});
        
        Z=ones(length(obs{:,1}),1);
        bear=ones(length(obs{:,1}));
        gradang=ones(length(obs{:,1}),1);
        
        bear=pi/4-obs{:,1};
        %gradang=ones(length(bear),1)*scooter_theta+bear;
        %gradang=wrapTo2Pi(gradang);
        gradang=wrapTo2Pi(bear);
        
        %obsang=rad2deg(gradang)
        
        Z(:,1)=gradang;
        %Z(:,2)=obs{:,2};
        
        iterval=1+(iter-1)*3;
        
        [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),DETECTS] = obs_update_IG(X_k1k,P_k1k,Z,env,current_data);
        
        
        no_detects=no_detects+DETECTS;
%         if obs_count>0
%             innovation(in_count,:)=in;
%             in_count=in_count+obs_count;
%         else
%             in_count=in_count+1;
%             
%         end
%         
        %app.X_k1k1(3,app.iter)=wrapTo2Pi(app.X_k1k1(3,app.iter));
        
        
    elseif(current_data=='C2')
        
        
        rows=observations.lamp_times==current_time;
        %obs=app.observations(rows,{'bearings','LM'});
        obs=observations(rows,{'bearings'});
        
        Z=ones(length(obs{:,1}),1);
        bear=ones(length(obs{:,1}));
        gradang=ones(length(obs{:,1}),1);
        
        %obsang=rad2deg(gradang)
        
        bear=pi/4+obs{:,1};
        %gradang=ones(length(bear),1)*scooter_theta-bear;
        %gradang=wrapTo2Pi(gradang);
        gradang=wrapTo2Pi(-bear);
        
        Z(:,1)=gradang;
        %Z(:,2)=obs{:,2};
        
        iterval=1+(iter-1)*3;
        
        [X_k1k1(:,iter),P_k1k1(1:3,iterval:iterval+2),DETECTS] = obs_update_IG(X_k1k,P_k1k,Z,env,current_data);
         no_detects=no_detects+DETECTS;
        %app.X_k1k1(3,app.iter)=wrapTo2Pi(app.X_k1k1(3,app.iter));
        
    end
    
    
    iterval=1+(iter-1)*3;
    
    %scatter(app.localfig,app.X_k1k1(1,app.iter),app.X_k1k1(2,app.iter),sz,'r','x');
    
    
    
    if (isnan(P_k1k1(1,iterval:iterval+2))==0)
        
        PK=P_k1k1(1:3,iterval:iterval+2);
        pose=X_k1k1(:,iter);
        time=current_time;
        
        datt(iter,:)=time;
        
        p=0.95;
        s = -2 * log(1 - p);
        
        [V, D] = eig(PK(1:2,1:2) * s);
        t = linspace(0, 2 * pi);
        
        a = (V * sqrt(D)) * [cos(t(:))'; sin(t(:))'];
        
        
        GTx=ppval(GTinterp_x,time);
        GTy=ppval(GTinterp_y,time);
        
        
        %RTK_calib=[cos(pose(3,1)) -sin(pose(3,1)) -0.55; sin(pose(3,1)) cos(pose(3,1)) 0;0 0 1]*[GTx; GTy; 1];
        
        %app.error(:,app.iter)=RTK_calib(1:2,1)-pose(1:2,1);
        
        error(:,iter)=[GTx;GTy]-pose(1:2,1)+[0.15*cos(pose(3,1));0.15*sin(pose(3,1))];
        
        sigmabounds(:,iter)=2*sqrt(diag(PK));
        
    end
    
    
    
end



%PLOT

figure
hold on
sz=1.25;
for i=1:size(env,1)
    scatter(env(i,1),env(i,2),'x','g')
    %text(env(i,1),env(i,2), num2str(i));
end

%% ERROR SAMPLE FOR MSY




scatter(X_k1k1(1,:),X_k1k1(2,:),sz,'b');

% scatter(GTX(198),GTY(198),'h','filled','r')
% scatter(GTX(498),GTY(498),'h','filled','r')
% scatter(GTX(744),GTY(744),'h','filled','r')
% scatter(GTX(888),GTY(888),'h','filled','r')
% scatter(GTX(994),GTY(994),'h','filled','r')
% scatter(GTX(1170),GTY(1170),'h','filled','r')
% scatter(GTX(1253),GTY(1253),'h','filled','r')
% scatter(GTX(1469),GTY(1469),'h','filled','r')
% scatter(GTX(1531),GTY(1531),'h','filled','r')
% scatter(GTX(1673),GTY(1673),'h','filled','r')
% scatter(GTX,GTY,sz,'r')

xlabel('X (m)')
ylabel('Y (m)')


figure
subplot(2,1,1)
hold on
plot(datt, sigmabounds(1,:),'r');
plot(datt, -sigmabounds(1,:),'r');
plot(datt,error(1,:),'b');
xlabel('time (s)')
ylabel('X error (m)')
hold off
subplot(2,1,2)
hold on
plot(datt, sigmabounds(2,:),'r');
plot(datt, -sigmabounds(2,:),'r');
plot(datt,error(2,:),'b');
hold off
xlabel('time (s)')
ylabel('Y error (m)')

MAXX=max(abs(error(1,:)));
MAXY=max(abs(error(2,:)));
no_detects;