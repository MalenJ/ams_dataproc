classdef EKF_2RS < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure             matlab.ui.Figure
        LOADButton           matlab.ui.control.Button
        ITERATEButton        matlab.ui.control.Button
        FORWARDButton        matlab.ui.control.Button
        BACKWARDButton       matlab.ui.control.Button
        xerror               matlab.ui.control.UIAxes
        yerror               matlab.ui.control.UIAxes
        thetaerror           matlab.ui.control.UIAxes
        MeanXerrorLabel      matlab.ui.control.Label
        MeanYerrorLabel      matlab.ui.control.Label
        MeanThetaerrorLabel  matlab.ui.control.Label
        uxLabel              matlab.ui.control.Label
        uyLabel              matlab.ui.control.Label
        uthetaLabel          matlab.ui.control.Label
    end

    
    properties (Access = public)
        
        iter=1;
        pose_GT;
        GT;
        UMAT;
        STAMP;
        viz=Visualizer2D;
        detector = ObjectDetector;
        env;
        
        X_k;
        X_k1k1;
        
        P_k;
        P_k1k1
        
        
        plt
        pathplt
        
        xsigmapltu
        xsigmapltb
        xerrplt
        
        ysigmapltu
        ysigmapltb
        yerrplt
        
        thetasigmapltu
        thetasigmapltb
        thetaerrplt
        
        srange=5;
        error;
        sigmabounds;
        
        
        
    end
    
    methods (Access = public)
        
        function Z = sensor(app,pose,map)
            map_num=size(map,1);
            
            dx=map(:,1)-ones(map_num,1)*pose(1,1);
            dy=map(:,2)-ones(map_num,1)*pose(2,1);
            var_theta=(0.0175)^2;
            
            theta=atan2(dy,dx)-ones(map_num,1)*pose(3,1)+sqrt(var_theta)*randn(map_num,1);
            range=sqrt((dx.^2)+(dy.^2));
            
            zs=find(range<app.srange);
           
            
            Z=[range(zs) theta(zs) map(zs,3)];
            
        end
        
        function a = plotcov(app,PK,pose,poseGT)
            rng(1);
            p=0.95;
            s = -2 * log(1 - p);
            
            [V, D] = eig(PK(1:2,1:2) * s);
            t = linspace(0, 2 * pi);
            
            a = (V * sqrt(D)) * [cos(t(:))'; sin(t(:))'];
            
            
            app.plt=plot(a(1, :) + pose(1,1), a(2, :) + pose(2,1));
            
            app.error(:,app.iter)=poseGT-pose;
            app.sigmabounds(:,app.iter)=2*sqrt(diag(PK));
           
            
            hold(app.xerror,'on');
            app.xsigmapltu=plot(app.xerror,app.STAMP(1,1:app.iter), app.sigmabounds(1,1:app.iter),'b');
            app.xsigmapltb=plot(app.xerror,app.STAMP(1,1:app.iter), -app.sigmabounds(1,1:app.iter),'b');
            app.xerrplt=plot(app.xerror,app.STAMP(1,1:app.iter) ,app.error(1,1:app.iter),'r');
            plot(app.xerror,app.STAMP,zeros(1,length(app.STAMP)),'k');
            
            hold(app.yerror,'on');
            app.ysigmapltu=plot(app.yerror,app.STAMP(1,1:app.iter), app.sigmabounds(2,1:app.iter),'b');
            app.ysigmapltb=plot(app.yerror,app.STAMP(1,1:app.iter), -app.sigmabounds(2,1:app.iter),'b');
            app.yerrplt=plot(app.yerror,app.STAMP(1,1:app.iter) ,app.error(2,1:app.iter),'r');
            plot(app.yerror,app.STAMP,zeros(1,length(app.STAMP)),'k');
            
            hold(app.thetaerror,'on');
            app.thetasigmapltu=plot(app.thetaerror,app.STAMP(1,1:app.iter), app.sigmabounds(3,1:app.iter),'b');
            app.thetasigmapltb=plot(app.thetaerror,app.STAMP(1,1:app.iter), -app.sigmabounds(3,1:app.iter),'b');
            app.thetaerrplt=plot(app.thetaerror,app.STAMP(1,1:app.iter) ,app.error(3,1:app.iter),'r');
            plot(app.thetaerror,app.STAMP,zeros(1,length(app.STAMP)),'k');
            
            app.uxLabel.Text=num2str(mean(app.error(1,1:app.iter)));
            app.uyLabel.Text=num2str(mean(app.error(2,1:app.iter)));
            app.uthetaLabel.Text=num2str(mean(app.error(3,1:app.iter)));
            
            
            
        end
        
    end
    

    methods (Access = private)

        % Button pushed function: LOADButton
        function LOADButtonPushed(app, event)
            
            filename='2019-02-11-Wentworth-P7-yolo';
            
            imu_data=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-imu.csv'));
            enc_data=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-encoder.csv'));
            data_stream=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-sorted.csv'));
            
            load('/home/maleen/git/ams_primary/mapping/2019-02-11-Wentworth-P7-yololabeled.mat','yolo_ex');
            
            load('/home/maleen/git/ams_primary/mapping/2019-02-11-Wentworth-gtmap.mat','gtmap_enu');
            
            load('2019-01-11-matsim.mat','pose','U_kmat','tVec','map');
            
            %DEFINE MAP
            app.env=[transpose(gtmap_enu(1,:)) transpose(gtmap_enu(2,:)) transpose(gtmap_enu(4,:))];
            
            app.GT=pose;
            app.UMAT=U_kmat;
            app.STAMP=tVec;
            
            
            %Object detector
            app.viz.hasObjDetector=true;
            app.detector.fieldOfView=2*pi;
            app.detector.maxDetections=20;
            app.detector.maxRange=app.srange;
            app.detector.sensorAngle=0;
            attachObjectDetector(app.viz,app.detector);
            app.viz.objectColors=[zeros(length(app.env), 1) zeros(length(app.env), 1) ones(length(app.env),1)];
            
        end

        % Button pushed function: FORWARDButton
        function FORWARDButtonPushed(app, event)
            app.iter=app.iter+1;
            
            colours=[zeros(length(app.env), 1) zeros(length(app.env), 1) ones(length(app.env),1)];
            app.viz.objectColors=colours;
            
            app.pose_GT=app.GT(:,app.iter);
            Z=app.sensor(app.pose_GT,app.env);
            d = app.detector(app.pose_GT,app.env);
            
            if (isempty(d))
                
            else
                
                LM_ID=d(:,3);
                
                colours(LM_ID,:)=[ones(size(d,1), 1) zeros(size(d,1), 1) zeros(size(d,1),1)];
                
                app.viz.objectColors=colours;
                
            end
            
            app.viz(app.pose_GT,app.env)
            hold on
            
            rng(1);
            U_k=app.UMAT(:,app.iter-1)+[sqrt(0.1)*randn(1,1);sqrt(0.2)*randn(1,1)] ;
            
            if (app.iter-1==1)
                app.X_k=app.GT(:,app.iter);
                app.P_k=zeros(3);
                U_k=app.UMAT(:,app.iter-1);
            else
                iterval=1+(app.iter-2)*3;
                app.X_k=app.X_k1k1(:,app.iter-1);
                app.P_k=app.P_k1k1(1:3,iterval:iterval+2);
                U_k=app.UMAT(:,app.iter-1);
                
                delete(app.plt)
                
                delete(app.xsigmapltu)
                delete(app.xsigmapltb)
                delete(app.xerrplt)
                
                delete(app.ysigmapltu)
                delete(app.ysigmapltb)
                delete(app.yerrplt)
                
                delete(app.thetasigmapltu)
                delete(app.thetasigmapltb)
                delete(app.thetaerrplt)
                
            end
            
            dt=0.1;
            
            [X_k1k,P_k1k] = scooter_predict(app.X_k,app.P_k,U_k,dt);
            %[app.X_k1k1,app.P_k1k1] = scooter_predict(app.X_k,app.P_k,U_k,dt);
            sz=4;
            
            
            [I,JH,S] = scooter_observe(X_k1k,P_k1k,Z,app.env);
            
            iterval=1+(app.iter-1)*3;
            
            [app.X_k1k1(:,app.iter),app.P_k1k1(1:3,iterval:iterval+2)] = scooter_update(X_k1k,P_k1k,I,JH,S);
            
            app.pathplt(app.iter)=scatter(app.X_k1k1(1,app.iter),app.X_k1k1(2,app.iter),sz,'r','x');
            
            a = app.plotcov(app.P_k1k1(1:3,iterval:iterval+2),app.X_k1k1(:,app.iter),app.pose_GT);
            
            
        end

        % Button pushed function: BACKWARDButton
        function BACKWARDButtonPushed(app, event)
            
            app.iter=app.iter-1;
              
            colours=[zeros(13, 1) zeros(13, 1) ones(13,1)];
            app.viz.objectColors=colours;
            
            app.pose_GT=app.GT(:,app.iter);
            Z=app.sensor(app.pose_GT,app.env);
            d = app.detector(app.pose_GT,app.env);
            
            if (isempty(d))
                
            else
                
                LM_ID=d(:,3);
                
                colours(LM_ID,:)=[ones(size(d,1), 1) zeros(size(d,1), 1) zeros(size(d,1),1)];
                
                app.viz.objectColors=colours;
                
            end
            
            app.viz(app.pose_GT,app.env)
            hold on
            
            rng(1);
            U_k=app.UMAT(:,app.iter-1)+[sqrt(0.1)*randn(1,1);sqrt(0.2)*randn(1,1)] ;
            
            if (app.iter-1==1)
                app.X_k=app.GT(:,app.iter);
                app.P_k=zeros(3);
                U_k=app.UMAT(:,app.iter-1);
            else
                iterval=1+(app.iter-2)*3;
                app.X_k=app.X_k1k1(:,app.iter-1);
                app.P_k=app.P_k1k1(1:3,iterval:iterval+2);
                U_k=app.UMAT(:,app.iter-1);
                
                delete(app.plt)
                
                delete(app.xsigmapltu)
                delete(app.xsigmapltb)
                delete(app.xerrplt)
                
                delete(app.ysigmapltu)
                delete(app.ysigmapltb)
                delete(app.yerrplt)
                
                delete(app.thetasigmapltu)
                delete(app.thetasigmapltb)
                delete(app.thetaerrplt)
                
            end
            
            dt=0.1;
            
            [X_k1k,P_k1k] = scooter_predict(app.X_k,app.P_k,U_k,dt);
            %[app.X_k1k1,app.P_k1k1] = scooter_predict(app.X_k,app.P_k,U_k,dt);
            sz=4;
            
            
            [I,JH,S] = scooter_observe(X_k1k,P_k1k,Z,app.env);
            
            [app.X_k1k1(:,app.iter),app.P_k1k1(1:3,iterval:iterval+2)] = scooter_update(X_k1k,P_k1k,I,JH,S);
            
            delete(app.pathplt(app.iter+1))
            
            %app.pathplt(app.iter)=scatter(app.X_k1k1(1,app.iter),app.X_k1k1(2,app.iter),sz,'r','x');
            
            a = app.plotcov(app.P_k1k1(1:3,iterval:iterval+2),app.X_k1k1(:,app.iter),app.pose_GT);
            
            
        end

        % Button pushed function: ITERATEButton
        function ITERATEButtonPushed(app, event)
            
            for i=2:length(app.UMAT)
                
                app.iter=i;
                
                colours=[zeros(13, 1) zeros(13, 1) ones(13,1)];
                app.viz.objectColors=colours;
                
                app.pose_GT=app.GT(:,app.iter);
                Z=app.sensor(app.pose_GT,app.env);
                d = app.detector(app.pose_GT,app.env);
                
                if (isempty(d))
                    
                else
                    
                    LM_ID=d(:,3);
                    
                    colours(LM_ID,:)=[ones(size(d,1), 1) zeros(size(d,1), 1) zeros(size(d,1),1)];
                    
                    app.viz.objectColors=colours;
                    
                end
                
                
                app.viz(app.pose_GT,app.env)
                hold on
                
                rng(1);
                U_k=app.UMAT(:,app.iter-1)+[sqrt(0.1)*randn(1,1);sqrt(0.2)*randn(1,1)] ;
                
                if (app.iter-1==1)
                    app.X_k=app.GT(:,app.iter);
                    app.P_k=zeros(3);
                    U_k=app.UMAT(:,app.iter-1);
                else
                    app.X_k=app.X_k1k1;
                    app.P_k=app.P_k1k1;
                    U_k=app.UMAT(:,app.iter-1);
                    delete(app.plt)
                    
                    delete(app.xsigmapltu)
                    delete(app.xsigmapltb)
                    delete(app.xerrplt)
                    
                    delete(app.ysigmapltu)
                    delete(app.ysigmapltb)
                    delete(app.yerrplt)
                    
                    delete(app.thetasigmapltu)
                    delete(app.thetasigmapltb)
                    delete(app.thetaerrplt)
                    
                end
                
                dt=0.1;
                
                [X_k1k,P_k1k] = scooter_predict(app.X_k,app.P_k,U_k,dt);
                %[app.X_k1k1,app.P_k1k1] = scooter_predict(app.X_k,app.P_k,U_k,dt);
                sz=4;
                
                
                [I,JH,S] = scooter_observe(X_k1k,P_k1k,Z,app.env);
                
                [app.X_k1k1,app.P_k1k1] = scooter_update(X_k1k,P_k1k,I,JH,S);
                
                scatter(app.X_k1k1(1,1),app.X_k1k1(2,1),sz,'r','x')
                
                a = app.plotcov(app.P_k1k1,app.X_k1k1,app.pose_GT);
                
            end
            
        end
    end

    % App initialization and construction
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure
            app.UIFigure = uifigure;
            app.UIFigure.Position = [100 100 1163 822];
            app.UIFigure.Name = 'UI Figure';

            % Create LOADButton
            app.LOADButton = uibutton(app.UIFigure, 'push');
            app.LOADButton.ButtonPushedFcn = createCallbackFcn(app, @LOADButtonPushed, true);
            app.LOADButton.Position = [72 741 215 43];
            app.LOADButton.Text = 'LOAD';

            % Create ITERATEButton
            app.ITERATEButton = uibutton(app.UIFigure, 'push');
            app.ITERATEButton.ButtonPushedFcn = createCallbackFcn(app, @ITERATEButtonPushed, true);
            app.ITERATEButton.Position = [130 622 100 22];
            app.ITERATEButton.Text = 'ITERATE';

            % Create FORWARDButton
            app.FORWARDButton = uibutton(app.UIFigure, 'push');
            app.FORWARDButton.ButtonPushedFcn = createCallbackFcn(app, @FORWARDButtonPushed, true);
            app.FORWARDButton.Position = [187 677 100 22];
            app.FORWARDButton.Text = 'FORWARD';

            % Create BACKWARDButton
            app.BACKWARDButton = uibutton(app.UIFigure, 'push');
            app.BACKWARDButton.ButtonPushedFcn = createCallbackFcn(app, @BACKWARDButtonPushed, true);
            app.BACKWARDButton.Position = [72 677 100 22];
            app.BACKWARDButton.Text = 'BACKWARD';

            % Create xerror
            app.xerror = uiaxes(app.UIFigure);
            title(app.xerror, 'X error')
            xlabel(app.xerror, 'Time')
            ylabel(app.xerror, 'X')
            app.xerror.Position = [335 570 684 236];

            % Create yerror
            app.yerror = uiaxes(app.UIFigure);
            title(app.yerror, 'Y error')
            xlabel(app.yerror, 'Time')
            ylabel(app.yerror, 'Y')
            app.yerror.Position = [335 295 684 236];

            % Create thetaerror
            app.thetaerror = uiaxes(app.UIFigure);
            title(app.thetaerror, 'Theta error')
            xlabel(app.thetaerror, 'Time')
            ylabel(app.thetaerror, 'Theta')
            app.thetaerror.Position = [335 27 684 236];

            % Create MeanXerrorLabel
            app.MeanXerrorLabel = uilabel(app.UIFigure);
            app.MeanXerrorLabel.HorizontalAlignment = 'center';
            app.MeanXerrorLabel.Position = [1029 760 76 24];
            app.MeanXerrorLabel.Text = 'Mean X error';

            % Create MeanYerrorLabel
            app.MeanYerrorLabel = uilabel(app.UIFigure);
            app.MeanYerrorLabel.HorizontalAlignment = 'center';
            app.MeanYerrorLabel.Position = [1029 486 76 23];
            app.MeanYerrorLabel.Text = 'Mean Y error';

            % Create MeanThetaerrorLabel
            app.MeanThetaerrorLabel = uilabel(app.UIFigure);
            app.MeanThetaerrorLabel.HorizontalAlignment = 'center';
            app.MeanThetaerrorLabel.Position = [1029 216 98 29];
            app.MeanThetaerrorLabel.Text = 'Mean Theta error';

            % Create uxLabel
            app.uxLabel = uilabel(app.UIFigure);
            app.uxLabel.HorizontalAlignment = 'center';
            app.uxLabel.Position = [1029 722 76 22];
            app.uxLabel.Text = 'ux';

            % Create uyLabel
            app.uyLabel = uilabel(app.UIFigure);
            app.uyLabel.HorizontalAlignment = 'center';
            app.uyLabel.Position = [1029 446 76 22];
            app.uyLabel.Text = 'uy';

            % Create uthetaLabel
            app.uthetaLabel = uilabel(app.UIFigure);
            app.uthetaLabel.HorizontalAlignment = 'center';
            app.uthetaLabel.Position = [1029 174 98 22];
            app.uthetaLabel.Text = 'utheta';
        end
    end

    methods (Access = public)

        % Construct app
        function app = EKF_2RS

            % Create and configure components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end