function [X_k1k1,P_k1k1,detects] = obs_update_IG(X_k1k,P_k1k,Z,map,cam)

var_theta=(deg2rad(14))^2;
IG_gate=0.46;

%SLOW VALS
%var_theta=(0.4)^2;
%IG<0.3 && range_p(m)<20
% %0.46 1.07 1.64 2.71 3.84

detects=0;

cam
rad2deg(Z)
% f=find(Z(:,2)==0);
%
% Z(f,:)=[];

map_num=size(map,1);
Z_num=size(Z,1);

%PREDICTED OBSERVATIONS

a=-0.095;
if (cam=='C1')
    
    b=0.055;
    
    
elseif (cam=='C2')
    
    b=-0.055;
   
end

xr = X_k1k(1,1) + a*cos(X_k1k(3,1)) - b*sin(X_k1k(3,1));
yr = X_k1k(2,1) + a*sin(X_k1k(3,1)) + b*cos(X_k1k(3,1));


dx=map(:,1)-ones(map_num,1)*xr;
dy=map(:,2)-ones(map_num,1)*yr;
theta_p=wrapTo2Pi(atan2(dy,dx)-ones(map_num,1)*X_k1k(3,1));
range_p=sqrt((dx.^2)+(dy.^2));

%SELECT LANDMARKS

DA_MAT=zeros(Z_num,map_num);
IG_MAT=ones(Z_num,map_num)*1000;
IG_MAT2=ones(Z_num,map_num)*1000;


%INNOVATION

% pose=rad2deg(X_k1k(3,1))
% predicted=rad2deg(theta_p(LM_ID))
% observed=[rad2deg(Z(:,1)) Z(:,2)]

%INNOVATION GATE


for z=1:Z_num
    
    I(:,z)=ones(map_num,1)*Z(z,1)-theta_p;
    
    %INNOVATION COVARIANCE
    
    a_count=0;
    
    for m=1:map_num
        
        
        % if m==3
        
        
        %lmskip='skipped'
        
        %else
        
        xm=map(m,1);
        ym=map(m,2);
        x=X_k1k(1,1);
        y=X_k1k(2,1);
        phi=X_k1k(3,1);
        
        %JH=[-(y-ym)./((xm-x).^2 + (y-ym).^2),(x-xm)./((x-xm).^2 + (ym-y).^2), -ones(size(LM_ID,1),1)];
        
        JH=[-(y-ym+b*cos(phi)+a*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2 +(y-ym+b*cos(phi)+a*sin(phi)).^2)...
            (x-xm+a*cos(phi)-b*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)...
            (((a*cos(phi)-b*sin(phi))./(x-xm+a*cos(phi)-b*sin(phi))+((b*cos(phi)+a*sin(phi)).*(y-ym+b*cos(phi)+a*sin(phi)))./(x-xm+a*cos(phi)-b*sin(phi)).^2).*(x-xm+a*cos(phi)-b*sin(phi)).^2)./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)-1];
        
        
        R=var_theta;
        
        S=R+JH*P_k1k*transpose(JH);
        
        %IG=I(m,z)/sqrt(S);
        
        IG=transpose(I(m,z))*inv(S)*I(m,z);
        
        if IG < IG_gate && range_p(m)<10
           % DA_MAT(z,m)=1;
            IG_MAT(z,m)=IG;
            m;
            a_count=a_count+1;
        end
        % end
    end
    %da_count(z,1)=a_count;
    %     if a_count>0
    %         [minIG,I_minIG] = mink(IG_MAT(z,:),1);
    %         IG_MAT2(z,I_minIG)=minIG;
    %     end
    
end
%
for m=1:map_num
    
    [min_lm,I_min_lm] = mink(IG_MAT(:,m),1);
    
    if min_lm<1000
        IG_MAT2(I_min_lm,m)=min_lm;
    end
end


for z=1:Z_num
    
    [min_ig,I_min_ig] = mink(IG_MAT2(z,:),1);
    
    if min_ig<1000
        DA_MAT(z,I_min_ig)=1;
        da_count(z,1)=1;
    else
        da_count(z,1)=0;
    end
end



finz_count=1;


for c=1:Z_num
    
    if da_count(c,1)>0
        indx=find(DA_MAT(c,:)==1);
        % if abs(range_p(indx))<10
        fin_inov(finz_count,1)=I(indx,c);
        LM_ID(finz_count,1)=indx;
        finz_count=finz_count+1;
        %         landma=indx
        %         predict=theta_p;
        %         actual=Z(c,1)
        
        %end
        
    end
    
end

if finz_count>1
    
    %      cam
    %      LM_ID
    %
    
    detects=size(LM_ID,1);
    xm=map(LM_ID,1);
    ym=map(LM_ID,2);
    x=ones(size(LM_ID,1),1)*X_k1k(1,1);
    y=ones(size(LM_ID,1),1)*X_k1k(2,1);
    phi=ones(size(LM_ID,1),1)*X_k1k(3,1);
    
    %JH=[-(y-ym)./((xm-x).^2 + (y-ym).^2),(x-xm)./((x-xm).^2 + (ym-y).^2), -ones(size(LM_ID,1),1)];
    
    JH=[-(y-ym+b*cos(phi)+a*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2 +(y-ym+b*cos(phi)+a*sin(phi)).^2)...
        (x-xm+a*cos(phi)-b*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)...
        (((a*cos(phi)-b*sin(phi))./(x-xm+a*cos(phi)-b*sin(phi))+((b*cos(phi)+a*sin(phi)).*(y-ym+b*cos(phi)+a*sin(phi)))./(x-xm+a*cos(phi)-b*sin(phi)).^2).*(x-xm+a*cos(phi)-b*sin(phi)).^2)./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)-1];
    

    
    
    R=eye(size(LM_ID,1))*var_theta;
    
    S=R + JH*P_k1k*transpose(JH);
    
    %UPDATE
    
    if cond(S) <10
        K=P_k1k*transpose(JH)*inv(S);
    else
        K=zeros(3,size(S,1));
    end
    
    X_k1k1=X_k1k+ K*fin_inov;
    
    P_k1k1=P_k1k-K*S*transpose(K);
    
    innov=fin_inov;
    innovcov=S;
    obs_count=finz_count-1;
    
else
    
    X_k1k1=X_k1k;
    P_k1k1=P_k1k;
    
    innov=0;
    innovcov=0;
    obs_count=0;
    
end

end

