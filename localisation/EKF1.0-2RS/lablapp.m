classdef LM_label < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                matlab.ui.Figure
        detection_image         matlab.ui.control.UIAxes
        FORWARDButton           matlab.ui.control.Button
        BACKWARDButton          matlab.ui.control.Button
        LOADDATAButton          matlab.ui.control.Button
        map                     matlab.ui.control.UIAxes
        yoloiter                matlab.ui.control.Label
        YOLOITERATIONLabel      matlab.ui.control.Label
        ITERATIONLabel          matlab.ui.control.Label
        overalliter             matlab.ui.control.Label
        LANDMARKIDListBoxLabel  matlab.ui.control.Label
        LANDMARKIDListBox       matlab.ui.control.ListBox
        detectsize              matlab.ui.control.Label
        EXPORTDATAButton        matlab.ui.control.Button
        LM_NO                   matlab.ui.control.Label
        SAVELABELSwitchLabel    matlab.ui.control.Label
        saveon                  matlab.ui.control.Switch
    end

    
    properties (Access = public)
        iter=0 % Description
        filename='2019-02-11-Wentworth-P7-yolo';
        detection_size;
        x_pos;
        y_pos;
        
        yolonum
        scooter_LX;
        scooter_LY;
        scooter_LTHETA;
        yolo_table;
        %scooter_LTHETAX;
        %scooter_LTHETAY;
        bearings;
        centroid_x;
        centroid_y;
        LM;
        h;
        gtx;
        gty;
        cam;
        gtlabel;
        
    end
    
    methods (Access = private)
        
        function results = raycast(app,k,c)
            
            scooter_theta=app.scooter_LTHETA(k);
            scooter_theta=wrapTo2Pi(scooter_theta);
            
            if (c=='C1')
                bear=pi/4-app.bearings(k);
                gradang=scooter_theta+bear;
                gradang=wrapTo2Pi(gradang);
                lampgradient=tan(gradang);
                
            elseif(c=='C2')
                bear=pi/4+app.bearings(k);
                gradang=scooter_theta-bear;
                gradang=wrapTo2Pi(gradang);
                lampgradient=tan(gradang);
            end
            
            %lampgradient=wrapTo2Pi(lampgradient);
            
            bearing=rad2deg(app.bearings(k))
            beard=rad2deg(bear)
            scoot=rad2deg(scooter_theta)
            lampg=rad2deg(gradang)
            
            anglecheck=rad2deg(gradang);
            
            
            if abs(lampgradient) > 1
                raylength=abs(60/lampgradient);
            else
                raylength=60;
            end
            
            if (anglecheck<=90) || (anglecheck>=270)
                
                xplotz=linspace(0,raylength);
                
            elseif (anglecheck>90 && anglecheck<270)
                
                xplotz=linspace(-raylength,0);
                
            end
            
            yplotz=xplotz.*lampgradient;
            app.h(k)=plot(app.map,(xplotz+app.scooter_LX(k)),(yplotz+app.scooter_LY(k)));
            
            
            results=0;
            
        end
        
    end
    

    methods (Access = private)

        % Button pushed function: FORWARDButton
        function FORWARDButtonPushed(app, event)
            
            app.iter=app.iter+1;
            
            
            if (app.iter>0 && app.iter<app.detection_size)
                if app.iter>1
                    delete(app.h(app.iter-1))
                end
                
                cnum=app.cam{app.iter};
                
                app.raycast(app.iter,cnum);
                
                
                I = imread(strcat('/home/maleen/git/ams_primary/data_process/img_data/', app.filename, '-' ,  num2str(app.yolonum(app.iter)+1) ,'.jpg'));
                RGB = insertMarker(I,[app.centroid_x(app.iter) app.centroid_y(app.iter)],'x','color','white','size',25);
                imshow(RGB,'Parent',app.detection_image)
                
                if strcmp(app.saveon.Value,'On')
                    value = app.LANDMARKIDListBox.Value;
                    app.LM(app.iter)=str2num(value);
                    app.LM_NO.Text=num2str(app.LM(app.iter));
                else
                    app.LM_NO.Text=num2str(app.LM(app.iter));
                end
                
                app.yoloiter.Text=num2str(app.yolonum(app.iter)+1);
                app.overalliter.Text=num2str(app.iter);
                
            else
                
                app.yoloiter.Text='OUT OF BOUNDS';
                app.overalliter.Text=num2str(app.iter);
                
            end
            
            
        end

        % Button pushed function: BACKWARDButton
        function BACKWARDButtonPushed(app, event)
            
            app.iter=app.iter-1;
            
            
            if (app.iter>0 && app.iter<app.detection_size)
                if app.iter>1
                     delete(app.h(app.iter+1))
                end
               
                cnum=app.cam{app.iter};
                app.raycast(app.iter,cnum);
                
                I = imread(strcat('/home/maleen/git/ams_primary/data_process/img_data/', app.filename, '-' , num2str(app.yolonum(app.iter)+1) ,'.jpg'));
                RGB = insertMarker(I,[app.centroid_x(app.iter) app.centroid_y(app.iter)],'x','color','white','size',25);
                imshow(RGB,'Parent',app.detection_image)
                
                
                app.LM_NO.Text=num2str(app.LM(app.iter));
                
                
                app.yoloiter.Text=num2str(app.yolonum(app.iter)+1);
                app.overalliter.Text=num2str(app.iter);
                
            else
                
                app.yoloiter.Text='OUT OF BOUNDS';
                app.overalliter.Text=num2str(app.iter);
                
            end
            
        end

        % Button pushed function: LOADDATAButton
        function LOADDATAButtonPushed(app, event)
            %odom_data=csvread(strcat('./csv_data/', app.filename, '-odom.csv'), 1, 0, [1 0 1851 5]);
            %lamp_data=csvread(strcat('./csv_data/', app.filename, '-lampdata.csv'), 1, 0, [1 0 522 10]);
            load('2019-02-11-Wentworth-P7-prelabel.mat','deadreck_data','lamp_data','enu_rot');
            
            load('2019-02-11-Wentworth-gtmap.mat','gtmap_enu');
            
            app.yolo_table=lamp_data;
            
            app.x_pos=transpose(enu_rot(1,:));
            app.y_pos=transpose(enu_rot(2,:));
            
            app.yolonum=lamp_data{:,2};
            app.bearings=lamp_data{:,5};
            app.scooter_LX=lamp_data{:,8};
            app.scooter_LY=lamp_data{:,9};
            app.scooter_LTHETA=lamp_data{:,10};
            app.cam=lamp_data{:,4};
            %app.scooter_LTHETAX=lamp_data(:,10);
            %app.scooter_LTHETAY=lamp_data(:,11);
            app.detection_size=length(app.yolonum);
            
            app.centroid_x=lamp_data{:,6};
            app.centroid_y=lamp_data{:,7};
            
            app.gtx=transpose(gtmap_enu(1,:));
            app.gty=transpose(gtmap_enu(2,:));
            app.gtlabel=transpose(gtmap_enu(4,:));
            
            load('2019-02-11-Wentworth-P7-yololabeled.mat','yolo_ex')
            app.LM=yolo_ex{:,13};
            
            %app.LM=zeros(app.detection_size,1);
            
            app.detectsize.Text=strcat(num2str(app.detection_size), ' data points loaded');
            
            hold(app.map,'on')
            app.map.DataAspectRatio=[1,1,1];
            app.map.PlotBoxAspectRatio=[2,1.5,1];
            plot(app.map,app.x_pos,app.y_pos)
            scatter(app.map,app.gtx,app.gty)
            text(app.map,app.gtx,app.gty,num2str(app.gtlabel))
            
        end

        % Button pushed function: EXPORTDATAButton
        function EXPORTDATAButtonPushed(app, event)
            exportname=strcat(app.filename,'labeled.mat');
            
            x_posEX=app.x_pos;
            y_posEX=app.y_pos;
            
            
            
            scooter_LXEX=app.scooter_LX;
            scooter_LYEX=app.scooter_LY;
            scooter_LTHETAEX=app.scooter_LTHETA;
            %scooter_LTHETAXEX=app.scooter_LTHETAX;
            %scooter_LTHETAYEX=app.scooter_LTHETAY;
            
            bearingsEX=app.bearings;
            centroid_xEX=app.centroid_x;
            centroid_yEX=app.centroid_y;
            yolonumEX=app.yolonum;
            
            LMEX=app.LM;
            
            app.yolo_table.LM=LMEX;
            
            yolo_ex=app.yolo_table;
            
            save(exportname,'yolo_ex');
        end

        % Value changed function: LANDMARKIDListBox
        function LANDMARKIDListBoxValueChanged(app, event)
            value = app.LANDMARKIDListBox.Value;
            app.LM(app.iter)=str2num(value);
            app.LM_NO.Text=num2str(value);
            
        end
    end

    % App initialization and construction
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure
            app.UIFigure = uifigure;
            app.UIFigure.Position = [100 100 1184 803];
            app.UIFigure.Name = 'UI Figure';

            % Create detection_image
            app.detection_image = uiaxes(app.UIFigure);
            title(app.detection_image, 'DETECTION IMAGE')
            app.detection_image.Position = [531 427 491 367];

            % Create FORWARDButton
            app.FORWARDButton = uibutton(app.UIFigure, 'push');
            app.FORWARDButton.ButtonPushedFcn = createCallbackFcn(app, @FORWARDButtonPushed, true);
            app.FORWARDButton.Position = [284 468 132 64];
            app.FORWARDButton.Text = 'FORWARD';

            % Create BACKWARDButton
            app.BACKWARDButton = uibutton(app.UIFigure, 'push');
            app.BACKWARDButton.ButtonPushedFcn = createCallbackFcn(app, @BACKWARDButtonPushed, true);
            app.BACKWARDButton.Position = [139 468 132 64];
            app.BACKWARDButton.Text = 'BACKWARD';

            % Create LOADDATAButton
            app.LOADDATAButton = uibutton(app.UIFigure, 'push');
            app.LOADDATAButton.ButtonPushedFcn = createCallbackFcn(app, @LOADDATAButtonPushed, true);
            app.LOADDATAButton.Position = [173 574 209 58];
            app.LOADDATAButton.Text = 'LOAD DATA';

            % Create map
            app.map = uiaxes(app.UIFigure);
            title(app.map, 'MAP')
            xlabel(app.map, 'X')
            ylabel(app.map, 'Y')
            app.map.Box = 'on';
            app.map.XGrid = 'on';
            app.map.YGrid = 'on';
            app.map.Position = [491 34 569 380];

            % Create yoloiter
            app.yoloiter = uilabel(app.UIFigure);
            app.yoloiter.Position = [311 667 46 22];
            app.yoloiter.Text = '';

            % Create YOLOITERATIONLabel
            app.YOLOITERATIONLabel = uilabel(app.UIFigure);
            app.YOLOITERATIONLabel.Position = [191 667 109 22];
            app.YOLOITERATIONLabel.Text = 'YOLO ITERATION:';

            % Create ITERATIONLabel
            app.ITERATIONLabel = uilabel(app.UIFigure);
            app.ITERATIONLabel.Position = [191 704 72 22];
            app.ITERATIONLabel.Text = 'ITERATION:';

            % Create overalliter
            app.overalliter = uilabel(app.UIFigure);
            app.overalliter.Position = [309 704 48 22];
            app.overalliter.Text = '';

            % Create LANDMARKIDListBoxLabel
            app.LANDMARKIDListBoxLabel = uilabel(app.UIFigure);
            app.LANDMARKIDListBoxLabel.HorizontalAlignment = 'right';
            app.LANDMARKIDListBoxLabel.Position = [139 413 88 22];
            app.LANDMARKIDListBoxLabel.Text = 'LANDMARK ID';

            % Create LANDMARKIDListBox
            app.LANDMARKIDListBox = uilistbox(app.UIFigure);
            app.LANDMARKIDListBox.Items = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'};
            app.LANDMARKIDListBox.ItemsData = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'};
            app.LANDMARKIDListBox.ValueChangedFcn = createCallbackFcn(app, @LANDMARKIDListBoxValueChanged, true);
            app.LANDMARKIDListBox.Position = [262 137 154 300];
            app.LANDMARKIDListBox.Value = '10';

            % Create detectsize
            app.detectsize = uilabel(app.UIFigure);
            app.detectsize.Position = [395 592 156 22];
            app.detectsize.Text = '';

            % Create EXPORTDATAButton
            app.EXPORTDATAButton = uibutton(app.UIFigure, 'push');
            app.EXPORTDATAButton.ButtonPushedFcn = createCallbackFcn(app, @EXPORTDATAButtonPushed, true);
            app.EXPORTDATAButton.Position = [173 52 209 58];
            app.EXPORTDATAButton.Text = 'EXPORT DATA';

            % Create LM_NO
            app.LM_NO = uilabel(app.UIFigure);
            app.LM_NO.FontSize = 48;
            app.LM_NO.Position = [1031 454 70 80];
            app.LM_NO.Text = '';

            % Create SAVELABELSwitchLabel
            app.SAVELABELSwitchLabel = uilabel(app.UIFigure);
            app.SAVELABELSwitchLabel.HorizontalAlignment = 'center';
            app.SAVELABELSwitchLabel.Position = [145 308 77 22];
            app.SAVELABELSwitchLabel.Text = 'SAVE LABEL';

            % Create saveon
            app.saveon = uiswitch(app.UIFigure, 'slider');
            app.saveon.Position = [160 345 45 20];
        end
    end

    methods (Access = public)

        % Construct app
        function app = LM_label

            % Create and configure components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end