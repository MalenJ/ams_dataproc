classdef edge_obsZ
    %EDGE_OBSZ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        sensortype
        img_time
        edge_cam
        ranges
        bearings
        %depthedge
        flagger
    end
    
    methods
        
        function obj = edge_obsZ(FC_params,img_time,depthedge,T_robot_backcam,usampvec,vsampvec)
            obj.sensortype='BC';
            obj.img_time=img_time;
            PP=FC_params.PrincipalPoint;
            FL=FC_params.FocalLength;

            j=1;

            %250s
            for u=1:length(usampvec)
                
                usamp=usampvec(u);
               
                for v=1:length(vsampvec)
                    
                    vsamp=vsampvec(v);
                    depth=double(depthedge(usamp,vsamp))/1000;
                    
                    if depth>0
                        
                        z_cam=depth;
                        x_cam=((usamp-PP(2))*depth)/(FL(2));
                        y_cam=((vsamp-PP(1))*depth)/(FL(1));
                        
                        obj.edge_cam(:,j)=T_robot_backcam*[z_cam;-y_cam;-x_cam;1];
                        
                        j=j+1;
                    end

                end

            end
            
            
            
            for i=1:size(obj.edge_cam,2)
                
                if obj.edge_cam(3,i) < 0
                
                    obj.ranges(i)=sqrt((obj.edge_cam(1,i))^2+(obj.edge_cam(2,i))^2);
                    obj.bearings(i)=wrapTo2Pi(atan2(obj.edge_cam(2,i),obj.edge_cam(1,i)));

                end
            end
            
            obj.flagger=size(obj.ranges);
            
        end
        

    end
end

