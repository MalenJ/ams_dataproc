close all
clear all

%Load localisation CSV's

% fileroot='2020-02-28-Wentworth';
% filename='2020-02-28-Wentworth-P1';
% configname='VINS2.mat';

% fileroot='2020-02-19-Village';
% filename='2020-02-19-Village-P1';
% configname='VINS3.mat';

fileroot='2021-01-11-UTS';
filename='2021-01-11-UTS-P1';
configname='VINS6.mat';


%Paths
CONFIGPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/config/'); %CSV DATA
CSVPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
IMGPATH=strcat('/media/maleen/malen_ssd/phd/critical_ams_data/',fileroot,'/image_data/',filename,'/'); %IMAGE DATA

MAPPATH_feature='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/'; %MAP DATA
MAPPATH_edge='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/';

%Load config
load(strcat(CONFIGPATH, filename,'-prepconfig-', configname))

odom_sorted=readtable(strcat(CSVPATH, filename, '-sorted.csv'));
enc_data=readtable(strcat(CSVPATH, filename, '-encoder.csv'));
imu_data=readtable(strcat(CSVPATH, filename, '-imu.csv'));
rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));

lamp_data_real=readtable(strcat(CSVPATH,filename, '-yrs-lampdata.csv'));
% lamp_data_lb=readtable(strcat(CSVPATH,filename, '-ylb-lampdata.csv'));
% lamp_data_lf=readtable(strcat(CSVPATH,filename, '-ylf-lampdata.csv'));
% lamp_data_rb=readtable(strcat(CSVPATH,filename, '-yrb-lampdata.csv'));
% lamp_data_rf=readtable(strcat(CSVPATH,filename, '-yrf-lampdata.csv'));
% lamp_sorted_lb=readtable(strcat(CSVPATH,filename, '-ylb-sorted_yolo.csv'));
% lamp_sorted_lf=readtable(strcat(CSVPATH,filename, '-ylf-sorted_yolo.csv'));
% lamp_sorted_rb=readtable(strcat(CSVPATH,filename, '-yrb-sorted_yolo.csv'));
% lamp_sorted_rf=readtable(strcat(CSVPATH,filename, '-yrf-sorted_yolo.csv'));

%Back camera
backcam_depthdata=readtable(strcat(CSVPATH,filename, '-back_depth.csv'));
backcam_edgedata=readtable(strcat(CSVPATH,filename, '-back_rgb.csv'));
load(strcat(IMGPATH, filename, '-depthimgs.mat'),'depth_imgs');

%Camera Params
load(strcat(CALIBPATH,'backcam/back_calibapp.mat'),'cameraParams');
BC_params=cameraParams;
load(strcat(CALIBPATH,'left/left_calibapp.mat'),'cameraParams');
LC_params=cameraParams;
load(strcat(CALIBPATH,'right/right_calibapp.mat'),'cameraParams');
RC_params=cameraParams;
% load(strcat(CALIBPATH,'left_front/leftfront_calibapp.mat'),'cameraParams');
% LF_params=cameraParams;
% load(strcat(CALIBPATH,'left_back/leftback_calibapp.mat'),'cameraParams');
% LB_params=cameraParams;
% load(strcat(CALIBPATH,'right_back/rightback_calibapp.mat'),'cameraParams');
% RB_params=cameraParams;
% load(strcat(CALIBPATH,'right_front/rightfront_calibapp.mat'),'cameraParams');
% RF_params=cameraParams;

centerX=ones(1,6);
centerX(1)=213;%LF_params.PrincipalPoint(1);
centerX(2)=213;%RF_params.PrincipalPoint(1);
centerX(3)=213;%LB_params.PrincipalPoint(1);
centerX(4)=213;%RB_params.PrincipalPoint(1);
centerX(5)=LC_params.PrincipalPoint(1);
centerX(6)=RC_params.PrincipalPoint(1);

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');

%LOAD EL MAP
load(strcat(MAPPATH_feature,el_map),maptype);

%LOAD EDGE MAP
I=imread(strcat(MAPPATH_edge,ed_map)); %funk1

%% DATA STREAM

%BFYT=vertcat(lamp_data_lb,lamp_data_lf,lamp_data_rb,lamp_data_rf);%RICOH
BFYT=lamp_data_real;
yolo_observations = sortrows(BFYT,'lamp_times');
yolo_time=yolo_observations{:,3};

uj=1;
labels={};

for i=1:size(yolo_time)
    
    if i==1
        overall_time(uj)=yolo_time(i);
        labels(uj)={'Y'};
        uj=uj+1;
    else
        if yolo_time(i)>yolo_time(i-1)
            overall_time(uj)=yolo_time(i);
            labels(uj)={'Y'};
            uj=uj+1;
        end
        
    end
    
end

yolo_sorted=table(transpose(overall_time),transpose(labels));
yolo_sorted.Properties.VariableNames = {'overall_time' 'labels'};

BFTS=vertcat(odom_sorted(:,{'overall_time', 'labels'}),yolo_sorted);
data_stream = sortrows(BFTS,'overall_time');


%% Analyse RTK

% rtk_time=rtk_data{:,2};
% rtk_lat=rtk_data{:,3};
% rtk_long=rtk_data{:,4};
% rtk_alti=rtk_data{:,5};
%
% rtk_iter=find(rtk_time<stationary_time, 1, 'last' ); %Wentworth
%
% if calc_RTK_base==1
%     disp('calc_RTK_base==1')
%     base_lat=rtk_lat(RTK_base_iter);
%     base_long=rtk_long(RTK_base_iter);
%     base_alti=rtk_alti(RTK_base_iter);
% elseif calc_RTK_base==2
%     disp('calc_RTK_base==2')
%     % base_lat=mean(rtk_lat(1:rtk_iter));
%     % base_long=mean(rtk_long(1:rtk_iter));
%     % base_alti=mean(rtk_alti(1:rtk_iter));
% else
%     disp('calc_RTK_base==0')
% end
%
% [rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);
%
%
%
% figure
% hold on
% scatter(rtk_enux,rtk_enuy)
% scatter(0,0,'r','x')
% axis('equal')
%
% figure
% scatter(rtk_time,rtk_enux)
%
%
% %% ROTATE AND TRANSLATE RTK DATA
%
% %Finding rotation between RTK map and odometry
% if calc_RZ==1
%     disp('calc_RZ==1')
%     RTK_POS=[ppval(rtkinterp_x ,54) ppval(rtkinterp_y ,54) 0];
%     RTAB_POS=[0 1 0];
%     t=atan2(norm(cross(RTK_POS,RTAB_POS)), dot(RTK_POS,RTAB_POS))-deg2rad(3);
%     rad2deg(t);
%     Rz = [cos(-t-pi/2) -sin(-t-pi/2) RTK_tx; sin(-t-pi/2) cos(-t-pi/2) RTK_ty; 0 0 1];
% else
%     disp('calc_RZ==0')
%     Rz=[1 0 RTK_tx;0 1 RTK_ty;0 0 1];
% end
%
% %Translation was figured out based on running EKF in the begining and then translating it based on RTK location relative to the baselink.
%
%
% enu_rot=Rz*[rtk_enux'; rtk_enuy';ones(1,length(rtk_enux))];
%
% figure
% scatter(enu_rot(1,:),enu_rot(2,:))
%
% rtk_enux=enu_rot(1,:);
% rtk_enuy=enu_rot(2,:);
% rtkinterp_x = spline([0;rtk_time],[0;rtk_enux']);
% rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy']);
%
% %% RTK Heading
%
% for i=1:length(rtk_time)-15
%
%     rtkXdist=rtk_enux(i+15)-rtk_enux(i);
%     rtkYdist=rtk_enuy(i+15)-rtk_enuy(i);
%
%     sqrt(rtkXdist^2+rtkYdist^2);
%     rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
%     rtk_heading(i,:)=atan2(rtkYdist,rtkXdist);
%     rtkdist_timer(i,:)=rtk_time(i,:);
%
% end
%
% rtk_heading=wrapTo2Pi(rtk_heading);
% rtkinterp_z = spline([0;rtk_time],[0;rtk_enuz]);
% rtkinterp_theta= spline([0;rtkdist_timer],[0;rtk_heading]);
%
% figure
% hold on
% scatter(rtkdist_timer,rad2deg(rtk_heading),'b')
%
% % figure
% % hold on
% % plot(rtkdist_timer,rad2deg(rtk_heading),'b')
% % plot(rtkdist_timer,rad2deg(ppval(rtkinterp_theta,rtkdist_timer)),'r')
% % % %
% % figure
% % hold on
% % plot(rtk_enux,rtk_enuy,'b')
% % plot(ppval(rtkinterp_x,test),ppval(rtkinterp_y,test),'-x')

%% ODOM DATA

enc_interp=spline(enc_data{:,3},enc_data{:,6});

encXcalib=6.608284615932709e-05;
encYcalib=6.603486193757895e-05;
calib=mean([encXcalib,encYcalib]);

%IMU DATA
imu_iter=find(imu_data{:,2}<stationary_time, 1, 'last' );

imu_bias=mean(imu_data{1:imu_iter,3});
imu_data{:,3}=imu_data{:,3}-imu_bias;

imu_smooth=movmean(imu_data{:,3},50);

figure
hold on
plot(imu_data{:,2},imu_data{:,3},'b')
plot(imu_data{:,2},imu_smooth,'r')

imu_interp=spline(imu_data{:,2},imu_data{:,3});
imu_interp1=spline(imu_data{:,2},imu_smooth);
imu_interp2=interp1(imu_data{:,2},imu_smooth,'linear','pp');

%Figuring out initial orientation

%initial_theta=rtk_heading(450);

%% BACK CAMERA

roter=eul2tform(cam2base_eul_fin);
T_robot_backcam=makehgtform('translate',cam2base_trans_fin)*roter;

j=0;
for i=1:length(backcam_depthdata.back_depth_time)
    
    depth_time=backcam_depthdata.back_depth_time(i);
    
    row=backcam_edgedata.back_rgb_time==depth_time;
    edge_indx=find(row>0);
    
    if ~isnan(edge_indx)
        j=j+1;
        
        edgeimg=imread(strcat(IMGPATH,'edge/', filename ,'-edge-',num2str(edge_indx), ".jpg"));
        %edgeimg=imread(strcat(IMGPATH,dataset,'edge/l5/', filename ,'-edge-',num2str(edge_indx), ".jpg"));
        BW = im2bw(edgeimg,0.5);
        %imshow(BW)
        
        BWU = undistortImage(BW, BC_params);
        depthimgU = undistortImage(depth_imgs{i},BC_params);
        depthedge_time(j)=depth_time;
        depthedge{j}=immultiply(BWU,depthimgU);
        
    else
        disp('skip')
    end
    
end

%% EL MAP

if location==0
    disp('Location= Wentworth')
    [map_enu(:,1), map_enu(:,2), map_enu(:,3)]=geodetic2enu(map_sat(:,1),map_sat(:,2),map_sat(:,3),base_lat,base_long,base_alti, wgs84Ellipsoid);
    map_enu_ID=transpose(linspace(1,length(map_enu(:,1)),length(map_enu(:,1))));
    map_enu_semantic=[1;4;1;4;4;1;1;1;4;1;1;1;1;1;1;1;4];
    LMmap=[map_enu_ID,map_enu(:,1)+0.35,map_enu(:,2)-1.7,map_enu_semantic];
elseif location==1
    disp('Location= Village')
    LMmap=final_map;
elseif location==2
    disp('Location= UTS')
    LMmap=final_map;
end

figure
axis equal
hold on
for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3))
    text(LMmap(i,2),LMmap(i,3), num2str(i));
end

%% EDGE MAP

binary_map=imbinarize(rgb2gray(imcomplement(I)));
% SE = strel('square',5);
% dilatedmap = imdilate(binary_map,SE);
dfobj = DistanceFunctions2D(binary_map, -xMin, -yMin, sizeX, sizeY, res);
clear depth_imgs;

%% RTAB DATA

if prep_RTAB==1
    disp('RTAB=1')
    rtab_data=readtable(strcat(CSVPATH,filename, '-rtabdata.csv'));
    
    rtab_time=rtab_data{:,3};
    rtabx=rtab_data{:,4};
    rtaby=rtab_data{:,5};
    rtabquat=rtab_data{:,6:9};
    rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);
    rtabinterp_x= interp1(rtab_time,rtabx,'linear','pp');%RTAB_world(1,:));
    rtabinterp_y = interp1(rtab_time,rtaby,'linear','pp');%RTAB_world(2,:));
    rtabinterp_z = interp1(rtab_time,zeros(length(rtabx),1),'linear','pp');%RTAB_world(3,:));
    rtabinterp_theta = interp1(rtab_time,rtabeul(:,1),'linear','pp');
    
    % rtabinterp_x= spline(rtab_time,rtabx);%RTAB_world(1,:));
    % rtabinterp_y = spline(rtab_time,rtaby);%RTAB_world(2,:));
    % rtabinterp_z = spline(rtab_time,zeros(length(rtabx),1));%RTAB_world(3,:));
    % rtabinterp_theta = spline(rtab_time,rtabeul(:,1));
    
    for i=1:length(rtab_time)-1
        
        rtabkXdist=rtabx(i+1)-rtabx(i);
        rtabkYdist=rtaby(i+1)-rtaby(i);
        
        rtabdist(i,:)=sqrt(rtabkXdist^2+rtabkYdist^2);
        rtab_heading(i,:)=atan2(rtabkYdist,rtabkXdist);
        rtabdist_timer(i,:)=rtab_time(i,:);
        
    end
    
    rtabinterp_heading=interp1(rtabdist_timer,rtab_heading,'linear','pp');
end

%% VINS data

%close all

if prep_VINS==1
    disp('VINS=1')
    
    VINSVIO_data=readtable(strcat(CSVPATH, filename, '-VINS-vio.csv'));
    %VINSVIO_data=readtable(strcat(CSVPATH,  'vio.csv'));
    
    vins_time=VINSVIO_data{:,1}/1000000000-data_zero;
    vins_x=VINSVIO_data{:,2};
    vins_y=VINSVIO_data{:,3};
    vins_z=VINSVIO_data{:,4};
    
    vinsx_interp=spline(vins_time,vins_x);
    vinsy_interp=spline(vins_time,vins_y);
    
    if location==0
        RTK_POS=[ppval(rtkinterp_x ,30) ppval(rtkinterp_y ,30) 0];
        VINS_POS=[ppval(vinsx_interp ,30) ppval(vinsy_interp ,30) 0];
        t=atan2(norm(cross(RTK_POS,VINS_POS)), dot(RTK_POS,VINS_POS))-deg2rad(4.5);
        rad2deg(t)
        Rzvins=[cos(t) -sin(t) ppval(rtkinterp_x ,15); sin(t) cos(t) (ppval(rtkinterp_y ,15)-0.25); 0 0 1];
        vins_pose=Rzvins*[vins_x'; vins_y';ones(1,length(vins_x))];
        vins_x=vins_pose(1,:);
        vins_y=vins_pose(2,:);
        
    elseif location==1 
        
        tvins=deg2rad(1.55);
        
        Rzvins=[cos(tvins) -sin(tvins) 0; sin(tvins) cos(tvins) 0; 0 0 1];
        vins_pose=Rzvins*[vins_xt'; vins_yt';ones(1,length(vins_xt))];
        vins_xt=vins_pose(1,:);
        vins_yt=vins_pose(2,:);
        
        
    elseif location==2 %THIS WASNT DONE FOR ANY OF THE UTS RESULTS SO HAD TO MANUALLY DO IT IN THE EKF MAIN
        RTAB_POS=[ppval(rtabinterp_x ,100) ppval(rtabinterp_y ,100) 0];
        VINS_POS=[ppval(vinsx_interp ,100) ppval(vinsy_interp ,100) 0];
        t=atan2(norm(cross(RTAB_POS,VINS_POS)), dot(RTAB_POS,VINS_POS))-deg2rad(7);
        rad2deg(t)
        Rzvins=[cos(t) -sin(t) ppval(rtabinterp_x ,15)-4; sin(t) cos(t) (ppval(rtabinterp_y ,15)+1); 0 0 1];
        vins_pose=Rzvins*[vins_x'; vins_y';ones(1,length(vins_x))];
        vins_x=vins_pose(1,:);
        vins_y=vins_pose(2,:);
    end
    
    vinsquat=VINSVIO_data{:,5:8};
    vinseul =(quat2eul([vinsquat(:,1) vinsquat(:,2:4)]));
    vins_yaw=wrapTo2Pi(vinseul(:,1));
    
    vinsx_interp=spline(vins_time,vins_x);
    vinsy_interp=spline(vins_time,vins_y);
    vinsyaw_interp=spline(vins_time,vins_yaw);
    
    vins_vel_smooth=movmean(vins_yaw,20);
    
    figure
    title('vins odom')
    hold on
    %    scatter(rtk_enux,rtk_enuy,'r')
    scatter(0,0,'r','x')
    scatter(vins_x,vins_y,'b')
    
    axis('equal')
    
    
    figure
    hold on
    
    plot(vins_time,rad2deg(vins_yaw),'r');
    plot(vins_time,rad2deg(vins_vel_smooth),'b');
    
    
    vins_label= repmat({'V'},length(vins_time),1);
    
    VINS_sorted=table(vins_time,vins_label);
    VINS_sorted.Properties.VariableNames = {'overall_time' 'labels'};
    
    BFTS2=vertcat(data_stream(:,{'overall_time', 'labels'}),VINS_sorted);
    
    data_stream = sortrows(BFTS2,'overall_time');
    
end

%%

clear backcam_depthdata backcam_edgedata BFTS BFTS2 BFYT BW BWU depthimgU enc_data lamp_data_real odom_sorted yolo_sorted;



