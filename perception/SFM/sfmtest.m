clear all
close all

imageDIR='yolo2/';
imds = imageDatastore(imageDIR);
%
figure
montage(imds.Files, 'Size', [2, 2]);
CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/IROScalib/'; %CLIB DATA
load(strcat(CALIBPATH,'right/right_calibapp.mat'),'cameraParams');
RC_params=cameraParams;

img1_lb=[174,346];
img1_ub=[190,346];

img2_lb=[975,346];
img2_ub=[994,346];


img1_width=img1_ub(1)-img1_lb(1);

img2_width=img2_ub(1)-img2_lb(1);

matchedPoints1=transpose([img1_lb(1):1:img1_ub(1);img1_ub(2)*ones(1,img1_width+1)]);
matchedPoints2=transpose([img2_lb(1):img2_width/img1_width:img2_ub(1);img1_ub(2)*ones(1,img1_width+1)]);


[E,inliersIndex,status] = estimateEssentialMatrix(matchedPoints1,matchedPoints2,RC_params);

inlierPoints1 = matchedPoints1(inliersIndex, :);
inlierPoints2 = matchedPoints2(inliersIndex, :);

[orient, loc] = relativeCameraPose(E, cameraParams, inlierPoints1, inlierPoints2);


camMatrix1 = cameraMatrix(cameraParams, eye(3), [0 0 0]);

% Compute extrinsics of the second camera
[R, t] = cameraPoseToExtrinsics(orient, loc);
camMatrix2 = cameraMatrix(cameraParams, R, t);

% Compute the 3-D points
points3D = triangulate(matchedPoints1, matchedPoints2, camMatrix1, camMatrix2);


% Create the point cloud
ptCloud = pointCloud(points3D);

cameraSize = 0.1;
figure
plotCamera('Size', cameraSize, 'Color', 'r', 'Label', '1', 'Opacity', 0);
hold on
grid on
plotCamera('Location', loc, 'Orientation', orient, 'Size', cameraSize, ...
    'Color', 'b', 'Label', '2', 'Opacity', 0);

% Visualize the point cloud
pcshow(ptCloud, 'VerticalAxis', 'y', 'VerticalAxisDir', 'down', ...
    'MarkerSize', 45);

% Rotate and zoom the plot
camorbit(0, -30);
camzoom(1.5);

% Label the axes
xlabel('x-axis');
ylabel('y-axis');
zlabel('z-axis')