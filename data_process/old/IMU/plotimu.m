
close all

%load('imuvecs.mat')
% rawRPY_Z_ts=timeseries(rawRPY_Z,raw_stamp,'Name','RPY_Z');
% rawMAG_Z_ts=timeseries(rawMAG_Z,raw_stamp);
% rawACC_Z_ts=timeseries(rawACC_Z,raw_stamp);
angv_zorig=realAngv_Z-ones(1,imu_size)*realAngv_Z(1,1);

drifterror=zeros(1,imu_size);
driftlinet=polyfit([0,1],[realAngv_Z(1,1),realAngv_Z(1,end)],1);
x = linspace(0,1,imu_size);

drifterror= polyval(driftlinet,x);
angv_zcorrect=realAngv_Z-drifterror;

theta=cumtrapz(raw_stamp,angv_zorig);
corrected_theta=cumtrapz(raw_stamp,angv_zcorrect);


figure
plot(raw_stamp, rawRPY_Z)
title('Raw Yaw')
figure
plot(raw_stamp, rawMAG_Z)
title('Raw Mag Z')
figure
plot(raw_stamp, rawACC_Z)
title('Raw Acc Z')
figure
plot(raw_stamp, realAngv_Z)
title('Angular Vel Z')
figure
plot(raw_stamp, rawGYRO_Z)
title('Raw gyro Z')
figure
hold on
plot(raw_stamp, theta)
plot(raw_stamp, corrected_theta)
title('angular disp')
figure
plot(raw_stamp, drifterror)
title('drift error linear')