close all


imu_size=length(imurawdata);
raw_stamp=zeros(1,imu_size);
rawRPY_X=zeros(1,imu_size);
rawRPY_Y=zeros(1,imu_size);
rawRPY_Z=zeros(1,imu_size);
rawGYRO_X=zeros(1,imu_size);
rawGYRO_Y=zeros(1,imu_size);
rawGYRO_Z=zeros(1,imu_size);
rawMAG_X=zeros(1,imu_size);
rawMAG_Y=zeros(1,imu_size);
rawMAG_Z=zeros(1,imu_size);
rawACC_X=zeros(1,imu_size);
rawACC_Y=zeros(1,imu_size);
rawACC_Z=zeros(1,imu_size);
realAngv_X=zeros(1,imu_size);
realAngv_Y=zeros(1,imu_size);
realAngv_Z=zeros(1,imu_size);

for i=1:imu_size
    
   raw_stamp(1,i)=(imurawdata{i,1}.Header.Stamp.Sec+(imurawdata{i,1}.Header.Stamp.Nsec)/1000000000)-(imurawdata{1,1}.Header.Stamp.Sec+(imurawdata{1,1}.Header.Stamp.Nsec)/1000000000);
   rawRPY_X(1,i)=imurawdata{i,1}.RPY.X-imurawdata{1,1}.RPY.X;
   rawRPY_Y(1,i)=imurawdata{i,1}.RPY.Y-imurawdata{1,1}.RPY.Y;
   rawRPY_Z(1,i)=imurawdata{i,1}.RPY.Z-imurawdata{1,1}.RPY.Z;
   
   rawGYRO_X(1,i)=imurawdata{i, 1}.Gyroscope.X;
   rawGYRO_Y(1,i)=imurawdata{i, 1}.Gyroscope.Y;
   rawGYRO_Z(1,i)=imurawdata{i, 1}.Gyroscope.Z;
   
   rawMAG_X(1,i)=imurawdata{i, 1}.Magnetometer.X;
   rawMAG_Y(1,i)=imurawdata{i, 1}.Magnetometer.Y;
   rawMAG_Z(1,i)=imurawdata{i, 1}.Magnetometer.Z;
   
   rawACC_X(1,i)=imurawdata{i, 1}.Accelerometer.X;
   rawACC_Y(1,i)=imurawdata{i, 1}.Accelerometer.Y;
   rawACC_Z(1,i)=imurawdata{i, 1}.Accelerometer.Z;
   
   realAngv_X(1,i)=imurealdata{i, 1}.AngularVelocity.X;
   realAngv_Y(1,i)=imurealdata{i, 1}.AngularVelocity.Y;
   realAngv_Z(1,i)=imurealdata{i, 1}.AngularVelocity.Z;
    
end


