close all

%Load rosbags
% bag=rosbag('/home/maleen/2018-09-19-17-12-11.bag');
% 
% %Select topics
% gpsbag=select(bag,'Topic','/piksi/navsatfix_best_fix');
% rtkbag=select(bag,'Topic','/piksi/navsatfix_rtk_fix');
% enubag=select(bag,'Topic','/piksi/enu_pose_best_fix');
% 
% %Read messages
% % gpsfix=readMessages(gpsbag);
% % rtkfix=readMessages(rtkbag);
% enufix=readMessages(enubag);
enusize=length(enufix);
enu_stamp=zeros(1,enusize);
enu_x=zeros(1,enusize);
enu_y=zeros(1,enusize);
enu_z=zeros(1,enusize);

% 
% gps_size=length(gpsfix);
% rtk_size=length(rtkfix);
% 
% gps_stamp=zeros(1,gps_size);
% rtk_stamp=zeros(1,rtk_size);
% 
% 
% x_rtk=zeros(1,rtk_size);
% y_rtk=zeros(1,rtk_size);
% z_rtk=zeros(1,rtk_size);
% 
% rtk_lat=zeros(1,rtk_size);
% rtk_long=zeros(1,rtk_size);
% rtk_alti=zeros(1,rtk_size);
% 
% lat=zeros(1,gps_size);
% long=zeros(1,gps_size);
% alti=zeros(1,gps_size);
% 
base_lat=-33.8830699601;
base_long=151.200418323;
base_alti=39.9730949684;

% x_gps=zeros(1,gps_size);
% y_gps=zeros(1,gps_size);
% z_gps=zeros(1,gps_size);

%Read data
% 
% for i=1:gps_size
% 
%     gps_stamp(1,i)=gpsfix{i,1}.Header.Stamp.Sec+(gpsfix{i,1}.Header.Stamp.Nsec)/1000000000;
%     lat(1,i)=gpsfix{i,1}.Latitude;
%     long(1,i)=gpsfix{i,1}.Longitude; 
%     alti(1,i)=gpsfix{i,1}.Altitude; 
% end

%[x_gps, y_gps, z_gps]= lla2ecef(deg2rad(lat),deg2rad(long),alti);
%[x_gps, y_gps, z_gps] = geodetic2enu(lat,long,alti,base_lat,base_long,base_alti, wgs84Ellipsoid);



% for i=1:rtk_size
% 
%     rtk_stamp(1,i)=(rtkfix{i,1}.Header.Stamp.Sec+(rtkfix{i,1}.Header.Stamp.Nsec)/1000000000);
%     rtk_lat(1,i)=rtkfix{i,1}.Latitude;
%     rtk_long(1,i)=rtkfix{i,1}.Longitude;
%     rtk_alti(1,i)=rtkfix{i,1}.Altitude;
%     
% end

for i=1:enusize

    enu_stamp(1,i)=(enufix{i,1}.Header.Stamp.Sec+(enufix{i,1}.Header.Stamp.Nsec)/1000000000);
    enu_x(1,i)=enufix{i,1}.Pose.Pose.Position.X;
    enu_y(1,i)=enufix{i,1}.Pose.Pose.Position.Y;
    enu_z(1,i)=enufix{i,1}.Pose.Pose.Position.Z;
    
    
    
end


%[rtk_lat, rtk_long, rtk_alti]=enu2geodetic(x_rtk,y_rtk,z_rtk,base_lat,base_long,base_alti, wgs84Ellipsoid);



