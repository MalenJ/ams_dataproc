close all

datacell=csvread('cellular2.csv', 1, 0, [1 0 219 6]); 

lat=datacell(:,1);
long=datacell(:,2);
alti= datacell(:,3);

hold on
plot(long, lat, '.g', 'MarkerSize', 20) 
plot_google_map('MapType','satellite','MapScale', 1) 
legend('Assisted GPS')
