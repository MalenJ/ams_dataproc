close all

%Load rosbags
bag=rosbag('/home/maleen/2018-09-19-17-12-11.bag');

%Select topics
wifibag=select(bag,'Topic','/wifidata_ts');
encbag=select(bag,'Topic','/encoder_ticks_ts');
%imurawbag=select(bag,'Topic','/imuraw_ts');
%imurealbag=select(bag,'Topic','/imureal_ts');


%Read messages
wifidata=readMessages(wifibag);
encdata=readMessages(encbag);
% imurawdata=readMessages(imurawbag);
% imurealdata=readMessages(imurealbag);
