

encXcalib=6.608284615932709e-05;
encYcalib=6.603486193757895e-05;

L=0.475;

meancalib=mean([encXcalib,encYcalib]);

d=zeros(1,enc_size-1);
d_theta=zeros(1,enc_size-1);
dx=zeros(1,enc_size-1);
dy=zeros(1,enc_size-1);
dx_enc=zeros(1,enc_size-1);
dy_enc=zeros(1,enc_size-1);
totaldist=0;

X_pos=zeros(1,enc_size-1);
Y_pos=zeros(1,enc_size-1);
Xd=0;
Yd=0;
theta_sum=0;

for i=1:enc_size-1
    
 dx_enc(1,i)=(enc_X(1,i+1)-enc_X(1,i))*encXcalib;
 
 dy_enc(1,i)=(enc_Y(1,i+1)-enc_Y(1,i))*encYcalib;
 
 d_theta(1,i)=(dx_enc(1,i)-dy_enc(1,i))/L;
 
 
  d(1,i)=(enc_mean(1,i+1)-enc_mean(1,i))*meancalib;
 
  totaldist=totaldist+d(1,i)
  
  theta_sum=theta_sum+ d_theta(1,i);
 
  dx(1,i)=d(1,i)*cos(theta_sum);
  dy(1,i)=d(1,i)*sin(theta_sum);
    
end


for i=1:enc_size-1
    
    Xd=Xd+dx(1,i);
    Yd=Yd+dy(1,i);
    
    X_pos(1,i)=Xd;
    Y_pos(1,i)=Yd;
    
end

figure
scatter(X_pos,Y_pos)