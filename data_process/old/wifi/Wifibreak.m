
close all

encXcalib=6.608284615932709e-05;
encYcalib=6.603486193757895e-05;

meancalib=mean([encXcalib,encYcalib]);

imuZ_theta_ts=timeseries(-theta,raw_stamp);

    
wifi_ts=timeseries(wifi_scan, wifi_stamp);



d=zeros(1,enc_size-1);
imuZ=zeros(1,enc_size-1);
dx=zeros(1,enc_size-1);
dy=zeros(1,enc_size-1);
totaldist=0;

X_pos=zeros(1,enc_size-1);
Y_pos=zeros(1,enc_size-1);
Xd=0;
Yd=0;

celldist=0;
cellsize=5;

cellnum=0;

for i=1:enc_size-1
    
  d(1,i)=(enc_mean(1,i+1)-enc_mean(1,i))*meancalib;
  
    
  imuZ_sample=getsampleusingtime(imuZ_theta_ts, enc_stamp(1,i), enc_stamp(1,i+1));
  imuZ_vec=imuZ_sample.Data;
  
  
  imuZ(1,i)=mean(imuZ_vec);
  
  if(isnan(imuZ(1,i)))
     imuZ(1,i)=imuZ(1,i-1); 
  end
  
  
  if celldist>=cellsize
      cellnum=cellnum+1;
      celltimes=enc_stamp(1,i+1);
      cellinfoX(cellnum,:)=[cellnum celltimes celldist i];
      celldist=0;
  end
  
  celldist=celldist+d(1,i);
  

  
  totaldist=totaldist+d(1,i);
  
  dx(1,i)=d(1,i)*cos(imuZ(1,i));
  dy(1,i)=d(1,i)*sin(imuZ(1,i));
  
%   X_pos(1,i)=dx;
%   Y_pos(1,i)=dy;
    
end


for i=1:enc_size-1
    
    Xd=Xd+dx(1,i);
    Yd=Yd+dy(1,i);
    
    X_pos(1,i)=Xd;
    Y_pos(1,i)=Yd;
    
end

cellinfo=zeros(size(cellinfoX,1)+1,size(cellinfoX,2));
cellinfo(2:end,:)=cellinfoX;
cellinfo(1,4)=1;

for i=1:size(cellinfo,1)-1 
    
    wifisamp=getsampleusingtime(wifi_ts,cellinfo(i,2), cellinfo(i+1,2));
    wifidata=wifisamp.data;
    
    filename=strcat('scan' ,num2str(i) ,'.txt');
    fileID = fopen(filename,'w');
    
    for k=1:size(wifidata,3)
       
       PRINT=strcat('Scan ', num2str(k),';',wifidata(:,:,k),';','\n','\n'); 
       fprintf(fileID, PRINT); 
        
    end
    fclose(fileID);
end



figure
hold on
scatter(X_pos,Y_pos,'y')
for i=1:size(cellinfo,1) 
    scatter(X_pos(1,cellinfo(i,4)),Y_pos(1,cellinfo(i,4)),'r','x')
    text(X_pos(1,cellinfo(i,4)),Y_pos(1,cellinfo(i,4)), num2str(i));
end

title('Wifi cells')
xlabel('x/m')
ylabel('y/m')
