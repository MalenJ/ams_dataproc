close all
%load('enc_wifi_imu.mat')

enc_size=length(encdata);
imu_size=length(imudata);
wifi_size=length(wifidata);


enc_stamp=zeros(1,enc_size);
imu_stamp=zeros(1,imu_size);
wifi_stamp=zeros(1,wifi_size);
wifi_scan=strings(1,wifi_size);

enc_X=zeros(1,enc_size);
enc_Y=zeros(1,enc_size);

imu_X=zeros(1,imu_size);
imu_Y=zeros(1,imu_size);
imu_Z=zeros(1,imu_size);


for i=1:enc_size
    
    enc_stamp(1,i)=(encdata{i,1}.Header.Stamp.Sec+(encdata{i,1}.Header.Stamp.Nsec)/1000000000)-(encdata{1,1}.Header.Stamp.Sec+(encdata{1,1}.Header.Stamp.Nsec)/1000000000);
    enc_X(1,i)=encdata{i,1}.Ticks.X;
    enc_Y(1,i)=-encdata{i,1}.Ticks.Y;
    
end

for i=1:imu_size
    
    imu_stamp(1,i)=(imudata{i,1}.Header.Stamp.Sec+(imudata{i,1}.Header.Stamp.Nsec)/1000000000)-(encdata{1,1}.Header.Stamp.Sec+(encdata{1,1}.Header.Stamp.Nsec)/1000000000);
    imu_X(1,i)=imudata{i,1}.RPY.X-imudata{1,1}.RPY.X;
    imu_Y(1,i)=imudata{i,1}.RPY.Y-imudata{1,1}.RPY.Y;
    imu_Z(1,i)=imudata{i,1}.RPY.Z-imudata{1,1}.RPY.Z;
    
end


for i=1:wifi_size

    wifi_stamp(1,i)=(wifidata{i,1}.Header.Stamp.Sec+(wifidata{i,1}.Header.Stamp.Nsec)/1000000000)-(encdata{1,1}.Header.Stamp.Sec+(encdata{1,1}.Header.Stamp.Nsec)/1000000000);
    wifi_scan{i}=char(wifidata{i,1}.Scan);

end


encXY=[enc_X;enc_Y];
enc_mean=mean(encXY);

coef=ones(1, 500)/500;
imu_filtered=filter(coef,1,imu_Z(1,:));

enc_X_ts=timeseries(enc_X,enc_stamp);
enc_Y_ts=timeseries(enc_Y,enc_stamp);
enc_mean_ts=timeseries(enc_mean,enc_stamp);
% figure
% hold on
% plot(enc_X_ts,'r')
% plot(enc_Y_ts, 'b')
% plot(enc_mean_ts, 'g')

% imu_Z_ts=timeseries(imu_Z,imu_stamp);
% imusmooth=timeseries(imu_filtered,imu_stamp);
% figure
% hold on
% plot(imu_Z_ts, 'b')
% plot(imusmooth, 'r')
% 
% 
% 
% imu_X_ts=timeseries(imu_X,imu_stamp);
% imu_Y_ts=timeseries(imu_Y,imu_stamp);
% figure
% plot(imu_X_ts, 'g')
% figure
% plot(imu_Y_ts, 'y')


