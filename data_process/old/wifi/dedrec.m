%load('enc_wifi_imu.mat')

close all

encXcalib=6.608284615932709e-05;
encYcalib=6.603486193757895e-05;

meancalib=mean([encXcalib,encYcalib]);


imuZ_theta_ts=timeseries(-corrected_theta,raw_stamp);


d=zeros(1,enc_size-1);
imuZ=zeros(1,enc_size-1);
dx=zeros(1,enc_size-1);
dy=zeros(1,enc_size-1);
totaldist=0;

X_pos=zeros(1,enc_size-1);
Y_pos=zeros(1,enc_size-1);
Xd=0;
Yd=0;



for i=1:enc_size-1
    
  d(1,i)=(enc_mean(1,i+1)-enc_mean(1,i))*meancalib;
  imuZ_sample=getsampleusingtime(imuZ_theta_ts, enc_stamp(1,i), enc_stamp(1,i+1));
  imuZ_vec=imuZ_sample.Data;
  
  
  imuZ(1,i)=mean(imuZ_vec);
  
  if(isnan(imuZ(1,i)))
     imuZ(1,i)=imuZ(1,i-1); 
  end
  
  totaldist=totaldist+d(1,i);
  
  dx(1,i)=d(1,i)*cos(imuZ(1,i));
  dy(1,i)=d(1,i)*sin(imuZ(1,i));
  
%   X_pos(1,i)=dx;
%   Y_pos(1,i)=dy;
    
end


for i=1:enc_size-1
    
    Xd=Xd+dx(1,i);
    Yd=Yd+dy(1,i);
    
    X_pos(1,i)=Xd;
    Y_pos(1,i)=Yd;
    
end

% Xgeo=(X_pos+ones(1,enc_size-1)*enu_x(1,5));
% Ygeo=(-Y_pos+ones(1,enc_size-1)*enu_y(1,5));
% Zgeo=(ones(1,enc_size-1)*z_gps(1,5));
% 
% a=[Xgeo(1,100) Ygeo(1,100) Zgeo(1,100)];
% b=[enu_x(1,100) enu_y(1,100) enu_z(1,100)];
% 
% theta2 = atan2(norm(cross(a,b)),dot(a,b));
% 
% 
% Xgeo=Xgeo*cos(theta2)-Ygeo*sin(theta2);
% Ygeo=Xgeo*sin(theta2)+Ygeo*cos(theta2);
% 
% [enc_lat, enc_long, enc_alti]=enu2geodetic(Xgeo,Ygeo,Zgeo,base_lat,base_long,base_alti, wgs84Ellipsoid);
% 
% 
% figure
% hold on
% scatter(Xgeo(1,:),Ygeo(1,:))
% scatter(enu_x(1,:),enu_y(1,:), 'r')

figure
scatter(X_pos,Y_pos)




% figure
% hold on
% plot(long(1,:), lat(1,:), '.b', 'MarkerSize', 20)  
% plot_google_map('MapType','satellite','MapScale', 2) 
%legend('Final GPS fix', 'RTK baseline')