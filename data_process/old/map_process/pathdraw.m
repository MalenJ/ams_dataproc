close all 
clear all

bag1=rosbag('/home/maleen/rosbags/2019-06-14-Alumni-local.bag');

%bag2=rosbag('/home/maleen/rosbags/2019-06-06-MSY/2019-06-13-13-09-06.bag');

mapmode=select(bag1,'Topic','/rtabmap/localization_pose');

%localmode=select(bag2,'Topic','/rtabmap/localization_pose');

mapmode_msgs=readMessages(mapmode);
%localmode_msgs=readMessages(localmode);

mapsize=length(mapmode_msgs);
%localsize=length(localmode_msgs);

for i=1:mapsize

    mapsize_stamp(1,i)=mapmode_msgs{i,1}.Header.Stamp.Sec+(mapmode_msgs{i,1}.Header.Stamp.Nsec)/1000000000;
    mapX(1,i)=mapmode_msgs{i,1}.Pose.Pose.Position.X;
    mapY(1,i)=mapmode_msgs{i,1}.Pose.Pose.Position.Y;
    
end

% for i=1:localsize
% 
%     local_stamp(1,i)=localmode_msgs{i,1}.Header.Stamp.Sec+(localmode_msgs{i,1}.Header.Stamp.Nsec)/1000000000;
%     localX(1,i)=localmode_msgs{i,1}.Pose.Pose.Position.X;
%     localY(1,i)=localmode_msgs{i,1}.Pose.Pose.Position.Y;
%     
% end

figure 
hold on
% scatter(localX, localY, 'r')
scatter(mapX, mapY, 'b', 'x')