close all
clear all

%LoadCSV

filename='2019-05-07-Wentworth-P2';

%rtk_data=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-RTK.csv'));

rtabdata= readtable(strcat('/home/maleen/Desktop/poses.txt'));


%CONVERT RTK
% 
% rtk_time=rtk_data{:,2};
% rtk_lat=rtk_data{:,3};
% rtk_long=rtk_data{:,4};
% rtk_alti=rtk_data{:,5};
% 
% base_lat=mean(rtk_lat(1:50));
% base_long=mean(rtk_long(1:50));
% base_alti=mean(rtk_alti(1:50));
% 
% [rtk_enux, rtk_enuy, lrtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);
% 
% 
% figure
% 
% plot(rtk_enux,rtk_enuy,'r')
% 
% 
%RTAB DATA

rtab_time=rtabdata{:,1};
rtab_x=rtabdata{:,2};
rtab_y=rtabdata{:,3};
rtab_theta=rtabdata{:,7};

figure
axis equal
scatter(rtab_x,rtab_y,'b')
