clear all
close all

bag=rosbag('/home/maleen/rosbags/2019-04-03-Wentworth-660-yolo.bag');

depthbag=select(bag,'Topic','/cam_1/depth/image_rect_raw ');
aligndepthbag=select(bag,'Topic','/cam_1/aligned_depth_to_color/image_raw');
rgbbag=select(bag,'Topic','/cam');

bbag=select(bag,'Topic','/darknet_ros/detection_image');

yolobox=select(bag,'Topic','/cam');

depth_msgs=readMessages(depthbag,100);
align_depth_msgs=readMessages(aligndepthbag,100);
rgb_msgs=readMessages(rgbbag,100);
bbmsg=readMessages(bbag,500);

img = readImage(depth_msgs{1});


%depthimg=reshape(typecast(align_depth_msgs{1}.Data, 'uint16'), 640, 480);


%img=getRGBImageFromRosMsg(bbmsg{1});
% 
imshow(img)

