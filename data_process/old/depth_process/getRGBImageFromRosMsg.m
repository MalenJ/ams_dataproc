% This function takes a ROS sensor_msgs/Image type message and outputs an
% RGB image
% Note : This implementation is only for rgb8 enconding
% Readers should be able to modify this script to acommodate other encoding
% types such as rgba8, bgr8 etc.

function [img] = getRGBImageFromRosMsg(msg)
% First, let's get the image height (rows) and width (columns)
    h = msg.Height;
    w = msg.Width;
% The pixel values for the 3 channels are available in the Data
    data = msg.Data;
% Step specifies the number of values in the msg.Data array corresponds to
% one row of the output image. For rgb8 encoding, this is always equal to
% the msg.Width * 3
    step = msg.Step;
    img = zeros(h, w, 3);
    
    for i=1:h
        for j=1:w
            img(i,j,1) = data((i-1)*step + (j-1)*3 + 1);
            img(i,j,2) = data((i-1)*step + (j-1)*3 + 2);
            img(i,j,3) = data((i-1)*step + (j-1)*3 + 3);
        end
    end
% We need to make sure we output a uint8 MxNx3 matrix
    img = uint8(img);    
end
