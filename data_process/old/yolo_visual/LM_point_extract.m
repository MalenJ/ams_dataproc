close all
clear all

filename='2018-11-23-Wentworth-P1-yolo';
load(strcat(filename,'.mat'))

load('/home/maleen/git/ams_primary/calibration/cam/2018-12-02-cameracalib/2018-12-02-cameracalib.mat');


camH=1.54;

j=1;



for i=1:size(scooter_LXEX)
    
    if LMEX(i)>0
        
        
        scooter_LX(j,1)=scooter_LXEX(i);
        scooter_LY(j,1)=scooter_LYEX(i);
        scooter_LTHETA(j,1)=scooter_LTHETAEX(i);
        scooter_LTHETAX(j,1)=scooter_LTHETAXEX(i);
        scooter_LTHETAY(j,1)=scooter_LTHETAYEX(i);
        
        bearings(j,1)=bearingsEX(i);
        centroid_x(j,1)=centroid_xEX(i);
        centroid_y(j,1)=centroid_yEX(i);
        yolonum(j,1)=yolonumEX(i);
        
        LM(j,1)=LMEX(i);
        
        x=pi/2;%-scooter_LTHETAX(j,1);
        y=0;%+scooter_LTHETA(j,1);%
        z=pi/2-scooter_LTHETA(j,1)+deg2rad(-5);
        
        RX=[1 0 0; 0 cos(x) -sin(x); 0 sin(x) cos(x)];
        RY=[cos(y) 0 sin(y); 0 1 0; -sin(y) 0 cos(y)];
        RZ=[cos(z) -sin(z) 0; sin(z) cos(z) 0; 0 0 1];
        
        %Orientation{j,1} = [cos(z) -sin(z) 0; sin(z) cos(z) 0; 0 0 1];
        Orientation{j,1} =RX*RY*RZ;
        Location{j,1}=[scooter_LX(j,1) scooter_LY(j,1) camH];
        
        j=j+1;
        
    end
    
    
end

load('landmarksENU.mat');
load('2018-11-23-Wentworth-P1-RTKroute.mat')

loopsize=7;
j=1;

figure
hold on
plot(x_posEX, y_posEX)

RTKX=5.7;
RTKY=-19.32;
c=-1.5648;
rot = [cos(c) -sin(c); sin(c) cos(c)];


for i=1:size(landmarks_enu,1)
    landmarks_enu(i,1)=landmarks_enu(i,1)-RTKX;
    landmarks_enu(i,2)=landmarks_enu(i,2)-RTKY;
    landmarks_enu(i,1:2)=transpose(rot*transpose(landmarks_enu(i,1:2)));
end


while(loopsize>0)
    
    w=waitforbuttonpress;
    
    if (w==1)
        
        LMS=find(LM==j);
        
        for k=1:size(LMS)
            
            scooter_theta=scooter_LTHETA(LMS(k));
            anglecheck=wrapTo360(rad2deg(scooter_theta));
            lampgradient=tan(scooter_theta+deg2rad(-5)-bearings(LMS(k)));
            
            if abs(lampgradient) > 1
                raylength=abs(30/lampgradient);
            else
                raylength=50;
            end
            
            if (anglecheck<=90) || (anglecheck>=270)
                
                xplotz=linspace(0,raylength);
                
            elseif (anglecheck>90 && anglecheck<270)
                
                xplotz=linspace(-raylength,0);
                
            end
            
            yplotz=xplotz.*lampgradient;
            
            P(k)=plot((xplotz+scooter_LX(LMS(k))),(yplotz+scooter_LY(LMS(k))));
            
            P2=scatter(landmarks_enu(:,1),landmarks_enu(:,2),'d','r');
            
        end
        
        w=0;
        
        while (w==0)
            w=waitforbuttonpress;
        end
        delete(P);
        delete(P2);
        
        if j==7
            
            j=0;
            
        end
        
        j=j+1;
        
    end
    
end









% RTK1 5.703, -19.19 (TRANSLATION)
% RTK2 7.02,25 (1.3170, 44.1900)
%
% SCOOTER1 0,0
% scooter2 44.78 -1.071

%scooter to GPS= 0.235


% a=[1.3170, 44.1900 0];
% b=[ 45.015 -1.071 0];
%
% atan2(norm(cross(a,b)), dot(a,b))
%
%   1.5649






