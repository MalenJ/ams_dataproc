close all
clear all

filename='2018-11-23-Wentworth-P1-yolo';
load(strcat(filename,'.mat'))

load('/home/maleen/git/mobility_scooter/calibration/cam/2018-12-02-cameracalib/2018-12-02-cameracalib.mat');

camH=1.54;

j=1;

for i=1:size(scooter_LXEX)
    
    if LMEX(i)>0
        
        scooter_LX(j,1)=scooter_LXEX(i);
        scooter_LY(j,1)=scooter_LYEX(i);
        scooter_LTHETA(j,1)=scooter_LTHETAEX(i);
        scooter_LTHETAX(j,1)=scooter_LTHETAXEX(i);
        scooter_LTHETAY(j,1)=scooter_LTHETAYEX(i);
        
        bearings(j,1)=bearingsEX(i);
        centroid_x(j,1)=centroid_xEX(i);
        centroid_y(j,1)=centroid_yEX(i);
        yolonum(j,1)=yolonumEX(i);
        
        LM(j,1)=LMEX(i);
        
        
        x=pi/2;%-scooter_LTHETAX(j,1);
        y=0;%+scooter_LTHETA(j,1);%
        z=pi/2-scooter_LTHETA(j,1);
        
        RX=[1 0 0; 0 cos(x) -sin(x); 0 sin(x) cos(x)];
        RY=[cos(y) 0 sin(y); 0 1 0; -sin(y) 0 cos(y)];
        RZ=[cos(z) -sin(z) 0; sin(z) cos(z) 0; 0 0 1];
        
        
        
        %Orientation{j,1} = [cos(z) -sin(z) 0; sin(z) cos(z) 0; 0 0 1];
        Orientation{j,1} =RX*RY*RZ;
        Location{j,1}=[scooter_LX(j,1) scooter_LY(j,1) camH];
        
        j=j+1;
        
    end
    
end

cameraSize = 2;

figure
hold on
plot(x_posEX, y_posEX, 'b')

P=plotCamera('Location',Location{1},'Orientation',Orientation{1},'Size',...
    cameraSize,'Color','r','Label',num2str(0),'Opacity',0);


for i=1:size(scooter_LTHETA)
    
    w=waitforbuttonpress;
    delete(P);
    
    loc=Location{i};
    orient=Orientation{i};
    
    P=plotCamera('Location',loc,'Orientation',orient,'Size',...
        cameraSize,'Color','r','Label',num2str(yolonum(i)),'Opacity',.5);
    axis equal
    hold on
    ray=transpose(orient*transpose([0 0 -1]));
    quiver3(loc(1),loc(2),loc(3),ray(1),ray(2),ray(3))
    
end