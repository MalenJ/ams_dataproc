close all
clear all

filename='2018-11-23-Wentworth-P1-yolo-x2';
load(strcat(filename,'.mat'))
load('/home/maleen/git/mobility_scooter/calibration/cam/2018-12-02-cameracalib/2018-12-02-cameracalib.mat');
load('landmarksENU.mat');
load('2018-11-23-Wentworth-P1-RTKroute.mat')

LMpoints(1,:)=[8.699,0.5723];
LMpoints(2,:)=[26.65,3.041];
LMpoints(3,:)=[48.06,-0.4103];
LMpoints(4,:)=[32.76,15.28];
LMpoints(5,:)=[28.6,23.52];
LMpoints(6,:)=[24.88,17.65];
LMpoints(7,:)=[13,2];

% LMpoints(1,:)=[9.751,0.751];
% LMpoints(2,:)=[29.46,3.881];
% LMpoints(3,:)=[49.95,0.025];
% LMpoints(4,:)=[30.64,16.61];
% LMpoints(5,:)=[27.13,23.34];
% LMpoints(6,:)=[25.09,16.86];
% LMpoints(7,:)=[9.751,0.751];

v_fov=deg2rad(42.5);
v_width=480;
centerY=v_width/2;
rad_per_pix=v_fov/v_width;
camH=1.54;

ViewId=uint32(yolonum);

zheights=zeros(size(centroid_y,1),1);

idsize=1;

for i=1:7
    
    k = find(LM==i);
    
    
    v_bearing=zeros(size(centroid_y(k),1),1);
    dists=zeros(size(centroid_y(k),1),1);
    z=zeros(size(centroid_y(k),1),1);
    
    v_bearing=(-centroid_y(k)+(ones(size(centroid_y(k),1),1)*centerY))*rad_per_pix;
    
    for n=1:size(centroid_y(k),1)
        dists(n)=norm(LMpoints(i,:)- [scooter_LX(k(n)) scooter_LY(k(n))]);
    end
    
    imageH=dists.*tan(v_bearing);
    
    z=ones(size(centroid_y(k),1),1).*camH + imageH;
    
    zheights(k)=z;
    meanZ=mean(z);
    tolerence=5;

    xyzPoints(i,:)=[LMpoints(i,:) meanZ];
    
  
    window=find(zheights(k)<(mean(z)+tolerence) & (zheights(k)>mean(z)-tolerence));
    pointTracks(1,i)=pointTrack(ViewId(k(window)),[centroid_x(k(window)) centroid_y(k(window))]);
        
%     for ids=1:size(window)
%         
%         ViewId(idsize,1)=ViewId2(k(window(ids)));
%         Orientation{idsize,1}=Orientation2{k(window(ids))};
%         Location{idsize,1}=Location2{k(window(ids))};
%         idsize=idsize+1;
%     end
    %pointTracks(1,i)=pointTrack(ViewId(k),[centroid_x(k) centroid_y(k)]);
        
    %xyzPoints(i,:)=[ones(size(centroid_y(k),1),1)*LMpoints(i,:) z];
    
end

cameraPoses=table(ViewId,Orientation,Location);

%[xyzRefinedPoints,refinedPoses,reprojectionErrors] = bundleAdjustment(xyzPoints,pointTracks,cameraPoses,cameraParams,'MaxIterations',50);
% 
% 
[xyzRefinedPoints,refinedPoses,reprojectionErrors] = bundleAdjustment(xyzPoints,pointTracks,cameraPoses,cameraParams, 'PointsUndistorted', true, 'AbsoluteTolerance', 1e-9,...
       'RelativeTolerance', 1e-9, 'MaxIterations', 1000);

pos=refinedPoses.Location;

figure
hold on
plot(x_posEX, y_posEX, 'b')

scatter3(xyzPoints(1:6,1),xyzPoints(1:6,2),xyzPoints(1:6,3),'x','b')

for i=1:length(pos)
    %scatter3(pos{i}(1),pos{i}(2),pos{i}(3), 'g')
    %scatter3(Location{i}(1),Location{i}(2),Location{i}(3),'x', 'b')
end

scatter3(xyzRefinedPoints(1:6,1),xyzRefinedPoints(1:6,2),xyzRefinedPoints(1:6,3),'k')


RTKX=5.7;
RTKY=-19.32;
c=-1.5648;
rot = [cos(c) -sin(c); sin(c) cos(c)];
%vrrotvec2mat(r);
for i=1:size(landmarks_enu,1)
    landmarks_enu(i,1)=landmarks_enu(i,1)-RTKX;
    landmarks_enu(i,2)=landmarks_enu(i,2)-RTKY;
    landmarks_enu(i,1:2)=transpose(rot*transpose(landmarks_enu(i,1:2)));
end

scatter(landmarks_enu(:,1),landmarks_enu(:,2),'d','r');


for i=1:size(x_rtk,1)
    x_rtk(i,1)=x_rtk(i,1)-RTKX;
    y_rtk(i,1)=y_rtk(i,1)-RTKY;
    rtk_path(i,:)=[x_rtk(i,1) y_rtk(i,1)];
    rtk_path(i,:)=transpose(rot*transpose(rtk_path(i,:)));
end


plot(rtk_path(:,1),rtk_path(:,2),'r');


for i=1:6
    
   error(i)=norm(landmarks_enu(i,1:2)-xyzRefinedPoints(i,1:2)) 
    
end
