close all

%LoadCSV

filename='2018-11-23-Wentworth-P1-yolo';

odom_data=csvread(strcat('./csv_data/', filename, '-odom.csv'), 1, 0, [1 0 1851 5]); 
lamp_data=csvread(strcat('./csv_data/', filename, '-lampdata.csv'), 1, 0, [1 0 520 8]);

x_pos=odom_data(:,5);
y_pos=odom_data(:,6);

scooter_LX=lamp_data(:,5);
scooter_LY=lamp_data(:,6);
scooter_LTHETA=lamp_data(:,7);
bearings=lamp_data(:,4);
centroid_x=lamp_data(:,8);
centroid_y=lamp_data(:,9);

%L=animatedline('Color','r');

subplot(2,1,1);
hold on
plot(x_pos,y_pos)
mainplot=plot(nan);
axis equal


loopsize=lamp_data(end,2)+1;

iter=1;

for i=250:loopsize
    
    
    
     w=waitforbuttonpress;
       
     for k=iter:size(lamp_data, 1)
        
        if ((lamp_data(k,2)+1)==i)
            
            scooter_theta=scooter_LTHETA(k);
            anglecheck=wrapTo360(rad2deg(scooter_theta));
            lampgradient=tan(scooter_theta-bearings(k));
   
            if abs(lampgradient) > 1
                raylength=abs(30/lampgradient);
            else
                raylength=50;
            end
            
               if (anglecheck<=90) || (anglecheck>=270)

                xplotz=linspace(0,raylength);

               elseif (anglecheck>90 && anglecheck<270)

                xplotz=linspace(-raylength,0);

               end
    
            yplotz=xplotz.*lampgradient;
           

            subplot(2,1,1);
            hold on
            plot((xplotz+scooter_LX(k)),(yplotz+scooter_LY(k)))
            hold off
            
            I = imread(strcat('./img_data/', filename, '-' , num2str(i) ,'.jpg'));
          
            subplot(2,1,2);
            imshow(I)
            iter=iter+1;  
        end
        
        
     end
     
end
    


