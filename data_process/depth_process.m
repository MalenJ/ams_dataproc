clear all
close all

fileroot='2021-01-11-UTS/';
filename='2021-01-11-UTS-P1';

bag=rosbag(strcat('/home/maleen/rosbags/',fileroot,filename,'.bag'));
%bag=rosbag(strcat('/media/maleen/malen_ssd/phd/critical_ams_data/',fileroot,filename,'.bag'));

aligndepthbag=select(bag,'Topic','/back_cam/aligned_depth_to_color/image_raw');

align_depth_msgs=readMessages(aligndepthbag);

baglength=length(align_depth_msgs);

j=0;
for i=1:baglength
    j=j+1;
    depth_imgs{j} = readImage(align_depth_msgs{i});
    
    
end


%%

% 
% 
% bag=rosbag('/home/maleen/output2.bag');
% 
% aligndepthbag=select(bag,'Topic','/back_cam/aligned_depth_to_color/image_raw');
% 
% align_depth_msgs=readMessages(aligndepthbag);
% 
% baglength=length(align_depth_msgs);
% 
% 
% for i=1:baglength
%     
%     j=j+1;
%     depth_imgs{j} = readImage(align_depth_msgs{i});
% end