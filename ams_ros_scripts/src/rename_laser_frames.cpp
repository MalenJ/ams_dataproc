/*
    This script renames the laser message frame ID (because of wrong TF tree)
    Written as a ROS node for simplifying the management of dependencies
*/


#include <ros/ros.h>

#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/LaserScan.h>
#include <arf_msgs/imuRaw.h>
#include <arf_msgs/imuReal.h>
#include <arf_msgs/encoder.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <piksi_rtk_msgs/MeasurementState_V2_4_1.h>
#include <sensor_msgs/NavSatFix.h>
#include <piksi_rtk_msgs/VelNed.h>
#include <tf2_msgs/TFMessage.h>
#include <dynamic_reconfigure/ConfigDescription.h>
#include <dynamic_reconfigure/Config.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
int main(int argc, char** argv) {
    ros::init(argc, argv, "rename_laser_frames");

    ros::NodeHandle n;

    if (argc < 3) {
        std::cerr << "Error : Need the input and output file names\n";
        return -1;
    }

    std::string inputFileName, outputFileName;

    inputFileName = argv[1];
    outputFileName = argv[2];

    std::cout << "Input File Name : " << inputFileName << std::endl;
    std::cout << "Output File Name : " << outputFileName << std::endl;

    rosbag::Bag inputBag, outputBag;

    try {
        inputBag.open(inputFileName, rosbag::bagmode::Read);
        std::cout << "Input rosbag opened and ready to be read" << std::endl;
    } catch (rosbag::BagIOException e) {
        std::cerr << "Error opening rosbag : " << inputFileName << "\n";
        return -1;
    }
    
    try {
        outputBag.open(outputFileName, rosbag::bagmode::Write);
        std::cout << "Output rosbag opened and ready to be written" << std::endl;
    } catch (rosbag::BagIOException e) {
        std::cerr << "Error opening rosbag : " << outputFileName << "\n";
        std::cout << "Something is wrong. This should not happen if the file name is correct" << std::endl;
        std::cout << "Even when the file does not exist" << std::endl;
        return -1;
    }

    rosbag::View inputBagView(inputBag);

    int msgCount = 0;
    int laserMsgCount = 0;

    std::cout << "Starting the process" << std::endl;

    foreach(rosbag::MessageInstance const m, inputBagView) {
        if (m.getTopic() == "/scan") {
            sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
            sensor_msgs::LaserScan laserMsg = *msg;

            laserMsg.header.frame_id = "laser_inverted";

            outputBag.write(m.getTopic(), m.getTime(), laserMsg);
            
            laserMsgCount++;
            // std::cout << "Laser messages written to bag : " << laserMsgCount << std::endl;
        } 
        
        else if (m.getTopic() == "/tf_static") {
            outputBag.write(m.getTopic(), m.getTime(), m, m.getConnectionHeader());
        }

        else {
            outputBag.write(m.getTopic(), m.getTime(), m); // Using first tf timestamp
        }

        msgCount++;

        if (msgCount%5 == 0) {
            std::cout << "." << std::flush;
        } else {
            std::cout << "*" << std::flush;
        }
        // std::cout << "Total Messages written to bag : " << msgCount << std::endl;
    }

    inputBag.close();
    outputBag.close();

    std::cout << "DONE!" << std::endl;

    return 0;
}


