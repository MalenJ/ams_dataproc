close all
clear all

filename='2020-01-28-Village-P4-mapstat-prep.mat';
%PATH='/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/localisation_data/2019-09-07-Village/';

global sigma_bearing;
global IG_bearing;
global sigma_bearingP;

sigma_bearing=17;
IG_bearing=0.48;
sigma_bearingP=0.001;

load(strcat(filename));

yolo_time=yolo_observations{:,3};

%CHANGE DEPENDING ON RTK OR RTAB
GT_interpX=rtkinterp_x;
GT_interpY=rtkinterp_x;
GT_interpTHETA=rtkinterp_theta;

uj=1;

for i=1:size(yolo_time)
    
    if i==1
        unique_time(uj)=yolo_time(i);
        uj=uj+1;
    else
        if yolo_time(i)>yolo_time(i-1)
            unique_time(uj)=yolo_time(i);
            uj=uj+1;
        end
        
    end
    
end


final_obs=[];
innovations=[];
centerx=240;
start_time=40;
startiter=find(unique_time<start_time, 1, 'last' );

for i=startiter:size(unique_time,2)
    
    current_time=unique_time(i);
    trans=[ppval(rtkinterp_x,current_time),ppval(rtkinterp_y,current_time),ppval(rtkinterp_z,current_time)];
    rotz=ppval(rtkinterp_theta,current_time);
    T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
    T_world_robot=T_world_RTK*inv(T_robot_RTK);
    
    GTx=T_world_robot(1,4);
    GTy=T_world_robot(2,4);
    GTtheta=rotz;
    
    X_k=[GTx;GTy;GTtheta];
    
    %P_k=zeros(3);
    P_k=[(0.05)^2 0 0;0 (0.05)^2 0;0 0 (deg2rad(0.1))^2];
    
    rows1=yolo_observations.lamp_times==current_time;
    
    cameras_obs1=yolo_observations(rows1,{'yolo_index','camera','centroid_x','yolo_labels'});
    
    rowslf=strcmp(cameras_obs1.camera,'LF');
    rowslb=strcmp(cameras_obs1.camera,'LB');
    rowsrf=strcmp(cameras_obs1.camera,'RF');
    rowsrb=strcmp(cameras_obs1.camera,'RB');
    
    if max(rowslf>0)
        
        cameras_obs=cameras_obs1(rowslf,{'yolo_index','camera','centroid_x','yolo_labels'});
        bear_obslf=stat_bearing_obZ(ricoh_centerX,current_time,cameras_obs);
        %         centroidslf=cameras_obs1(rowslf,{'centroid_x'});
        %         semanticslf=cameras_obs1(rowslf,{'yolo_labels'});
        %         bear_obslf=stat_bearing_obZ(centerx,current_time,'LF',centroidslf{:,1},semanticslf{:,1});
        [X_k1k1(:,i),P_k1k1,bear_obslf,fin_inov] = stat_observation_bearing(X_k,P_k,bear_obslf,LMmap);
        fin_inov;
        final_obs=[final_obs bear_obslf];
        innovations=[innovations;fin_inov];
    end
    
yolo_sorted=array2table([transpose(unique_time),transpose(unique_label)], 'VariableNames','overall_time','labels');
    
    if max(rowslb>0)
        
        cameras_obs=cameras_obs1(rowslb,{'yolo_index','camera','centroid_x','yolo_labels'});
        bear_obslb=stat_bearing_obZ(ricoh_centerX,current_time,cameras_obs);
        %         centroidslb=cameras_obs1(rowslb,{'centroid_x'});
        %         semanticslb=cameras_obs1(rowslb,{'yolo_labels'});
        %         bear_obslf=stat_bearing_obZ(centerx,current_time,'LB',centroidslb{:,1},semanticslb{:,1});
        [X_k1k1(:,i),P_k1k1,bear_obslb,fin_inov] = stat_observation_bearing(X_k,P_k,bear_obslf,LMmap);
        final_obs=[final_obs bear_obslb];
        innovations=[innovations;fin_inov];
    end
    
    if max(rowsrf>0)
        
        cameras_obs=cameras_obs1(rowsrf,{'yolo_index','camera','centroid_x','yolo_labels'});
        bear_obsrf=stat_bearing_obZ(ricoh_centerX,current_time,cameras_obs);
        %         centroidsrf=cameras_obs1(rowsrf,{'centroid_x'});
        %         semanticsrf=cameras_obs1(rowsrf,{'yolo_labels'});
        %         bear_obsrf=stat_bearing_obZ(centerx,current_time,'RF',centroidsrf{:,1},semanticsrf{:,1});
        [X_k1k1(:,i),P_k1k1,bear_obsrf,fin_inov] = stat_observation_bearing(X_k,P_k,bear_obsrf,LMmap);
        final_obs=[final_obs bear_obsrf];
        innovations=[innovations;fin_inov];
    end
    
    if max(rowsrb>0)
        
        cameras_obs=cameras_obs1(rowsrb,{'yolo_index','camera','centroid_x','yolo_labels'});
        bear_obsrb=stat_bearing_obZ(ricoh_centerX,current_time,cameras_obs);
        %         centroidsrb=cameras_obs1(rowsrb,{'centroid_x'});
        %         semanticsrb=cameras_obs1(rowsrb,{'yolo_labels'});
        %         bear_obsrb=stat_bearing_obZ(centerx,current_time,'RB',centroidsrb{:,1},semanticsrb{:,1});
        [X_k1k1(:,i),P_k1k1,bear_obsrb,fin_inov] = stat_observation_bearing(X_k,P_k,bear_obsrb,LMmap);
        final_obs=[final_obs bear_obsrb];
        innovations=[innovations;fin_inov];
    end
    
    
end


thousrows=find(innovations==1000);
innovations(thousrows)=[];
figure
plot(rad2deg(innovations))
mean(rad2deg(innovations))
2*sqrt(var(rad2deg(innovations)))