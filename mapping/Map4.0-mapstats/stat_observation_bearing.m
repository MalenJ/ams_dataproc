function [X_k1k1,P_k1k1,Obs,fin_inov] = stat_observation_bearing(X_k1k,P_k1k,Obs,LMmap)
global sigma_bearing;
global IG_bearing;
%global realtimeplotter;
global sigma_bearingP;


var_theta=(deg2rad(sigma_bearing))^2;
IG_gate=IG_bearing;
detects=0;

%%0.46 1.07 1.64 2.71 3.84

cam=Obs.sensortype{:};
Z=Obs.bearings;
semlab=Obs.labels;

map_num=size(LMmap,1);
Z_num=size(Z,1);

a=Obs.a;
b=Obs.b;

%PREDICTED OBSERVATIONS
%obsorig=rad2deg(Z)

% if (cam=='LC')
%     a=-0.037;
%     b=0.1073;
% elseif (cam=='RC')
%     a=-0.0292;
%     b=-0.0895;
%
% elseif (cam=='LF')
%     a=-0.0149;
%     b=0.0144;
% elseif (cam=='LB')
%     a=0.0679;
%     b=0.0522;
% elseif (cam=='RF')
%     a=0.0061;
%     b=0.0030;
% elseif (cam=='RB')
%     a=0.0473;
%     b=-0.0701;
% end


xr = X_k1k(1,1) + a*cos(X_k1k(3,1)) - b*sin(X_k1k(3,1));
yr = X_k1k(2,1) + a*sin(X_k1k(3,1)) + b*cos(X_k1k(3,1));

dx=LMmap(:,2)-ones(map_num,1)*xr;
dy=LMmap(:,3)-ones(map_num,1)*yr;

theta_p=wrapTo2Pi(atan2(dy,dx)-ones(map_num,1)*X_k1k(3,1));
range_p=sqrt((dx.^2)+(dy.^2));

select_indx=find(range_p <25);

%SELECT LANDMARKS

if ~isempty(select_indx)
    
    select_map=LMmap(select_indx,:);
    select_map_num=size(select_map,1);
    
    DA_MAT=zeros(Z_num,map_num);
    IG_MAT=ones(Z_num,map_num)*1000;
    IG_MAT2=ones(Z_num,map_num)*1000;
    
    I=ones([map_num,Z_num])*1000;
    
    %INNOVATION GATE
    
    for z=1:Z_num
        
        if strcmp(semlab{z},'post')
            semantic_indx=find(select_map(:,4) ==1);
            
        elseif strcmp(semlab{z},'tree')
            semantic_indx=find(select_map(:,4) ==4);
            
        elseif strcmp(semlab{z},'parking meter')
            semantic_indx=find(select_map(:,4) ==2);
            
        elseif strcmp(semlab{z},'street sign')
            semantic_indx=find(select_map(:,4) ==3);
            
        end
        
        if  ~isempty(semantic_indx)
            
            semantic_map=select_map(semantic_indx,:);
            semantic_map_num=size(semantic_map,1);
            
            finalIDs=semantic_map(:,1);
            
            I(finalIDs,z)=ones(length(finalIDs),1)*Z(z,1)-theta_p(finalIDs);
            
            predictedobs=[finalIDs rad2deg(theta_p(finalIDs))];
            
            %INNOVATION COVARIANCE
            
            a_count=0;
            
            for m=1:semantic_map_num
                
                map_ID=semantic_map(m,1);
                
                
                xm=semantic_map(m,2);
                ym=semantic_map(m,3);
                
                x=X_k1k(1,1);
                y=X_k1k(2,1);
                phi=X_k1k(3,1);
                
                %JH=[-(y-ym)./((xm-x).^2 + (y-ym).^2),(x-xm)./((x-xm).^2 + (ym-y).^2), -ones(size(LM_ID,1),1)];
                
                JH=[-(y-ym+b*cos(phi)+a*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2 +(y-ym+b*cos(phi)+a*sin(phi)).^2)...
                    (x-xm+a*cos(phi)-b*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)...
                    (((a*cos(phi)-b*sin(phi))./(x-xm+a*cos(phi)-b*sin(phi))+((b*cos(phi)+a*sin(phi)).*(y-ym+b*cos(phi)+a*sin(phi)))./(x-xm+a*cos(phi)-b*sin(phi)).^2).*(x-xm+a*cos(phi)-b*sin(phi)).^2)./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)-1];
                
                R=var_theta;
                
                S=R+JH*P_k1k*transpose(JH);
                
                IG=transpose(I(map_ID,z))*inv(S)*I(map_ID,z);
                
                if IG < IG_gate
                    
                    IG_MAT(z,map_ID)=IG;
                    a_count=a_count+1;
                    
                end
                
            end
            
        end
    end
    
    %END OF IG LOOP
    
    %START OF DATA ASSOSICATION
    
    for m=1:select_map_num
        
        map_ID=select_map(m,1);
        [min_lm,I_min_lm] = mink(IG_MAT(:,map_ID),1);
        
        if min_lm<1000
            IG_MAT2(I_min_lm,map_ID)=min_lm;
        end
        
    end
    
    finz_count=0;
    
    associations=zeros(length(Z),1);
    associateinovs=1000*ones(length(Z),1);
    
    for z=1:Z_num
        
        [min_ig,I_min_ig] = mink(IG_MAT2(z,:),1);
        
        if min_ig<1000
            finz_count=finz_count+1;
            
            DA_MAT(z,I_min_ig)=1;
            
            fin_Z(finz_count,1)=Z(z);
            fin_ID(finz_count,1)=I_min_ig;
            fin_inov(finz_count,1)=I(I_min_ig,z);
            associations(z,1)=I_min_ig;
            associateinovs(z,1)=I(I_min_ig,z);
            %         else
            da_count(z,1)=0;
        end
        
    end
    
    Obs.associations=associations;
    Obs.innovations=associateinovs;
    
    for ass=1:length(associations)
        if associations(ass) > 0
            Obs.predictedR(ass)=range_p(associations(ass));
            Obs.predictedB(ass)=theta_p(associations(ass));
        end
        
    end
    
    
    %END OF DATA ASSOCIATION
    finz_count;
    
    if finz_count>0
        
        fin_inov;
        
        xm=LMmap(fin_ID,1);
        ym=LMmap(fin_ID,2);
        
        
        
        x=ones(finz_count,1)*X_k1k(1,1);
        y=ones(finz_count,1)*X_k1k(2,1);
        phi=ones(finz_count,1)*X_k1k(3,1);
        
        JH=[-(y-ym+b*cos(phi)+a*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2 +(y-ym+b*cos(phi)+a*sin(phi)).^2)...
            (x-xm+a*cos(phi)-b*sin(phi))./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)...
            (((a*cos(phi)-b*sin(phi))./(x-xm+a*cos(phi)-b*sin(phi))+((b*cos(phi)+a*sin(phi)).*(y-ym+b*cos(phi)+a*sin(phi)))./(x-xm+a*cos(phi)-b*sin(phi)).^2).*(x-xm+a*cos(phi)-b*sin(phi)).^2)./((x-xm+a*cos(phi)-b*sin(phi)).^2+(y-ym+b*cos(phi)+a*sin(phi)).^2)-1];
        
        R=eye(finz_count)*var_theta;
        
        S=R + JH*P_k1k*transpose(JH);
        
        S=S +sigma_bearingP * eye(size(S));
        
        
        
        %UPDATE
        
        if cond(S) <10
            K=P_k1k*transpose(JH)*inv(S);
        else
            K=zeros(3,size(S,1));
        end
        %         Z
        %         associations
        %         fin_ID
        %         fin_inov
        %         K
        %K=P_k1k*transpose(JH)*inv(S)
        %K=K+sigma_bearingP^2 * eye(size(S))
        
        
        %disp("Intigrating bearing observation")
        
        X_k1k1=X_k1k+K*fin_inov;
        P_k1k1=P_k1k-K*S*transpose(K);
        
    else
        
        %disp("Discarding bearing observation")
        
        X_k1k1=X_k1k;
        P_k1k1=P_k1k;
        fin_inov=1000;
        
        
    end
    
else
    %disp("Discarding bearing observation")
    X_k1k1=X_k1k;
    P_k1k1=P_k1k;
    fin_inov=1000;
    
end

ob.pose=X_k1k1;

end

