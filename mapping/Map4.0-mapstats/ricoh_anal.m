close all
clear all

bag=rosbag('/media/maleen/AMS_SSD/2020-01-28-Wentworth/2020-01-28-Wentworth-P4-ricoh1.bag');

%Select topics
eyebag=select(bag,'Topic','/left_back_eye/image_raw');

msgs=readMessages(eyebag);

eye_size=length(msgs);

for i=1:eye_size
    
    gps_stamp(1,i)=msgs{i,1}.Header.Stamp.Sec+(msgs{i,1}.Header.Stamp.Nsec)/1000000000-1580199316.424338023;
    
end
