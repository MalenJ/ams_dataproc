close all
clear all
%Load

fileroot='2020-01-28-Wentworth';
filename='2020-01-28-Wentworth-P4';

CSVPATH=strcat('/home/maleen/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
IMGPATH=strcat('/home/maleen/media/AMS_SSD/',fileroot,'/img_data'); %IMAGE DATA

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/2020_02_08_calib/'; %CLIB DATA

%Camera Params

% load(strcat(CALIBPATH,'back_calibapp.mat'),'cameraParams');
% BC_params=cameraParams;
% load(strcat(CALIBPATH,'left_calibapp.mat'),'cameraParams');
% LC_params=cameraParams;
% load(strcat(CALIBPATH,'right_calibapp.mat'),'cameraParams');
% RC_params=cameraParams;


load(strcat(CALIBPATH,'left_front/','leftfront_calibapp.mat'),'cameraParams');
LF_params=cameraParams;
load(strcat(CALIBPATH,'left_back/','leftback_calibapp.mat'),'cameraParams');
LB_params=cameraParams;
load(strcat(CALIBPATH,'right_back/','rightback_calibapp.mat'),'cameraParams');
RB_params=cameraParams;
load(strcat(CALIBPATH,'right_front/','rightfront_calibapp.mat'),'cameraParams');
RF_params=cameraParams;

rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));

%rtab_data=readtable(strcat(CSVPATH,filename, '-rtabdata.csv'));

lasertime_data=readtable(strcat(CSVPATH,filename, '-laser_timedata.csv'));
laserrage_data=readtable(strcat(CSVPATH,filename, '-laser_rangedata.csv'));

lamp_data_lb=readtable(strcat(CSVPATH,filename, '-ylb-lampdata.csv'));
lamp_data_lf=readtable(strcat(CSVPATH,filename, '-ylf-lampdata.csv'));
lamp_data_rb=readtable(strcat(CSVPATH,filename, '-yrb-lampdata.csv'));
lamp_data_rf=readtable(strcat(CSVPATH,filename, '-yrf-lampdata.csv'));

lamp_sorted_lb=readtable(strcat(CSVPATH,filename, '-ylb-sorted_yolo.csv'));
lamp_sorted_lf=readtable(strcat(CSVPATH,filename, '-ylf-sorted_yolo.csv'));
lamp_sorted_rb=readtable(strcat(CSVPATH,filename, '-yrb-sorted_yolo.csv'));
lamp_sorted_rf=readtable(strcat(CSVPATH,filename, '-yrf-sorted_yolo.csv'));


MAPPATH_feature='/home/maleen/cloud_academic/ams_data/map_data/RTK_maps/'; %MAP DATA
load(strcat(MAPPATH_feature,'2019-03-22-Wentworth-map/2019-03-22-Wentworth-satmap.mat'),'map_sat');
%% Analyse RTK

%front_time=front_rgb_time{:,3};

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};

rtk_iter=find(rtk_time<15, 1, 'last' );

base_lat=mean(rtk_lat(1:rtk_iter));
base_long=mean(rtk_long(1:rtk_iter));
base_alti=mean(rtk_alti(1:rtk_iter));

%Wentworthmap
base_lat=-33.878911084893204;
base_long=1.511945376668094e+02;
base_alti=26.187976892944185;

%Villagemap
% base_lat=-33.880632824130295;
% base_long=1.511895158304587e+02;
% base_alti=43.434044433462200;


[rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);

for i=1:length(rtk_time)-15
    
    rtkXdist=rtk_enux(i+15)-rtk_enux(i);
    rtkYdist=rtk_enuy(i+15)-rtk_enuy(i);
    
    sqrt(rtkXdist^2+rtkYdist^2);
    rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
    rtk_heading(i,:)=atan2(rtkYdist,rtkXdist);
    rtkdist_timer(i,:)=rtk_time(i,:);
    
end


rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);
rtkinterp_z = spline([0;rtk_time],[0;rtk_enuz]);
rtkinterp_theta= spline([0;rtkdist_timer],[0;rtk_heading]);

test=linspace(0,212,106);
% %
figure
hold on
plot(rtkdist_timer,rad2deg(rtk_heading),'b')
plot(rtkdist_timer,rad2deg(ppval(rtkinterp_theta,rtkdist_timer)),'r')
% %
% figure
% hold on
% plot(rtk_enux,rtk_enuy,'b')
% plot(ppval(rtkinterp_x,test),ppval(rtkinterp_y,test),'-x')

T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]);


%%

[map_enu(:,1), map_enu(:,2), map_enu(:,3)]=geodetic2enu(map_sat(:,1),map_sat(:,2),map_sat(:,3),base_lat,base_long,base_alti, wgs84Ellipsoid);
map_enu_ID=transpose(linspace(1,length(map_enu(:,1)),length(map_enu(:,1))));
map_enu_semantic=[1;4;1;4;4;1;1;1;4;1;1;1;1;1;1;1;4];
LMmap=[map_enu_ID,map_enu(:,1)+0.35,map_enu(:,2)-1.7,map_enu_semantic];


%% LASER DATA
laser_time=lasertime_data{:,3};
laser_range=laserrage_data{:,:};

%% YOLO DATA


BFYT=vertcat(lamp_data_lb,lamp_data_lf,lamp_data_rb,lamp_data_rf);
yolo_observations = sortrows(BFYT,'lamp_times');

ricoh_centerX=ones(1,4);
ricoh_centerX(1)=240;%LF_params.PrincipalPoint(1);
ricoh_centerX(2)=240;%RF_params.PrincipalPoint(1);

ricoh_centerX(3)=240;%LB_params.PrincipalPoint(1);
ricoh_centerX(4)=240;%RB_params.PrincipalPoint(1);



%% RTAB


rtab_data=readtable(strcat(CSVPATH,filename, '-rtabdata.csv'));
rtab_time=rtab_data{:,3};
rtabx=rtab_data{:,4};
rtaby=rtab_data{:,5};
rtabquat=rtab_data{:,6:9};
rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);

% rtab_starttime=rtab_time(1);
%
% trans=[ppval(rtkinterp_x,rtab_starttime),ppval(rtkinterp_y,rtab_starttime),ppval(rtkinterp_z,rtab_starttime)];
% rotz=ppval(rtkinterp_theta,22);
% T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
% RTAB_world=T_world_robot*[rtabx';rtaby';zeros(1,length(rtabx));ones(1,length(rtabx))];

rtabinterp_x= interp1(rtab_time,rtabx,'linear','pp');%RTAB_world(1,:));
rtabinterp_y = interp1(rtab_time,rtaby,'linear','pp');%RTAB_world(2,:));
rtabinterp_z = interp1(rtab_time,zeros(length(rtabx),1),'linear','pp');%RTAB_world(3,:));
rtabinterp_theta = interp1(rtab_time,rtabeul(:,1),'linear','pp');

% rtabinterp_x= spline(rtab_time,rtabx);%RTAB_world(1,:));
% rtabinterp_y = spline(rtab_time,rtaby);%RTAB_world(2,:));
% rtabinterp_z = spline(rtab_time,zeros(length(rtabx),1));%RTAB_world(3,:));
% rtabinterp_theta = spline(rtab_time,rtabeul(:,1));

for i=1:length(rtab_time)-1
    
    rtabkXdist=rtabx(i+1)-rtabx(i);
    rtabkYdist=rtaby(i+1)-rtaby(i);
    
    rtabdist(i,:)=sqrt(rtabkXdist^2+rtabkYdist^2);
    rtab_heading(i,:)=atan2(rtabkYdist,rtabkXdist);
    rtabdist_timer(i,:)=rtab_time(i,:);
    
end

rtabinterp_heading=interp1(rtabdist_timer,rtab_heading,'linear','pp');