close all
clear all

%Wentworthmap: 3 and 7
base_lat=-33.878911084893204;
base_long=1.511945376668094e+02;
base_alti=26.187976892944185;


%Load ELmap
MAPPATH_feature='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/'; %MAP DATA
load(strcat(MAPPATH_feature,'/RTK_maps/2019-03-22-Wentworth-map/2019-03-22-Wentworth-satmap.mat'),'map_sat');

[map_enu(:,1), map_enu(:,2), map_enu(:,3)]=geodetic2enu(map_sat(:,1),map_sat(:,2),map_sat(:,3),base_lat,base_long,base_alti, wgs84Ellipsoid);
map_enu_ID=transpose(linspace(1,length(map_enu(:,1)),length(map_enu(:,1))));
map_enu_semantic=[1;4;1;4;4;1;1;1;4;1;1;1;1;1;1;1;4];
LMmap=[map_enu_ID,map_enu(:,1)+0.35,map_enu(:,2)-1.7,map_enu_semantic];

%Load survey locations

dataset='2020-10-03-Wentworth';
fileroot='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/';

lm1=3;
lm2=7;
load(strcat(fileroot,dataset,'/survey/',dataset,'-l',num2str(lm1)','.mat'),'landmarks_enu');
LM1=[mean(landmarks_enu(:,1)) mean(landmarks_enu(:,2)) mean(landmarks_enu(:,3))];
load(strcat(fileroot,dataset,'/survey/',dataset,'-l',num2str(lm2)','.mat'),'landmarks_enu');
LM2=[mean(landmarks_enu(:,1)) mean(landmarks_enu(:,2)) mean(landmarks_enu(:,3))];


figure
axis equal
hold on
for i=1:length(LMmap)
    scatter(LMmap(i,2),LMmap(i,3))
    text(LMmap(i,2),LMmap(i,3), num2str(i));
end

scatter(LM1(1,1),LM1(1,2),'x','k')
scatter(LM2(1,1),LM2(1,2),'x','k')

%% Figuring out displaement

LM1disp=LMmap(lm1,2:3)-LM1(:,1:2)
LM2disp=LMmap(lm2,2:3)-LM2(:,1:2)

meanshift=mean([LM1disp;LM2disp])

% meanshift=LM1disp;

LM1(:,1:2)=LM1(:,1:2)+meanshift;
LM2(:,1:2)=LM2(:,1:2)+meanshift;


scatter(LM1(1,1),LM1(1,2),'x','r')
scatter(LM2(1,1),LM2(1,2),'x','r')

