
close all
clear all
path='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/';

load(strcat(path,'/2020-12-23-UTS/P1-WIDE.mat'));
loop1=edge_world_fin;

% load(strcat(path,'/2020-12-23-UTS/L2-NARROW.mat'));
% loop2=edge_world_fin;
% 
% load(strcat(path,'/2020-12-23-UTS/L3-NARROW.mat'));
% loop3=edge_world_fin;
% 
% load(strcat(path,'/2020-12-23-UTS/L4-NARROW.mat'));
% loop4=edge_world_fin;



sz=1;
figure
hold on
axis equal
scatter(loop1(1,:),loop1(2,:),sz,'filled', 'b')
% scatter(loop2(1,:),loop2(2,:),sz,'filled', 'r')
% scatter(loop3(1,:),loop3(2,:),sz,'filled', 'g')
% scatter(loop4(1,:),loop4(2,:),sz,'filled', 'c')

%mapdata=[loop1,loop2,loop3,loop4];

mapdata=[loop1];



%Village
% k=find(loop1(2,:)>38);
% loop1(:,k)=[];
% k=find(loop3(1,:)<-5);
% loop3(:,k)=[];
% map = robotics.BinaryOccupancyGrid(95,80,25); 
% setOccupancy(map, [transpose(loop1(1,:)+10*ones(1,size(loop1,2))) transpose(loop1(2,:)+40*ones(1,size(loop1,2)))], ones(1,size(loop1,2)))

%UTS
map = robotics.BinaryOccupancyGrid(200,200,25); 
setOccupancy(map, [transpose(mapdata(1,:)+30*ones(1,size(mapdata,2))) transpose(mapdata(2,:)+195*ones(1,size(mapdata,2)))], ones(1,size(mapdata,2)))



figure
show(map)
occupancymat = occupancyMatrix(map);

imwrite(~occupancymat,strcat('P1-map-WIDE-dirty.bmp'))


%% Wentworth
% 
% 
% close all
% clear all
% path='/home/maleen/cloud_academic/ams_data/map_data/edge_maps/';
% 
% load(strcat(path,'/2019-08-22-Wentworth/forwardrun2.mat'));
% forwardmap=edge_world;
% 
% load(strcat(path,'/2019-08-22-Wentworth/reverserun2.mat'));
% reversemap=edge_world;
% 
% sz=1;
% figure
% hold on
% axis equal
% scatter(forwardmap(1,:),forwardmap(2,:),sz,'filled', 'r')
% scatter(reversemap(1,:),reversemap(2,:),sz,'filled', 'b')
% 
% 
% map = robotics.BinaryOccupancyGrid(30,60,25); 
% 
% setOccupancy(map, [transpose(forwardmap(1,:)+25*ones(1,size(forwardmap,2))) transpose(forwardmap(2,:)+25*ones(1,size(forwardmap,2)))], ones(1,size(forwardmap,2)))
% figure
% show(map)
% occupancymat = occupancyMatrix(map);
% imwrite(~occupancymat,strcat('wentforward2-map.bmp'))