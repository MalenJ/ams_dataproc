clear all
close all

filename='output';
rtkvel_data=readtable(strcat('/home/maleen/cloud_academic/ams_data/csv_data/edgemap_data/', filename, '-RTKvel.csv'));

rtkvel_time=rtkvel_data{:,2};
rtkvel_n=rtkvel_data{:,3};
rtkvel_e=rtkvel_data{:,4};
rtkvel_d=rtkvel_data{:,5};


vel=sqrt(rtkvel_n.^2+rtkvel_e.^2+rtkvel_d.^2)/1000;


heading=wrapTo2Pi(atan2(rtkvel_n,rtkvel_e));


figure(1)
plot(vel)

figure(2)
plot(rtkvel_time,rad2deg(heading),'-x')