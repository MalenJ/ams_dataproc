close all
clear all

filename='output';

front_rgb_time=readtable(strcat('/home/maleen/cloud_academic/ams_data/csv_data/edgemap_data/', filename, '-front_rgb.csv'));
front_time=front_rgb_time{:,3};

depthMAT=load(strcat('/home/maleen/cloud_academic/ams_data/csv_data/edgemap_data/', filename, '-depthimgs.mat'),'depth_imgs');
depth_imgs=depthMAT.depth_imgs;


three_sigmamat=zeros(480,640);

rangemat=zeros(480,640);
figure
hold on
for i=1:440
    
    for u=1:480
        
        for v=1:640
            
            d(i,u,v)=double(depth_imgs{i}(u,v))/1000;
        end
        
    end
   mesh(double(depth_imgs{i})/1000) 
end

% figure
% hold on
% for u=1:480
%     
%     for v=1:640
% 
%         three_sigmamat(u,v)=3*var(d(:,u,v));
%         rangemat(u,v)=max(d(:,u,v))-min(d(:,u,v));
%         
%     end
%     
% end

mesh(three_sigmamat)

% plot(front_time(1:440),d)
% plot(front_time(1:440),ones(440)*mean(d))
% 
% figure
% hist(d);
% three_sigma=3*var(d)
% range=max(d)-min(d)


