close all
clear all

%Load localisation CSV's

fileroot='2021-01-11-UTS';
filename='2021-01-11-UTS-P1';

CSVPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
IMGPATH=strcat('/media/maleen/malen_ssd/phd/critical_ams_data/',fileroot,'/image_data/',filename,'/'); %IMAGE DATA
CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/ICRA2021calib/'; %CLIB DATA

odom_sorted=readtable(strcat(CSVPATH, filename, '-sorted.csv'));
rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));
enc_data=readtable(strcat(CSVPATH, filename, '-encoder.csv'));
imu_data=readtable(strcat(CSVPATH, filename, '-imu.csv'));

rtab_data=readtable(strcat(CSVPATH,filename, '-RTAB.csv'));

backcam_depthdata=readtable(strcat(CSVPATH,filename, '-back_depth.csv'));
backcam_edgedata=readtable(strcat(CSVPATH,filename, '-back_rgb.csv'));
load(strcat(IMGPATH, filename, '-depthimgs.mat'),'depth_imgs');

%Camera Params
load(strcat(CALIBPATH,'backcam/back_calibapp.mat'),'cameraParams');
BC_params=cameraParams;
load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');


% %% Analyse RTK
% 
% T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]);
% 
% rtk_time=rtk_data{:,2};
% rtk_lat=rtk_data{:,3};
% rtk_long=rtk_data{:,4};
% rtk_alti=rtk_data{:,5};
% 
% %Wentworth ICRA
% % base_lat=-33.878911084893204;
% % base_long=1.511945376668094e+02;
% % base_alti=26.187976892944185;
% 
% % %VILLAGE ICRA
% % base_lat=-33.880632824130295;
% % base_long=1.511895158304587e+02;
% % base_alti=43.434044433462200;
% 
% rtk_iter=find(rtk_time<40, 1, 'last' );
% 
% % base_lat=mean(rtk_lat(1:rtk_iter));
% % base_long=mean(rtk_long(1:rtk_iter));
% % base_alti=mean(rtk_alti(1:rtk_iter));
% 
% base_lat=rtk_lat(421);
% base_long=rtk_long(421);
% base_alti=rtk_alti(421);
% 
% [rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);
% 
% rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
% rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);
% 
% figure
% hold on
% scatter(rtk_enux,rtk_enuy)
% scatter(0,0,'r','x')
% axis('equal')
% 
% % figure
% % scatter(rtk_time,rtk_enux)
% 
% 
% %% Finding rotation between RTK map and odometry
% 
% RTK_POS=[ppval(rtkinterp_x ,54) ppval(rtkinterp_y ,54) 0];
% RTAB_POS=[0 1 0];
% % 
% t=atan2(norm(cross(RTK_POS,RTAB_POS)), dot(RTK_POS,RTAB_POS))-deg2rad(3);
% 
% rad2deg(t)
% 
% %% ROTATE AND TRANSLATE RTK DATA
% 
% Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0; sin(-t-pi/2) cos(-t-pi/2) 0; 0 0 1]; %Translation was figured out based on running EKF in the begining and then translating it based on RTK location relative to the baselink. 
% 
% for i=1:length(rtk_enuy)
%     
%     enu_rot(:,i)=Rz*[rtk_enux(i); rtk_enuy(i);1];
%     
% end
% 
% figure
% scatter(enu_rot(1,:),enu_rot(2,:))
% 
% rtk_enux=enu_rot(1,:);
% rtk_enuy=enu_rot(2,:);
% 
% 
% %% RTK Heading
% 
% for i=1:length(rtk_time)-15
% 
%     rtkXdist=rtk_enux(i+15)-rtk_enux(i);
%     rtkYdist=rtk_enuy(i+15)-rtk_enuy(i);    
%     
%     sqrt(rtkXdist^2+rtkYdist^2);
%     rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
%     rtk_heading(i,:)=atan2(rtkYdist,rtkXdist);
%     rtkdist_timer(i,:)=rtk_time(i,:);
% 
% end
% 
% rtk_heading=wrapTo2Pi(rtk_heading);
% rtkinterp_z = spline([0;rtk_time],[0;rtk_enuz]);
% rtkinterp_theta= spline([0;rtkdist_timer],[0;rtk_heading]);
% 
% %test=linspace(0,212,106);
% 
% figure
% hold on
% scatter(rtkdist_timer,rad2deg(rtk_heading),'b')
% 
% % figure
% % hold on
% % plot(rtkdist_timer,rad2deg(rtk_heading),'b')
% % plot(rtkdist_timer,rad2deg(ppval(rtkinterp_theta,rtkdist_timer)),'r')
% % % % 
% % figure
% % hold on
% % plot(rtk_enux,rtk_enuy,'b')
% % plot(ppval(rtkinterp_x,test),ppval(rtkinterp_y,test),'-x')

%% FOR RTAB

%rtab_iter=find(rtk_time<22, 1, 'last' );

rtab_time=rtab_data{:,3};
rtabx=rtab_data{:,4};

rtaby=rtab_data{:,5};
rtabquat=rtab_data{:,6:9};
rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);

starttime=rtab_time(1);
endtime=rtab_time(length(rtab_time));

% rtab_starttime=rtab_time(1);
% 
% trans=[ppval(rtkinterp_x,rtab_starttime),ppval(rtkinterp_y,rtab_starttime),ppval(rtkinterp_z,rtab_starttime)];
% rotz=ppval(rtkinterp_theta,22);
% T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);

% T_world_robot=T_world_RTK*inv(T_robot_RTK);
% RTAB_world=T_world_robot*[rtabx';rtaby';zeros(1,length(rtabx));ones(1,length(rtabx))];

rtabinterp_x= interp1(rtab_time,rtabx,'linear','pp');%RTAB_world(1,:));
rtabinterp_y = interp1(rtab_time,rtaby,'linear','pp');%RTAB_world(2,:));
rtabinterp_z = interp1(rtab_time,zeros(length(rtabx),1),'linear','pp');%RTAB_world(3,:));
rtabinterp_theta = interp1(rtab_time,rtabeul(:,1),'linear','pp');
%END RTAB
for i=1:length(rtab_time)-1
    
    rtabkXdist=rtabx(i+1)-rtabx(i);
    rtabkYdist=rtaby(i+1)-rtaby(i);
    
    rtabdist(i,:)=sqrt(rtabkXdist^2+rtabkYdist^2);
    rtab_heading(i,:)=atan2(rtabkYdist,rtabkXdist);
    rtabdist_timer(i,:)=rtab_time(i,:);
    
end

rtabinterp_heading=interp1(rtabdist_timer,rtab_heading,'linear','pp');

%% PROJECTION

%Load cam intrinsics

PP=BC_params.PrincipalPoint;
FL=BC_params.FocalLength;

j=0;

%load('D.mat')

for i=1:length(backcam_depthdata.back_depth_time)
    
    depth_time=backcam_depthdata.back_depth_time(i);
    
    row=backcam_edgedata.back_rgb_time==depth_time;
    edge_indx=find(row>0);
    
    if ~isnan(edge_indx)
        j=j+1;
        
        edgeimg=imread(strcat(IMGPATH,'edge/', filename ,'-edge-',num2str(edge_indx), ".jpg"));
        BW = im2bw(edgeimg,0.5);
        
        BWU = undistortImage(BW, BC_params);
        depthimgU = undistortImage(depth_imgs{i},BC_params);
        
        depthedge_time(j)=depth_time;
        depthedge{j}=immultiply(BWU,depthimgU);
        
    else
        disp('skip')
    end
    
end

%T_robot_cam=(ld.TF_base_link);

roter=eul2tform(cam2base_eul_fin);
T_robot_cam=makehgtform('translate',cam2base_trans_fin)*roter;


%% Sectioning RTK
% figure
% scatter(rtk_time,rtk_enux)
% 
% starttime=95;% 521;
% endtime=120;%540;
% starttime
% % starttime=50+154.5;
% % endtime=50+166;
% 
% %VIllage
% startI=find(rtk_time<starttime, 1, 'last' );
% endI=find(rtk_time<endtime, 1, 'last' );
% 
% rtkinterp_x_sec = spline(rtk_time(startI:endI),rtk_enux(startI:endI));
% rtkinterp_y_sec = spline(rtk_time(startI:endI),rtk_enuy(startI:endI));
% rtkinterp_theta_sec= spline(rtk_time(startI:endI),rtk_heading(startI:endI));
% 
% figure
% scatter(rtk_enux(startI:endI),rtk_enuy(startI:endI))

%% Depthedge process

j=1;
startiter=find(depthedge_time<starttime, 1, 'last' );
enditer=find(depthedge_time<endtime, 1, 'last' );

%450 onwards

%for k=1:105

k=0;

for i=startiter:enditer

    time=depthedge_time(i);
    
    %FOR RTK
%     trans=[ppval(rtkinterp_x_sec,time),ppval(rtkinterp_y_sec,time),ppval(rtkinterp_z,time)];
%     rotz=ppval(rtkinterp_theta_sec,time);
%     T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
%     T_world_robot=T_world_RTK*inv(T_robot_RTK);

    %FOR RTAB
    trans=[ppval(rtabinterp_x,time),ppval(rtabinterp_y,time),ppval(rtabinterp_z,time)];
    rotz=ppval(rtabinterp_theta,time);
    T_world_robot=makehgtform('translate',trans,'zrotate',rotz);   
    
    
    for u=200:250 %300-350
        
        for v=1:640
            
            depth=double(depthedge{i}(u,v))/1000;
            
            if depth>0
                
                z_cam(j)=depth;
                x_cam(j)=((u-PP(2))*depth)/(FL(2));
                y_cam(j)=((v-PP(1))*depth)/(FL(1));
                
                T_world_cam=T_world_robot*T_robot_cam;

                edge_world(:,j)=T_world_cam*[z_cam(j);-y_cam(j);-x_cam(j);1];
                
                %edge_test(:,j)=T_robot_cam*[z_cam(j);-y_cam(j);-x_cam(j);1];
                
                if edge_world(3,j) <=0.1
                    %YOU ARE FILTERING OUT RELATIVE TO THE WORLD. THIS IS
                    %ALRIGHT IN THE CASE OF RTAB BECAUSE YOUR HEIGHT IS
                    %ALWAYS ZERO. BUT IN RTK THIS IS NOT THE CASE.
                    %RE-EVALUATE. MAKE THE CONDITION RELATIVE TO ROBOT. 
                    k=k+1;
                    edge_world_fin(:,k)=edge_world(:,j);
                    
                end

                j=j+1;
            end 
        end
           
    end
end

%imagerelativetorobot=T_robot_cam*[z_cam;-y_cam;-x_cam;ones(size(y_cam))];


sz=1;
sz
figure
hold on

%scatter3(rtk_enux,rtk_enuy,rtk_enuz, sz, 'b')
%scatter3(RTAB_world(1,:),RTAB_world(2,:),RTAB_world(3,:),sz,'b')

%scatter3(rtabx,rtaby,zeros(length(rtabx),1), sz, 'b')

scatter3(edge_world_fin(1,:),edge_world_fin(2,:),edge_world_fin(3,:),sz,'filled', 'r')
%scatter3(z_cam,-y_cam,-x_cam,sz,'filled')
%scatter3(imagerelativetorobot(1,:),imagerelativetorobot(2,:),imagerelativetorobot(3,:),sz,'filled')
axis equal
xlabel('x')
ylabel('y')
zlabel('z')

%%
% close all
% path='/home/maleen/cloud_academic/ams_data/map_data/edge_maps/';
% loopfun=edge_world;
% 
% load(strcat(path,'/2019-09-04-Village/loop1.mat'),'edge_world_fin');
% loop1=edge_world_fin;
% % 
% load(strcat(path,'/2019-09-04-Village/loop2.mat'),'edge_world_fin');
% loop2=edge_world_fin;
% 
% load(strcat(path,'/2019-09-04-Village/loop3.mat'),'edge_world_fin');
% loop3=edge_world_fin;
% % 
% load(strcat(path,'/2019-09-04-Village/loop4.mat'),'edge_world_fin');
% loop4=edge_world_fin;
% 
% sz=1;
% figure
% hold on
% axis equal
% scatter(loop1(1,:),loop1(2,:),sz,'filled', 'b')
% scatter(0,0,'x','r')
% scatter(loop2(1,:),loop2(2,:),sz,'filled', 'r')
% scatter(loop3(1,:),loop3(2,:),sz,'filled', 'g')
% scatter(loop4(1,:),loop4(2,:),sz,'filled', 'c')
% scatter(rtk_enux,rtk_enuy,2)
% scatter(loopfun(1,:),loopfun(2,:),sz,'filled', 'k')

