close all
clear all


base_lat=-33.8788600739;
base_long=151.194447734;
base_alti=19.967417881;


filename=strcat('/home/maleen/2018-11-23-Wentworth/paths/','2018-11-23-Wentworth-P1','.bag');
bag=rosbag(filename);

rtkbag=select(bag,'Topic','/piksi/navsatfix_rtk_fix');
rtkfix=readMessages(rtkbag);

rtk_size=length(rtkfix);
rtk_stamp=zeros(rtk_size,1);
rtk_lat=zeros(rtk_size,1);
rtk_long=zeros(rtk_size,1);
rtk_alti=zeros(rtk_size,1);
x_rtk=zeros(rtk_size,1);
y_rtk=zeros(rtk_size,1);
z_rtk=zeros(rtk_size,1);

for i=1:rtk_size
    
    rtk_stamp(i,1)=(rtkfix{i,1}.Header.Stamp.Sec+(rtkfix{i,1}.Header.Stamp.Nsec)/1000000000);
    rtk_lat(i,1)=rtkfix{i,1}.Latitude;
    rtk_long(i,1)=rtkfix{i,1}.Longitude;
    rtk_alti(i,1)=rtkfix{i,1}.Altitude;
    
end

 [x_rtk, y_rtk, z_rtk]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);
 
 plot(x_rtk,y_rtk);