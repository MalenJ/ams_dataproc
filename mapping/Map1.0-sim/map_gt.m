close all;

load('2019-02-11-Wentworth-gtmap.mat');


figure
hold on
scatter(0,0,'x','b')
scatter(gtmap_enu(1,:),gtmap_enu(2,:),'x','r')
legend('base', 'streetlamps')

figure
hold on
plot(landmarks_lla(:,2), landmarks_lla(:,1), '.r', 'MarkerSize', 20)  
plot_google_map('MapType','satellite','MapScale', 2, 'APIKey','AIzaSyBzN8VsdTIaHixp3DOc9137qKV_rDP5m7g') 

