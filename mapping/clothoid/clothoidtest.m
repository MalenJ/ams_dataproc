close all
clear all


% lb = clothoidLaneBoundary;
% 
% lb.Curvature = 0;            % degrees/m
% %lb.CurvatureDerivative = 0.02; % degrees/m^2
% lb.HeadingAngle = 0;           % degrees
% lb.CurveLength = 50;           % meters
% lb.LateralOffset = 1.8;        % meters (to left of ego)
% 
% 
% rb = lb;
% rb.LateralOffset = -2;
% 
% 
% bep = birdsEyePlot('XLimits',[0 50],'YLimits',[-10 10]);
% lbPlotter = laneBoundaryPlotter(bep,'DisplayName','Left-lane boundary','Color','r');
% rbPlotter = laneBoundaryPlotter(bep,'DisplayName','Right-lane boundary','Color','g');
% plotLaneBoundary(lbPlotter,lb)
% plotLaneBoundary(rbPlotter,rb);
% grid
% hold on
% 
% x = 0:5:50;
% yl = computeBoundaryModel(lb,x);
% yr = computeBoundaryModel(rb,x);
% plot(x,yl,'ro')
% plot(x,yr,'go')
% hold off


x0=0;
y0=0;
theta0=0;
k0=0;
dk=0;
L=5;

l1 = ClothoidCurve( x0, y0, theta0, k0, dk, L );
l2 = ClothoidCurve( 5, 0, theta0, k0, 0.1, 2 );


npts = 1000;
figure
hold on
l1.plot(npts,'Color','red','LineWidth',3);
l2.plot(npts,'Color','black','LineWidth',3);