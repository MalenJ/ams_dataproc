close all 
clear all

filename='2021-01-11-UTS/2021-01-11-UTS';
load(strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/el_maps/',filename,'-FIN.mat'),'finmap','rtabx','rtaby');
l1=finmap;
rtab_l1=[rtabx,rtaby];

filename='2021-01-11-UTS/2020-12-23-UTS';
load(strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/el_maps/',filename,'-FIN.mat'),'finmap2','rtabx','rtaby');
l2=[l1,finmap2]; 
rtab_l2=[rtab_l1;[rtabx, rtaby]]; 


filename='2021-01-11-UTS/2021-01-11-UTS';
load(strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/el_maps/',filename,'-FIN2.mat'),'finmap2','rtabx','rtaby');
l3=[l2,finmap2]; 
rtab_l3=[rtab_l2;[rtabx, rtaby]]; 



figure
axis('equal')
hold on
grid on
sz=10;
grid minor
scatter(rtab_l3(:,1),rtab_l3(:,2),sz,'b','filled')
scatter(l3(1,:),l3(2,:),'x');
scatter(l2(1,:),l2(2,:),'g');

for i=1:length(l3)
    text(l3(1,i),l3(2,i), num2str(i));
end

deletelms=[1,4,27,117,120,121,125,128,135,145,146,147,148,149,150,151,152,158,159,160,161];

l3(:,deletelms)=[];


final_map=zeros(length(l3),4);
for i=1:length(l3)
    
    final_map(i,1)=i;
    final_map(i,2)=l3(1,i);
    final_map(i,3)=l3(2,i);
    final_map(i,4)=l3(3,i);
    
end




