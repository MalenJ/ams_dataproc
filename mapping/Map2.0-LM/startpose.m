close all 
clear all

bag1=rosbag('/home/maleen/rosbags/2019-06-20-demo/startpose.bag');

mapmode=select(bag1,'Topic','/rtabmap/localization_pose');

%localmode=select(bag2,'Topic','/rtabmap/localization_pose');

mapmode_msgs=readMessages(mapmode);
%localmode_msgs=readMessages(localmode);

mapsize=length(mapmode_msgs);


for i=1:mapsize

    mapsize_stamp(1,i)=mapmode_msgs{i,1}.Header.Stamp.Sec+(mapmode_msgs{i,1}.Header.Stamp.Nsec)/1000000000;
    mapX(1,i)=mapmode_msgs{i,1}.Pose.Pose.Position.X;
    mapY(1,i)=mapmode_msgs{i,1}.Pose.Pose.Position.Y;
    
    x=mapmode_msgs{i,1}.Pose.Pose.Orientation.X;
    y=mapmode_msgs{i,1}.Pose.Pose.Orientation.Y;
    z=mapmode_msgs{i,1}.Pose.Pose.Orientation.Z;
    w=mapmode_msgs{i,1}.Pose.Pose.Orientation.W;
    
    rtabeul = quat2eul([w x y z]);
    mapTheta(1,i)=wrapTo2Pi(rtabeul(:,1));
    
end

poseX=mean(mapX)
poseY=mean(mapY)
poseTheta=mean(mapTheta)
