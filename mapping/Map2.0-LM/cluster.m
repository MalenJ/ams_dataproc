close all 
clear all

fileroot='2021-01-11-UTS';
filename='2021-01-11-UTS-P1';
PATH='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/';
load(strcat(PATH,fileroot,'/',filename,'-post2.mat'));


LM1=LM;
%rtabmsy=[rtabx, rtaby];
rtab1=[rtabx,rtaby];

% filename='2019-08-22-Wentworth/2019-08-22-Wentworth-rear4-yolo';
% load(strcat('/home/maleen/cloud_academic/ams_data/map_data/el_maps/',filename,'-elpost.mat'));
% % 
% % LMmerge=[LM1;LM]; 
% % %rtabmerge=[rtabmsy;[rtabx, rtaby]]; 
% 
% rtkmerge=[rtk1;[rtk_enux,rtk_enuy]];
 
epsilon=0.25;
minpts=2;

idxz = dbscan(LM1,epsilon,minpts);

inliers=find(idxz>-1);
idfin=idxz(inliers);
LM_fin=LM1(inliers,:);

figure
axis('equal')
hold on
grid on
sz=10;
grid minor
scatter(rtab1(:,1),rtab1(:,2),sz,'b','filled')
gscatter(LM_fin(:,1),LM_fin(:,2),idfin,'rgb','x');
%scatter(LMmapRTK(:,2)+0.35,LMmapRTK(:,3)-1.7,'k')
%scatter(gtmap_enu(1,:),gtmap_enu(2,:),sz,'k','filled')


outlier_percent=(length(LM1)-length(inliers))*100/length(LM1)

lm_num=max(idfin);
finmap=ones(4,lm_num);

for i=1:lm_num
    
    indexes=find(idfin==i);
    
    %remove i-1 if merging two maps
    finmap(:,i)=[mean(LM_fin(indexes,1)); mean(LM_fin(indexes,2)); mode(LM_fin(indexes,3)); i];
end
 
sz=10;

for i=1:length(finmap)
    text(finmap(1,i),finmap(2,i), num2str(finmap(4,i)));
    scatter(finmap(1,i),finmap(2,i),sz,'r','filled')
end


%% DELETE PROBLEMATIC


%VILLAGE
%indexes=[51 52 53 54 57];
% indexes=[1,2,3,4,5,6,8,10,11,12,13,14,16,22,24,25,28,30,31,32];%loop1
%indexes=[2,3,4,6,8,9,10,11,14,19,21,22,26,28,33,34];%loop2
%indexes=[1,2,4,7,8];%loop3_1
%indexes=[3,5,6,7,9,12];%loop3_2
%indexes=[3,4,6,7,9,11,12,13,15,18,19,21,22,23,25,26,27,29,33,36,37,38];%loop4

%UTS
% indexes=[1 4 6 7 8 10 13 22 24 27 38 40 50 52 56 58 59 60 62 63 67 70 75 76 78 79 82 86 87 94 97 98 100 102 106 107 109 ...
%          111 113 114 115 117 118 119 136 139 140 143 144 146 149 151 154 158 163 166 167 168 169 173 177];

%finmap(:,indexes)=[];


%UTS2
%indexes=[64 65 67 72 74 76 78 79 81 82 100 101 97 116 117 118 119 121 19 131 132 135 136 139 140 142 144 145];
indexes=[1,6,12,22,25,28,33,39,45,52,59,62,63,47,50,53,65];

finmap2=finmap(:,indexes);

figure
axis('equal')
hold on
grid on
sz=10;
grid minor
scatter(rtab1(:,1),rtab1(:,2),sz,'b','filled')
mapID=0;

for i=1:length(finmap2)
    
    finmap2(4,i)=mapID;
    text(finmap2(1,i),finmap2(2,i), num2str(finmap2(4,i)));
    scatter(finmap2(1,i),finmap2(2,i),sz,'r','filled')
    mapID=mapID+1;
end
