% close all
% clear all
% 
% %LoadCSV
% 
% fileroot='2021-01-11-UTS';
% filename='2021-01-11-UTS-P1';
% PATH='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/';
% load(strcat(PATH,fileroot,'/',filename,'-mapready2.mat'));

yoloimg_path=strcat('/media/maleen/malen_ssd/phd/critical_ams_data/',fileroot,'/image_data/',filename,'/yolo/');

starttime=15;
endtime=200;



%VARS
a_laser=-0.0465;
b_laser=0.0148;
szz=20;
angle_increment=0.00436332309619; %Hokiyo scans are counter clockwise
min_scanrange=0.10;
max_scanrange=30;
range_noise=0.20;
time_diff=0.1;

%% Sectioning RTK
% % figure
% % scatter(rtk_time,rtk_enux)

% startI=find(rtk_time<starttime, 1, 'last' );
% endI=find(rtk_time<endtime, 1, 'last' );
%
% rtkinterp_x_sec = spline(rtk_time(startI:endI),rtk_enux(startI:endI));
% rtkinterp_y_sec = spline(rtk_time(startI:endI),rtk_enuy(startI:endI));
% rtkinterp_theta_sec= spline(rtk_time(startI:endI),rtk_heading(startI:endI));
%
% %figure
% %scatter(rtk_enux(startI:endI),rtk_enuy(startI:endI))
%%

figure
subplot(1,2,1)
axis equal
hold on
grid on
sz=10;
grid minor
scatter(rtabx,rtaby,sz,'b','filled')
%scatter(rtabx(startI:endI),rtaby(startI:endI),sz,'r','filled')

iterateplot=1;

iter=1;

startiter=find(yolo_time<starttime, 1, 'last' );
enditer=find(yolo_time<endtime, 1, 'last' );

%filename='2019-09-04-Village-P4-loop';
for i=startiter:enditer
    
    
    obs_bearing=yolo_bearing(i,1);
    cam=yolo_cam{i};
    time=yolo_time(i,1);
    Idx = knnsearch(laser_time,time);
    laserT=laser_time(Idx);
    scan=laser_range(Idx,:);
    
    if abs(laserT-time)<time_diff
        
        if strcmp(cam,'LC')
            
            ang=-2.35619449615;
            scan_bin=1;
            
            a_cam=bear_obs(i).a;
            b_cam=bear_obs(i).b;
            
            gamma=atan2(abs(b_cam-b_laser),abs(a_cam-a_laser));
            d=sqrt((a_cam-a_laser)^2+(b_cam-b_laser)^2);
            
            %bear=obs_bearing;
            gradang=obs_bearing;
            
            c=d*sin(gradang+gamma);
            
            rad2deg(gradang);
            
            while (-ang >gradang) %(2*pi-gradang)
                
                ang=ang+angle_increment;
                
                if -ang< pi-gamma
                    
                    beta=pi/2+ang+gradang;
                    R=c/cos(beta);
                    scan_range=scan(scan_bin);
                    
                    if scan_range>min_scanrange && abs(scan_range-R)<range_noise && scan_range<max_scanrange
                        range2LM=scan_range;
                        
                        %RTAB
                        robot_x=ppval(rtabinterp_x ,time);
                        robot_y=ppval(rtabinterp_y ,time);
                        heading=wrapTo2Pi(ppval(rtabinterp_theta ,time));
                        rad2deg(heading);
                        
                        %                         %RTK
                        %                         trans=[ppval(rtkinterp_x_sec,time),ppval(rtkinterp_y_sec,time),ppval(rtkinterp_z,time)];
                        %                         rotz=ppval(rtkinterp_theta_sec,time);
                        %                         T_world_robot=makehgtform('translate',trans,'zrotate',rotz);
                        %                         robot_x=T_world_robot(1,4);
                        %                         robot_y=T_world_robot(2,4);
                        %                         heading=rotz;
                        
                        bearing2LM=-ang+heading;
                        
                        XLM=robot_x+a_laser*cos(heading)-b_laser*sin(heading)+range2LM*cos(bearing2LM);
                        YLM=robot_y+a_laser*sin(heading)+b_laser*cos(heading)+range2LM*sin(bearing2LM);
                        
                        clear semantic
                        semantic=yolo_label{i};
                        
                        if strcmp(semantic,'post')
                            label=1;
                        elseif strcmp(semantic,'parking meter')
                            label=2;
                        elseif strcmp(semantic,'street sign')
                            label=3;
                        elseif strcmp(semantic,'tree')
                            label=4;
                        elseif strcmp(semantic,'mailbox')
                            label=5;
                        end
                        
                        LM(iter,:)=[XLM,YLM,label];
                        iter=iter+1;
                        
                        if iterateplot==1
                            cam
                            label
                            subplot(1,2,1)
                            scatter(XLM,YLM,szz,'r','x')
                            scatter(robot_x,robot_y,szz,'b','x')
                            subplot(1,2,2)
                            yolonum(i);
                            I = imread(strcat(yoloimg_path, filename, '-' ,  num2str(yolonum(i)) ,'.jpg'));
                            imshow(I)
                            w=waitforbuttonpress;
                        end
                        
                    end
                end
                
                scan_bin=scan_bin+1;
                
            end
            
            
        elseif strcmp(cam,'RC')
            
            ang=2.35619449615;
            scan_bin=1081;
            
            a_cam=bear_obs(i).a;
            b_cam=bear_obs(i).b;
            
            
            gamma=atan2(abs(b_cam-b_laser),abs(a_cam-a_laser));
            d=sqrt((a_cam-a_laser)^2+(b_cam-b_laser)^2);
            
            %bear=obs_bearing;
            gradang=obs_bearing;
            
            c=d*sin((2*pi-gradang)+gamma);
            
            rad2deg(gradang);
            
            while (ang >(2*pi-gradang))
                
                ang=ang-angle_increment;
                
                if ang< pi-gamma
                    
                    beta=pi/2-ang+(2*pi-gradang);
                    R=c/cos(beta);
                    scan_range=scan(scan_bin);
                    
                    if scan_range>min_scanrange && abs(scan_range-R)<range_noise && scan_range<max_scanrange
                        range2LM=scan_range;
                        
                        %RTAB
                        robot_x=ppval(rtabinterp_x ,time);
                        robot_y=ppval(rtabinterp_y ,time);
                        heading=wrapTo2Pi(ppval(rtabinterp_theta ,time));
                        rad2deg(heading);
                        
                        %                         %RTK
                        %                         trans=[ppval(rtkinterp_x_sec,time),ppval(rtkinterp_y_sec,time),ppval(rtkinterp_z,time)];
                        %                         rotz=ppval(rtkinterp_theta_sec,time);
                        %                         T_world_robot=makehgtform('translate',trans,'zrotate',rotz);
                        %                         robot_x=T_world_robot(1,4);
                        %                         robot_y=T_world_robot(2,4);
                        %                         heading=rotz;
                        
                        bearing2LM=-ang+heading;
                        
                        XLM=robot_x+a_laser*cos(heading)-b_laser*sin(heading)+range2LM*cos(bearing2LM);
                        YLM=robot_y+a_laser*sin(heading)+b_laser*cos(heading)+range2LM*sin(bearing2LM);
                        
                        clear semantic
                        semantic=yolo_label{i};
                        
                        if strcmp(semantic,'post')
                            label=1;
                        elseif strcmp(semantic,'parking meter')
                            label=2;
                        elseif strcmp(semantic,'street sign')
                            label=3;
                        elseif strcmp(semantic,'tree')
                            label=4;
                        elseif strcmp(semantic,'mailbox')
                            label=5;
                        end
                        
                        
                        LM(iter,:)=[XLM,YLM,label];
                        
                        iter=iter+1;
                        if iterateplot==1
                            cam
                            label
                            subplot(1,2,1)
                            scatter(XLM,YLM,szz,'m','x')
                            scatter(robot_x,robot_y,szz,'b','x')
                            subplot(1,2,2)
                            yolonum(i);
                            I = imread(strcat(yoloimg_path, filename, '-' ,  num2str(yolonum(i)) ,'.jpg'));
                            imshow(I)
                            w=waitforbuttonpress;
                        end
                        
                    end
                end
                
                scan_bin=scan_bin-1;
                
            end
            
        end
        
        if iterateplot==1
            pause(.0001);
        end
    end
end

if iterateplot==0
    
    subplot(1,2,1)
    scatter(LM(:,1),LM(:,2),szz,'m','x')
    
end


%%
% load(strcat(MAPPATH_feature,'/el_maps/2019-09-04-Village/2019-09-04-Village-ICRAMAP.mat'),'LMmap');
%
%
% if iterateplot==0
%
%     scatter(LM(:,1),LM(:,2),szz,'m','x')
%
%     for i=1:length(LMmap)
%         scatter(LMmap(i,2),LMmap(i,3))
%         text(LMmap(i,2),LMmap(i,3), num2str(i));
%     end
%
% end


