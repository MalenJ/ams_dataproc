close all
clear all

%LoadCSV

filename='2019-06-21-MSY-morn-yolo';
rtab_data=readtable(strcat('/home/maleen/malen_cloud/sync/AMS/map_data/map_data_demo/', filename, '-rtabdata.csv'));
lasertime_data=readtable(strcat('/home/maleen/malen_cloud/sync/AMS/map_data/map_data_demo/', filename, '-laser_timedata.csv'));
laserrage_data=readtable(strcat('/home/maleen/malen_cloud/sync/AMS/map_data/map_data_demo/', filename, '-laser_rangedata.csv'));
rtk_data=readtable(strcat('/home/maleen/malen_cloud/sync/AMS/map_data/map_data_demo/', filename, '-RTK.csv'));
lamp_data=readtable(strcat('/home/maleen/malen_cloud/sync/AMS/map_data/map_data_demo/', filename, '-lampdata.csv'));
odom_data=readtable(strcat('/home/maleen/malen_cloud/sync/AMS/map_data/map_data_demo/', filename, '-odomdata.csv'));

%RTABMAP
rtab_time=rtab_data{:,3};
rtabx=rtab_data{:,4};
rtaby=rtab_data{:,5};
rtabquat=rtab_data{:,6:9};
rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);

% figure 
% hold on
% plot(rtab_time,rad2deg(rtabeul(:,1)))


%CONVERT RTK

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};

base_lat=mean(rtk_lat(1:50));
base_long=mean(rtk_long(1:50));
base_alti=mean(rtk_alti(1:50));

[rtk_enux, rtk_enuy, lrtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);


rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);

%ODOM DATA

odomx=odom_data{:,4};
odomy=odom_data{:,5};
odomtheta=odom_data{:,6};

%LASER DATA
laser_time=lasertime_data{:,3};
laser_range=laserrage_data{:,:};

%YOLO DATA
yolo_time=lamp_data{:,3};
yolo_cam=lamp_data{:,4};
yolo_bearing=lamp_data{:,5};
yolo_label=lamp_data{:,12};
yolonum=lamp_data{:,2};

yolo_bx1=lamp_data{:,10};
yolo_bx2=lamp_data{:,11};

% angle_min: -2.35619449615
% angle_max: 2.35619449615
% angle_increment: 0.00436332309619
% time_increment: 1.73611151695e-05
% scan_time: 0.0250000003725
% range_min: 0.0230000000447
% range_max: 60.0

% 
for i=1:length(rtab_time)-1
    
%     rtabXdist=rtabx(i+1)-rtabx(i);
%     rtabYdist=rtaby(i+1)-rtaby(i);
%     
%     
%     rtabdist(i,:)=sqrt(rtabXdist^2+rtabYdist^2);
%     rtab_heading(i,:)=wrapTo2Pi(atan2(rtabYdist,rtabXdist));
    
    
    if i>1
        
        if (rtabeul(i,1)-rtabeul(i-1,1)>deg2rad(90))
            
            rtabeul(i)=rtabeul(i-1);
        end
        
    end
%     
%     rtabmean_time(i,:)=(rtab_time(i+1)+rtab_time(i))/2;
    
end
plot(rtab_time,rad2deg(rtabeul(:,1)))

rtabinterp_x= spline(rtab_time,rtabx);
rtabinterp_y = spline(rtab_time,rtaby);
%rtabinterp_theta = spline(rtab_time,rtabtheta);
rtabinterp_theta = spline(rtab_time,rtabeul(:,1));
%plot(rtabmean_time,rad2deg(rtab_heading))

%Figuring out rotation

RTK_POS=[ppval(rtkinterp_y ,75)+0.15*cos(ppval(rtabinterp_theta ,75)), -ppval(rtkinterp_x ,75)+0.15*sin(ppval(rtabinterp_theta ,75)) 0];
RTAB_POS=[ppval(rtabinterp_x ,75) ppval(rtabinterp_y ,75) 0];

t=atan2(norm(cross(RTK_POS,RTAB_POS)), dot(RTK_POS,RTAB_POS));


%% ALLIGNED DATA


Rz = [cos(t) -sin(t) -0.15; sin(t) cos(t) -0.15; 0 0 1];
Rmap= [cos(t) -sin(t) 0.2; sin(t) cos(t) 0.2; 0 0 1];

for i=1:length(rtk_enuy)
    
    enu_rot(:,i)=Rz*[rtk_enuy(i); -rtk_enux(i);1];
    
end
%%
rotinterp_x = spline([0; rtk_time],[0; transpose(enu_rot(1,:))]);
rotinterp_y = spline([0; rtk_time],[0; transpose(enu_rot(2,:))]);

figure
axis equal
hold on
plot(rtk_enuy,-rtk_enux,'r')
plot(rtabx,rtaby,'b')
title('pre aligned')

figure
axis equal
hold on
plot(enu_rot(1,:),enu_rot(2,:),'r')
scatter(ppval(rotinterp_x ,75),ppval(rotinterp_y, 75),'x','r')
scatter(ppval(rtabinterp_x ,75),ppval(rtabinterp_y ,75),'o','b')
plot(rtabx,rtaby,'b')
title('post aligned')


