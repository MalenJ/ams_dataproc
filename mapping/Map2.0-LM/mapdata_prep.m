close all
clear all

%Load localisation CSV's

fileroot='2021-01-11-UTS';
filename='2021-01-11-UTS-P1';

CSVPATH=strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/datasets/',fileroot,'/csv_data/'); %CSV DATA
IMGPATH=strcat('/media/maleen/malen_ssd/phd/critical_ams_data/',fileroot,'/image_data/',filename,'/'); %IMAGE DATA
CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/ICRA2021calib/'; %CLIB DATA

MAPPATH_feature='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/el_maps/'; %MAP DATA
MAPPATH_edge='/media/maleen/malen_ssd/phd/cloud_academic/ams_data/map_data/edge_maps/';

odom_sorted=readtable(strcat(CSVPATH, filename, '-sorted.csv'));
rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));

rtab_data=readtable(strcat(CSVPATH,filename, '-rtabdata.csv'));

enc_data=readtable(strcat(CSVPATH, filename, '-encoder.csv'));
imu_data=readtable(strcat(CSVPATH, filename, '-imu.csv'));

lasertime_data=readtable(strcat(CSVPATH,filename, '-laser_timedata.csv'));
laserrage_data=readtable(strcat(CSVPATH,filename, '-laser_rangedata.csv'));


lamp_data_real=readtable(strcat(CSVPATH, filename, '-yrs-lampdata.csv'));
% lamp_data_lb=readtable(strcat(CSVPATH,filename, '-ylb-lampdata.csv'));
% lamp_data_lf=readtable(strcat(CSVPATH,filename, '-ylf-lampdata.csv'));
% lamp_data_rb=readtable(strcat(CSVPATH,filename, '-yrb-lampdata.csv'));
% lamp_data_rf=readtable(strcat(CSVPATH,filename, '-yrf-lampdata.csv'));
% lamp_sorted_lb=readtable(strcat(CSVPATH,filename, '-ylb-sorted_yolo.csv'));
% lamp_sorted_lf=readtable(strcat(CSVPATH,filename, '-ylf-sorted_yolo.csv'));
% lamp_sorted_rb=readtable(strcat(CSVPATH,filename, '-yrb-sorted_yolo.csv'));
% lamp_sorted_rf=readtable(strcat(CSVPATH,filename, '-yrf-sorted_yolo.csv'));

backcam_depthdata=readtable(strcat(CSVPATH,filename, '-back_depth.csv'));
backcam_edgedata=readtable(strcat(CSVPATH,filename, '-back_rgb.csv'));

%Depth Image data
% load(strcat(IMGPATH, filename, '-depthimgs.mat'),'depth_imgs');

%Camera Params

load(strcat(CALIBPATH,'backcam/back_calibapp.mat'),'cameraParams');
BC_params=cameraParams;

load(strcat(CALIBPATH,'left/left_calibapp.mat'),'cameraParams');
LC_params=cameraParams;
load(strcat(CALIBPATH,'right/right_calibapp.mat'),'cameraParams');
RC_params=cameraParams;

% load(strcat(CALIBPATH,'left_front/leftfront_calibapp.mat'),'cameraParams');
% LF_params=cameraParams;
% load(strcat(CALIBPATH,'left_back/leftback_calibapp.mat'),'cameraParams');
% LB_params=cameraParams;
% load(strcat(CALIBPATH,'right_back/rightback_calibapp.mat'),'cameraParams');
% RB_params=cameraParams;
% load(strcat(CALIBPATH,'right_front/rightfront_calibapp.mat'),'cameraParams');
% RF_params=cameraParams;

centerX=ones(1,6);
centerX(1)=213;%LF_params.PrincipalPoint(1);
centerX(2)=213;%RF_params.PrincipalPoint(1);
centerX(3)=213;%LB_params.PrincipalPoint(1);
centerX(4)=213;%RB_params.PrincipalPoint(1);
centerX(5)=LC_params.PrincipalPoint(1);
centerX(6)=RC_params.PrincipalPoint(1);

load(strcat(CALIBPATH,'backcam/back_finalresult.mat'),'cam2base_trans_fin','cam2base_eul_fin');


% %LOAD EL MAP
% 
% %load(strcat(MAPPATH_feature,'/RTK_maps/2019-03-22-Wentworth-map/2019-03-22-Wentworth-satmap.mat'),'map_sat');
% load(strcat(MAPPATH_feature,'/el_maps/2019-09-04-Village/2019-09-04-Village-ELMAP.mat'),'final_map');
% 
% %LOAD EDGE MAP
% 
% %I=imread(strcat(MAPPATH_edge,'2019-08-22-Wentworth/','2019-08-22-Wentworth-map-clean3.png'));
% I=imread(strcat(MAPPATH_edge,'2019-09-04-Village/','village_map3.png'));
% %% Analyse RTK
% 
% T_robot_RTK=makehgtform('translate',[-0.1409 0.009 1.4687]);
% 
% rtk_time=rtk_data{:,2};
% rtk_lat=rtk_data{:,3};
% rtk_long=rtk_data{:,4};
% rtk_alti=rtk_data{:,5};
% 
% %Wentworth ICRA
% % base_lat=-33.878911084893204;
% % base_long=1.511945376668094e+02;
% % base_alti=26.187976892944185;
% 
% % %VILLAGE ICRA
% % base_lat=-33.880632824130295;
% % base_long=1.511895158304587e+02;
% % base_alti=43.434044433462200;
% 
% rtk_iter=find(rtk_time<40, 1, 'last' );
% 
% base_lat=mean(rtk_lat(1:rtk_iter));
% base_long=mean(rtk_long(1:rtk_iter));
% base_alti=mean(rtk_alti(1:rtk_iter));
% 
% [rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);
% 
% rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
% rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);
% 
% figure
% hold on
% scatter(rtk_enux,rtk_enuy)
% scatter(0,0,'r','x')
% axis('equal')
% 
% % figure
% % scatter(rtk_time,rtk_enux)
% 
% 
% %% Finding rotation between RTK map and odometry
% 
% RTK_POS=[ppval(rtkinterp_x ,54) ppval(rtkinterp_y ,54) 0];
% RTAB_POS=[0 1 0];
% % 
% t=atan2(norm(cross(RTK_POS,RTAB_POS)), dot(RTK_POS,RTAB_POS))-deg2rad(3);
% 
% rad2deg(t)
% 
% %% ROTATE AND TRANSLATE RTK DATA
% 
% Rz = [cos(-t-pi/2) -sin(-t-pi/2) 0; sin(-t-pi/2) cos(-t-pi/2) 0.24; 0 0 1]; %Translation was figured out based on running EKF in the begining and then translating it based on RTK location relative to the baselink. 
% 
% for i=1:length(rtk_enuy)
%     
%     enu_rot(:,i)=Rz*[rtk_enux(i); rtk_enuy(i);1];
%     
% end
% 
% figure
% scatter(enu_rot(1,:),enu_rot(2,:))
% 
% rtk_enux=enu_rot(1,:);
% rtk_enuy=enu_rot(2,:);
% 
% 
% %% RTK Heading
% 
% for i=1:length(rtk_time)-15
% 
%     rtkXdist=rtk_enux(i+15)-rtk_enux(i);
%     rtkYdist=rtk_enuy(i+15)-rtk_enuy(i);    
%     
%     sqrt(rtkXdist^2+rtkYdist^2);
%     rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
%     rtk_heading(i,:)=atan2(rtkYdist,rtkXdist);
%     rtkdist_timer(i,:)=rtk_time(i,:);
% 
% end
% 
% rtk_heading=wrapTo2Pi(rtk_heading);
% rtkinterp_z = spline([0;rtk_time],[0;rtk_enuz]);
% rtkinterp_theta= spline([0;rtkdist_timer],[0;rtk_heading]);
% 
% %test=linspace(0,212,106);
% 
% figure
% hold on
% scatter(rtkdist_timer,rad2deg(rtk_heading),'b')
% 
% % figure
% % hold on
% % plot(rtkdist_timer,rad2deg(rtk_heading),'b')
% % plot(rtkdist_timer,rad2deg(ppval(rtkinterp_theta,rtkdist_timer)),'r')
% % % % 
% % figure
% % hold on
% % plot(rtk_enux,rtk_enuy,'b')
% % plot(ppval(rtkinterp_x,test),ppval(rtkinterp_y,test),'-x')
% 
% %%
% 
% % [map_enu(:,1), map_enu(:,2), map_enu(:,3)]=geodetic2enu(map_sat(:,1),map_sat(:,2),map_sat(:,3),base_lat,base_long,base_alti, wgs84Ellipsoid);
% % map_enu_ID=transpose(linspace(1,length(map_enu(:,1)),length(map_enu(:,1))));
% % map_enu_semantic=[1;4;1;4;4;1;1;1;4;1;1;1;1;1;1;1;4];
% % LMmapRTK=[map_enu_ID,map_enu(:,1),map_enu(:,2),map_enu_semantic];


%% LASER DATA
laser_time=lasertime_data{:,3};
laser_range=laserrage_data{:,1:1081};

%% DATA STREAM

%BFYT=vertcat(lamp_data_lb,lamp_data_lf,lamp_data_rb,lamp_data_rf);%RICOH
BFYT=lamp_data_real;
yolo_observations = sortrows(BFYT,'lamp_times');
yolo_time=yolo_observations{:,3};

uj=1;
labels={};

for i=1:size(yolo_time)
    
    if i==1
        overall_time(uj)=yolo_time(i);
        labels(uj)={'Y'};
        uj=uj+1;
    else
        if yolo_time(i)>yolo_time(i-1)
            overall_time(uj)=yolo_time(i);
            labels(uj)={'Y'};
            uj=uj+1;
        end
        
    end
    
end

yolo_sorted=table(transpose(overall_time),transpose(labels));
yolo_sorted.Properties.VariableNames = {'overall_time' 'labels'};

BFTS=vertcat(odom_sorted(:,{'overall_time', 'labels'}),yolo_sorted);
data_stream = sortrows(BFTS,'overall_time');

%% YOLO DATA
yolonum=yolo_observations{:,2};
yolo_time=yolo_observations{:,3};
yolo_cam=yolo_observations{:,4};
%yolo_bearing=lamp_data{:,5};
yolo_centroidx=yolo_observations{:,6};
yolo_label=yolo_observations{:,12};


for i=1:size(yolo_time,1)
    
    current_time=yolo_time(i);
    cam=yolo_cam{i};
    
    cameras_obs=yolo_observations(i,{'yolo_index','camera','centroid_x','yolo_labels'});
    
    bear_obs(i)=bearing_obZ(centerX,current_time,cameras_obs);
    
    %Dimension is centroid x here
    yolo_bearing(i,1)=bear_obs(i).bearings;
    
    
    
end

%% RTAB

rtab_time=rtab_data{:,3};
rtabx=rtab_data{:,4};
rtaby=rtab_data{:,5};
rtabquat=rtab_data{:,6:9};
rtabeul = quat2eul([rtabquat(:,4) rtabquat(:,1:3)]);

% rtab_starttime=rtab_time(1);
%
% trans=[ppval(rtkinterp_x,rtab_starttime),ppval(rtkinterp_y,rtab_starttime),ppval(rtkinterp_z,rtab_starttime)];
% rotz=ppval(rtkinterp_theta,22);
% T_world_RTK=makehgtform('translate',trans,'zrotate',rotz);
% RTAB_world=T_world_robot*[rtabx';rtaby';zeros(1,length(rtabx));ones(1,length(rtabx))];

rtabinterp_x= interp1(rtab_time,rtabx,'linear','pp');%RTAB_world(1,:));
rtabinterp_y = interp1(rtab_time,rtaby,'linear','pp');%RTAB_world(2,:));
rtabinterp_z = interp1(rtab_time,zeros(length(rtabx),1),'linear','pp');%RTAB_world(3,:));
rtabinterp_theta = interp1(rtab_time,rtabeul(:,1),'linear','pp');

% rtabinterp_x= spline(rtab_time,rtabx);%RTAB_world(1,:));
% rtabinterp_y = spline(rtab_time,rtaby);%RTAB_world(2,:));
% rtabinterp_z = spline(rtab_time,zeros(length(rtabx),1));%RTAB_world(3,:));
% rtabinterp_theta = spline(rtab_time,rtabeul(:,1));

for i=1:length(rtab_time)-1
    
    rtabkXdist=rtabx(i+1)-rtabx(i);
    rtabkYdist=rtaby(i+1)-rtaby(i);
    
    rtabdist(i,:)=sqrt(rtabkXdist^2+rtabkYdist^2);
    rtab_heading(i,:)=atan2(rtabkYdist,rtabkXdist);
    rtabdist_timer(i,:)=rtab_time(i,:);
    
end

rtabinterp_heading=interp1(rtabdist_timer,rtab_heading,'linear','pp');