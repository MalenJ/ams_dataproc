clear all
close all


lm_num=24;

for i=1:lm_num
   matvar=sprintf(['l',num2str(i),'.mat']);
   lmvar=sprintf(['l',num2str(i)]);
   load(matvar,lmvar)
   map_sat(i,:)=eval(lmvar);
   
end


figure
hold on
plot(map_sat(:,2), map_sat(:,1), '.g', 'MarkerSize', 10)  
plot_google_map('MapType','satellite','Scale',2, 'APIKey','AIzaSyBzN8VsdTIaHixp3DOc9137qKV_rDP5m7g') 