close all
clear all

%LoadCSV

filename='2019-03-22-Wentworth-P4-yolo';

data_stream=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-sorted.csv'));
rtk_data=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-RTK.csv'));

enc_data=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-encoder.csv'));
imu_data=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-imu.csv'));

lamp_data=readtable(strcat('/home/maleen/git/ams_primary/data_process/csv_data/', filename, '-lampdata.csv'));



%CONVERT RTK

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};

base_lat=mean(rtk_lat(1:100));
base_long=mean(rtk_long(1:100));
base_alti=mean(rtk_alti(1:100));

[rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);

for i=1:length(rtk_time)-20

    rtkXdist=rtk_enux(i+20)-rtk_enux(i);
    rtkYdist=rtk_enuy(i+20)-rtk_enuy(i);    
    
    sqrt(rtkXdist^2+rtkYdist^2);
    rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
    rtk_heading(i,:)=atan2(rtkYdist,rtkXdist);
    rtkdist_timer(i,:)=rtk_time(i,:);

end


rtkinterp_x = spline([0;rtk_time],[0;rtk_enux]);
rtkinterp_y = spline([0;rtk_time],[0;rtk_enuy]);
rtkinterp_z = spline([0;rtk_time],[0;rtk_enuz]);
rtkinterp_theta= spline([0;rtkdist_timer],[0;rtk_heading]);



%DEAD RECK DATA

dead_time=deadreck_data{:,5};
dead_x=deadreck_data{:,6};
dead_y=deadreck_data{:,7};
dead_theta=deadreck_data{:,8};

deadinterp_x= spline([0; dead_time],[0; dead_x]);
deadinterp_y= spline([0; dead_time], [0; dead_y]);

imu_interp=spline(imu_data{:,2},imu_data{:,3});


%IMU DATA

imu_time=imu_data{:,2};
thetaZ=imu_data{:,4};

imuinterp= spline(imu_time,thetaZ);


%Figuring out rotation

[mins,I_mins] = mink(abs(thetaZ),100);

RTK_POS=[ppval(rtkinterp_y ,40) -ppval(rtkinterp_x ,40) 0];%+0.15*cos(ppval(imu_interp ,53.891642703000000)), -ppval(rtkinterp_x ,53.891642703000000)+0.15*sin(ppval(imu_interp ,53.891642703000000)) 0];
ENC_POS=[ppval(deadinterp_x ,40) ppval(deadinterp_y ,40) 0];

t=atan2(norm(cross(RTK_POS,ENC_POS)), dot(RTK_POS,ENC_POS));


%% ALLIGNED DATA


Rz = [cos(t) -sin(t) -0.15; sin(t) cos(t) 0; 0 0 1];
Rmap= [cos(t) -sin(t) 0; sin(t) cos(t) 0; 0 0 1];

for i=1:length(rtk_enuy)
    
    enu_rot(:,i)=Rz*[rtk_enuy(i); -rtk_enux(i);1];
    
end

rotinterp_x = spline([0; rtk_time],[0; transpose(enu_rot(1,:))]);
rotinterp_y = spline([0; rtk_time],[0; transpose(enu_rot(2,:))]);

figure
axis equal
hold on
plot(rtk_enuy,-rtk_enux,'r')
scatter(RTK_POS(1),RTK_POS(2),'x','r')
scatter(ENC_POS(1),ENC_POS(2),'o','b')
plot(dead_x,dead_y,'b')
title('pre aligned')

figure
axis equal
hold on
plot(enu_rot(1,:),enu_rot(2,:),'r')
scatter(ppval(rotinterp_x ,40),ppval(rotinterp_y, 40),'x','r')
scatter(ppval(deadinterp_x ,40),ppval(deadinterp_y ,40),'o','b')
plot(dead_x,dead_y,'b')
title('post aligned')


rtk=ppval(rotinterp_x ,28);
enc=ppval(deadinterp_x ,28);
difference=rtk-enc;

%EXTRACTING LAMP POS

lamp_time=lamp_data{:,3};

rtkx=ppval(rotinterp_x ,lamp_time);
rtky=ppval(rotinterp_y ,lamp_time);

lamp_thetha=ppval(imuinterp ,lamp_time);

for j=1:size(lamp_time)
    
    lamp_rtk(:,j)=[cos(lamp_thetha(j)) -sin(lamp_thetha(j)) rtkx(j); sin(lamp_thetha(j)) cos(lamp_thetha(j)) rtky(j);0 0 1]*[0.055; 0; 1];
    
end

lamp_deadx=ppval(deadinterp_x ,lamp_time);
lamp_deady=ppval(deadinterp_y ,lamp_time);


lamp_data.lamp_rtkx=transpose(lamp_rtk(1,:));
lamp_data.lamp_rtky=transpose(lamp_rtk(2,:));
lamp_data.lamp_thetha=lamp_thetha;

lamp_data.lamp_deadx=lamp_deadx;
lamp_data.lamp_deady=lamp_deady;




