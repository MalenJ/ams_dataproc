close all
clear all

% load('/home/maleen/git/ams_primary/mapping/2019-06-06-MSY/2019-06-06-MSY-P2-prelabeled.mat','base_lat','base_long','base_alti','Rmap','Rz');
% load('/home/maleen/git/ams_primary/mapping/Maps/2019-04-11-MSY-map-tree/2019-04-11-MSY-satmap-tree.mat','map_sat');

load('/home/maleen/cloud academic/ams_primary/mapping/2019-06-06-MSY/2019-06-06-MSY-P2-prelabeled.mat','base_lat','base_long','base_alti','Rmap','Rz');
load('/home/maleen/cloud_academic/ams_data/map_data/RTK_maps/2019-03-22-Wentworth-map/2019-03-22-Wentworth-satmap.mat','map_sat');


[map_enu(:,1), map_enu(:,2), map_enu(:,3)]=geodetic2enu(map_sat(:,1),map_sat(:,2),map_sat(:,3),base_lat,base_long,base_alti, wgs84Ellipsoid);



% for i=1:length(map_enu)
%     
%     gtmap_enu(1:3,i)=Rmap*[map_enu(i,2); -map_enu(i,1);1];
%     gtmap_enu(4,i)=i;
%     
% end



figure
hold on
plot(map_sat(:,2), map_sat(:,1), '.g', 'MarkerSize', 10)  
plot_google_map('MapType','satellite','Scale',2, 'APIKey','AIzaSyBzN8VsdTIaHixp3DOc9137qKV_rDP5m7g') 

figure
hold on

for i=1:length(map_enu)
    
    scatter(map_enu(i,1),map_enu(i,2))
    text(map_enu(i,1),map_enu(i,2), num2str(i));
    
end