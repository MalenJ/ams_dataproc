close all
clear all

% base_lat=-33.8788600739;
% base_long=151.194447734;
% base_alti=19.967417881;

% base_lat=-33.879030153200000;
% base_long=1.511945303460000e+02;
% base_alti=21.708669899700000;

%load('2019-02-11-Wentworth-P7-prelabel.mat','base_lat','base_long','base_alti','Rz');

lnum=41;

landmarks=2;

landmarks_lla=zeros(landmarks,3);
landmarks_enu=zeros(landmarks,3);


for j=1:landmarks
    
    filename=strcat('/home/maleen/rosbags/maps/2019-04-11-MSY-map/l',num2str(lnum),'/','2019-04-11-MSY-l',num2str(lnum),'-' ,num2str(j) ,'.bag');
    bag=rosbag(filename);
    
    rtkbag=select(bag,'Topic','/piksi/navsatfix_best_fix');
    rtkfix=readMessages(rtkbag);
    
    rtk_size=length(rtkfix);
    rtk_stamp=zeros(rtk_size,1);
    rtk_lat=zeros(rtk_size,1);
    rtk_long=zeros(rtk_size,1);
    rtk_alti=zeros(rtk_size,1);
    
    for i=1:rtk_size

    rtk_stamp(i,1)=(rtkfix{i,1}.Header.Stamp.Sec+(rtkfix{i,1}.Header.Stamp.Nsec)/1000000000);
    rtk_lat(i,1)=rtkfix{i,1}.Latitude;
    rtk_long(i,1)=rtkfix{i,1}.Longitude;
    rtk_alti(i,1)=rtkfix{i,1}.Altitude;
    
    end
    
    landmarks_lla(j,:)=[mean(rtk_lat) mean(rtk_long) mean(rtk_alti)];
    %[landmarks_enu(j,1), landmarks_enu(j,2), landmarks_enu(j,3)]=geodetic2enu(landmarks_lla(j,1),landmarks_lla(j,2),landmarks_lla(j,3),base_lat,base_long,base_alti, wgs84Ellipsoid);
    
end

% gtmap_enu=ones(4,landmarks);
% 
% for i=1:length(landmarks_enu)
%     
%     gtmap_enu(1:3,i)=Rz*[landmarks_enu(i,2); -landmarks_enu(i,1);1];
%     gtmap_enu(4,i)=i;
%     
% end


lplot=[mean(landmarks_lla(:,1)) mean(landmarks_lla(:,2)) mean(landmarks_lla(:,3))];
l41=lplot;

figure
hold on
plot(landmarks_lla(:,2), landmarks_lla(:,1), '.r', 'MarkerSize', 20)  
plot(lplot(:,2), lplot(:,1), '.b', 'MarkerSize', 20)  
plot_google_map('MapType','satellite','MapScale',2, 'APIKey','AIzaSyBzN8VsdTIaHixp3DOc9137qKV_rDP5m7g') 


