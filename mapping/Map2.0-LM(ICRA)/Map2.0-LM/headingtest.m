
close all
clear all

%LoadCSV

filename='2019-05-23-MSY-P3-prep';

load(strcat('/home/maleen/git/ams_primary/mapping/Map2.0/',filename,'.mat'));


for i=1:length(rtab_time)-1

    rtabXdist=rtabx(i+1)-rtabx(i);
    rtabYdist=rtaby(i+1)-rtaby(i);
    
  
    rtabdist(i,:)=sqrt(rtabXdist^2+rtabYdist^2);
    rtab_heading(i,:)=wrapTo2Pi(atan2(rtabYdist,rtabXdist));
    
    
    if i>1
    
     if (rtab_heading(i)-rtab_heading(i-1))>deg2rad(90)
         
         rtab_heading(i)=rtab_heading(i-1);
     end
     
    end
    
    rtabmean_time(i,:)=(rtab_time(i+1)+rtab_time(i))/2;
    

end
figure 
hold on
B = smoothdata(rtab_heading)
%plot(rtabmean_time,rad2deg(B),'r')
plot(rtabmean_time,rad2deg(rtab_heading),'b')

figure
plot(rtab_time,rad2deg(rtabtheta),'b')
