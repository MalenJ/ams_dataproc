close all 
clear all

filename='2019-09-04-Village/fin/loop1';
load(strcat('/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/map_data/el_maps/',filename,'-fin.mat'));
l1=finmap;
rtab_l1=[rtabx,rtaby];

filename='2019-09-04-Village/fin/loop2';
load(strcat('/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/map_data/el_maps/',filename,'-fin.mat'));
l2=[l1,finmap]; 
rtab_l2=[rtab_l1;[rtabx, rtaby]]; 

filename='2019-09-04-Village/fin/loop3_1';
load(strcat('/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/map_data/el_maps/',filename,'-fin.mat'));
l3=[l2,finmap]; 
rtab_l3=[rtab_l2;[rtabx, rtaby]]; 

filename='2019-09-04-Village/fin/loop3_2';
load(strcat('/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/map_data/el_maps/',filename,'-fin.mat'));
l4=[l3,finmap]; 
rtab_l4=[rtab_l3;[rtabx, rtaby]]; 

filename='2019-09-04-Village/fin/loop4';
load(strcat('/home/ravindra/HDD-4TB/Maleen/cloud_academic/ams_data/map_data/el_maps/',filename,'-fin.mat'));
l5=[l4,finmap]; 
rtab_l5=[rtab_l4;[rtabx, rtaby]]; 



figure
axis('equal')
hold on
grid on
sz=10;
grid minor
scatter(rtab_l5(:,1),rtab_l5(:,2),sz,'b','filled')
scatter(l5(1,:),l5(2,:),'x');

for i=1:length(l5)
    text(l5(1,i),l5(2,i), num2str(i));
end

deletelms=[1,2,3,31,35,42,57];

l5(:,deletelms)=[];


% figure
% axis('equal')
% hold on
% grid on
% sz=10;
% grid minor
% scatter(rtab_l5(:,1),rtab_l5(:,2),sz,'b','filled')
% scatter(l5(1,:),l5(2,:),'x');
% 
% for i=1:length(l5)
%     text(l5(1,i),l5(2,i), num2str(i));
% end


final_map=zeros(length(l5),4);
for i=1:length(l5)
    
    final_map(i,1)=i;
    final_map(i,2)=l5(1,i);
    final_map(i,3)=l5(2,i);
    final_map(i,4)=l5(3,i);
    
    
end


figure
axis('equal')
hold on
grid on
sz=10;
grid minor
scatter(rtab_l5(:,1),rtab_l5(:,2),sz,'b','filled')
scatter(final_map(:,2),final_map(:,3),'x');

for i=1:length(l5)
    text(final_map(:,2),final_map(:,3), num2str(final_map(:,1)));
end

