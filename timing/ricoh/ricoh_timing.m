close all
clear all

subdata=readmatrix('subtimes2.csv');
subdiff=readmatrix('subdiff.csv');

pubdata1=readmatrix('pubtimes1.csv');
pubdata2=readmatrix('pubtimes2.csv');
pubdata3=readmatrix('pubtimes3.csv');
pubdata4=readmatrix('pubtimes4.csv');

deltapub1=pubdata1-subdata;
deltapub2=pubdata2-subdata;
deltapub3=pubdata3-subdata;
deltapub4=pubdata4-subdata;

mean_subdiff=mean(subdiff)
mean_deltapub1=mean(deltapub1)
mean_deltapub2=mean(deltapub2)
mean_deltapub3=mean(deltapub3)
mean_deltapub4=mean(deltapub4)

figure

y = [83 (mean_subdiff+mean_deltapub1) (mean_subdiff+mean_deltapub2) (mean_subdiff+mean_deltapub3) (mean_subdiff+mean_deltapub4)];
barh(y)
