close all
clear all

imgnums=20;

fin_data=zeros(15,12); 

figure 
hold on

for i=1:imgnums
    
    filname=strcat(num2str(i),'.csv');
    A = readmatrix(filname);
    [rows, columns] = find(isnan(A));
    A(rows,:)=[];
    
    M = mean(A,1);
    
    fin_data(i,:)= M(:,3:14);
    %fin_data(i,:)= [M(:,3:5) M(:,7:9) M(:,11:13) M(:,15:17)];
    
    vizimg=i;
    
    scatter3(fin_data(vizimg,1),fin_data(vizimg,2),fin_data(vizimg,3),'b')
    scatter3(fin_data(vizimg,4),fin_data(vizimg,5),fin_data(vizimg,6),'b')
    scatter3(fin_data(vizimg,7),fin_data(vizimg,8),fin_data(vizimg,9),'b')
    scatter3(fin_data(vizimg,10),fin_data(vizimg,11),fin_data(vizimg,12),'b')
    
    pause(0.1)

end


csvwrite('right_camopti.csv',fin_data)


%%
% 
% figure 
% vizimg=10;
% hold on

% scatter3(fin_data(vizimg,1),fin_data(vizimg,2),fin_data(vizimg,3),'b')
% scatter3(fin_data(vizimg,4),fin_data(vizimg,5),fin_data(vizimg,6),'b')
% scatter3(fin_data(vizimg,7),fin_data(vizimg,8),fin_data(vizimg,9),'b')
% scatter3(fin_data(vizimg,10),fin_data(vizimg,11),fin_data(vizimg,12),'b')
%scatter3(fin_data(vizimg,13),fin_data(vizimg,14),fin_data(vizimg,15),'r')

%%
% 
    filname=strcat('mainframe.csv');
    A = readmatrix(filname);
    [rows, columns] = find(isnan(A));
    A(rows,:)=[];
    
    M = mean(A,1);
    
    finmainframe(:)= M(:,3:11);
    
    csvwrite('finmainframe.csv',finmainframe)