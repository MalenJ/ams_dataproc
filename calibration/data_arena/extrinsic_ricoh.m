%% lOAD DATA
close all
clear all
opti_data=readtable('rightback_camopti.csv');
ld = load('rightback_calibapp.mat','cameraParams');



%% WORLD TO BASELINK TRANSFORM

% %2020_01_31_calib
% wheel_l=[0.34171,0.14064,1.4813];
% wheel_r=[-0.2297,0.14359,1.4088];
% %RTK=[];
% laser=[0.073994,1.4291,1.3839];

%2020_02_08_calib
wheel_l=[-0.0079272,0.14121,1.131];
wheel_r=[-0.5798,0.14121,1.0715];
RTK=[-0.26488,1.6088,0.9686];


world2base_trans=(wheel_r+wheel_l)/2;

base2wheel_l=wheel_l-world2base_trans;
base2wheel_r=wheel_r-world2base_trans;

Rx = [1 0 0; 0 cos(pi/2) -sin(pi/2);0 sin(pi/2) cos(pi/2)]; %rotation of coordinate frame of data arena
r = vrrotvec(Rx*transpose(base2wheel_l/norm(base2wheel_l)),[0;1;0]); %rotation of robot relative to data arena (world)

world2base_rot = vrrotvec2mat(r)*Rx;

wheel_l_base=world2base_rot*(transpose(wheel_l-world2base_trans));
wheel_r_base=world2base_rot*(transpose(wheel_r-world2base_trans));
RTK_base=world2base_rot*(transpose(RTK-world2base_trans));
%laser_base=world2base_rot*(transpose(laser-world2base_trans));


%% OPTI DATA TO BASE

opti_corners=opti_data{:,1:12};


for i=1:size(opti_corners,1)
    
    
    opti_corners_base(i,1:3)=transpose(world2base_rot*(transpose(opti_corners(i,1:3)-world2base_trans)));
    opti_corners_base(i,4:6)=transpose(world2base_rot*(transpose(opti_corners(i,4:6)-world2base_trans)));
    opti_corners_base(i,7:9)=transpose(world2base_rot*(transpose(opti_corners(i,7:9)-world2base_trans)));
    opti_corners_base(i,10:12)=transpose(world2base_rot*(transpose(opti_corners(i,10:12)-world2base_trans)));
    
    midpoint(i,:)=(opti_corners_base(i,10:12)+opti_corners_base(i,4:6))/2;
    %front_cam day 1
    e1=opti_corners_base(i,10:12)-opti_corners_base(i,4:6);
    e2=opti_corners_base(i,1:3)-opti_corners_base(i,4:6);
    e3=cross(e1,e2);
  
    board2base_rot(i+(i-1)*2:i+(i-1)*2+2,1:3)=transpose([e1/norm(e1);e2/norm(e2);e3/norm(e3)]);
    
    rot=transpose([e1/norm(e1);e2/norm(e2);e3/norm(e3)]);

    %dist= [0.127;0.111;-0.005];
    dist= [0.1210;0.0560;-0.005];%(0.006)
   
    board2base_trans(i,:)=transpose(rot*dist)+opti_corners_base(i,4:6); %opti_corners_base(i,7:9); %
   
    TF_base_board(i+(i-1)*3:i+(i-1)*3+3,1:4)=[rot(1,:) board2base_trans(i,1);rot(2,:) board2base_trans(i,2);rot(3,:) board2base_trans(i,3);0 0 0 1];
    
    
end


%opti_centers=[mean([opti_corners(:,1), opti_corners(:,4) ,opti_corners(:,7) ,opti_corners(:,10)],2),mean([opti_corners(:,2), opti_corners(:,5) ,opti_corners(:,8) ,opti_corners(:,11)],2),mean([opti_corners(:,3), opti_corners(:,6) ,opti_corners(:,9) ,opti_corners(:,12)],2)];
figure 
vizimg=5;
hold on
plot3([opti_corners_base(vizimg,1),opti_corners_base(vizimg,10)],[opti_corners_base(vizimg,2),opti_corners_base(vizimg,11)],[opti_corners_base(vizimg,3),opti_corners_base(vizimg,12)])
plot3([opti_corners_base(vizimg,4),opti_corners_base(vizimg,7)],[opti_corners_base(vizimg,5),opti_corners_base(vizimg,8)],[opti_corners_base(vizimg,6),opti_corners_base(vizimg,9)])

scatter3(opti_corners_base(vizimg,1),opti_corners_base(vizimg,2),opti_corners_base(vizimg,3),'r')
scatter3(opti_corners_base(vizimg,4),opti_corners_base(vizimg,5),opti_corners_base(vizimg,6),'b')
scatter3(opti_corners_base(vizimg,7),opti_corners_base(vizimg,8),opti_corners_base(vizimg,9),'r')
scatter3(opti_corners_base(vizimg,10),opti_corners_base(vizimg,11),opti_corners_base(vizimg,12),'b')
scatter3(board2base_trans(vizimg,1),board2base_trans(vizimg,2),board2base_trans(vizimg,3),'r')

scatter3(wheel_l_base(1,1),wheel_l_base(2,1),wheel_l_base(3,1),'b','x')
scatter3(wheel_r_base(1,1),wheel_r_base(2,1),wheel_r_base(3,1),'b','x')
scatter3(RTK_base(1,1),RTK_base(2,1),RTK_base(3,1),'r','x')

xlabel('x-axis')
ylabel('y-axis')
zlabel('z-axis')
axis('equal')


%% BOARD RELATIVE TO THE CAMERA

%%2020_01_31
%images=[1,4,5,6,7,8,11,14,15,16,17,18,19]; %left_back fisheye
%images=[2,5,6,7,8,12,16,17,18,19]; %left_back std
%images=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]; %left_front std
%images=[1,2,4,5,6,8,9,10,11,13,15]; % right back
%images=[1,2,4,5,6,8,10,11,13,15,16]; %right_front

%2020_02_08
%images=[2,3,4,6,8,9,14,15,16,17,18]; %left_front
%images=[1,3,4,5,8,15,16,17,18,19]; %left_back
%images=[1,2,3,4,5,13,15,16,17,18]; %right_front
images=[3,5,8,9,12,16,17,18,19,20]; %right_back


%rot_opti2depth=eul2rotm([-pi,0,0],'xyz');
for j=1:length(images)
    
    i=images(j);
    
    rot=transpose(ld.cameraParams.RotationMatrices(:,:,i));%*rot_opti2depth;
    trans=ld.cameraParams.TranslationVectors(i,:)/1000;
    
    TF_cam_board(j+(j-1)*3:j+(j-1)*3+3,1:4)=[rot(1,:) trans(1,1);rot(2,:) trans(1,2);rot(3,:) trans(1,3);0 0 0 1];
    
    
end


%% CAM TO BASE TF

rot_opti2depth=eul2rotm([pi/2,0,pi/2],'xyz'); %MATLAB frame to robot frame!! 



for i=1:size(opti_corners,1)
    
    %CAM RELATIVE TO THE BASE
    TF_base_cam(i+(i-1)*3:i+(i-1)*3+3,1:4)= TF_base_board(i+(i-1)*3:i+(i-1)*3+3,1:4)*inv(TF_cam_board(i+(i-1)*3:i+(i-1)*3+3,1:4));
   
    TF=TF_base_board(i+(i-1)*3:i+(i-1)*3+3,1:4)*inv(TF_cam_board(i+(i-1)*3:i+(i-1)*3+3,1:4));
    
    cam2base_trans(i,:)=[TF(1,4),TF(2,4),TF(3,4)];
    
    eul(i,:) =(rotm2eul(TF(1:3,1:3)*rot_opti2depth)); %wrapTo2Pi
    
    scatter3(TF(1,4),TF(2,4),TF(3,4),'g')
    text(TF(1,4),TF(2,4),TF(3,4), num2str(i))
    
end

cam2base_trans_fin=mean(cam2base_trans,1)
var_trans=2*sqrt(var(cam2base_trans,1))
cam2base_eul_fin=mean(eul,1)
var_eul=2*sqrt(var(rad2deg(eul),1))

epsilon=0.025;
minpts=2;
idxz = dbscan(cam2base_trans(:,1:2),epsilon,minpts);
inliers=find(idxz>-1);
idfin=idxz(inliers);
cam_fin=cam2base_trans(inliers,:);
scatter3(cam2base_trans_fin(1,1),cam2base_trans_fin(1,2),cam2base_trans_fin(1,3),'r')
figure
hold on
gscatter(cam_fin(:,1),cam_fin(:,2),idfin,'rgb');
scatter(RTK_base(1,1),RTK_base(2,1),'r','x')
scatter(wheel_l_base(1,1),wheel_l_base(2,1),'b','x')
scatter(wheel_r_base(1,1),wheel_r_base(2,1),'b','x')

% 
figure
hold on
plot(cam2base_trans(:,1),'r')
plot(ones(length(cam2base_trans))*cam2base_trans_fin(1,1),'k')
plot(cam2base_trans(:,2),'g')
plot(ones(length(cam2base_trans))*cam2base_trans_fin(1,2),'k')
plot(cam2base_trans(:,3),'b')
plot(ones(length(cam2base_trans))*cam2base_trans_fin(1,3),'k')
title('Final translations and their mean')
legend('x','y','z')


figure
hold on
plot(eul(:,1),'r')
plot(ones(length(cam2base_trans))*cam2base_eul_fin(1,1),'k')
plot(eul(:,2),'g')
plot(ones(length(cam2base_trans))*cam2base_eul_fin(1,2),'k')
plot(eul(:,3),'b')
plot(ones(length(cam2base_trans))*cam2base_eul_fin(1,3),'k')

title('Final rotations and their mean')
legend('Z','Y','X')

