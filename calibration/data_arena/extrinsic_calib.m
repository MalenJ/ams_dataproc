%% lOAD DATA
close all
clear all
opti_data=readtable('backcamopti.csv');
ld = load('back_calibapp.mat','cameraParams');



%% WORLD TO BASELINK TRANSFORM

%front_cam day 1
% RTK=[-0.2923,1.5727,0.8794];
% laser=[-0.2974,1.6708,0.9937];
% wheel_l=[-0.0229,0.1333,1.0111];
% wheel_r=[-0.5938,0.132,0.9907];

%leftcam and right cam day 2
% RTK=[-0.2423,1.5832,0.2458];
% laser=[-0.2475,1.4165,0.3623];
% wheel_l=[0.0275,0.1354,0.3808];
% wheel_r=[-0.5441,0.1361,0.3684];

%front and back cam day 3
% wheel_l=[0.1517,0.1324,1.0517];
% wheel_r=[-0.4196,0.131,1.039];

%2019_12_19_calib
% wheel_l=[0.1673,0.1337,1.0725];
% wheel_r=[-0.4069,0.1361,1.0906];
% RTK=[-0.1209,1.6581,0.9498];
% laser=[-0.1202,1.4308,1.0281];

% %2020_01_06_calib
wheel_l=[0.19397,0.13888,0.98166];
wheel_r=[-0.38085,0.13709,0.93954];
RTK=[-0.079106,1.6072,0.82527];
laser=[-0.1202,1.4308,1.0281];
dist=[0.1230;0.1165;-0.005];

% %2020_01_17_calib
% wheel_l=[0.40504,0.13711,0.69283];
% wheel_r=[-0.16951,0.13918,0.74011];
% RTK=[-0.079106,1.6072,0.82527];
% laser=[0.12473,1.4291,0.66336];
% dist=[0.1230;0.1165;-0.005];

%2020_09_03_calib
% wheel_l=[0.17287,0.13651,0.91896];
% wheel_r=[-0.40162,0.13727,0.93221];
% RTK=[-0.10928,1.6577,0.79498];
% laser=[-0.1202,1.4308,1.0281];%NOT MEASURED PROPERLY
% dist= [0.1210;0.0560;-0.005];

% % %2020_09_11_calib
% wheel_l=[0.24486,0.14001,1.024];
% wheel_r=[-0.32976,0.14032,0.98607];
% RTK=[-0.019159,1.6098,0.87077];
% laser=[-0.1202,1.4308,1.0281];%NOT MEASURED PROPERLY
% dist= [0.1210;0.0560;-0.005];


world2base_trans=(wheel_r+wheel_l)/2;

base2wheel_l=wheel_l-world2base_trans;
base2wheel_r=wheel_r-world2base_trans;

Rx = [1 0 0; 0 cos(pi/2) -sin(pi/2);0 sin(pi/2) cos(pi/2)]; %rotation of coordinate frame of data arena
r = vrrotvec(Rx*transpose(base2wheel_l/norm(base2wheel_l)),[0;1;0]); %rotation of robot relative to data arena (world)

world2base_rot = vrrotvec2mat(r)*Rx;

RTK_base=world2base_rot*(transpose(RTK-world2base_trans));
laser_base=world2base_rot*(transpose(laser-world2base_trans));
wheel_l_base=world2base_rot*(transpose(wheel_l-world2base_trans));
wheel_r_base=world2base_rot*(transpose(wheel_r-world2base_trans));


%% OPTI DATA TO BASE

opti_corners=opti_data{:,1:12};


for i=1:size(opti_corners,1)
    
    
    opti_corners_base(i,1:3)=transpose(world2base_rot*(transpose(opti_corners(i,1:3)-world2base_trans)));
    opti_corners_base(i,4:6)=transpose(world2base_rot*(transpose(opti_corners(i,4:6)-world2base_trans)));
    opti_corners_base(i,7:9)=transpose(world2base_rot*(transpose(opti_corners(i,7:9)-world2base_trans)));
    opti_corners_base(i,10:12)=transpose(world2base_rot*(transpose(opti_corners(i,10:12)-world2base_trans)));
    
    midpoint(i,:)=(opti_corners_base(i,10:12)+opti_corners_base(i,4:6))/2;
%     front_cam day 1
    e1=opti_corners_base(i,10:12)-opti_corners_base(i,4:6);
    e2=opti_corners_base(i,1:3)-opti_corners_base(i,4:6);
    e3=cross(e1,e2);

%   left_cam and right_cam day 2 and front day 3
%     e1=opti_corners_base(i,1:3)-opti_corners_base(i,7:9);
%     e2=opti_corners_base(i,10:12)-opti_corners_base(i,7:9);
%     e3=cross(e1,e2);
    
%   back day 3
%    e1=opti_corners_base(i,4:6)-opti_corners_base(i,10:12);
%    e2=opti_corners_base(i,7:9)-opti_corners_base(i,10:12);
%    e3=-cross(e1,e2);
%     
    board2base_rot(i+(i-1)*2:i+(i-1)*2+2,1:3)=transpose([e1/norm(e1);e2/norm(e2);e3/norm(e3)]);
    
    rot=transpose([e1/norm(e1);e2/norm(e2);e3/norm(e3)]);
    %day1&2:[0.1221;0.1119;-0.005] (0.506 cm radius)
    %day3:[0.1221;0.1119;-0.005] (0.6 cm radius)
    %dist= [0.1220;0.117;-0.005];
    %dist=[0.1230;0.1165;-0.005];
    
    %dist= [0.126;0.1165;-0.005];
   
    board2base_trans(i,:)=transpose(rot*dist)+opti_corners_base(i,4:6); %opti_corners_base(i,7:9); %
    %board2base_trans(i,:)=transpose(transpose([e1/norm(e1);e2/norm(e2);e3/norm(e3)])*[0.25;0;0])+opti_corners_base(i,4:6)

    TF_base_board(i+(i-1)*3:i+(i-1)*3+3,1:4)=[rot(1,:) board2base_trans(i,1);rot(2,:) board2base_trans(i,2);rot(3,:) board2base_trans(i,3);0 0 0 1];
    
    
end


%opti_centers=[mean([opti_corners(:,1), opti_corners(:,4) ,opti_corners(:,7) ,opti_corners(:,10)],2),mean([opti_corners(:,2), opti_corners(:,5) ,opti_corners(:,8) ,opti_corners(:,11)],2),mean([opti_corners(:,3), opti_corners(:,6) ,opti_corners(:,9) ,opti_corners(:,12)],2)];
figure 
vizimg=5;
hold on
plot3([opti_corners_base(vizimg,1),opti_corners_base(vizimg,10)],[opti_corners_base(vizimg,2),opti_corners_base(vizimg,11)],[opti_corners_base(vizimg,3),opti_corners_base(vizimg,12)])
plot3([opti_corners_base(vizimg,4),opti_corners_base(vizimg,7)],[opti_corners_base(vizimg,5),opti_corners_base(vizimg,8)],[opti_corners_base(vizimg,6),opti_corners_base(vizimg,9)])
scatter3(opti_corners_base(vizimg,1),opti_corners_base(vizimg,2),opti_corners_base(vizimg,3),'r')
scatter3(opti_corners_base(vizimg,4),opti_corners_base(vizimg,5),opti_corners_base(vizimg,6),'b')
scatter3(opti_corners_base(vizimg,7),opti_corners_base(vizimg,8),opti_corners_base(vizimg,9),'r')
scatter3(opti_corners_base(vizimg,10),opti_corners_base(vizimg,11),opti_corners_base(vizimg,12),'b')
scatter3(board2base_trans(vizimg,1),board2base_trans(vizimg,2),board2base_trans(vizimg,3),'r')
scatter3(RTK_base(1,1),RTK_base(2,1),RTK_base(3,1),'r','x')
scatter3(wheel_l_base(1,1),wheel_l_base(2,1),wheel_l_base(3,1),'b','x')
scatter3(wheel_r_base(1,1),wheel_r_base(2,1),wheel_r_base(3,1),'b','x')
xlabel('x-axis')
ylabel('y-axis')
zlabel('z-axis')
axis('equal')


%% BOARD RELATIVE TO THE CAMERA

%images=[1,2,3,4,5,6,7,8,9,10,11,26,27,28,29];% front_cam

%images=[1,2,3,5,6,7,8,9,10,11,12,13,14,15]; %left_cam
%images=[1,2,3,4,5,6,7,8,9,10,11,12,13,14]; %right_cam

% images=[2,3,4,7,8,10,11,12,13,14]; %front_cam2
%images=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]; %rearcam

%2019_12_19
%images=[1,2,3,4,5,6,7,8,9,10,11]; %Back_Cam
%images=[1,2,3,4,5,6,7,8,9,10,11,12,13,14]; %Left_cam
%images=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];%right_cam

%2020_01_06
images=[1,2,3,4,7,8,9,10,11,12,13,14,15]; %Back_Cam

%2020_01_17
% images=[2,3,4,5,6,7,8,9,10,11,12,13,14,15]; %Left_cam
% images=[1,2,3,4,5,6,7,8,10,11,12,13,14,15]; %right_cam

%2020_09_03
% images=[1,2,3,6,7,9,11,12,13,14,15,18,19,20]; %Back_Cam

%2020_09_11
% images=[1,2,4,5,6,7,9,11,12,14,15,16,17,18]; %Left_cam
% images=[1,2,3,7,8,11,12,13,15,16,17,18,19,20]; %right_cam


for j=1:length(images)
    
    i=images(j);
    
    rot=transpose(ld.cameraParams.RotationMatrices(:,:,i));
    trans=ld.cameraParams.TranslationVectors(i,:)/1000;
    
    TF_cam_board(j+(j-1)*3:j+(j-1)*3+3,1:4)=[rot(1,:) trans(1,1);rot(2,:) trans(1,2);rot(3,:) trans(1,3);0 0 0 1];
    
    
end


%% CAM TO BASE TF

rot_opti2depth=eul2rotm([pi/2,0,pi/2],'xyz');

for i=1:size(opti_corners,1)
    
    %CAM RELATIVE TO THE BASE
    TF_base_cam(i+(i-1)*3:i+(i-1)*3+3,1:4)= TF_base_board(i+(i-1)*3:i+(i-1)*3+3,1:4)*inv(TF_cam_board(i+(i-1)*3:i+(i-1)*3+3,1:4));
   
    TF=TF_base_board(i+(i-1)*3:i+(i-1)*3+3,1:4)*inv(TF_cam_board(i+(i-1)*3:i+(i-1)*3+3,1:4));
    
    cam2base_trans(i,:)=[TF(1,4),TF(2,4),TF(3,4)];
    
    eul(i,:) =(rotm2eul(TF(1:3,1:3)*rot_opti2depth)); 
    
    scatter3(TF(1,4),TF(2,4),TF(3,4),'g')
    text(TF(1,4),TF(2,4),TF(3,4), num2str(i))
    
end

eul(:,1)=wrapTo2Pi(eul(:,1));

cam2base_trans_fin=mean(cam2base_trans,1)
var_trans=2*sqrt(var(cam2base_trans,1))
cam2base_eul_fin=mean(eul,1)
var_eul=2*sqrt(var(eul,1))
% 
figure
hold on
plot(cam2base_trans(:,1),'r')
plot(ones(length(cam2base_trans))*cam2base_trans_fin(1,1),'k')
plot(cam2base_trans(:,2),'g')
plot(ones(length(cam2base_trans))*cam2base_trans_fin(1,2),'k')
plot(cam2base_trans(:,3),'b')
plot(ones(length(cam2base_trans))*cam2base_trans_fin(1,3),'k')
title('Final translations and their mean')
legend('x','y','z')


figure
hold on
plot(eul(:,1),'r')
plot(ones(length(cam2base_trans))*cam2base_eul_fin(1,1),'k')
plot(eul(:,2),'g')
plot(ones(length(cam2base_trans))*cam2base_eul_fin(1,2),'k')
plot(eul(:,3),'b')
plot(ones(length(cam2base_trans))*cam2base_eul_fin(1,3),'k')

title('Final rotations and their mean')
legend('Z','Y','X')

%% CAMERA LINK TO BASELINK TF'S

rot_base_depth=eul2rotm(cam2base_eul_fin);
trans_base_depth=cam2base_trans_fin;
TF_base_depth=[rot_base_depth(1,:) trans_base_depth(1,1);rot_base_depth(2,:) trans_base_depth(1,2);rot_base_depth(3,:) trans_base_depth(1,3);0 0 0 1];


%aligned_depth_to_color frame rlative to camera_link
%front_cam
% rot_link_depth=quat2rotm([0.999995,1.81961e-06,0.00126282,0.00306001]);
% trans_link_depth=[-0.000242394,0.014775,0.00015938];

%back_cam
%rot_link_depth=quat2rotm([0.999977,-0.0045949,0.00276861,0.00419544]);
%trans_link_depth=[-0.000402429,0.0147971,-1.95472e-06];

%back_camd435i
rot_link_depth=quat2rotm([0.999967,-0.00630896,0.0024854,0.00443657]);
trans_link_depth=[-0.00055712,0.0148958,-4.23172e-05];

% %left_cam
% rot_link_depth=quat2rotm([0.999994,0.00287661,-0.000148942,0.0020701]);
% trans_link_depth=[-0.000579095,0.0148472,0.000192365];

%right_cam
% rot_link_depth=quat2rotm([0.999992,0.00118857,0.000132285,0.00374941]);
% trans_link_depth=[-0.000264576,0.0147411,8.9912e-05];


TF_link_depth=[rot_link_depth(1,:) trans_link_depth(1,1);rot_link_depth(2,:) trans_link_depth(1,2);rot_link_depth(3,:) trans_link_depth(1,3);0 0 0 1];

TF_base_link=TF_base_depth*inv(TF_link_depth);

camlink_trans=[TF_base_link(1,4),TF_base_link(2,4),TF_base_link(3,4)]

camlink_rot= rotm2eul(TF_base_link(1:3,1:3))



