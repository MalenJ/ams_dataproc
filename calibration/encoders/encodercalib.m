close all


start_index=53;
end_index=170;

start_time1=rtk_stamp(1,start_index);
start_time2=rtk_stamp(1,start_index+1);

end_time1=rtk_stamp(1,end_index);
end_time2=rtk_stamp(1,end_index+1);

%Total distance
[dist1,dist2]=RTKdistance(rtk_lat,rtk_long,start_index,end_index);


encX_start = getsampleusingtime(encXts,start_time1,start_time2);
encX_end = getsampleusingtime(encXts,end_time1,end_time2);

encY_start = getsampleusingtime(encYts,start_time1,start_time2);
encY_end = getsampleusingtime(encYts,end_time1,end_time2);

encXticks=abs(encX_start.Data-encX_end.Data);
encYticKs=abs(encY_start.Data-encY_end.Data);
 
% dist1;
% dist2;

dist1perticks_X=dist1/encXticks;
dist1perticks_Y=dist1/encYticKs;

dist2perticks_X=dist2/encXticks;
dist2perticks_Y=dist2/encYticKs;


save('calib9.mat','start_index','end_index','dist1', 'dist2','encXticks','encYticKs','dist1perticks_X','dist1perticks_Y','dist2perticks_X','dist2perticks_Y')