
encX_vals=zeros(9,1);
encY_vals=zeros(9,1);



for i=1:9
    
    filename=strcat('/home/maleen/git/mobility_scooter/Calibration/Encoders/Results/calib' ,num2str(i) ,'.mat');
    load(filename);
    
    encX_vals(i,1)=dist1perticks_X;
    encY_vals(i,1)=dist1perticks_Y;
    
    
end

encX_vals
encY_vals


mean_encX=mean(encX_vals)

mean_encY=mean(encY_vals)


std_encX=std(encX_vals)
std_encY=std(encY_vals)


T=table(encX_vals,encY_vals)
writetable(T,'myData.txt','WriteRowNames',true)  


