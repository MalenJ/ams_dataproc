function [dist1,dist2]=RTKdistance(lat,long,start_index, end_index)



lat=lat(1,start_index:end_index);
long=long(1,start_index:end_index);


fixlength=size(lat,2);

dist1=0;


for i=1:(fixlength-1)
    
    latlon1=[lat(1,i), long(1,i)];
    latlon2=[lat(1,i+1), long(1,i+1)];

    [d1,d2]=lldistkm(latlon1,latlon2);
    
    dist1=dist1+d1;
    
end

dist1=dist1*1000;


latlon1=[lat(1,1), long(1,1)];
latlon2=[lat(1,end), long(1,end)]; 
[d1,d2]=lldistkm(latlon1,latlon2);

dist2=d1*1000;


hold on
plot(long(1,:), lat(1,:), '.b', 'MarkerSize', 20) 
plot_google_map('MapType','satellite','MapScale', 2) 
legend('Final GPS fix', 'RTK baseline')





end
