close all

%Load rosbags
bag=rosbag('/home/maleen/2018-09-13-Wentworth/encrtk9.bag');


%Select topics
rtkbag=select(bag,'Topic','/piksi/navsatfix_rtk_fix');
encoderbag=select(bag,'Topic','/encoder_ticks');

%Read messages
rtkfix=readMessages(rtkbag);
rtk_size=length(rtkfix);

encodermsgs=readMessages(encoderbag);
encoder_size=length(encodermsgs);

%encoderticks=readMessages(encoderbag);

rtk_stamp=zeros(1,rtk_size);
rtk_lat=zeros(1,rtk_size);
rtk_long=zeros(1,rtk_size);
rtk_alti=zeros(1,rtk_size);


encoder_stamp=zeros(1,encoder_size);
encoder_X=zeros(1,encoder_size);
encoder_Y=zeros(1,encoder_size);

for i=1:rtk_size

    rtk_stamp(1,i)=(rtkfix{i,1}.Header.Stamp.Sec+(rtkfix{i,1}.Header.Stamp.Nsec)/1000000000)-(rtkfix{1,1}.Header.Stamp.Sec+(rtkfix{1,1}.Header.Stamp.Nsec)/1000000000);
    rtk_lat(1,i)=rtkfix{i,1}.Latitude;
    rtk_long(1,i)=rtkfix{i,1}.Longitude;
    rtk_alti(1,i)=rtkfix{i,1}.Altitude;
    
end


for i=1:encoder_size
    
    encoder_stamp(1,i)=(encodermsgs{i,1}.Header.Stamp.Sec+(encodermsgs{i,1}.Header.Stamp.Nsec)/1000000000)-(rtkfix{1,1}.Header.Stamp.Sec+(rtkfix{1,1}.Header.Stamp.Nsec)/1000000000);
    encoder_X(1,i)=encodermsgs{i,1}.Ticks.X;
    encoder_Y(1,i)=encodermsgs{i,1}.Ticks.Y;
    
end

rtkts=timeseries(rtk_lat,rtk_stamp,'Name','rtk');

encXts = timeseries(encoder_X,encoder_stamp,'Name','x encoder');
encYts = timeseries(encoder_Y,encoder_stamp,'Name','y encoder');



figure
plot(rtkts, 'r')
figure
subplot(2,1,1); 
plot(encXts, 'g')
subplot(2,1,2);
plot(encYts, 'b')


