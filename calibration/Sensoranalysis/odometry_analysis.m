%% IMU
clear all
close all

filename='2019-07-25-Wentworth/2019-07-25-Wentworth-yolo';

CSVPATH='/home/maleen/cloud_academic/ams_data/csv_data/localisation_data/'; %CSV DATA


rtk_data=readtable(strcat(CSVPATH, filename, '-RTK.csv'));

enc_data=readtable(strcat(CSVPATH, filename, '-encoder.csv'));
imu_data=readtable(strcat(CSVPATH, filename, '-imu.csv'));

imuendstationary=find(imu_data{:,2}<25, 1, 'last' );

stationary_imu=rad2deg(imu_data{1:imuendstationary,3});

mean_imu=mean(stationary_imu)
var_imu=var(stationary_imu-mean_imu)

figure

hist(stationary_imu-mean_imu)
title('imu histogram')

% imu_bias=mean(imu_data{1:1247,3});
% imu_data{:,3}=imu_data{:,3}-imu_bias;
% imu_interp=spline(imu_data{:,2},imu_data{:,3});


%% Encoders % error

rtk_time=rtk_data{:,2};
rtk_lat=rtk_data{:,3};
rtk_long=rtk_data{:,4};
rtk_alti=rtk_data{:,5};

base_lat=mean(rtk_lat(1:100));
base_long=mean(rtk_long(1:100));
base_alti=mean(rtk_alti(1:100));

[rtk_enux, rtk_enuy, rtk_enuz]=geodetic2enu(rtk_lat,rtk_long,rtk_alti,base_lat,base_long,base_alti, wgs84Ellipsoid);


RTKtotaldistance=0;

RTKstartiter=find(rtk_time<270, 1, 'last' );
RTKenditer=find(rtk_time<455, 1, 'last' );

for i=RTKstartiter:RTKenditer
    
    rtkXdist=rtk_enux(i+1)-rtk_enux(i);
    rtkYdist=rtk_enuy(i+1)-rtk_enuy(i);    
    
    lineardist=sqrt(rtkXdist^2+rtkYdist^2);
    rtkdist(i,:)=sqrt(rtkXdist^2+rtkYdist^2);
    RTKtotaldistance=RTKtotaldistance+lineardist;

end


encXcalib=6.608284615932709e-05;
encYcalib=6.603486193757895e-05;
calib=mean([encXcalib,encYcalib]);

enc_startiter=find(enc_data{:,3}<270, 1, 'last' );
enc_enditer=find(enc_data{:,3}<455, 1, 'last' );

ENCtotaldistance=0;

for i=enc_startiter:enc_enditer
    
    encdist=(enc_data{i+1,6}-enc_data{i,6})*calib;
    ENCtotaldistance=ENCtotaldistance+encdist;
    
end

%ENCtotaldistance=(enc_data{enc_enditer,6}-enc_data{enc_startiter,6})*calib;

percentagerror=(ENCtotaldistance-RTKtotaldistance)*100/RTKtotaldistance;

%% Encoders variance

encendstationary=find(enc_data{:,3}<25, 1, 'last' );

stationary_enc=enc_data{1:encendstationary,6};

for i=1:encendstationary
    
    enc_vel(i,1)=((enc_data{i+1,6}-enc_data{i,6})*calib)/(enc_data{i+1,3}-enc_data{i,3});

end

mean_enc=mean(enc_vel)
var_enc=var(enc_vel)

figure

hist(enc_vel-mean_enc)
title('enc histogram')
