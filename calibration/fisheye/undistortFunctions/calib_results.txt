#polynomial coefficients for the DIRECT mapping function (ocam_model.ss in MATLAB). These are used by cam2world

5 -1.773164e+02 0.000000e+00 1.229402e-03 1.773922e-06 4.924564e-09 

#polynomial coefficients for the inverse mapping function (ocam_model.invpol in MATLAB). These are used by world2cam

10 287.436107 178.048193 8.237537 29.320635 13.998452 1.272306 7.526307 5.063283 -0.263822 -0.492992 

#center: "row" and "column", starting from 0 (C convention)

324.904523 323.485097

#affine parameters "c", "d", "e"

0.999595 -0.000312 -0.000244

#image size: "height" and "width"

640 640

