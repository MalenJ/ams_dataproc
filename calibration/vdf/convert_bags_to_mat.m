% Script to convert bags to mat
clear
clc
bagFile = '/home/janindu/data/Maleen/2019-07-02-MSY-rtab.bag';

%% Read from bag
disp('Reading rosbag')
bag = rosbag(bagFile);

disp('Select rTab msgs')
rTabPosSelect = select(bag, 'Topic', '/rtabmap/localization_pose');
disp('Select rgb msgs')
imgSelect = select(bag, 'Topic', '/front_cam/color/image_raw');
disp('Select depth msgs')
depthSelect = select(bag, 'Topic', '/front_cam/aligned_depth_to_color/image_raw');

disp('Read rTab msgs')
rTabPosMsgs = readMessages(rTabPosSelect);
disp('Read rgb msgs')
imgMsgs = readMessages(imgSelect);
disp('Read depth msgs')
depthMsgs = readMessages(depthSelect);

save('2019-07-02-MSY-rtab.mat', '-v7.3')
