% Script to analyze the data in the rosbag
clear
clc
bagFile = '/home/janindu/data/Maleen/2019-07-02-MSY-rtab.bag';


%% Read from bag
disp('Reading rosbag')
bag = rosbag(bagFile);

disp('Select rTab msgs')
rTabPosSelect = select(bag, 'Topic', '/rtabmap/localization_pose');
disp('Select rgb msgs')
imgSelect = select(bag, 'Topic', '/front_cam/color/image_raw');
disp('Select depth msgs')
depthSelect = select(bag, 'Topic', '/front_cam/aligned_depth_to_color/image_raw');

disp('Read rTab msgs')
rTabPosMsgs = readMessages(rTabPosSelect);
disp('Read rgb msgs')
imgMsgs = readMessages(imgSelect);
disp('Read depth msgs')
depthMsgs = readMessages(depthSelect);

%% Process poses
% [x y z yaw pitch roll ts seq]
rTabPoses = zeros(size(rTabPosMsgs,1), 8);

for i=1:size(rTabPosMsgs,1)
   t = rTabPosMsgs{i}.Header.Stamp.Sec + (rTabPosMsgs{i}.Header.Stamp.Nsec/1000000000); 
   seq = double(rTabPosMsgs{i}.Header.Seq); 
   x = rTabPosMsgs{i}.Pose.Pose.Position.X;
   y = rTabPosMsgs{i}.Pose.Pose.Position.Y;
   z = rTabPosMsgs{i}.Pose.Pose.Position.Z;
   q = rTabPosMsgs{i}.Pose.Pose.Orientation;
   eul = quat2eul([q.W, q.X, q.Y, q.Z]);
   rTabPoses(i,:) = [x y z eul t seq];
end

figure(1)
hold on
plot(rTabPoses(:,1), rTabPoses(:,2), '.b')
xlabel('X(m)')
ylabel('Y(m)')

figure(2)
hold on
plot(rTabPoses(:,4), 'r')
ylabel('Yaw (rad)')

%% Process images

figure(3)
% figure(4)
% figure(5)

for i=1:length(imgMsgs)
   img = readImage(imgMsgs{i});
   gsImg = imgaussfilt(rgb2gray(img), 1.5);
   
   edgeCanny = edge(gsImg, 'Canny');
   edgeSobel = edge(gsImg, 'Sobel');
%    edgeLog = edge(gsImg, 'log');
%    edgePrewitt = edge(gsImg, 'Prewitt');
   
   figure(3)
   imshowpair(edgeCanny, edgeSobel, 'montage')
   
%    figure(4)
%    imshowpair(edgeCanny, edgeLog, 'montage')
%    
%    figure(5)
%    imshowpair(edgeCanny, edgePrewitt, 'montage');
   
   
end























