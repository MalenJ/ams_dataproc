% Script to estimate camera pitch
clear
clc
load 2019-07-15-CAS-RTAB.mat
load ../calibration/front_calibapp.mat

%% Image data
imH = 640;
imV = 480;

vFOV = 0.7417649;
hFOV = 1.211259;
radPpxV = vFOV/imV;
radPpxH = hFOV/imH;

% take the bottom middle part of the image
% 480x640 image : 351:451,320
% From stationary images

thetaEst = zeros(150,50);
hEst = zeros(150,50);
d = zeros(150,50);
atanTop = zeros(150,50);
atanBot = zeros(150,50);
atanIn = zeros(150,50);

hOffset = 320;
vOffset = 350;

%% Estimate depth
for i=1:150
    depthImg = readImage(depthMsgs{i});
    rgbImg = readImage(imgMsgs{i});
    rgbImgU = undistortImage(rgbImg, front_cameraParams);
    depthImgU = undistortImage(depthImg, front_cameraParams);
    
    for j=1:50
        d1 = double(depthImgU(j+vOffset,hOffset))/1000;
        d2 = double(depthImgU(j+50+vOffset,hOffset))/1000;
        
        a1 = (j+vOffset - front_cameraParams.PrincipalPoint(1))*radPpxV;
        b1 = (hOffset - front_cameraParams.PrincipalPoint(2))*radPpxH;
        
        a2 = (j+vOffset+50 - front_cameraParams.PrincipalPoint(1))*radPpxV;
        b2 = (hOffset - front_cameraParams.PrincipalPoint(2))*radPpxH;
        
        ca1 = cos(a1);
        ca2 = cos(a2);
        
        sa1 = sin(a1);
        sa2 = sin(a2);
        
        cb1 = cos(b1);
        cb2 = cos(b2);
        
        thetaEst(i,j) = atan(((d2*cb2*ca2)-(d1*cb1*ca1))/((d1*cb1*sa1)-(d2*cb2*sa2)));
        hEst(i,j) = d1*cb1*cos(thetaEst(i,j)-a1);
        d(i,j) = d1;
        
        atanTop(i,j) = ((d2*cb2*ca2)-(d1*cb1*ca1));
        atanBot(i,j) = ((d1*cb1*sa1)-(d2*cb2*sa2));
        atanIn(i,j) = ((d2*cb2*ca2)-(d1*cb1*ca1))/((d1*cb1*sa1)-(d2*cb2*sa2));
    end
end


%% Plot results
figure
hold on
plot(d(:,1), 'r')
plot(d(:,10), 'g')
plot(d(:,20), 'b')
plot(d(:,30), 'm')
plot(d(:,40), 'c')
plot(d(:,50), 'k')
title('Depth At Fixed Pixel')
xlabel('Iteration')
ylabel('Depth (m)')
legend({'Pixel 1', 'Pixel 10', 'Pixel 20', 'Pixel 30', 'Pixel 40', 'Pixel 50'})

figure
hold on
plot(thetaEst(:,1), 'r')
plot(thetaEst(:,10), 'g')
plot(thetaEst(:,20), 'b')
plot(thetaEst(:,30), 'm')
plot(thetaEst(:,40), 'c')
plot(thetaEst(:,50), 'k')
title('Theta Estimate At Fixed Pixel')
xlabel('Iteration')
ylabel('Theta (rad)')
legend({'Pixel 1', 'Pixel 10', 'Pixel 20', 'Pixel 30', 'Pixel 40', 'Pixel 50'})

figure
hold on
plot(hEst(:,1), 'r')
plot(hEst(:,10), 'g')
plot(hEst(:,20), 'b')
plot(hEst(:,30), 'm')
plot(hEst(:,40), 'c')
plot(hEst(:,50), 'k')
title('Height Estimate At Fixed Pixel')
xlabel('Iteration')
ylabel('Height (m)')
legend({'Pixel 1', 'Pixel 10', 'Pixel 20', 'Pixel 30', 'Pixel 40', 'Pixel 50'})

figure
hold on
yyaxis left
plot(d(:,1), 'r')
ylabel('Depth (m)')
yyaxis right
plot(thetaEst(:,1), 'b')
ylabel('Theta (rad)')
title('Depth vs Theta Noise at Pixel 1')

figure
hold on
yyaxis left
plot(d(:,25), 'r')
ylabel('Depth (m)')
yyaxis right
plot(thetaEst(:,25), 'b')
ylabel('Theta (rad)')
title('Depth vs Theta Noise at Pixel 25')

figure
hold on
yyaxis left
plot(d(:,50), 'r')
ylabel('Depth (m)')
yyaxis right
plot(thetaEst(:,50), 'b')
ylabel('Theta (rad)')
title('Depth vs Theta Noise at Pixel 50')