clear
clc

% Create an image with a box
mat = zeros(500,500);
mat(375,125:375) = 1;
mat(125,125:375) = 1;
mat(125:375,125) = 1;
mat(125:375,375) = 1;

dtObj = DistanceFunctions2D(mat, 0, 0, 500, 500, 0.05);

DTx = zeros(500,500);
DTy = zeros(500,500);
dDTxdx = zeros(500,500);
dDTxdy = zeros(500,500);
dDTydx = zeros(500,500);
dDTydy = zeros(500,500);

for i=500:-1:1
    for j=1:500
        DTx(i,j) = dtObj.queryDTx([i*0.05,j*0.05]');
        DTy(i,j) = dtObj.queryDTy([i*0.05,j*0.05]');
        dDTxdx(i,j) = dtObj.querydDTxdx([i*0.05,j*0.05]');
        dDTxdy(i,j) = dtObj.querydDTxdy([i*0.05,j*0.05]');
        dDTydx(i,j) = dtObj.querydDTydx([i*0.05,j*0.05]');
        dDTydy(i,j) = dtObj.querydDTydy([i*0.05,j*0.05]');
    end
end

figure
hold on
title('DTx')
mesh(DTx)

figure
hold on
title('DTy')
mesh(DTy)

figure
hold on
title('dDTxdx')
mesh(dDTxdx)

figure
hold on
title('dDTxdy')
mesh(dDTxdy)

figure
hold on
title('dDTydx')
mesh(dDTydx)

figure
hold on
title('dDTydy')
mesh(dDTydy)
