close all
clear all

CALIBPATH='/home/maleen/git/ams_dataproc/calibration/data_arena/ICRA2021calib/'; %CALIB DATA

rot_opti2depth=eul2rotm([pi/2,0,pi/2],'xyz');
depth_T_opti=eye(4);
depth_T_opti(1:3,1:3)=rot_opti2depth;

laser_rot=eul2rotm([-pi/2,0,-pi/2],'xyz');
laser_tf=eye(4);
laser_tf(1:3,1:3)=laser_rot;

%% LEFT SIDE

load(strcat(CALIBPATH,'left/left_finalresult.mat'),'TF_base_depth')

base_T_depthleftcam=TF_base_depth;

base_T_optileftcam=base_T_depthleftcam*inv(depth_T_opti);

load('LeftLaserCalibResults.mat','delta','phi')

leftcam_T_laser=eye(4);

leftcam_T_laser(1:3,1:3)=inv(phi);
leftcam_T_laser(1:3,4)=delta';

base_T_laser_l=(base_T_optileftcam*leftcam_T_laser)*laser_tf;

xyz_left=base_T_laser_l(1:3,4)';

angs_left=(rotm2eul(base_T_laser_l(1:3,1:3)))

%% RIGHT SIDE

load(strcat(CALIBPATH,'right/right_finalresult.mat'),'TF_base_depth')

base_T_depthrightcam=TF_base_depth;

base_T_optirightcam=base_T_depthrightcam*inv(depth_T_opti);

load('RightLaserCalibResults.mat','delta','phi')

rightcam_T_laser=eye(4);

rightcam_T_laser(1:3,1:3)=inv(phi);
rightcam_T_laser(1:3,4)=delta';

base_T_laser_r=(base_T_optirightcam*rightcam_T_laser)*laser_tf;

xyz_right=base_T_laser_r(1:3,4)';

angs_right=(rotm2eul(base_T_laser_r(1:3,1:3)))

%%

xyz_mean=mean([xyz_left;xyz_right])

angs_mean=mean([wrapTo2Pi(angs_left);wrapTo2Pi(angs_right)])


