close all
clear all

%skipset=[16,17,18,19,20]; %left NO NEED SKIPSETS :D
%skipset=[11,12,16,17,18,19,22,23]; %right

for i=1:30
    
    %if ~ismember(i,skipset)

        bag=rosbag(strcat('/media/maleen/malen_ssd/phd/cloud_academic/ams_data/calibration/laser-cam/rightcam/',num2str(i),'.bag'));
        
        laserbag=select(bag,'Topic','/scan');
        lasermsgs=readMessages(laserbag);
        
        laser_size=length(lasermsgs);
        ranges=zeros(laser_size,1081);
        
        for j=1:laser_size
            
            ranges(j,:)=lasermsgs{j,1}.Ranges;
            
        end
        
        laser_stamp(i)=(lasermsgs{floor(laser_size/2),1}.Header.Stamp.Sec+(lasermsgs{floor(laser_size/2),1}.Header.Stamp.Nsec)/1000000000);
        
        StartAngleRads(i)=lasermsgs{floor(laser_size/2),1}.AngleMin;
        AngleIncrementRads(i)=lasermsgs{floor(laser_size/2),1}.AngleIncrement;
        EndAngleRads(i)=lasermsgs{floor(laser_size/2),1}.AngleMax;
        RangeUnitType(i)=3;
        NoAngles(i)=1081;
        finranges(i,:)=mean(ranges);
        %findata(i,:)=[laser_stamp(i) StartAngleRads AngleIncrementRads EndAngleRads RangeUnitType NoAngles mean(ranges)];
   % end
end

T = table(laser_stamp', StartAngleRads', AngleIncrementRads', EndAngleRads', RangeUnitType', NoAngles', finranges);
T2= table(laser_stamp');

writetable(T, 'rightscans.txt','Delimiter',' ','WriteVariableNames',0)
writetable(T2, 'time_rightscans.txt','Delimiter',' ','WriteVariableNames',0)

